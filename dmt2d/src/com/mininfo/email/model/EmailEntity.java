package com.mininfo.email.model;

import java.io.Serializable;

/**  
 * @Class Name : UserEntity.java
 * @Description : 사용자 정보  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 고광훈
 * @since 2018-12-13
 * @version 1.0
 */

public class EmailEntity implements Serializable {
	
	private static final long serialVersionUID = -1363967297514791836L;
	
	private String userId = "";				//사용자 아이디
	private String userEmail = "";			//사용자 이메일
	private String passHintSelectCode = "";	//사용자 힌트
	private String passAnswer = "";			//사용자 힌트 정답 
	private String authCode = "";			//인증코드
	
	/** 이메일 발송 관련 **/
	private String mail_server; 
	private String mail_port; 
	private String mail_from; 
	private String mail_from_name; 
	private String mail_subject; 
	private String mail_auth_yn; 
	private String mail_auth_id; 
	private String mail_auth_pw;
	
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("[mail_server : ").append(mail_server).append("] ")
		.append("[mail_port : ").append(mail_port).append(" ] ")
		.append("[mail_from : ").append(mail_from).append(" ] ")
		.append("[mail_from_name : ").append(mail_from_name).append(" ] ")
		.append("[mail_subject : ").append(mail_subject).append(" ] ")
		.append("[mail_auth_yn : ").append(mail_auth_yn).append(" ] ")
		.append("[mail_auth_id : ").append(mail_auth_id).append(" ] ")
		.append("[mail_auth_pw : ").append(mail_auth_pw).append(" ] ");
		
		return sb.toString();
	}
	
	public String getMail_server() {
		return mail_server;
	}
	public void setMail_server(String mail_server) {
		this.mail_server = mail_server;
	}
	public String getMail_port() {
		return mail_port;
	}
	public void setMail_port(String mail_port) {
		this.mail_port = mail_port;
	}
	public String getMail_from() {
		return mail_from;
	}
	public void setMail_from(String mail_from) {
		this.mail_from = mail_from;
	}
	public String getMail_from_name() {
		return mail_from_name;
	}
	public void setMail_from_name(String mail_from_name) {
		this.mail_from_name = mail_from_name;
	}
	public String getMail_subject() {
		return mail_subject;
	}
	public void setMail_subject(String mail_subject) {
		this.mail_subject = mail_subject;
	}
	public String getMail_auth_yn() {
		return mail_auth_yn;
	}
	public void setMail_auth_yn(String mail_auth_yn) {
		this.mail_auth_yn = mail_auth_yn;
	}
	public String getMail_auth_id() {
		return mail_auth_id;
	}
	public void setMail_auth_id(String mail_auth_id) {
		this.mail_auth_id = mail_auth_id;
	}
	public String getMail_auth_pw() {
		return mail_auth_pw;
	}
	public void setMail_auth_pw(String mail_auth_pw) {
		this.mail_auth_pw = mail_auth_pw;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getPassHintSelectCode() {
		return passHintSelectCode;
	}
	public void setPassHintSelectCode(String passHintSelectCode) {
		this.passHintSelectCode = passHintSelectCode;
	}
	public String getPassAnswer() {
		return passAnswer;
	}
	public void setPassAnswer(String passAnswer) {
		this.passAnswer = passAnswer;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

}