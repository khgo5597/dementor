package com.mininfo.email.model;

import java.io.Serializable;
import java.util.Date;

public class AuthCodeEntity implements Serializable {
	
	private static final long serialVersionUID = -1363967297514791836L;
	
	private String user_id;
	private String auth_code;
	private Date create_date;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAuth_code() {
		return auth_code;
	}
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
