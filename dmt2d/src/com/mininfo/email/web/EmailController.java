package com.mininfo.email.web;

import java.io.PrintWriter;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mininfo.common.SequenceGenerator;
import com.mininfo.common.mail.MailSender;
import com.mininfo.common.service.CommonService;
import com.mininfo.email.model.AuthCodeEntity;
import com.mininfo.email.model.EmailEntity;
import com.mininfo.email.service.EmailService;
import com.mininfo.keysetting.service.KeySettingService;

/**  
 * @Class Name : EmailController.java
 * @Description : 키분실 이메일 발송  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2018-11-15     고광훈                       최초생성
 * 
 * @author 고광훈
 * @since 2018-11-15
 * @version 1.0
 */

@Controller
public class EmailController {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	//EmailService 서비스 선언
	@Resource(name="emailService")
	private EmailService emailService;
	
	//KeySettingService 서비스 선언
	@Resource(name = "keySettingService")
	private KeySettingService keySettingService;
	
	//공통 서비스 선언
	@Resource(name="commonService")
	private CommonService commonService;
	/**
	 * 사용자 해지
	 * @param HttpServletRequest userId
	 * @param HttpServletRequest req
	 * @param HttpServletResponse res
	 * @return 
	 * @return void
	 * @exception Exception
	 */
	
	/**
	 * 이메일 등록 여부 확인
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/emailCheck.do")
	public @ResponseBody void emailCheck(
			@RequestParam(value="data") String data, 
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");
		
		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();
				
		try {
			//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
			
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
		    	eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userId");
				
//				System.out.println(" userId 아이디 ==>> " + userId);
				
				// Session에 아이디 저장
				HttpSession session = req.getSession();
				session.setAttribute("userId", userId);
				
				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				}else {
					
					// 이메일 등록여부 체크
					boolean checkResult = emailService.emailCheck(userId);
//					System.out.println(" checkResult ==>>" + checkResult);
					
					if(checkResult) {
						resultJson.put("userId", userId);
						eobj.put("code", "101000");
						eobj.put("data", resultJson);
/*						RequestDispatcher disp = req.getRequestDispatcher("/emailSearch.do");
						disp.forward(req, res);*/
					}else {
//						resultJson.put("userId", userId);
						eobj.put("code", "102250");
//						eobj.put("data", resultJson);
/*						RequestDispatcher disp = req.getRequestDispatcher("/emailUpdate.do");
						disp.forward(req, res);*/
					}
				}
			}		    
		}catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
	    writer.write(eobj.toJSONString());
	    writer.flush();
	    writer.close();
	}
	
	/**
	 * 인증 키 찾기 페이지
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/keySearch.do")
	public String keySearch(
			HttpServletRequest req, 
			HttpServletResponse res) throws Exception {
		
		/** SESSION 설정  */
		HttpSession session = req.getSession();
		
		String userId , status;
		userId = (String)session.getAttribute("userId");
		status = "1"; // 인증 키 찾기 페이지
//		System.out.println(" session   userId ===========>>" + userId);
		
		// 데이터 세팅
		EmailEntity emailEntity = new EmailEntity();
		emailEntity.setUserId(userId);
		emailEntity = emailService.userInfo(emailEntity);
		
		req.setAttribute("userId", emailEntity.getUserId());
		req.setAttribute("passHintSelectCode", emailEntity.getPassHintSelectCode());
		
		return "/email/keySearch";
	}	
	
	/**
	 * 힌트 설정 페이지 호출
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/hintUpdateView.do")
	public String hintUpdateView() throws Exception {
		return "/email/hintUpdate";
	}	
	
	/**
	 * 힌트 등록 및 수정  - (DB 저장) 
	 * @param 
	 * @return 
	 * @exception Exception
	 */
//	@SuppressWarnings("unchecked")
	@RequestMapping(value="/hintUpdate.do")
	public @ResponseBody String hintUpdate(
			EmailEntity emailEntity,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		String code = "N";
		
		try {
			
			/** 데이터 세팅 화인 -- 확인 후 주석처리나 삭제 **/
//			System.out.println("+==================================================");
			
			String userId = emailEntity.getUserId();
			String userEmail = emailEntity.getUserEmail();
			String passHintSelectCode = emailEntity.getPassHintSelectCode();
			String passAnswer = emailEntity.getPassAnswer();
			
//			System.out.println(" userId==>> " + userId);
//			System.out.println(" passHintSelectCode==>> " + passHintSelectCode);
//			System.out.println(" passAnswer==>> " + passAnswer);
//			System.out.println(" userEmail==>> " + userEmail);
			
//			System.out.println("+==================================================");
			
			// 입력된 힌트 정보 update
			emailService.hintUpdate(emailEntity);
			code = "S";
			
		} catch (Exception e) {
			logger.error("{}", e);
		}
	    
	    return code;
	}
	
	/**
	 * 인증 키 찾기 page - 입력 정보 확인
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/answerCheck.do")
	public @ResponseBody void answerCheck(
			EmailEntity emailEntity,
			@RequestParam(value="data") String data, 
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/html;charset=UTF-8");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");
		
		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();
				
		try {
			//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
			
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
		    	eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userId");
//				System.out.println(" userId 아이디 ==>> " + userId);
				
				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				}else {
					
					// 기존 정보 : preInfo
					EmailEntity preInfo = new EmailEntity();
					preInfo.setUserId(userId);
					preInfo = emailService.userInfo(preInfo);
					
					// 입력 정보 : postInfo
					EmailEntity postInfo = new EmailEntity();
					String passHintSelectCode = (String)jsonData.get("passHintSelectCode");
					String passAnswer = (String)jsonData.get("passAnswer");
					String userEmail = (String)jsonData.get("userEmail");
					
//					System.out.println(" passHintSelectCode ........>>>>> "  + passHintSelectCode);
//					System.out.println(" passAnswer ........>>>>> "  + passAnswer);
//					System.out.println(" userEmail ........>>>>> "  + userEmail);
					
					postInfo.setUserId(userId);
					postInfo.setPassHintSelectCode(passHintSelectCode);
					postInfo.setPassAnswer(passAnswer);
					postInfo.setUserEmail(userEmail);
					
					// 기존 정보와 입력정보 비교
					String resultCode = emailService.infoCheck(preInfo, postInfo); // 기존 정보, 입력정보
					System.out.println(" resultCode ==>>" + resultCode);
					
					if(resultCode.equalsIgnoreCase("S")) {
						resultJson.put("userId", userId);
						eobj.put("code", "101000");
						eobj.put("data", resultJson);
/*						RequestDispatcher disp = req.getRequestDispatcher("/emailSearch.do");
						disp.forward(req, res);*/
					}else {
//						resultJson.put("userId", userId);
						eobj.put("code", "102250");
//						eobj.put("data", resultJson);
/*						RequestDispatcher disp = req.getRequestDispatcher("/emailUpdate.do");
						disp.forward(req, res);*/
					}
				}
			}		    
		}catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
	    writer.write(eobj.toJSONString());
	    writer.flush();
	    writer.close();
	}
	
	// 자바 메일 보내기 + 인증 코드 테이블 저장
	
	/**
	 * 자바 메일 발송
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/sendMail.do")
	public @ResponseBody String sendMail(
			HttpServletRequest req, 
			HttpServletResponse res) throws Exception {
		
		String code = null;
		
		String userId     = req.getParameter("userId");
//		String status      = req.getParameter("status");
		String userEmail       = req.getParameter("userEmail").trim();
		String url         = "http://" + req.getServerName() + ":" + req.getServerPort();
		
//		System.out.println(userEmail + " ::: " + userId);
//		System.out.println("url=>      " + url);
		
		SequenceGenerator strSeq = new SequenceGenerator(5, 10);
		String auth_code = strSeq.nextValue();
//		System.out.println(" auth_code ==>>>>>>    " + auth_code);
		
		String mail_server = "";
		String mail_port = "";
		String mail_to = userEmail;
		String mail_from = "";
		String mail_from_name = "manager";
		String mail_subject = "[DEMENTOR] - Auth Code Email"; // 제목
		String mail_auth_yn = "N";
		String mail_auth_id = "";
		String mail_auth_pw = "";
		String mail_contets = getMailContents(url, userEmail, auth_code);

		Properties pro = new Properties();
		pro.put("mail_to", userEmail);	// 받는 사람
		pro.put("mail_from", "dementor@dementor.co.kr");	// 보내는 사람
		pro.put("mail_from_name", mail_from_name);
		pro.put("mail_subject", mail_subject);	// 제목
		pro.put("mail_contets", mail_contets);	// 내용
		
		MailSender mailUtil = new MailSender();
		
		try {
			
			/** 메일 발송 **/
			mailUtil.sendMail(pro);
			
			AuthCodeEntity authCodeEntity = new AuthCodeEntity();
			authCodeEntity.setUser_id(userId);
			authCodeEntity.setAuth_code(auth_code);
			
			// 발송된 인증 코드 저장
			emailService.insertUserAuthCode(authCodeEntity);
			code = "S";
		} catch (Exception ex) {
			logger.info("sendMail Exception : "+ pro.toString());
//			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, ex.getMessage());
//			req.setAttribute("RESULT", resultEntity);
//			return PageConst.contentsPage;
		}
		
		req.setAttribute("userId", userId);
//		req.setAttribute("status", status);
		req.setAttribute("auth_code", auth_code);
		
//		System.out.println(" code :::::::::::::>>>>>> " + code);
		return code;
	}
	
	/**
	 * 인증 코드 입력 폼
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/authCodeForm.do")
	public String authCodeForm(
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		/** SESSION 설정  */
		HttpSession session = req.getSession();
		
		String userId;
		userId = (String)session.getAttribute("userId");
//		System.out.println(" session   userId ===========>>" + userId);
		
		// 데이터 세팅
		EmailEntity emailEntity = new EmailEntity();
		emailEntity.setUserId(userId);
		emailEntity = emailService.userInfo(emailEntity);
		
		req.setAttribute("userId", emailEntity.getUserId());
		req.setAttribute("passHintSelectCode", emailEntity.getPassHintSelectCode());
		req.setAttribute("passAnswer", emailEntity.getPassAnswer());
		req.setAttribute("userEmail", emailEntity.getUserEmail());
		
		return "/email/authCodeForm";
	}
	
	/**
	 * 자바 메일 발송
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/authCodeCheck.do")
	public @ResponseBody String authCodeCheck(
			HttpServletRequest req, 
			HttpServletResponse res) throws Exception {
		
		String code = null;
		
		String userId = req.getParameter("userId");
		String auth_code = req.getParameter("auth_code");
		
		HttpSession session = req.getSession();
		if(userId == null) {
			userId = (String)session.getAttribute("userId");		
		}
		
		try {
			AuthCodeEntity authCodeEntity = new AuthCodeEntity();
			authCodeEntity.setUser_id(userId);
			authCodeEntity = emailService.getUserAuthCode(authCodeEntity);
			
			// 시간?????
//			Date createDate = authCodeEntity.getCreate_date();
//			long createDateLong = createDate.getTime();
//			long age = System.currentTimeMillis() - createDateLong;
//			
//			SettingDao settingDao = new SettingDaoImpl();
//			int expire_minute = settingDao.getAuthCodeExpireMinte(); // 만료 시간???
			
//			if(expire_minute<0) expire_minute = 5;
			
//			if (age > (expire_minute * 60 * 1000L)) {
//	        	String msg = CodeMessageHandler.getInstance().getCodeMessage("2512");
//	        	String msg2 = CodeMessageHandler.getInstance().getCodeMessage("2514");
//	        	StringBuffer sb = new StringBuffer()
//	        	.append(msg)        	
//	        	.append(expire_minute)
//	        	.append(msg2);
//	        	
//
//	        	ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
//				req.setAttribute("RESULT", resultEntity);	
//				req.setAttribute("USER", userHintEntity);
//				req.setAttribute("STATUS", status);
//				
//				logger.info("fail= "+sb.toString());
//				return authCodePage;
//	        }
			
			// 인증 코드 비교
			String input_authCode = auth_code;
			String db_authCode = authCodeEntity.getAuth_code();
			if(!input_authCode.equals(db_authCode)){ // 인증코드가 맞을 않을 때
				
				logger.info(userId + " input auth_code = "+ input_authCode); // 입력된 인증코드 
				logger.info(userId + " datebase auth_code = "+ db_authCode); // DB 인증코드
				
//				System.out.println("   FAIL  !!!!!!!!!!!!!!!!!!!!!!! ");
				return code = "F";
			}
			
			code = "S";
		} catch (Exception ex) {
			logger.debug(" Exception e ");
			return code = "F";
		}
		
//		System.out.println(" code :::::::::::::>>>>>> " + code);
		return code;
	}
	
	// 발송된 메일의 화면 UI
	private String getMailContents(String url, String email, String authCode) throws Exception{
//		String msg = CodeMessageHandler.getInstance().getCodeMessage("3001");
		String msg = "인증코드 :";
		StringBuffer sb = new StringBuffer();
		sb.append("<html>")
		.append("<head><title>그래픽인증 - 동국대</title>")
		.append("<meta http-equiv='Content-Type' content='text/html; charset=utf_8'>")
		.append("</head>")
		.append("<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>")
		.append("<table id='Table_01' width='514' border='0' cellpadding='0' cellspacing='0'>")
		.append("<tr>")
		.append("<td valign='top'>")
		.append("<img src='")
		.append(url)
		.append("/dmt2d/images/email_img/mail_01.gif' width='514' height='198' alt=''>")
		.append("</td>")
		.append("</tr>")
		.append("<tr>")
		.append("<td height='112' valign='top'>")
		.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>")
		.append("<tr>")
		.append("<td width='84' height='112'>")
		.append("<img src='")
		.append(url)
		.append("/dmt2d/images/email_img/01mail_jp_02.gif' width='84' height='112' border='0'>")
		.append("</td>")
		.append("<td valign='middle'>")
		.append(""+msg+" ") 
		.append(authCode)
		.append("</td>")	
		.append("<td width='110'>")
		.append("<img src='")
		.append(url)
		.append("/dmt2d/images/email_img/01mail_jp_04.gif' width='110' height='112' border='0'>")
		.append("</td>")	
		.append("</tr>")		
		.append("</table>")
		.append("</td>")
		.append("</tr>")
		.append("<tr>")
		.append("<td height='26'>")
		.append("<img src='")
		.append(url)
		.append("/dmt2d/images/email_img/01mail_jp_09.gif' width='514' height='26'>")		
		.append("</td>")
		.append("</tr>")
		.append("</table>")				
		.append("</body>")
		.append("</html>");	
		
		return sb.toString();
	}
}	
