package com.mininfo.email.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

import com.mininfo.email.model.AuthCodeEntity;
import com.mininfo.email.model.EmailEntity;

/**  
 * @Class Name : EmailDAO.java
 * @Description : 이메일등록 DAO Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2018-12-13     고광훈                        최초생성
 * 
 * @author 고광훈
 * @since 2018-12-13
 * @version 1.0
 */

@Repository("emailDAO")
public class EmailDAO {
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}
	
	/**
	 * 이메일 등록
	 * @param String userId - 사용자 아이디
	 * @return 
	 * @return void
	 * @exception Exception
	 */
	public EmailEntity emailCheck(EmailEntity emailEntity) throws Exception {
		return (EmailEntity)sqlMap.queryForObject("emailCheck", emailEntity);
	}
	
	public void hintUpdate(EmailEntity emailEntity) throws Exception {
		sqlMap.update("hintUpdate", emailEntity);
	}
	
	public EmailEntity userInfo(EmailEntity emailEntity) throws Exception {
		return (EmailEntity)sqlMap.queryForObject("userInfo", emailEntity);
	}
	
	// 발송 인증 코드 저장
	public void insertUserAuthCode(AuthCodeEntity authCodeEntity) throws Exception {
		sqlMap.insert("insertUserAuthCode", authCodeEntity);
	}
	
	// 인증 코드 조회
	public AuthCodeEntity getUserAuthCode(AuthCodeEntity authCodeEntity) throws Exception {
		return (AuthCodeEntity)sqlMap.queryForObject("selectUserAuthCode", authCodeEntity);
	}
}
