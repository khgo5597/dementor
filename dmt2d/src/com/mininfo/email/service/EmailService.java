package com.mininfo.email.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.email.dao.EmailDAO;
import com.mininfo.email.model.AuthCodeEntity;
import com.mininfo.email.model.EmailEntity;
import com.mininfo.keysetting.dao.KeySettingDAO;
import com.mininfo.keysetting.model.UserEntity;


/**  
 * @Class Name : CloseService.java
 * @Description : 서비스해지  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2018-12-13     고광훈                        최초생성
 * 
 * @author 고광훈
 * @since 2018-12-13
 * @version 1.0
 */

@Service("emailService")
public class EmailService {	
	//EmailDAO DAO 선언
	@Resource(name="emailDAO")
	private EmailDAO emailDAO;
	
	//KeySettingDAO DAO 선언
	@Resource
	private KeySettingDAO keySettingDAO;
	
	// 사용자 이메일 등록 여부 확인
	public boolean emailCheck(String userId) throws Exception {
		boolean checkResult = false;
		
		// 이메일 확인
		EmailEntity emailEntity = new EmailEntity();
		emailEntity.setUserId(userId);
		emailEntity = emailDAO.emailCheck(emailEntity);
		
		String userEmail = emailEntity.getUserEmail();
//		System.out.println(" userEmail ==>>>> " +  userEmail);
		
		if(userEmail.equalsIgnoreCase("")) {
//			System.out.println(" 이메일이 없음");
		}else {
//			System.out.println(" 이메일이 있음");
			checkResult = true;
		}
		return checkResult;
	}

	// 이메일 정보 저장
	public void hintUpdate(EmailEntity emailEntity) throws Exception{
		emailDAO.hintUpdate(emailEntity);
	}
	
	// 사용자 정보 조회
	public EmailEntity userInfo(EmailEntity emailEntity) throws Exception{
		emailEntity = emailDAO.userInfo(emailEntity);
		return emailEntity;
	}
	
	// 정보 비교
	public String infoCheck(EmailEntity preInfo, EmailEntity postInfo) throws Exception{
		String code = null;
		
//		System.out.println(" 기존정보 아이디 >>>>>>" + preInfo.getUserId());
//		System.out.println(" 입력정보 아이디 >>>>>>" + postInfo.getUserId());
		
		// 두 정보의 아이다가 같을 때
		if(!preInfo.getUserId().equalsIgnoreCase(postInfo.getUserId())) {
			code = "F";
		} else {
			// 키 여부 확인
			UserEntity userEntity = new UserEntity();
			userEntity.setUserId(preInfo.getUserId());
			userEntity = keySettingDAO.getUserKey(userEntity.getUserId());
			if(userEntity == null) return code = "F";
			
			// 힌트 코드, 답변, 이메일 비교
			if(!preInfo.getPassHintSelectCode().equalsIgnoreCase(postInfo.getPassHintSelectCode())) return code = "F";
			if(!preInfo.getPassAnswer().equalsIgnoreCase(postInfo.getPassAnswer())) return code = "F";
			if(!preInfo.getUserEmail().equalsIgnoreCase(postInfo.getUserEmail()))  return code = "F";
			
			// 위 코드가 정상적으로 다 완료되면...
			code = "S";
		}
		
		return code;
	}
	
	// 발송 인증 코드 저장
	public void insertUserAuthCode(AuthCodeEntity authCodeEntity) throws Exception {
		emailDAO.insertUserAuthCode(authCodeEntity);
	}
	
	// 인증 코드 조회
	public AuthCodeEntity getUserAuthCode(AuthCodeEntity authCodeEntity) throws Exception {
		return emailDAO.getUserAuthCode(authCodeEntity);
	}
}
