package com.mininfo.auth.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.auth.dao.AuthDAO;
import com.mininfo.auth.model.AuthEntity;
import com.mininfo.engine.PermutationServer;
import com.mininfo.engine.Seed;
import com.mininfo.engine.VerifySign;
import com.mininfo.keysetting.dao.KeySettingDAO;
import com.mininfo.keysetting.model.IconEntity;
import com.mininfo.keysetting.model.UserEntity;
import com.mininfo.util.DBencryption;

/**  
 * @Class Name : AuthService.java
 * @Description : 로그인 인증 처리  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Service("authService")
public class AuthService {
	
	//AuthDAO 서비스 선언
	@Resource(name="authDAO")
	private AuthDAO authDAO;
	
	//KeySettingDAO 서비스 선언
	@Resource(name="keySettingDAO")
	private KeySettingDAO keySettingDAO;
	
	/**
	 * 로그인시 사용자 아이콘 정보 조회
	 * @param String userId
	 * @return List<IconEntity>
	 * @exception Exception
	 */
	@SuppressWarnings("static-access")
	public String[] getMemberKeyUrl(String userId) throws Exception {
		
		int count=8;
		int numofImage=25;
		int[] imageAddress = new int[numofImage];
		
		int[] index=new int[numofImage];
		for(int i=0;i<numofImage;i++) index[i]=i;
		
		Seed userSeed = new Seed(userId.toCharArray(),count);
		PermutationServer reordering=new PermutationServer(index,userSeed);
		
		index=reordering.getArray();
		int[] answer = reordering.getAnswer();
		String s_answer = Arrays.toString(answer);
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);
		map.put("answer", s_answer.substring(1, s_answer.length() - 1));
		
		authDAO.updateAnswer(map);

		//사용자 키 정보 조회
		UserEntity user = keySettingDAO.getMemberKey(userId);

		int imageArr[] = new int[25];

		//사용자 아이콘 생성
		if(user != null) {
			imageArr[0] = user.getKey0();
			imageArr[1] = user.getKey1();
			imageArr[2] = user.getKey2();
			imageArr[3] = user.getKey3();
			imageArr[4] = user.getKey4();
			imageArr[5] = user.getKey5();
			imageArr[6] = user.getKey6();
			imageArr[7] = user.getKey7();
			imageArr[8] = user.getKey8();
			imageArr[9] = user.getKey9();
			imageArr[10] = user.getKey10();
			imageArr[11] = user.getKey11();
			imageArr[12] = user.getKey12();
			imageArr[13] = user.getKey13();
			imageArr[14] = user.getKey14();
			imageArr[15] = user.getKey15();
			imageArr[16] = user.getKey16();
			imageArr[17] = user.getKey17();
			imageArr[18] = user.getKey18();
			imageArr[19] = user.getKey19();
			imageArr[20] = user.getKey20();
			imageArr[21] = user.getKey21();
			imageArr[22] = user.getKey22();
			imageArr[23] = user.getKey23();
			imageArr[24] = user.getKey24();
		}
		
		int randomHoleCnt = keySettingDAO.getUserImg(userId);
		
		//홀키 위치 구하기
		int userImg = randomHoleCnt%25;
		//짝수 인지 홀수 인지 
		String userImgUp = userImg%2 == 0 ? "Y" : "N";	
		
		int userImgArr[] = new int[25];
		
		userImgArr[0] = imageArr[userImg];
		//짝수 이면 정방향 임의키 설정
		if(userImgUp.equals("Y")) {
			int x = 4;
			
			switch(userImg) {
				case 0 :
					userImgArr[1] = imageArr[1];
					userImgArr[2] = imageArr[2];
					userImgArr[3] = imageArr[3];
					break;
				case 2 :
					userImgArr[1] = imageArr[0];
					userImgArr[2] = imageArr[1];
					userImgArr[3] = imageArr[3];
					break;
				default :
					userImgArr[1] = imageArr[0];
					userImgArr[2] = imageArr[1];
					userImgArr[3] = imageArr[2];
					x = 3;
					break;
			}

			for(int i=4;i<imageArr.length;i++) {
				if(userImg != x) {
					userImgArr[i] = imageArr[x];
				} else {
					x++;
					userImgArr[i] = imageArr[x];
				}
				
				x++;
			}
		//홀수 이면 역방향 임의키 설정
		} else {
			int x = 20;
			
			switch(userImg) {
				case 23 :
					userImgArr[1] = imageArr[24];
					userImgArr[2] = imageArr[22];
					userImgArr[3] = imageArr[21];
					break;
				default :
					userImgArr[1] = imageArr[24];
					userImgArr[2] = imageArr[23];
					userImgArr[3] = imageArr[22];
					x = 21;
					break;
			}

			for(int i=4;i<imageArr.length;i++) {
				if(x != userImg) {
					userImgArr[i] = imageArr[x];
				} else {
					x--;
					userImgArr[i] = imageArr[x];
				}
				
				x--;
			}
		}
		
		for(int i=0;i<numofImage;i++){
			imageAddress[i]=userImgArr[index[i]];
		}
		
		String[] iconUrl = new String[25];

		//복호화 유틸
		DBencryption encr = new DBencryption();
		
		List<IconEntity> newmemberkeyurl = keySettingDAO.getMemberKeyIconUrl(user);

		if(newmemberkeyurl.size()>0) {
			for(int i=0; i<newmemberkeyurl.size(); i++){
				IconEntity icon = (IconEntity)newmemberkeyurl.get(i);
				for(int j=0; j<newmemberkeyurl.size(); j++){
					if(imageAddress[j] == Integer.parseInt(icon.getIconId())){	
						iconUrl[j] = encr.db_decoding(icon.getIconUrl());
					}
				}
			}
		}
		
		return iconUrl;
	}
	
	/**
	 * 사용자 인증 처리
	 * @param AuthEntity auth
	 * @param String userId
	 * @return Map<String, Object>
	 * @exception Exception
	 */
	public Map<String, Object> authCheck(AuthEntity auth, String userId) throws Exception {
		boolean passmind = false;
		
		String s_answer = authDAO.getAnswer(userId);
		int[] answer = new int[4];
		
		String[] ss_answer = s_answer.split(",");
		
		for (int i = 0; i < ss_answer.length; i++) {
			ss_answer[i] = ss_answer[i].trim();
		}

		for (int i = 0; i < ss_answer.length; i++) {
			answer[i] = Integer.parseInt(ss_answer[i]);
		}
		
		VerifySign check = new VerifySign();
		passmind = check.isVerifySign(auth.getUsrCorrect(), answer, auth.getCorrectDate(), userId);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("passmind", passmind);
		
		if(passmind) {
			map.put("keyCount", 0);
			map.put("level", auth.getCorrectLevel());
			map.put("accessResult", "S");
			
			authDAO.updateUserLevel(map);
			authDAO.insertLog(map);
		} else {
			int userFailCnt = authDAO.getUserFailCnt(userId)+1;
			map.put("keyCount", userFailCnt);
			map.put("accessResult", "F");
			
			authDAO.insertLog(map);
		}
		
		authDAO.updateUserFailCnt(map);
		
		return map;
	}
	
	/**
	 * 실패 횟수 제한 확인
	 * @param String userId
	 * @return boolean
	 * @exception Exception
	 */
	public Map<String, Object> getUserFailResult(String userId) throws Exception {
		boolean result = false;
		
		int userFailCnt = authDAO.getUserFailCnt(userId);
		int maxFailCnt = authDAO.getMaxFailCnt();
		
		if(userFailCnt >= maxFailCnt)
			result = true;
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("userFailCnt", userFailCnt);
		resultMap.put("maxFailCnt", maxFailCnt);
		resultMap.put("result", result);
		
		return resultMap;
	}
	
	/**
	 * 사용자 로그인 레벨 정보
	 * @param String userId
	 * @return int
	 * @exception Exception
	 */
	public int getUserLevel(String userId) throws Exception {
		return authDAO.getUserLevel(userId);
	}
}
