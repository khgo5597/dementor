package com.mininfo.auth.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

/**  
 * @Class Name : AuthDAO.java
 * @Description : 로그인 인증 처리  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Repository("authDAO")
public class AuthDAO {
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}

	/**
	 * answer 등록
	 * @param Map<String, String> map
	 * @return void
	 * @exception Exception
	 */
	public void updateAnswer(Map<String, String> map) throws Exception {
		sqlMap.update("updateAnswer", map);
	}
	
	/**
	 * answer 값 조회
	 * @param String userId
	 * @return String
	 * @exception Exception
	 */
	public String getAnswer(String userId) throws Exception {
		return (String)sqlMap.queryForObject("getAnswer", userId);
	}
	
	/**
	 * 사용자 실패 횟수 조회
	 * @param String userId
	 * @return int
	 * @exception Exception
	 */
	public int getUserFailCnt(String userId) throws Exception {
		return (Integer)sqlMap.queryForObject("getUserFailCnt", userId);
	}
	
	/**
	 * 실패 횟수 제한 조회
	 * @param 
	 * @return int
	 * @exception Exception
	 */
	public int getMaxFailCnt() throws Exception {
		return (Integer)sqlMap.queryForObject("getMaxFailCnt");
	}
	
	/**
	 * 실패 횟수 수정
	 * @param 
	 * @return int
	 * @exception Exception
	 */
	public void updateUserFailCnt(Map<String, Object> map) throws Exception {
		sqlMap.update("updateUserFailCnt", map);
	}
	
	/**
	 * 사용자 레벨 정보
	 * @param String userId
	 * @return int
	 * @exception Exception
	 */
	public int getUserLevel(String userId) throws Exception {
		return (Integer)sqlMap.queryForObject("getUserLevel", userId);
	}
	
	/**
	 * 실패 횟수 수정
	 * @param 
	 * @return int
	 * @exception Exception
	 */
	public void updateUserLevel(Map<String, Object> map) throws Exception {
		sqlMap.update("updateUserLevel", map);
	}
	
	/**
	 * 접속 로그 등록
	 * @param 
	 * @return int
	 * @exception Exception
	 */
	public void insertLog(Map<String, Object> map) throws Exception {
		sqlMap.insert("insertLog", map);
	}
}
