package com.mininfo.auth.model;

import java.io.Serializable;

/**  
 * @Class Name : AuthEntity.java
 * @Description : 인증  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

public class AuthEntity implements Serializable {

	private static final long serialVersionUID = -1363967297514791836L;
	
	private String correctDate = "";
	private String correctLevel = "";
	private String usrCorrect = "";
	
	public String getCorrectDate() {
		return correctDate;
	}
	public void setCorrectDate(String correctDate) {
		this.correctDate = correctDate;
	}
	public String getCorrectLevel() {
		return correctLevel;
	}
	public void setCorrectLevel(String correctLevel) {
		this.correctLevel = correctLevel;
	}
	public String getUsrCorrect() {
		return usrCorrect;
	}
	public void setUsrCorrect(String usrCorrect) {
		this.usrCorrect = usrCorrect;
	}
}
