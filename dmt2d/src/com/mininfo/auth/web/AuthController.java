package com.mininfo.auth.web;

import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mininfo.auth.model.AuthEntity;
import com.mininfo.auth.service.AuthService;
import com.mininfo.common.service.CommonService;

/**  
 * @Class Name : AuthController.java
 * @Description : 인증 처리  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Controller
public class AuthController {
	//private static final Logger logger = Logger.getLogger(AuthController.class);
	//AuthService 서비스 선언
	@Resource(name="authService")
	private AuthService authService;
	
	//공통 서비스 선언
	@Resource(name="commonService")
	private CommonService commonService;

	private static String iconImgPath = "/dmt2d/images/icon_img/";
	
	/**
	 * 사용자 이미지 요청
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/reqauthimgkey.do")
	public @ResponseBody void reqauthimgKey(
			@RequestParam(value="data") String data,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");

		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();

	    try {
	    	//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
		    
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
				eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userid");
				String devType = (String)jsonData.get("devtype");
				String imgType = "0";

				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				} else if(devType == null || devType.equals("")) {
					eobj.put("code", "102000");
				} else {
					
					if(!devType.equals("0"))
						imgType = "3";
					
					//사용자 가입여부 확인
					boolean checkResult = commonService.userCheck(userId);
					
					if(checkResult) {
						//실패 횟수 결과
						Map<String, Object> resultMap = authService.getUserFailResult(userId);
						boolean userFail = (Boolean) resultMap.get("result");
						
						if(!userFail) {
							//아이콘 URL 정보 조회
							String[] iconArray = authService.getMemberKeyUrl(userId);
							
							//사용자 레벨 정보
							int level = authService.getUserLevel(userId);
							
							//아이콘 리스트 Json
							JSONArray iconJsonArray = new JSONArray();
							for(String iconUrl : iconArray) {
								JSONObject iconJson = new JSONObject();
								iconJson.put("iconurl", iconImgPath+imgType+iconUrl);
								
								iconJsonArray.add(iconJson);
							}
							
							resultJson.put("icons", iconJsonArray);
							resultJson.put("servertime", System.currentTimeMillis());
							resultJson.put("level", level);
							resultJson.put("userid", userId);
							resultJson.put("maxfailcnt", (Integer)resultMap.get("maxFailCnt"));
							resultJson.put("userfailcnt", (Integer)resultMap.get("userFailCnt"));
							
							eobj.put("data", resultJson);
							eobj.put("code", "101000");
						} else {
							resultJson.put("maxfailcnt", (Integer)resultMap.get("maxFailCnt"));
							resultJson.put("userfailcnt", (Integer)resultMap.get("userFailCnt"));
							
							eobj.put("data", resultJson);
							eobj.put("code", "102250");
						}
					} else {
						eobj.put("code", "102260");
					}
				}
			}
	    } catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
		//결과 데이터 리턴
	    eobj.put("message", "");
	    writer.write(eobj.toJSONString());

	    writer.flush();
	    writer.close();
	}
	
	/**
	 * 인증 요청
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/reqauth.do")
	public @ResponseBody void reqauth(
			@RequestParam(value="data") String data,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");

		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();

	    try {
	    	//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
		    
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
				eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userid");
				String devType = (String)jsonData.get("devtype");
				String serverTime = String.valueOf((Long)jsonData.get("servertime"));
				String authValue = (String)jsonData.get("authvalue");
				String level = String.valueOf((Long)jsonData.get("correntLvl"));

				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				} else if(devType == null || devType.equals("")) {
					eobj.put("code", "102000");
				} else if(serverTime == null || serverTime.equals("")) {
					eobj.put("code", "102000");
				} else if(authValue == null || authValue.equals("")) {
					eobj.put("code", "102000");
				} else {
					
					//사용자 가입여부 확인
					boolean checkResult = commonService.userCheck(userId);
					
					if(checkResult) {
						
						if (isAuthTimeOut(serverTime)) {
							eobj.put("code", "102203");
						} else {
							AuthEntity auth = new AuthEntity();
							auth.setUsrCorrect(authValue);
							auth.setCorrectDate(serverTime);
							auth.setCorrectLevel(level);
							
							Map<String, Object> map = authService.authCheck(auth, userId);
							
							if((Boolean)map.get("passmind")) {
								resultJson.put("otk", "");
								eobj.put("data", resultJson);
								eobj.put("code", "101000");
								
								
							} else {
								//실패 횟수 결과
								Map<String, Object> resultMap = authService.getUserFailResult(userId);
								boolean userFail = (Boolean) resultMap.get("result");
								
								if(userFail)
									eobj.put("code", "102250");
								else
									eobj.put("code", "102200");
							}
						}
						
					} else {
						eobj.put("code", "102260");
					}
				}
			}
	    } catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
		//결과 데이터 리턴
	    eobj.put("message", "");
	    writer.write(eobj.toJSONString());

	    writer.flush();
	    writer.close();
	}
	
	/**
	 * 인증 타임 체크 확인.
	 * @param serverTime
	 * @return
	 */
	public boolean isAuthTimeOut(String serverTime) {
		boolean ret = true;
		
		try {
			// 현재 시간 
			long nowTime = System.currentTimeMillis() / 1000;
			
			// 이미지 요청시 전달했던 서버 시간 
			long serveritme = Long.parseLong(serverTime);
			
			int timeOut = 5;
			timeOut = timeOut * 60;
			
			ret = (nowTime - serveritme) < timeOut ? false : true; 
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
}