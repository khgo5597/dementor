package com.mininfo.close.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.close.dao.CloseDAO;

/**  
 * @Class Name : CloseService.java
 * @Description : 서비스해지  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Service("closeService")
public class CloseService {
	
	//CloseDAO DAO 선언
	@Resource(name="closeDAO")
	private CloseDAO closeDAO;
	
	/**
	 * 서비스 해지
	 * @param String userId - 사용자 아이디
	 * @return void
	 * @exception Exception
	 */
	public void userClose(String userId) throws Exception {
		closeDAO.userClose(userId);
	}
}
