package com.mininfo.close.web;

import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mininfo.close.service.CloseService;
import com.mininfo.common.service.CommonService;

/**  
 * @Class Name : CloseController.java
 * @Description : 서비스 해지  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Controller
public class CloseController {
	//CloseService 서비스 선언
	@Resource(name="closeService")
	private CloseService closeService;
	
	//공통 서비스 선언
	@Resource(name="commonService")
	private CommonService commonService;
	
	/**
	 * 사용자 해지
	 * @param HttpServletRequest userId
	 * @param HttpServletRequest req
	 * @param HttpServletResponse res
	 * @return void
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/closeUser.do")
	public @ResponseBody void userCheck(
			@RequestParam(value="data") String data,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");
		
		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
		JSONObject eobj = new JSONObject();
		
		try {
			//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
			System.out.println("obj:"+obj);
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
		    
		    //GET 방식 오류
//		    if(req.getMethod().equals("GET")) {
//				eobj.put("code", "102003");
//			} else {
				String userId = (String)jsonData.get("userid");
				
				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				} else {
					//사용자 가입여부 확인
					boolean checkResult = commonService.userCheck(userId);
					
					if(checkResult) {
						closeService.userClose(userId);
						eobj.put("code", "101000");
					} else {
						eobj.put("code", "102260");
					}
				}
			//}
		    
		} catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
		//결과 데이터 리턴
		eobj.put("message", "");
		writer.write(eobj.toJSONString());
		
		writer.flush();
		writer.close();
	}
}
