package com.mininfo.close.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

/**  
 * @Class Name : CloseDAO.java
 * @Description : 서비스해지 DAO Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Repository("closeDAO")
public class CloseDAO {
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}
	
	/**
	 * 서비스 해지
	 * @param String userId - 사용자 아이디
	 * @return void
	 * @exception Exception
	 */
	public void userClose(String userId) throws Exception {
		sqlMap.delete("deleteUserKey", userId);
//		sqlMap.delete("deleteUserDevice", userId);
		sqlMap.delete("deleteUser", userId);
	}
}
