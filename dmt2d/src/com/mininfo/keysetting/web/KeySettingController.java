package com.mininfo.keysetting.web;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mininfo.common.service.CommonService;
import com.mininfo.keysetting.model.CategoryEntity;
import com.mininfo.keysetting.model.IconEntity;
import com.mininfo.keysetting.model.UserEntity;
import com.mininfo.keysetting.service.KeySettingService;
import com.mininfo.util.DBencryption;

/**  
 * @Class Name : KeySettingController.java
 * @Description : 키 등록/변경/분실  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Controller
public class KeySettingController {
	//KeySetting 서비스 선언
	@Resource(name="keySettingService")
	private KeySettingService keySettingService;
	
	//공통 서비스 선언
	@Resource(name="commonService")
	private CommonService commonService;
	
	private static String cateImgPath = "http://localhost:8090/dmt2d/images/category/3/";
	private static String iconImgPath = "http://localhost:8090/dmt2d/images/icon_img/3";
	
	/**
	 * 아이콘 이미지 요청
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@RequestMapping(value="/list.do")
	public @ResponseBody void list(
			@RequestParam(value="data") String data,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");

		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();
		
		try {
			//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
		    
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
				eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userid");
				String devType = (String)jsonData.get("devtype");
				
				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				} else if(devType == null || devType.equals("")) {
					eobj.put("code", "102000");
				} else {
					//카테고리 정보 조회
					List<CategoryEntity> cateList = keySettingService.selectCate();
					
					if(cateList != null) {
						//카테고리 리스트 Json
						JSONArray cateJsonArray = new JSONArray();
						//복호화 유틸
						DBencryption encr = new DBencryption();
						//카테고리 
						String cateZip = "";
						
						for(CategoryEntity cate : cateList) {
							//아이콘 정보 조회
							List<IconEntity> iconList = keySettingService.iconList(Integer.parseInt(cate.getCategoryId()));
							
							//아이콘 리스트 Json
							JSONArray iconJsonArray = new JSONArray();
							
							//아이콘 Json 생성
							for(IconEntity icon : iconList) {
								JSONObject iconJson = new JSONObject();
								iconJson.put("iconid", Integer.parseInt(icon.getIconId()));
								iconJson.put("iconname", encr.db_decoding(icon.getIconName()));
								iconJson.put("iconurl", encr.db_decoding(icon.getIconUrl()));
								
								iconJsonArray.add(iconJson);
							}
							
							//카테고리 Json 생성
							JSONObject cateJson = new JSONObject();
							cateJson.put("categoryid", Integer.parseInt(cate.getCategoryId()));
							cateJson.put("categoryname", cate.getCategoryName());
							cateJson.put("iconzip", iconImgPath+cate.getIconZip());
							cateJson.put("iconitem", iconJsonArray);
							cateJsonArray.add(cateJson);
							
							cateZip = cate.getCategoryFileName();
						} 
						
						resultJson.put("category", cateJsonArray);
						resultJson.put("categoryzip", cateImgPath+cateZip);
						
						eobj.put("data", resultJson);
						eobj.put("code", "101000");
					} else {
						eobj.put("code", "102000");
					}
				}
			}
		} catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
		//결과 데이터 리턴
		eobj.put("message", "");
	    writer.write(eobj.toJSONString());

	    writer.flush();
	    writer.close();
	}
	
	/**
	 * 키 등록/변경
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/keyset.do")
	public @ResponseBody void keyset(
			@RequestParam(value="data") String data,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");

		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//result json
	    JSONObject resultJson = new JSONObject();
	    JSONObject eobj = new JSONObject();

	    try {
	    	//parameter Json parsing
			Object obj = JSONValue.parse(data.toString());
		    JSONObject jsonData = (JSONObject)((JSONObject)obj).get("data");
		    
		    //GET 방식 오류
		    if(req.getMethod().equals("GET")) {
				eobj.put("code", "102003");
			} else {
				String userId = (String)jsonData.get("userid");
				String devType = (String)jsonData.get("devtype");
				String hole = (String)jsonData.get("hole");
				String key1 = (String)jsonData.get("key1");
				String key2 = (String)jsonData.get("key2");
				String key3 = (String)jsonData.get("key3");
				
				//사용자 아이디 오류
				if(userId == null || userId.equals("")) {
					eobj.put("code", "102000");
				} else if(devType == null || devType.equals("")) {
					eobj.put("code", "102000");
				} else if(hole == null || hole.equals("")) {
					eobj.put("code", "102000");
				} else if(key1 == null || key1.equals("")) {
					eobj.put("code", "102000");
				} else if(key2 == null || key2.equals("")) {
					eobj.put("code", "102000");
				} else if(key3 == null || key3.equals("")) {
					eobj.put("code", "102000");
				} else {
				  	UserEntity userEntity = new UserEntity();
				  	userEntity.setKey0(Integer.parseInt(hole));
				  	userEntity.setKey1(Integer.parseInt(key1));
				  	userEntity.setKey2(Integer.parseInt(key2));
				  	userEntity.setKey3(Integer.parseInt(key3));

					userEntity.setUserId(userId);
					userEntity.setDevType(devType);

					//사용자 가입여부 확인
					boolean checkResult = commonService.userCheck(userId);
					
					if(checkResult)
						userEntity.setSettingType("U");
					else
						userEntity.setSettingType("I");
					
					keySettingService.userKeySetting(userEntity);
					
					resultJson.put("otk", "");
					eobj.put("data", resultJson);
					eobj.put("code", "101000");
				}
			}
	    } catch(Exception e) {
			eobj.put("code", "103000");
			e.printStackTrace();
		}
		
		//결과 데이터 리턴
	    eobj.put("message", "");
	    writer.write(eobj.toJSONString());

	    writer.flush();
	    writer.close();
	}
}