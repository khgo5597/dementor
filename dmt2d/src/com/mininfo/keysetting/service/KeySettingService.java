package com.mininfo.keysetting.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.keysetting.dao.KeySettingDAO;
import com.mininfo.keysetting.model.CategoryEntity;
import com.mininfo.keysetting.model.IconEntity;
import com.mininfo.keysetting.model.UserEntity;
import com.mininfo.util.DBencryption;

/**  
 * @Class Name : KeySettingService.java
 * @Description : 키 등록/변경/분실  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Service("keySettingService")
public class KeySettingService {
	
	//KeySetting DAO 선언
	@Resource(name="keySettingDAO")
	private KeySettingDAO keySettingDAO;
	
	/**
	 * 카테고리 조회
	 * @param 
	 * @return List<CategoryEntity> 카테고리 리스트 정보
	 * @exception Exception
	 */
	public List<CategoryEntity> selectCate() throws Exception {
		return keySettingDAO.selectCate();
	}
	
	/**
	 * 아이콘 조회
	 * @param int categoryId - 카테고리 아이디
	 * @return List<IconEntity> 아이콘 리스트 정보
	 * @exception Exception
	 */
	public List<IconEntity> iconList(int categoryId) throws Exception {
		return keySettingDAO.iconList(categoryId);
	}

	/**
	 * 사용자 등록
	 * @param EmailEntity user
	 * @return 
	 * @exception Exception
	 */
	public void userKeySetting(UserEntity userEntity) throws Exception {
		//로그인시 화면에 보여질 키 갯수
		int userViewSize = 24;
		
		//키 등록 리스트 생성
		List<Integer> key = new ArrayList<Integer>(userViewSize);
		key.add(userEntity.getKey0());
		key.add(userEntity.getKey1());
		key.add(userEntity.getKey2());
		key.add(userEntity.getKey3());
		
		IconEntity icon = new IconEntity();
		icon.setKey0(userEntity.getKey0());
		icon.setKey1(userEntity.getKey1());
		icon.setKey2(userEntity.getKey2());
		icon.setKey3(userEntity.getKey3());
		
		List<IconEntity> csteArray = keySettingDAO.getIconCount(icon);
		
		Random random = new Random();
		int incnt  = 1;
		
		for(int i=0;i<csteArray.size();i++) {
			icon = (IconEntity)csteArray.get(i);
			
			int cnt    = icon.getIconCount();
			
			int start  = icon.getStartId();
			int end    = icon.getEndId();
			
			double range = end - start + 1;
			
			int maxIcon = 0;
			if(i > 0)
				maxIcon = ( cnt * 6) - cnt ;
			else
				maxIcon = (( cnt * 6) + 1) -cnt;	
						
			for(int k=1;k<=maxIcon;k++) {
				while(true) {
				    int rd = 0;
					if(maxIcon == 25) {
						rd = start;
						start++;
					} else {
						rd = (int)(random.nextDouble() * range + start);
					}

					boolean result1 = false;
					for(int x=0 ; x<key.size(); x++) { 
						if(rd==key.get(x)){
							result1 = true; 
						}
					}
					if(result1) {
						continue;
					} else if(incnt < 25 ) {
						key.add(rd);
						incnt++;
					}
					break;					
				}								
			}
		}
		
		//홀키 위치 랜덤함수
		Random randomHole = new Random();
		//홀키 위치 랜덤 값 발생
		int randomHoleCnt = randomHole.nextInt(1000000000);
		//랜덤 값 설정
		userEntity.setUserImg(randomHoleCnt);
		
		//홀키 위치 구하기
		int userImg = randomHoleCnt%25;
		//짝수 인지 홀수 인지 
		String userImgUp = userImg%2 == 0 ? "Y" : "N";
		
		//홀키 위치에 따른 사용자 키 설정 리스트
		List<Integer> userImgList = new ArrayList<Integer>();
		for(int i=0;i<25;i++) userImgList.add(i);
		
		//짝수 이면 정방향 임의키 설정
		if(userImgUp.equals("Y")) {
			int x = 1;
			for(int i=0;i<=userViewSize;i++) {
				if(userImg == i) {
					userImgList.set(i, key.get(0));
				} else {
					userImgList.set(i, key.get(x));
					x++;
				}
			}
		//홀수 이면 역방향 임의키 설정
		} else {
			int x = 1;
			for(int i=userViewSize;i>=0;i--) {
				if(userImg == i) {
					userImgList.set(i, key.get(0));
				} else {
					userImgList.set(i, key.get(x));
					x++;
				}
			}
		}

		//userEntity에 사용자 키 설정
		userEntity.setKey0(userImgList.get(0));
		userEntity.setKey1(userImgList.get(1));
		userEntity.setKey2(userImgList.get(2));
		userEntity.setKey3(userImgList.get(3));
		userEntity.setKey4(userImgList.get(4));
		userEntity.setKey5(userImgList.get(5));
		userEntity.setKey6(userImgList.get(6));
		userEntity.setKey7(userImgList.get(7));
		userEntity.setKey8(userImgList.get(8));
		userEntity.setKey9(userImgList.get(9));
		userEntity.setKey10(userImgList.get(10));
		userEntity.setKey11(userImgList.get(11));
		userEntity.setKey12(userImgList.get(12));
		userEntity.setKey13(userImgList.get(13));
		userEntity.setKey14(userImgList.get(14));
		userEntity.setKey15(userImgList.get(15));
		userEntity.setKey16(userImgList.get(16));
		userEntity.setKey17(userImgList.get(17));
		userEntity.setKey18(userImgList.get(18));
		userEntity.setKey19(userImgList.get(19));
		userEntity.setKey20(userImgList.get(20));
		userEntity.setKey21(userImgList.get(21));
		userEntity.setKey22(userImgList.get(22));
		userEntity.setKey23(userImgList.get(23));
		userEntity.setKey24(userImgList.get(24));
		
		if(userEntity.getSettingType().equals("I"))
			keySettingDAO.userinsert(userEntity);
		else
			keySettingDAO.userUpdate(userEntity);
	}
	
	/**
	 * 사용자 키 조회
	 * @param String userId
	 * @return List<IconEntity>
	 * @exception Exception
	 */
	@SuppressWarnings("static-access")
	public List<IconEntity> getUserKeyIcon(String userId) throws Exception {
		UserEntity user = keySettingDAO.getMemberKey(userId);	

		List<Integer> key = new ArrayList<Integer>();
		
		key.add(user.getKey0());
		key.add(user.getKey1());
		key.add(user.getKey2());
		key.add(user.getKey3());
		key.add(user.getKey4());
		key.add(user.getKey5());
		key.add(user.getKey6());
		key.add(user.getKey7());
		key.add(user.getKey8());
		key.add(user.getKey9());
		key.add(user.getKey10());
		key.add(user.getKey11());
		key.add(user.getKey12());
		key.add(user.getKey13());
		key.add(user.getKey14());
		key.add(user.getKey15());
		key.add(user.getKey16());
		key.add(user.getKey17());
		key.add(user.getKey18());
		key.add(user.getKey19());
		key.add(user.getKey20());
		key.add(user.getKey21());
		key.add(user.getKey22());
		key.add(user.getKey23());
		key.add(user.getKey24());

		int randomHoleCnt = keySettingDAO.getUserImg(userId);
		
		//홀키 위치 구하기
		int userImg = randomHoleCnt%25;
		//짝수 인지 홀수 인지 
		String userImgUp = userImg%2 == 0 ? "Y" : "N";		
		
		if(userImgUp.equals("Y")) {
			user.setKey0((Integer)key.get(userImg));
			user.setKey1((Integer)key.get(0));
			user.setKey2((Integer)key.get(1));
			user.setKey3((Integer)key.get(2));
		} else {
			user.setKey0((Integer)key.get(userImg));
			user.setKey1((Integer)key.get(24));
			user.setKey2((Integer)key.get(23));
			user.setKey3((Integer)key.get(22));
		}

		List<IconEntity> userKey = new ArrayList<IconEntity>();
		
		key.set(0, user.getKey0());
		key.set(1, user.getKey1());
		key.set(2, user.getKey2());
		key.set(3, user.getKey3());
		
		//복호화 유틸
		DBencryption encr = new DBencryption();
		
		for(int i=0;i<4;i++) {
			IconEntity icon = keySettingDAO.getIconUrl((Integer)key.get(i));
			String iconUrl = encr.db_decoding(icon.getIconUrl());
			icon.setIconUrl(iconUrl);
			userKey.add(icon);
		}
		
		return userKey;
	}
	
	// 사용자 키 정보 조회
	public UserEntity getUserKey(String userId) throws Exception{
		UserEntity userKeyEntity = null;
		userKeyEntity = keySettingDAO.getUserKey(userId);
		return userKeyEntity;
	}
}
