package com.mininfo.keysetting.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

import com.mininfo.keysetting.model.CategoryEntity;
import com.mininfo.keysetting.model.IconEntity;
import com.mininfo.keysetting.model.UserEntity;

/**  
 * @Class Name : KeySettingDAO.java
 * @Description : 키 등록/변경/분실  DAO Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Repository("keySettingDAO")
public class KeySettingDAO {
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}
	
	/**
	 * 카테고리 조회
	 * @param 
	 * @return List<CategoryEntity> 카테고리 리스트 정보
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	public List<CategoryEntity> selectCate() throws Exception {
		return sqlMap.queryForList("getCateSelectList");
	}
	
	/**
	 * 아이콘 조회
	 * @param int categoryId - 카테고리 아이디
	 * @return List<IconEntity> 아이콘 리스트 정보
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	public List<IconEntity> iconList(int categoryId) throws Exception {
		return sqlMap.queryForList("getCateIcon", categoryId);
	}
	
	/**
	 * 사용자가 선택한 아이콘을 제외한 나머지 아이콘 조회
	 * @param IconEntity icon
	 * @return List<IconEntity> 아이콘 리스트 정보
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	public List<IconEntity> getIconCount(IconEntity icon) throws Exception {
		return sqlMap.queryForList("getIconCount", icon);
	}
	
	/**
	 * 사용자 등록
	 * @param EmailEntity user
	 * @return 
	 * @exception Exception
	 */
	public void userinsert(UserEntity user) throws Exception {
		sqlMap.insert("insertUserInfo", user);
		sqlMap.insert("insertUserKey", user);
//		sqlMap.insert("insertUserDevice", user);

	}
	
	/**
	 * 사용자 키 수정
	 * @param EmailEntity user
	 * @return 
	 * @exception Exception
	 */
	public void userUpdate(UserEntity user) throws Exception {
		sqlMap.update("updateUserKey", user);
//		sqlMap.update("updateUserDevice", user);
		sqlMap.update("updateUserInfo", user);
	}
	
	/**
	 * 사용자 키 조회
	 * @param EmailEntity user
	 * @return UserEntity
	 * @exception Exception
	 */
	public UserEntity getMemberKey(String userId) throws Exception {
		return (UserEntity)sqlMap.queryForObject("getMemberKey", userId);
	}
	
	/**
	 * 사용자 키 조회
	 * @param EmailEntity user
	 * @return UserEntity
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	public List<IconEntity> getMemberKeyIconUrl(UserEntity user) throws Exception {
		return (List<IconEntity>)sqlMap.queryForList("getMemberKeyIconUrl", user);
	}
	
	/**
	 * 사용자 키 조회
	 * @param String userId
	 * @return int
	 * @exception Exception
	 */
	public int getUserImg(String userId) throws Exception {
		return (Integer)sqlMap.queryForObject("getUserImg", userId);
	}
	
	/**
	 * 아이콘 아이디에 따른 정보 조회
	 * @param int iconId
	 * @return String
	 * @exception Exception
	 */
	public IconEntity getIconUrl(int iconId) throws Exception {
		return (IconEntity)sqlMap.queryForObject("getIconUrl", iconId);
	}
	
	/**
	 * 사용자 키 조회
	 * @param String userId
	 * @return UserEntity
	 * @exception Exception
	 */
	public UserEntity getUserKey(String userId) throws Exception {
		return (UserEntity)sqlMap.queryForObject("getUserKey", userId);
	}
}
