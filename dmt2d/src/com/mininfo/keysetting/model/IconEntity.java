package com.mininfo.keysetting.model;

import java.io.Serializable;

/**  
 * @Class Name : IconEntity.java
 * @Description : 아이콘 정보  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

public class IconEntity implements Serializable {

	private static final long serialVersionUID = -1363967297514791836L;
	
	private String iconId = "";				//아이콘 아이디
	private String iconUrl = "";			//아이콘 URL
	private String iconName = "";			//아이콘 명
	private String iconFileName = "";		//아이콘 파일명
	
	private int key0;						//홀키
	private int key1;						//암호키1
	private int key2;						//암호키2
	private int key3;						//암호키3
	
	private int categoryId;					//카테고리 아이디
	private int iconCount;					//카테고리 별 아이콘 갯수
	private int startId;					//시작 아이콘 아이디
	private int endId;						//마지막 아이콘 아이디
	private int maxCount;				//총 아이콘 갯수
	
	public String getIconId() {
		return iconId;
	}
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public String getIconFileName() {
		return iconFileName;
	}
	public void setIconFileName(String iconFileName) {
		this.iconFileName = iconFileName;
	}
	public int getKey0() {
		return key0;
	}
	public void setKey0(int key0) {
		this.key0 = key0;
	}
	public int getKey1() {
		return key1;
	}
	public void setKey1(int key1) {
		this.key1 = key1;
	}
	public int getKey2() {
		return key2;
	}
	public void setKey2(int key2) {
		this.key2 = key2;
	}
	public int getKey3() {
		return key3;
	}
	public void setKey3(int key3) {
		this.key3 = key3;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getIconCount() {
		return iconCount;
	}
	public void setIconCount(int iconCount) {
		this.iconCount = iconCount;
	}
	public int getStartId() {
		return startId;
	}
	public void setStartId(int startId) {
		this.startId = startId;
	}
	public int getEndId() {
		return endId;
	}
	public void setEndId(int endId) {
		this.endId = endId;
	}
	public int getMaxCount() {
		return maxCount;
	}
	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}
}
