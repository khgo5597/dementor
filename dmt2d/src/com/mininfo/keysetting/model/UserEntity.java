package com.mininfo.keysetting.model;

import java.io.Serializable;

/**  
 * @Class Name : UserEntity.java
 * @Description : 사용자 정보  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

public class UserEntity implements Serializable {
	
	private static final long serialVersionUID = -1363967297514791836L;
	
	private String userId = "";			//사용자 아이디
	private int keyCount;				//오류 횟수
	private int key0;					//키1
	private int key1;					//키2
	private int key2;					//키3
	private int key3;					//키4
	private int key4;					//키5
	private int key5;					//키6
	private int key6;					//키7
	private int key7;					//키8
	private int key8;					//키9
	private int key9;					//키10
	private int key10;					//키11
	private int key11;					//키12
	private int key12;					//키13
	private int key13;					//키14
	private int key14;					//키15
	private int key15;					//키16
	private int key16;					//키17
	private int key17;					//키18
	private int key18;					//키19
	private int key19;					//키20
	private int key20;					//키21
	private int key21;					//키22
	private int key22;					//키23
	private int key23;					//키24
	private int key24;					//키25
	
	private String devType = "";		//접속 타입
	private String answer = "";			//사용자 인증 시 필요한 키
	
	private String settingType = "";	//등록/변경 구분값
	private int userImg = 0;			//사용자 홀키 위치

	public int getUserImg() {
		return userImg;
	}
	public void setUserImg(int userImg) {
		this.userImg = userImg;
	}
	public String getDevType() {
		return devType;
	}
	public void setDevType(String devType) {
		this.devType = devType;
	}
	public String getSettingType() {
		return settingType;
	}
	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getKeyCount() {
		return keyCount;
	}
	public void setKeyCount(int keyCount) {
		this.keyCount = keyCount;
	}
	public int getKey0() {
		return key0;
	}
	public void setKey0(int key0) {
		this.key0 = key0;
	}
	public int getKey1() {
		return key1;
	}
	public void setKey1(int key1) {
		this.key1 = key1;
	}
	public int getKey2() {
		return key2;
	}
	public void setKey2(int key2) {
		this.key2 = key2;
	}
	public int getKey3() {
		return key3;
	}
	public void setKey3(int key3) {
		this.key3 = key3;
	}
	public int getKey4() {
		return key4;
	}
	public void setKey4(int key4) {
		this.key4 = key4;
	}
	public int getKey5() {
		return key5;
	}
	public void setKey5(int key5) {
		this.key5 = key5;
	}
	public int getKey6() {
		return key6;
	}
	public void setKey6(int key6) {
		this.key6 = key6;
	}
	public int getKey7() {
		return key7;
	}
	public void setKey7(int key7) {
		this.key7 = key7;
	}
	public int getKey8() {
		return key8;
	}
	public void setKey8(int key8) {
		this.key8 = key8;
	}
	public int getKey9() {
		return key9;
	}
	public void setKey9(int key9) {
		this.key9 = key9;
	}
	public int getKey10() {
		return key10;
	}
	public void setKey10(int key10) {
		this.key10 = key10;
	}
	public int getKey11() {
		return key11;
	}
	public void setKey11(int key11) {
		this.key11 = key11;
	}
	public int getKey12() {
		return key12;
	}
	public void setKey12(int key12) {
		this.key12 = key12;
	}
	public int getKey13() {
		return key13;
	}
	public void setKey13(int key13) {
		this.key13 = key13;
	}
	public int getKey14() {
		return key14;
	}
	public void setKey14(int key14) {
		this.key14 = key14;
	}
	public int getKey15() {
		return key15;
	}
	public void setKey15(int key15) {
		this.key15 = key15;
	}
	public int getKey16() {
		return key16;
	}
	public void setKey16(int key16) {
		this.key16 = key16;
	}
	public int getKey17() {
		return key17;
	}
	public void setKey17(int key17) {
		this.key17 = key17;
	}
	public int getKey18() {
		return key18;
	}
	public void setKey18(int key18) {
		this.key18 = key18;
	}
	public int getKey19() {
		return key19;
	}
	public void setKey19(int key19) {
		this.key19 = key19;
	}
	public int getKey20() {
		return key20;
	}
	public void setKey20(int key20) {
		this.key20 = key20;
	}
	public int getKey21() {
		return key21;
	}
	public void setKey21(int key21) {
		this.key21 = key21;
	}
	public int getKey22() {
		return key22;
	}
	public void setKey22(int key22) {
		this.key22 = key22;
	}
	public int getKey23() {
		return key23;
	}
	public void setKey23(int key23) {
		this.key23 = key23;
	}
	public int getKey24() {
		return key24;
	}
	public void setKey24(int key24) {
		this.key24 = key24;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
}