package com.mininfo.keysetting.model;

import java.io.Serializable;

/**  
 * @Class Name : CategoryEntity.java
 * @Description : 카테고리  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

public class CategoryEntity implements Serializable {

	private static final long serialVersionUID = -1363967297514791836L;
	
	private String categoryUrl = "";			//카테고리 Url 정보
	private String categoryId = "";				//카테고리 아이디
	private String categoryName = "";			//카테고리 명
	private String iconZip = "";				//아이콘 zip 정보
	private String categoryFileName = "";		//카테고리 파일 명
	
	public String getCategoryUrl() {
		return categoryUrl;
	}
	public void setCategoryUrl(String categoryUrl) {
		this.categoryUrl = categoryUrl;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getIconZip() {
		return iconZip;
	}
	public void setIconZip(String iconZip) {
		this.iconZip = iconZip;
	}
	public String getCategoryFileName() {
		return categoryFileName;
	}
	public void setCategoryFileName(String categoryFileName) {
		this.categoryFileName = categoryFileName;
	}
}
