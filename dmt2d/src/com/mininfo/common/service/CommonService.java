package com.mininfo.common.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.common.dao.CommonDAO;

/**  
 * @Class Name : CommonService.java
 * @Description : 공통 처리  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Service("commonService")
public class CommonService {
	
	//공통 DAO 선언
	@Resource(name="commonDAO")
	private CommonDAO commonDAO;
	
	/**
	 * 가입여부 확인
	 * @param String userId - 사용자 아이디
	 * @return boolean - 가입여부
	 * @exception Exception
	 */
	public boolean userCheck(String userId) throws Exception {
		boolean checkResult = false;
		
		String result = commonDAO.userCheck(userId);
		if(result != null && !result.equals(""))
			checkResult = true;
		else
			checkResult = false;
		
		return checkResult;
	}
}
