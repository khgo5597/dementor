package com.mininfo.common.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

/**  
 * @Class Name : CommonDAO.java
 * @Description : 공통 처리  DAO Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Repository("commonDAO")
public class CommonDAO {
	
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}
	
	/**
	 * 가입여부 확인
	 * @param String userId - 사용자 아이디
	 * @return String userId
	 * @exception Exception
	 */
	public String userCheck(String userId) throws Exception {
		System.out.println("userId:"+userId);
		return (String)sqlMap.queryForObject("useridCheck", userId);
	}
}
