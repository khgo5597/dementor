package com.mininfo.common;

public class SequenceGenerator {

	private long seq = System.currentTimeMillis();
    private int minLen;
    private int maxLen;
    
    public SequenceGenerator(int minLen, int maxLen) {
        this.minLen = minLen;
        this.maxLen = maxLen;
    }
    
    public synchronized final String nextValue() {
        return resize(generate());
    }
    
    private String resize(String v) {
        int vLen = v.length();

        if (vLen < minLen) {
            StringBuffer buf = new StringBuffer(minLen);

            for (int i = minLen - vLen; i > 0; i--) {
                buf.append('0');
            }

            buf.append(v);
            return buf.toString();
        }

        if (vLen > maxLen) {
            return v.substring(vLen - maxLen);
        }
        return v;
    }
    
    private String generate() {
        String v = Long.toString(seq++, Character.MAX_RADIX);

        if (seq < 0) {
            seq = 0;
        }
        return v;
    }
}
