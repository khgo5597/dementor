package com.mininfo.common.mail;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {
 
	public static void main(String args[]) throws UnsupportedEncodingException {
		// new MailSender();
	}

	public MailSender() throws UnsupportedEncodingException {

	}

	public void sendMail(Properties pro) throws UnsupportedEncodingException {

		boolean debug = Boolean.valueOf("true").booleanValue();
		
		String mail_to = pro.getProperty("mail_to");
		String mail_from = pro.getProperty("mail_from");
		String mail_from_name = pro.getProperty("mail_from_name");
		String mail_subject = pro.getProperty("mail_subject");
		String mail_contets = pro.getProperty("mail_contets");
		
		System.out.println("mail_to:"+mail_to);
		System.out.println("mail_from:"+mail_from);
		System.out.println("mail_from_name:"+mail_from_name);
		System.out.println("mail_subject:"+mail_subject);
		System.out.println("mail_contets:"+mail_contets);
		
		String d_email = "dementor@dementor.co.kr"; // 보내는 사람 
		String d_host = "smtp.gmail.com";
		String d_port = "465";
		
		Properties props = new Properties();
		props.put("mail.smtp.user", d_email);
		props.put("mail.smtp.host", d_host);
		props.put("mail.smtp.port", d_port);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", d_port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");

		Authenticator auth = new MyAuthentication();
		if (debug)
			props.put("mail.debug", "true");

		// Session session = Session.getDefaultInstance(props, auth);
		Session session = Session.getInstance(props, auth);
		session.setDebug(debug);

		try {
			InternetAddress address = new InternetAddress();
			address = new InternetAddress(mail_to);

			InternetAddress fromAddr = new InternetAddress(mail_from);
			Message msg = new MimeMessage(session);
			// InternetAddress[] address = {new InternetAddress(to)};

			// 닉네임(보내는사람) 세팅
			fromAddr.setPersonal(mail_from_name, "EUC-KR");
			msg.setFrom(fromAddr);
			msg.setRecipient(Message.RecipientType.TO, address);
			msg.setSentDate(new Date());

			// 제목
			msg.setSubject(mail_subject);

			// 내용
			msg.setContent(mail_contets, "text/html; charset=EUC-KR");

			// 메일 전송부분.
			Transport.send(msg);

		} catch (MessagingException mex) {
			mex.printStackTrace();
			Exception ex = mex;

			do {
				if (ex instanceof SendFailedException) {
					SendFailedException sfex = (SendFailedException) ex;
					Address[] invalid = sfex.getInvalidAddresses();
					if (invalid != null) {
						System.out.println("    ** Invalid Addresses");
						if (invalid != null) {
							for (int i = 0; i < invalid.length; i++)
								System.out.println("         " + invalid[i]);
						}
					}
					Address[] validUnsent = sfex.getValidUnsentAddresses();

					if (validUnsent != null) {
						System.out.println("    ** ValidUnsent Addresses");
						if (validUnsent != null) {
							for (int i = 0; i < validUnsent.length; i++)
								System.out
										.println("         " + validUnsent[i]);
						}
					}
					Address[] validSent = sfex.getValidSentAddresses();

					if (validSent != null) {
						System.out.println("    ** ValidSent Addresses");
						if (validSent != null) {
							for (int i = 0; i < validSent.length; i++)
								System.out.println("         " + validSent[i]);
						}
					}
				}

				if (ex instanceof MessagingException) {
					ex = ((MessagingException) ex).getNextException();
				} else {
					ex = null;
				}
			} while (ex != null);
		}
	}
}

class MyAuthentication extends Authenticator {

	PasswordAuthentication pa;

	public MyAuthentication() {
		// smtp server의 아이디와 패스워드를 입력한다.
		// pa = new PasswordAuthentication("soymilk.cafelatte@gmail.com", "elqpffhvj/");
		pa = new PasswordAuthentication("khgo5596@gmail.com", "0649eerra");
	}

	// 아래의 메소드는 시스템측에서 사용하는 메소드이다.
	public PasswordAuthentication getPasswordAuthentication() {
		return pa;
	}
}