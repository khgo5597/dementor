package com.mininfo.common.mail;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MailUtil {

	private final Log logger = LogFactory.getLog(getClass());

	public void sendMail(Properties pro) throws AddressException,
			MessagingException, UnsupportedEncodingException {

		String mail_server = pro.getProperty("mail_server");
		String mail_port = pro.getProperty("mail_port");
		String mail_to = pro.getProperty("mail_to");
		String mail_from = pro.getProperty("mail_from");
		String mail_from_name = pro.getProperty("mail_from_name");
		String mail_subject = pro.getProperty("mail_subject");
		String mail_auth_yn = pro.getProperty("mail_auth_yn");
		String mail_auth_id = pro.getProperty("mail_auth_id");
		String mail_auth_pw = pro.getProperty("mail_auth_pw");
		String mail_contets = pro.getProperty("mail_contets");

		//Setup mail server
		Authenticator auth = new PopupAuthenticator(mail_auth_id, mail_auth_pw);

		//Session
		Session mailSession = null;

		//Create session
		Properties mailProps = new Properties();
		mailProps.put("mail.smtp.host", mail_server);
		mailProps.put("mail.smtp.port", mail_port);

		if ("Y".equals(mail_auth_yn)) {
			mailProps.put("mail.smtp.auth", "true");
			mailSession = Session.getInstance(mailProps, auth);
		} else {
			mailProps.put("mail.smtp.auth", "false");
			mailSession = Session.getInstance(mailProps, null);
		}

		InternetAddress toAddrs = new InternetAddress(mail_to);
		InternetAddress fromAddr = new InternetAddress(mail_from,
				mail_from_name);

		//Create and initialize message
		Message message = new MimeMessage(mailSession);
		message.setFrom(fromAddr);
		message.setRecipient(Message.RecipientType.TO, toAddrs);
		message.setSubject(mail_subject);
		message.setContent(mail_contets, "text/html; charset=UTF-8");

		//Send message
		try {
			Transport.send(message);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			logger.info("MAIL Properties="+pro);
			ex.printStackTrace();
			throw new MessagingException(ex.getMessage());
		}
	}

	static class PopupAuthenticator extends Authenticator {

		private String id;
		private String pw;

		public PopupAuthenticator(String id, String pw) {
			this.id = id;
			this.pw = pw;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(id, pw);
		}
	}
}
