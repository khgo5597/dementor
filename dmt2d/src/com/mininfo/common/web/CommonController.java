package com.mininfo.common.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mininfo.common.service.CommonService;

/**  
 * @Class Name : CommonController.java
 * @Description : 공통 처리  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Controller
public class CommonController {
	
	//공통 서비스 선언
	@Resource(name="commonService")
	private CommonService commonService;

	/**
	 * 사용자 체크
	 * @param HttpServletRequest userId
	 * @param HttpServletRequest req
	 * @param HttpServletResponse res
	 * @return void
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/userCheck.do")
	public @ResponseBody void userCheck(
			@RequestParam("userId") String userId,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		//사용자 가입여부 확인
		boolean checkResult = commonService.userCheck(userId);

		//response 셋팅
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "no-cache");
		res.setCharacterEncoding("UTF-8");
		
		//PrintWriter 객체 생성
		PrintWriter writer = res.getWriter();
		
		//json 데이터 생성
		JSONObject obj = new JSONObject();
		obj.put("resultCode", checkResult);
		
		//json 데이터 쓰기
		writer.write(obj.toJSONString());
	}
	
	@RequestMapping(value="/jsonTest.do")
	public ModelAndView jsonTest() throws Exception {
		
		ModelAndView mv = new ModelAndView();
		
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		
		for(int i=0;i<10;i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("test1", "test1");
			map.put("test2", "test2");
			map.put("test3", "test3");
			map.put("test4", "test4");
			map.put("test5", "test5");
			map.put("test6", "test6");
			map.put("test7", "test7");
			map.put("test8", "test8");
			
			list.add(map);
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", "100000");
		resultMap.put("list", list);
		
		mv.addAllObjects(resultMap);
		mv.setViewName("jsonView");
		
		return mv;
	}
}