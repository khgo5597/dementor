package com.mininfo.common.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**  
 * @Class Name : ViewController.java
 * @Description : View 처리  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Controller
public class ViewController {
	
	/**
	 * 그래픽인증 안내페이지
	 * @param 
	 * @return /help/info
	 * @exception Exception
	 */
	@RequestMapping(value="/help/info.do")
	public String info() throws Exception {
		return "/help/info";
	}
	
	/**
	 * 로그인 페이지 이동
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/display.do")
	public String display() throws Exception {
		return "/auth/display";
	}
	
	/**
	 * 등록/수정/삭제 페이지 이동
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/setting.do")
	public String setting() throws Exception {
		return "/keySetting/setting";
	}
	
	/**
	 * 로그인 페이지 모바일
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/displayMobile.do")
	public String displayMobile() throws Exception {
		return "/auth/mdisplay";
	}
	
	/**
	 * 재발급 팝업
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/settingPopup.do")
	public String settingPopup() throws Exception {
		return "/keySetting/settingPopup";
	}
	
	/**
	 * 재발급 모바일
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/settingMobile.do")
	public String settingMobile() throws Exception {
		return "/keySetting/msetting";
	}
	
	/**
	 * 재발급 신모바일
	 * @param 
	 * @return 
	 * @exception Exception
	 */
	@RequestMapping(value="/settingNewMobile.do")
	public String settingNewMobile() throws Exception {
		return "/keySetting/settingNewMobile";
	}
}