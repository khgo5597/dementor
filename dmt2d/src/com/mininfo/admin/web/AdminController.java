package com.mininfo.admin.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mininfo.admin.model.AdminEntity;
import com.mininfo.admin.model.AppManagerEntity;
import com.mininfo.admin.model.accessLogEntity;
import com.mininfo.admin.model.categoryEntity;
import com.mininfo.admin.service.AdminService;
import com.mininfo.keysetting.model.CategoryEntity;
import com.mininfo.keysetting.service.KeySettingService;

/**  
 * @Class Name : AdminController.java
 * @Description : 관리자 처리  Controller Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

@Controller
public class AdminController {
	private static final Logger logger = Logger.getLogger(AdminController.class);
	//공통 서비스 선언
	@Resource(name="adminService")
	private AdminService adminService;
	@Resource(name="keySettingService")
	private KeySettingService keySettingService;
	/**
	 * 관리자 로그인 함수
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/adminIdCheck.do")
	public @ResponseBody String adminIdCheck(
			@RequestParam("userId") String userId,
			HttpServletRequest req,
			HttpServletResponse res
	) throws Exception{
		
		boolean result = false;
//		result = adminService.adminCheck(userId);
		result=true;
		String data =String.valueOf(result);
		return data;
	}
	
	/**
	 * 그래픽인증 관리자 로그인페이지 호출
	 * @param 
	 * @return /manager/loginAdmin
	 * @exception Exception
	 */
	@RequestMapping(value="/admin.do")
	public String admin() throws Exception {
		//jsp 페이지 리턴
		return "/manager/adminLogin";
	}
	
	/**
	 * 그래픽인증 관리자 로그인페이지 및 메인페이지로 이동
	 * @param 
	 * @return /manager/adminMain
	 * @exception Exception
	 */
	@RequestMapping(value="/adminMain.do", method= RequestMethod.POST )
	public String adminMain(@RequestParam("userId") String userId,
			Model model,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		AdminEntity adminEntity = new AdminEntity();
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");
		String time = format.format(calendar.getTime());
		adminEntity.setCreateDate(time);
		adminEntity.setAccess_date(time);
		//월별 조회를 위한 time값 셋팅
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy");
		String monthTime = monthFormat.format(calendar.getTime());
		//오늘(금일) 가입자 조회
		String joinTodayCount = Integer.toString(joinTodayCount(adminEntity));
		//오늘(금일) 로그인 횟수 조회
		String loginToday = Integer.toString(loginTodayCount(adminEntity));
		//오늘(금일) 로그인 실패 수 조회
		String loginFailTodayCount = Integer.toString(loginFailTodayCount(adminEntity));
		//총 가입자 수 조회
		String allUserListCount = Integer.toString(allUserListCount());
		//총 로그인 수 조회
		String allVistorCount = Integer.toString(allVistorCount());
		//총 탈퇴자 수 조회
		String allDropUserCount = Integer.toString(allDropUserCount());
		//총 접속 리스트 조회
		List<accessLogEntity> logList = new ArrayList<accessLogEntity>();
		logList = adminService.logList();
		
		//월별 접속자,가입자를 화면에 Graph 표기 하기 위해선 시간값과 접속자수가 필요하므로 셋팅한다. 
		Calendar year_calendar = Calendar.getInstance();
		SimpleDateFormat year_format = new SimpleDateFormat("yyyy");
		String year = year_format.format(year_calendar.getTime());
		AdminEntity monthlyCount = adminService.monthlyCount(year);
		for(int i = 0; i < 12; i++){
			calendar.set(Calendar.YEAR, Integer.parseInt(monthTime));
			calendar.set(Calendar.MONTH, i);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			
			monthlyCount.add(String.valueOf(calendar.getTimeInMillis()));
		}
		//월별 가입자 조회
		AdminEntity monthlyJoinCount = adminService.monthlyJoinCount(year);
		//월별 탈퇴자 조회
		//AdminEntity monthlyDropCount = adminService.monthlyDropCount(year);
		AdminEntity monthlyLoginFailCount = adminService.monthlyLoginFailCount(year);
		model.addAttribute("userId", userId);
		model.addAttribute("joinToday", joinTodayCount);
		model.addAttribute("loginToday", loginToday);
		model.addAttribute("loginFailTodayCount", loginFailTodayCount);
		model.addAttribute("userTotalCount", allUserListCount);
		model.addAttribute("allVistorCount", allVistorCount);
		model.addAttribute("allDropCount", allDropUserCount);
		model.addAttribute("monthlyCount", monthlyCount);
		model.addAttribute("monthlyJoinCount", monthlyJoinCount);
		model.addAttribute("monthlyLoginFailCount", monthlyLoginFailCount);
		//model.addAttribute("monthlyDropCount", monthlyDropCount);
		model.addAttribute("logList", logList);
		//jsp 페이지 리턴
		return "/manager/adminMain";
	}
	
	/**
	 * 오늘 가입자 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int joinTodayCount(AdminEntity adminEntity) throws Exception {
		int result = adminService.joinTodayCount(adminEntity);
		return result;
	}
	/**
	 * 오늘 로그인 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginTodayCount(AdminEntity adminEntity) throws Exception {
		int result = adminService.loginTodayCount(adminEntity);
		return result;
	}
	
	/**
	 * 오늘 로그인 실패 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginFailTodayCount(AdminEntity adminEntity) throws Exception{
		int result = adminService.loginFailTodayCount(adminEntity);
		return result;
	}
	/**
	 * 총 가입자 수 조회
	 * @return
	 * @throws Exception
	 */
	public int allUserListCount() throws Exception {
		int result = adminService.allUserListCount();
		return result;
	}
	/**
	 * 총 로그인 수 조회
	 * @return
	 * @throws Exception
	 */
	public int allVistorCount() throws Exception {
		int result = adminService.allVistorCount();
		return result;
	}
	/**
	 * 총 탈퇴자 수 조회
	 * @return
	 * @throws Exception
	 */
	public int allDropUserCount() throws Exception {
		int result = adminService.allDropUserCount();
		return result;
	}

	/**
	 * 사용자 조회 페이지 이동
	 * @param HttpServletRequest userId
	 * @param HttpServletRequest req
	 * @param HttpServletResponse res
	 * @return void 
	 * @exception Exception
	 */
	@RequestMapping(value="/userList.do", method= RequestMethod.POST)
	public String userList(
			Model model
			) throws Exception {
		//----- Sidebar Count 조회 Start -----			
		AdminEntity adminEntity = new AdminEntity();
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");
		String time = format.format(calendar.getTime());
		adminEntity.setCreateDate(time);
		adminEntity.setAccess_date(time);
		//월별 조회를 위한 time값 셋팅
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy");
		String monthTime = monthFormat.format(calendar.getTime());
		//오늘(금일) 가입자 조회
		String joinTodayCount = Integer.toString(joinTodayCount(adminEntity));
		//오늘(금일) 로그인 횟수 조회
		String loginToday = Integer.toString(loginTodayCount(adminEntity));
		//오늘(금일) 로그인 실패 수 조회
		String loginFailTodayCount = Integer.toString(loginFailTodayCount(adminEntity));
		//총 가입자 수 조회
		String allUserListCount = Integer.toString(allUserListCount());
		//총 로그인 수 조회
		String allVistorCount = Integer.toString(allVistorCount());
		//총 탈퇴자 수 조회
		String allDropUserCount = Integer.toString(allDropUserCount());
		//전체 사용자 수 조회
		String allUserList = Integer.toString(adminService.allUserListCount());
		//----- Sidebar Count 조회 END -----
		//사용자 리스트 조회
		List<AdminEntity> userList = new ArrayList<AdminEntity>();
		userList = adminService.userList();

		//model 추가
		model.addAttribute("joinToday", joinTodayCount);
		model.addAttribute("loginToday", loginToday);
		model.addAttribute("loginFailTodayCount", loginFailTodayCount);
		model.addAttribute("userTotalCount", allUserListCount);
		model.addAttribute("allVistorCount", allVistorCount);
		model.addAttribute("allDropCount", allDropUserCount);
		
		model.addAttribute("allUserList", allUserList);
		model.addAttribute("userList", userList);
		//페이지 리턴
		return "/manager/userList/userList";
	}
	/**
	 * 사용자 자세히보기 팝업 띄우기
	 * @param HttpServletRequest userId
	 * @param HttpServletRequest req
	 * @param HttpServletResponse res
	 * @return void 
	 * @exception Exception
	 */
	@RequestMapping(value="/userListPopup.do", method= RequestMethod.POST)
	public String userListPopup(@RequestParam("userId") String userId,
			@RequestParam("failcount") String failcount,
			@RequestParam("useyn") String useyn,
			Model model
			) throws Exception {
		//model 추가
		model.addAttribute("userId", userId);
		model.addAttribute("failcount", failcount);
		model.addAttribute("useyn", useyn);
		//페이지 리턴
		return "/manager/userList/userDetail";
	}
	/**
	 * 사용자 정보 수정
	 * @param userId
	 * @param failcount
	 * @param useyn
	 * @param model
	 * @param req
	 * @param res
	 * @throws Exception
	 */
	@RequestMapping(value="/manager/updateUser.do", method= RequestMethod.GET)
	public @ResponseBody void updateUser(
			@RequestParam("userId") String userId, 
			@RequestParam("failcount") String failcount,
			@RequestParam("useyn") String useyn,
			Model model,
			HttpServletRequest req,
			HttpServletResponse res
			) throws Exception {
		AdminEntity adminentity = new AdminEntity();
		adminentity.setUserId(userId);
		adminentity.setFailCount(failcount);
		adminentity.setUseyn(useyn);
		adminService.updateUser1(adminentity);
		adminService.updateUser2(adminentity);
		return;
	}
	/**
	 * 사용자 삭제(useyn -> N으로 변경)
	 * @param userId
	 * @param req
	 * @param res
	 * @throws Exception
	 */
	@RequestMapping(value="/manager/deleteUser.do", method= RequestMethod.GET)
	public void deleteUser(@RequestParam("userId") String userId,
			@RequestParam("status") String status,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		String useynStatus = status;
		AdminEntity adminentity = new AdminEntity();
		adminentity.setUseyn(useynStatus);
		
		logger.debug("userId = "+userId);
		String[] arrayString = userId.split(",");
		if(arrayString.length>=2){
			for(int i =0 ; i<arrayString.length; i++){
				adminentity.setUserId(arrayString[i]);
				adminService.updateUser3(adminentity);
				System.out.println("id = "+arrayString[i]);
			}
		}else{
			adminentity.setUserId(userId);
			adminService.updateUser3(adminentity);
		}
		return;
	}
	@RequestMapping(value="/appList.do", method= RequestMethod.POST)
	public String appList(
			Model model
			) throws Exception {
		//----- Sidebar Count 조회 Start -----			
		AdminEntity adminEntity = new AdminEntity();
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");
		String time = format.format(calendar.getTime());
		adminEntity.setCreateDate(time);
		adminEntity.setAccess_date(time);
		//월별 조회를 위한 time값 셋팅
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy");
		String monthTime = monthFormat.format(calendar.getTime());
		//오늘(금일) 가입자 조회
		String joinTodayCount = Integer.toString(joinTodayCount(adminEntity));
		//오늘(금일) 로그인 횟수 조회
		String loginToday = Integer.toString(loginTodayCount(adminEntity));
		//오늘(금일) 로그인 실패 수 조회
		String loginFailTodayCount = Integer.toString(loginFailTodayCount(adminEntity));
		//총 가입자 수 조회
		String allUserListCount = Integer.toString(allUserListCount());
		//총 로그인 수 조회
		String allVistorCount = Integer.toString(allVistorCount());
		//총 탈퇴자 수 조회
		String allDropUserCount = Integer.toString(allDropUserCount());
		//전체 사용자 수 조회
		String allUserList = Integer.toString(adminService.allUserListCount());
		//----- Sidebar Count 조회 END -----
		//어플 정보 조회
		List<AppManagerEntity> appList = new ArrayList<AppManagerEntity>();
		appList = adminService.appList();

		//model 추가
		model.addAttribute("joinToday", joinTodayCount);
		model.addAttribute("loginToday", loginToday);
		model.addAttribute("loginFailTodayCount", loginFailTodayCount);
		model.addAttribute("userTotalCount", allUserListCount);
		model.addAttribute("allVistorCount", allVistorCount);
		model.addAttribute("allDropCount", allDropUserCount);
		
		model.addAttribute("appList", appList);
		//페이지 리턴
		return "/manager/appList/appList";
	}
	/**
	 * 앱정보 수정 팝업 띄우기
	 * @param app_Id
	 * @param app_Name
	 * @param app_Desc
	 * @param app_Gubn
	 * @param model
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/appListPopup.do", method= RequestMethod.POST)
	public String appListPopup(@RequestParam("app_Id") String app_Id,
			@RequestParam("app_Name") String app_Name,
			@RequestParam("app_Desc") String app_Desc,
			@RequestParam("app_Gubn") String app_Gubn,
			Model model,
			HttpServletRequest req,
			HttpServletResponse res
			) throws Exception {
		
		//model 추가
		model.addAttribute("appId", app_Id);
		model.addAttribute("appName", app_Name);
		model.addAttribute("appDesc", app_Desc);
		model.addAttribute("appGubn", app_Gubn);
		//페이지 리턴
		return "/manager/appList/appDetail";
	}
	@RequestMapping(value="/manager/updateAppList.do", method= RequestMethod.GET)
	public @ResponseBody void updateAppList(@RequestParam("appId") String appId,
			@RequestParam("appName") String appName,
			@RequestParam("appDesc") String appDesc,
			@RequestParam("appGubn") String appGubn
			) throws Exception {
		System.out.println("appId = "+appId+" app_name = "+appName+" appDesc = "+appDesc+" appgubn = "+appGubn);
		AppManagerEntity appmanagerentity = new AppManagerEntity();
		appmanagerentity.setApp_id(appId);
		appmanagerentity.setApp_name(appName);
		appmanagerentity.setApp_desc(appDesc);
		appmanagerentity.setApp_gubn(appGubn);
		
		adminService.updateAppList(appmanagerentity);
	}
	@RequestMapping(value="/manager/deleteAppList.do", method= RequestMethod.GET)
	public @ResponseBody void deleteAppList(@RequestParam("appId") String appId) throws Exception{
		adminService.deleteAppList(appId);
	}
	/**
	 * 앱 자세히 보기 
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/appAddPopup.do", method= RequestMethod.POST)
	public String appAddPopup(HttpServletRequest req, HttpServletResponse res)throws Exception{
		return "/manager/appList/appAdd";
	}
	/**
	 * 앱을 추가한다.
	 * @param appId
	 * @param appName
	 * @param appDesc
	 * @param appGubn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/manager/addAppList.do", method= RequestMethod.GET)
	public @ResponseBody String addAppList(@RequestParam("appId") String appId,
			@RequestParam("appName") String appName,
			@RequestParam("appDesc") String appDesc,
			@RequestParam("appGubn") String appGubn
			) throws Exception{
		
		if(appId.isEmpty() || appId == null || appId.equalsIgnoreCase("null")){
			logger.error("::: addAppList APP_ID Format Error");
			String code = "1000";
			return code;
		}
		if(appGubn.isEmpty() || appId == null || appGubn.equalsIgnoreCase("null")){
			logger.error("::: addAppList APP_GUBN Format Error");
			String code = "2000";
			return code;
		}
		if(adminService.searchAppId(appId)){
			String code = "1001";
			return code;
		}
		AppManagerEntity appmanagerentity = new AppManagerEntity();
		appmanagerentity.setApp_id(appId);
		appmanagerentity.setApp_name(appName);
		appmanagerentity.setApp_desc(appDesc);
		appmanagerentity.setApp_gubn(appGubn);
		adminService.addAppList(appmanagerentity);
		return "true";
	}
	/**
	 * 카테고리 리스트를 화면에 뿌린다. 
	 * @param model
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/categoryList.do", method= RequestMethod.POST)
	public String categoryList(Model model,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception{
		//----- Sidebar Count 조회 Start -----			
		AdminEntity adminEntity = new AdminEntity();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");
		String time = format.format(calendar.getTime());
		adminEntity.setCreateDate(time);
		adminEntity.setAccess_date(time);
		// 월별 조회를 위한 time값 셋팅
		SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy");
		String monthTime = monthFormat.format(calendar.getTime());
		// 오늘(금일) 가입자 조회
		String joinTodayCount = Integer.toString(joinTodayCount(adminEntity));
		// 오늘(금일) 로그인 횟수 조회
		String loginToday = Integer.toString(loginTodayCount(adminEntity));
		// 오늘(금일) 로그인 실패 수 조회
		String loginFailTodayCount = Integer.toString(loginFailTodayCount(adminEntity));
		// 총 가입자 수 조회
		String allUserListCount = Integer.toString(allUserListCount());
		// 총 로그인 수 조회
		String allVistorCount = Integer.toString(allVistorCount());
		// 총 탈퇴자 수 조회
		String allDropUserCount = Integer.toString(allDropUserCount());
		// 전체 사용자 수 조회
		String allUserList = Integer.toString(adminService.allUserListCount());
		// ----- Sidebar Count 조회 END -----
		List<categoryEntity> cateList = adminService.cateList();
		//-- sidebar model --
		model.addAttribute("joinToday", joinTodayCount);
		model.addAttribute("loginToday", loginToday);
		model.addAttribute("loginFailTodayCount", loginFailTodayCount);
		model.addAttribute("userTotalCount", allUserListCount);
		model.addAttribute("allVistorCount", allVistorCount);
		model.addAttribute("allDropCount", allDropUserCount);
		//-- sidebar model --
		model.addAttribute("cateList", cateList);
		return "/manager/category/categoryList"; 
	}
	/**
	 * 카테고리에 해당하는 아이콘을 팝업으로 띄운다.
	 * @param category_id
	 * @param category_status
	 * @param model
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping(value="/cateListPopup.do", method= RequestMethod.POST)
	public String cateListPopup(
			@RequestParam("category_id") String category_id,
			@RequestParam("category_status") String category_status,
			Model model,
			HttpServletRequest req,
			HttpServletResponse res){
		model.addAttribute("category_id", category_id);
		model.addAttribute("category_status", category_status);
		return "/manager/category/categoryListDetail";
	}
	/**
	 * 카테고리 상태값 변경
	 * @param category_id
	 * @param category_status
	 * @param model
	 * @throws Exception
	 */
	
	@RequestMapping(value="/manager/changeCateStatus.do", method= RequestMethod.GET)
	public @ResponseBody void changeCateStatus(
			@RequestParam("category_id") String category_id,
			@RequestParam("category_status") String category_status,
			Model model
			)throws Exception{
		categoryEntity categoryentity = new categoryEntity();
		categoryentity.setCategory_id(category_id);
		//상태값을 변경하기 위해 기존 상태값을 받아서 비교 한다음 변경 값을 저장한다.
		if(category_status.equalsIgnoreCase("1")){
			category_status = "0";
		}else if(category_status.equalsIgnoreCase("0")){
			category_status = "1";
		}
		categoryentity.setCategory_status(category_status);
		logger.debug("category_status Change [category_status] = "+category_status+"");
		try{
			adminService.changeCateStatus(categoryentity);
		}catch(Exception e){
			
		}
	}
	
	
}