package com.mininfo.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**  
 * @Class Name : AdminEntity.java
 * @Description : 관리자페이지  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

public class AppManagerEntity implements Serializable {
	
	private static final long serialVersionUID = 3179338696799551906L;
	
	//어플 고유 아이디
	private String app_id = "";
	//어플 명
	private String app_name = "";
	//어플 설명
	private String app_desc = "";
	//어플 구분(N:native, H:hybrid)
	private String app_gubn = "";
	//생성일자
	private String reg_date ="";
	
	public String getApp_id() {
		return app_id;
	}
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}
	public String getApp_name() {
		return app_name;
	}
	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}
	public String getApp_desc() {
		return app_desc;
	}
	public void setApp_desc(String app_desc) {
		this.app_desc = app_desc;
	}
	public String getApp_gubn() {
		return app_gubn;
	}
	public void setApp_gubn(String app_gubn) {
		this.app_gubn = app_gubn;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	
}
