package com.mininfo.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**  
 * @Class Name : AdminEntity.java
 * @Description : 관리자페이지  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

public class AdminEntity implements Serializable {

	private static final long serialVersionUID = -1363967297514791836L;
	
	private String userId = "";
	private String failCount = "";
	private String level = "";
	private String createDate = "";
	private String updateDate = "";
	private String accessResult = "";
	private String useyn = "";
	private String access_date = "";
	
	//월별 카운트
	private List<String> month = new ArrayList<String>();
	
	private String jan = "";
	private String feb = "";
	private String mar = "";
	private String apr = "";
	private String may = "";
	private String jun = "";
	private String jul = "";
	private String aug = "";
	private String sep = "";
	private String oct = "";
	private String nov = "";
	private String dec = "";
	private int totalcount = 0;
	
	public Iterator<String> iterator() {
		return month.iterator();
	}
	
	public List<String> getMonth() {
		return month;
	}
	
	public String get(int i){
		return month.get(i);
	}
	
	public void add(String item) {
		month.add(item);
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFailCount() {
		return failCount;
	}
	public void setFailCount(String failCount) {
		this.failCount = failCount;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getAccessResult() {
		return accessResult;
	}
	public void setAccessResult(String accessResult) {
		this.accessResult = accessResult;
	}

	public String getUseyn() {
		return useyn;
	}

	public void setUseyn(String useyn) {
		this.useyn = useyn;
	}
	
	public String getAccess_date() {
		return access_date;
	}

	public void setAccess_date(String access_date) {
		this.access_date = access_date;
	}

	public String getJan() {
		return jan;
	}
	public void setJan(String jan) {
		this.jan = jan;
	}
	public String getFeb() {
		return feb;
	}
	public void setFeb(String feb) {
		this.feb = feb;
	}
	public String getMar() {
		return mar;
	}
	public void setMar(String mar) {
		this.mar = mar;
	}
	public String getApr() {
		return apr;
	}
	public void setApr(String apr) {
		this.apr = apr;
	}
	public String getMay() {
		return may;
	}
	public void setMay(String may) {
		this.may = may;
	}
	public String getJun() {
		return jun;
	}
	public void setJun(String jun) {
		this.jun = jun;
	}
	public String getJul() {
		return jul;
	}
	public void setJul(String jul) {
		this.jul = jul;
	}
	public String getAug() {
		return aug;
	}
	public void setAug(String aug) {
		this.aug = aug;
	}
	public String getSep() {
		return sep;
	}
	public void setSep(String sep) {
		this.sep = sep;
	}
	public String getOct() {
		return oct;
	}
	public void setOct(String oct) {
		this.oct = oct;
	}
	public String getNov() {
		return nov;
	}
	public void setNov(String nov) {
		this.nov = nov;
	}
	public String getDec() {
		return dec;
	}
	public void setDec(String dec) {
		this.dec = dec;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	
}
