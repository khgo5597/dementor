package com.mininfo.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**  
 * @Class Name : AdminEntity.java
 * @Description : 관리자페이지  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

public class categoryEntity implements Serializable {

	private static final long serialVersionUID = -5897183268846748506L;
	
	private String category_id = "";
	private String category_name = "";
	private String category_url = "";
	private String category_filename ="";
	private String category_status = "";
	private String icon_zip = "";
	private String reg_date = "";
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getCategory_url() {
		return category_url;
	}
	public void setCategory_url(String category_url) {
		this.category_url = category_url;
	}
	public String getCategory_filename() {
		return category_filename;
	}
	public void setCategory_filename(String category_filename) {
		this.category_filename = category_filename;
	}
	public String getCategory_status() {
		return category_status;
	}
	public void setCategory_status(String category_status) {
		this.category_status = category_status;
	}
	public String getIcon_zip() {
		return icon_zip;
	}
	public void setIcon_zip(String icon_zip) {
		this.icon_zip = icon_zip;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
}
