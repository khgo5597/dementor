package com.mininfo.admin.model;

import java.io.Serializable;

/**  
 * @Class Name : AdminEntity.java
 * @Description : 관리자페이지  Entity Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

public class accessLogEntity implements Serializable {
	private static final long serialVersionUID = -7879352202077548175L;
	//사용자 아이디
	String user_id;
	//접속일
	String access_date;
	//접속 결과
	String access_result;
	//접속 타입
	String access_type;
	//앱 아이디
	String app_id;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAccess_date() {
		return access_date;
	}
	public void setAccess_date(String access_date) {
		this.access_date = access_date;
	}
	public String getAccess_result() {
		return access_result;
	}
	public void setAccess_result(String access_result) {
		this.access_result = access_result;
	}
	public String getAccess_type() {
		return access_type;
	}
	public void setAccess_type(String access_type) {
		this.access_type = access_type;
	}
	public String getApp_id() {
		return app_id;
	}
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}
	
}
