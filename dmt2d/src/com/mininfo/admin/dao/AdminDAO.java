package com.mininfo.admin.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

import com.mininfo.admin.model.AdminEntity;
import com.mininfo.admin.model.AppManagerEntity;
import com.mininfo.admin.model.accessLogEntity;
import com.mininfo.admin.model.categoryEntity;
import com.mininfo.keysetting.model.CategoryEntity;

/**  
 * @Class Name : AdminDAO.java
 * @Description : 관리자 페이지  DAO Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2015-06-16     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2015-06-16
 * @version 1.0
 */

@Repository("adminDAO")
public class AdminDAO {
	
	//SqlMapClientTemplate 선언
	@Autowired
	private SqlMapClientTemplate sqlMap;
	
	//SqlMapClientTemplate 생성
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMap = sqlMapClientTemplate;
	}

	/**
	 * 관리자 아이디 확인
	 */
	public int adminCheck(String userId) throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("adminCheck", userId).toString());
	}
	
	/**
	 * 총 사용자 조회
	 * @param 
	 * @return int
	 * @exception Exception
	 */
	public int allUserList() throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("allUserCheck").toString());
	}
	
	/**
	 * 오늘(금일) 가입자 수 조회
	 * @return int
	 * @throws Exception
	 */
	public int joinTodayCount(AdminEntity adminEntity) throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("joinTodayCount", adminEntity).toString());
	}
	
	/**
	 * 오늘(금일) 로그인 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginTodayCount(AdminEntity adminEntity) throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("loginTodayCount", adminEntity).toString());
	}
	
	/**
	 * 오늘(금일) 로그인 실패 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginFailTodayCount(AdminEntity adminEntity) throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("loginFailTodayCount", adminEntity).toString());
	}
	
	/**
	 * 총 로그인 시도자 조회
	 * @return count
	 * @throws Exception
	 */
	public int allVistorCount() throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("allVistorCount").toString());
	}
	
	/**
	 * 총 탈퇴자 조회
	 * @return count
	 * @throws Exception
	 */
	public int allDropUser() throws Exception {
		return Integer.parseInt(sqlMap.queryForObject("allDropUser").toString());
	}
	
	/**
	 * 월별 접속자(로그인 시도) 조회
	 * @return List<AdminEntity> - 월별 접속자 정보
	 * @throws Exception
	 */
	public AdminEntity monthlyCount(String year) throws Exception {
		return (AdminEntity)sqlMap.queryForObject("monthlyCount", year);
	}
	
	/**
	 * 월별 가입자 조회 
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public AdminEntity monthlyJoinCount(String year) throws Exception {
		return (AdminEntity)sqlMap.queryForObject("monthlyJoinCount", year);
	}
	/**
	 * 월별 탈퇴자 조회
	 * @param year
	 * @return
	 * @throws Exception
	 *//*
	public AdminEntity monthlyDropCount(String year) throws Exception {
		return (AdminEntity)sqlMap.queryForObject("monthlyDropCount", year);
	}*/
	/**
	 * 월별 로그인 실패 횟수 조회
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public AdminEntity monthlyLoginFailCount(String year) throws Exception {
		return (AdminEntity)sqlMap.queryForObject("monthlyLoginFailCount", year);
	}
	/**
	 * 사용자 리스트 조회
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<AdminEntity> userList() throws Exception {
		return sqlMap.queryForList("userList");
	}
	/**
	 * 앱 정보 리스트 조회
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<AppManagerEntity> appList() throws Exception {
		return sqlMap.queryForList("appList");
	}
	
	public void updateUser1(AdminEntity adminentity) throws Exception {
		sqlMap.update("updateUser1", adminentity);
		
	}
	
	public void updateUser2(AdminEntity adminentity) throws Exception {
		sqlMap.update("updateUser2", adminentity);
	}
	
	public void updateUser3(AdminEntity adminentity) throws Exception {
		sqlMap.update("updateUser3", adminentity);
	}
	//앱 정보 수정
	public void updateAppList(AppManagerEntity appmanagerentity) throws Exception {
		sqlMap.update("updateAppList", appmanagerentity);
	}
	//앱 삭제
	public void deleteAppList(String appId) throws Exception {
		sqlMap.delete("deleteAppList", appId);
	}
	//앱 목록 조회
	public void addAppList(AppManagerEntity appmanagerentity) throws Exception {
		sqlMap.insert("addAppList", appmanagerentity);
	}
	//앱 검색
	public String searchAppId(String appId) throws Exception {
		String result = (String) sqlMap.queryForObject("searchAppId", appId);
		return result;
	}
	//로그 목록 조회
	@SuppressWarnings("unchecked")
	public List<accessLogEntity> logList() throws Exception {
		return sqlMap.queryForList("logList");
	}
	
	/**
	 * 카테고리 조회
	 * @param 
	 * @return List<CategoryEntity> 카테고리 리스트 정보
	 * @exception Exception
	 */
	@SuppressWarnings("unchecked")
	public List<categoryEntity> selectCate() throws Exception {
		return sqlMap.queryForList("cateList");
	}
	
	public void changeCateStatus(categoryEntity categoryentity) throws Exception {
		sqlMap.update("changeCateStatus", categoryentity);
	}
}
