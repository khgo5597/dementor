package com.mininfo.admin.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mininfo.admin.dao.AdminDAO;
import com.mininfo.admin.model.AdminEntity;
import com.mininfo.admin.model.AppManagerEntity;
import com.mininfo.admin.model.accessLogEntity;
import com.mininfo.admin.model.categoryEntity;
import com.mininfo.keysetting.model.CategoryEntity;

/**  
 * @Class Name : CommonService.java
 * @Description : 공통 처리  Service Class
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */

@Service("adminService")
public class AdminService {
	
	//공통 DAO 선언
	@Resource(name="adminDAO")
	private AdminDAO adminDAO;
	
	/**
	 * 관리자 확인
	 */
	public boolean adminCheck(String userId) throws Exception {
		boolean result = false;
		int ref = adminDAO.adminCheck(userId);
		if ( ref != 0 ) {
			result = true;
		}else {
			result = false;
		}
		return result;
	}
	/**
	 * 전체 사용자 조회
	 * @param 
	 * @return int - 총 사용자 수
	 * @exception Exception
	 */
	public int allUserListCount() throws Exception {
		
		int count = 0;
		
		count = adminDAO.allUserList();
		
		return count;
	}
	/**
	 * 오늘(금일)가입자수 조회
	 * @param 
	 * @return int - 오늘(금일) 가입자수
	 * @exception Exception
	 */
	public int joinTodayCount(AdminEntity adminEntity) throws Exception {

		int count = 0;
		
		count = adminDAO.joinTodayCount(adminEntity);
		
		return count;
	}
	/**
	 * 오늘(금일) 로그인 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginTodayCount(AdminEntity adminEntity) throws Exception {
		
		int count = 0;
		
		count = adminDAO.loginTodayCount(adminEntity);
		
		return count;
	}
	
	/**
	 * 오늘(금일) 로그인실패 수 조회
	 * @param adminEntity
	 * @return
	 * @throws Exception
	 */
	public int loginFailTodayCount(AdminEntity adminEntity) throws Exception {
		int count = 0;
		
		count = adminDAO.loginFailTodayCount(adminEntity);
		
		return count;
	}
	
	/**
	 * 총 로그인 시도자 조회
	 * @return int count
	 * @throws Exception
	 */
	public int allVistorCount() throws Exception {
		int count = 0;
		
		count = adminDAO.allVistorCount();
		
		return count;
	}
	
	/**
	 * 총 탈퇴자 조회
	 * @return int count
	 * @throws Exception
	 */
	public int allDropUserCount() throws Exception {
		int count = 0;
		
		count = adminDAO.allDropUser();
		
		return count;
		
	}
	
	/**
	 * 월별 접속자(로그인 시도) 조회
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public AdminEntity monthlyCount(String year) throws Exception {
		return adminDAO.monthlyCount(year);
	}
	
	/**
	 * 월별 가입자 조회
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public AdminEntity monthlyJoinCount(String year) throws Exception {
		return adminDAO.monthlyJoinCount(year);
	}
	
	/**
	 * 월별 탈퇴자 조회
	 * @param year
	 * @return
	 * @throws Exception
	 *//*
	public AdminEntity monthlyDropCount(String year) throws Exception {
		return adminDAO.monthlyDropCount(year);
	}*/
	/**
	 * 월별 로그인 실패 횟수 조회
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public AdminEntity monthlyLoginFailCount(String year) throws Exception {
		return adminDAO.monthlyLoginFailCount(year);
	}
	
	/**
	 * 사용자 리스트 조회
	 * @return List<AdminEntity>
	 * @throws Exception
	 */
	public List<AdminEntity> userList() throws Exception {
		return adminDAO.userList();
	}
	public List<AppManagerEntity> appList() throws Exception {
		return adminDAO.appList();
	}
	public void updateUser1(AdminEntity adminentity) throws Exception {
		adminDAO.updateUser1(adminentity);
	}
	public void updateUser2(AdminEntity adminentity) throws Exception {
		adminDAO.updateUser2(adminentity);
	} 
	public void updateUser3(AdminEntity adminentity) throws Exception {
		adminDAO.updateUser3(adminentity);
	}
	
	public void updateAppList(AppManagerEntity appmanagerentity) throws Exception {
		adminDAO.updateAppList(appmanagerentity);
	}
	public void deleteAppList(String appId) throws Exception {
		adminDAO.deleteAppList(appId);
	}
	
	public void addAppList(AppManagerEntity appmanagerentity) throws Exception {
		adminDAO.addAppList(appmanagerentity);
	}
	
	public boolean searchAppId(String appId) throws Exception {
		String result = adminDAO.searchAppId(appId);
		if(result != null && !result.equals("")){
			return true;
		}
		System.out.println("result = "+result);
		return false;
	}
	
	public List<accessLogEntity> logList() throws Exception{
		return adminDAO.logList();
	}
	/**
	 * 카테고리 조회
	 * @param 
	 * @return List<CategoryEntity> 카테고리 리스트 정보
	 * @exception Exception
	 */
	public List<categoryEntity> cateList() throws Exception {
		return adminDAO.selectCate();
	}
	
	public void changeCateStatus(categoryEntity categoryentity) throws Exception {
		adminDAO.changeCateStatus(categoryentity);
	}
}
