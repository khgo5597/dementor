<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
/**  
 * @Description : 그래픽인증 로그인 페이지
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<title>그래픽인증 - 동국대</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<%@ include file="../common/header.jsp"%>

<script src="/dmt2d/js/display.js" 	type="text/javascript"></script>
<script type="text/javascript">
	var type = "display";

	$(document).ready(function(){
		reqauthimgkey();
	});	
</script>
</head>
<body style="margin:0 0 0 0;">
	<div id="header" style="position:relative;">
		<div id="main" style="position:relative; width:100%; margin: 0 0 0 10px;">
			<!--
			############################################################################################# 
			그래픽인증 레이어 시작
			#############################################################################################
			-->
			<div id="dementor" style="padding-top:30px"></div>
			<!--
			############################################################################################# 
			그래픽인증 레이어 끝
			#############################################################################################
			-->		
		</div>
	</div>
</body>
</html>