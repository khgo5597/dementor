<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
/**  
 * @Description : 그래픽인증 로그인 페이지
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */
%>
<!DOCTYPE>
<html>
<head>
<title>그래픽인증 - 동국대학교</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<%@ include file="../common/mheader.jsp"%>

<script src="/dmt2d/js/display.js" 	type="text/javascript"></script>
<script type="text/javascript">
	var type = "display";
	
	$(document).ready(function(){
		reqauthimgkey();
	});
</script>
</head>
<body style="margin:0px;">
	<!--
	############################################################################################# 
	그래픽인증 레이어 시작
	#############################################################################################
	-->
	<div id="dementor" style="position:relative;"></div>
	<!--
	############################################################################################# 
	그래픽인증 레이어 끝
	#############################################################################################
	-->		
</body>
</html>