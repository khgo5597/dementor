<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="/dmt2d/js/jquery-1.5.min.js" 	type="text/javascript"></script>
<script src="/dmt2d/js/json2.js" 			type="text/javascript"></script>
<script src="/dmt2d/js/shaflash.js" 			type="text/javascript"></script> 
<script src="/dmt2d/js/result.js" 			type="text/javascript"></script> 

<%
	String userid = request.getParameter("userid");
	if(userid=="" || userid == null) {
		userid=(String)session.getAttribute("userId");
	}
	   
	String requestType = "";
	String domain = "gw.dongguk.edu";
	if(request.getParameter("requestType") != null) {
		requestType = request.getParameter("requestType");
		
		if(request.getAttribute("domain") != null) {
			domain = (String)request.getAttribute("domain");
		}
	}
%>

<script type="text/javascript">
	var userid      = "<%=userid%>";
	var url_op      = "<%=domain%>";
	var requestType = "<%=requestType%>";	
	
	if(requestType.indexOf('portal') <= -1) {
	    document.domain = "dongguk.edu";        
	}
</script>