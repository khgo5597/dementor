<!-- keySearch.jsp -->
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%-- 
<%@page import="com.mininfo.user.model.UserHintEntity"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.client.dao.MysqlclientDAO"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<%@ page import= "com.mininfo.common.util.CommonUtil"%>
 --%>

<%
	
// 	DementorProperty DP = null;
// 	String sendType = DP.getInstance().getAuthSendType();
	
// 	UserHintEntity userEntity = (UserHintEntity)request.getAttribute("USER");
	
// 	String userId     = CommonUtil.nullToStr(request.getParameter("userId"));
// 	String SENDTYPE   = CommonUtil.nullToStr(request.getParameter("SENDTYPE"));
// 	String processCmd = CommonUtil.nullToStr(request.getParameter("processCmd"));
// 	String status     = CommonUtil.nullToStr(request.getParameter("status")); 
	
	//3항연산자 처리. request값이 없는경우 userEntity에서 값을 받아옴
// 	userId    = (userId    == "") ? userEntity.getUser_id() : userId;
	

	// Message 설정
// 	String message = "";
// 	int resultCode = 0;
// 	int processCode = 0;
	
// 	if(request.getAttribute("RESULT") != null)
// 	{
// 		ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
// 		message = resultEntity.getResultMessage();
// 		resultCode = resultEntity.getResultCode();
// 		processCode = resultEntity.getProcessCode();
// 	}

// 	String strServerIP = request.getServerName();//서버 ip
// 	String strServerPort = Integer.toString(request.getServerPort());// 서버 port
// 	String strServerScheme = request.getScheme();
// 	String serverRootUrl = strServerScheme+"://"+ strServerIP +":"+ strServerPort +"/";  // Root 경로
	
// 	MysqlclientDAO client = new MysqlclientDAO();
// 	int cnt = client.dmtloginCnt(userEntity.getUser_id());
// 	int MaxCount = DP.getInstance().getAccessCount();	
	
// 	String userId     = (String)request.getParameter("userId");
// 	String SENDTYPE   = request.getParameter("SENDTYPE");
// 	String processCmd = request.getParameter("processCmd");
// 	String status     = (String)request.getParameter("status");


	String userId     = (String)request.getAttribute("userId");
// 	String status     = (String)request.getAttribute("status"); 
	String passHintSelectCode     = (String)request.getAttribute("passHintSelectCode"); 
	

// 	System.out.println(">> search_pw >>>>> userId     : "+userId);
// 	System.out.println(">> search_pw >>>>> passHintSelectCode 	  : "+passHintSelectCode);
	
	
	String message = "";
	int resultCode = 0;
	int processCode = 0;


	String strServerIP = request.getServerName();//서버 ip
	String strServerPort = Integer.toString(request.getServerPort());// 서버 port
	String strServerScheme = request.getScheme();
	String serverRootUrl = strServerScheme+"://"+ strServerIP +":"+ strServerPort +"/";  // Root 경로


%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>그래픽인증 - 동국대</title>
<link rel="stylesheet" type="text/css" href="/dmt2d/css/style.css" />
<script type="text/javascript" src="/dmt2d/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/dmt2d/js/common.js"></script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
img{display:block;}
table,td{
	margin:0px;
	padding:0px;
	border-collapse:collapse;
}
</style>

<script>
	/* 취소 버튼 */
	function goMain(){
		history.go(-1);
	}

</script>
<SCRIPT LANGUAGE="JavaScript">
	/* 유호성 검사 */
	function check(){
		var form = document.keySearch;
		
		// 답변
		var passAnswer = form.passAnswer;
		if(passAnswer.value == ""){
			alert("답변을 입력해 주십시요.");
			passAnswer.focus();
			return;
		}
		
		// 메일
		var userEmail = form.userEmail;
		if(userEmail.value == ""){
			alert("이메일을 입력하세요.");
			userEmail.focus();
			return;
		}else{
			var mail_exp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;
			if(!mail_exp.test(form.userEmail.value)){
				alert("잘못된 이메일 형식입니다.");
				userEmail.focus();
				return;
			}
		}
		
		//입력된 정보를 기존정보와 비교한다 확인하기 : check -> answerCheck -> sendMail -> authCodeForm
		answerCheck();
	}
	
	// 저장
	function save(){
		// 유효성 검사
		check();
	}
	
	// 입력된 정보를 기존 정보와 비교하기
	function answerCheck(){
		// 입력값 확인
		var form = document.keySearch;
		var userId, passHintSelectCode, passAnswer, userEmail;
		userId = form.userId.value;
		passHintSelectCode = form.passHintSelectCode.value;
		passAnswer = form.passAnswer.value;
		userEmail = form.userEmail.value;
		
		var jsonParam = {
			data : {
				userId : userId,
				passHintSelectCode : passHintSelectCode,
				passAnswer : passAnswer,
				userEmail : userEmail,
				devtype : "0"
			}
		};
		
		$.ajax({
			type : "post",
			url : "/dmt2d/answerCheck.do?data="+JSON.stringify(jsonParam),
			contentType : "text/html;charset=UTF-8",
			dataType : "json",
			success : function(data) {
				var result = eval(data);
				if(result.code == "102250") {	// 입력된 정보가 잘못된 경우
					alert('입력 정보가 일치하지 않습니다.\n' + '확인 후 다시 시도 해주십시오.');
					return;
				} else { // 정보가 일치
					// 자바 메일보내기
					sendMail();
				}
			},
			error : function(data) {
				alert("오류가 발생하였습니다.");
				return;
			}
		});
	}
	
	// 자바 메일 보내기
	function sendMail(){
		// 메일 보내기
		var form = document.keySearch;
		var userId, userEmail;
		userId = form.userId.value;
		email = form.userEmail.value;
		
		$.ajax({
			type : "post",
			url : "/dmt2d/sendMail.do",
			data : {
				userId : userId,
				userEmail : email
			},
			success : function(code) {
				if(code == "S") {
					alert("[ " + email + " ]로 인증코드가 발송되었습니다."); 
					// 인증 코드 입력 페이지 가기
					authCodeForm();
				} else { 
					alert("오류가 발생하였습니다.\ncode : " + code);
					return;
				}
			},
			error : function(data) {
				alert("오류가 발생하였습니다.");
				return;
			}
		});
	}
	
	// 인증 코드 입력 페이지 가기
	function authCodeForm(){
		document.location.href = "/dmt2d/authCodeForm.do";
	}
	
	function moveView(intWidth,intHeight){
		var bScrollbars;
        var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;

		if(intWidth >= screenWidth){ 
                intWidth = screenWidth - 20;
                bScrollbars = 1;
        }
        if(intHeight >= screenHeight){ 
                intHeight = screenHeight - 40;
                //intWidth = intWidth + 20;
                bScrollbars = 1;
        }

		var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;
		var intLeft = (screenWidth - intWidth) / 2;
		var intTop = (screenHeight - intHeight) / 2;

		if (navigator.appName.charAt(0) == "N") { 
				if (navigator.appVersion.charAt(0) == 2) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 3 ){ 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 4) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 5) { 
					intHeight += 6;
					intWidth -= 1;
				} 
		} 
		if (navigator.appName.charAt(0) == "M") {
				if (navigator.appVersion.charAt(0) == "2") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "3") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "4") {
						if(navigator.appVersion.indexOf("MSIE 5") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 6") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 7") != -1) {
						}
						if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
							intHeight -= 45;
						}
						if (navigator.appVersion.charAt(0) == "5") {
							intHeight -= 45;
						}
				}
		}

		window.moveTo(intLeft,intTop); 

		resizeTo(intWidth,intHeight);
	}

	function goSendPage() {
		var F = document.pwfaq;
		var firstStr = "<%-- <%=userEntity.getUser_email()%> --%>".split('@')[0].substr(0,1);
		var asta = "";
		//인증코드 생성
		var rUrl="/dmt2d/client/Password.do?cmd=getAuthCode&userId=<%=userId%>";
		
		if(F.authCode.value.length == 0){
			makeRequest(rUrl);
		}
		
		for(var i=0; i < "<%-- <%=userEntity.getUser_email()%> --%>".split('@')[0].length;i++){
			asta = asta+"*";
		}
		
		firstStr = firstStr+asta;
		alert(firstStr+"@"+"<%-- <%=userEntity.getUser_email()%> --%>".split('@')[1]+" 으로 인증키를 발송하였습니다.");
		F.action = "/dmt2d/jsp/grac/sendMailPage.jsp";
		F.submit();
	}
	
	function goSend(strResult) {
		var F = document.pwfaq;
		var firstStr = "<%-- <%=userEntity.getUser_email()%> --%>".split('@')[0].substr(0,1);
		var asta = "";
		//인증코드 생성
		var rUrl="/dmt2d/client/Password.do?cmd=getAuthCode&userId=<%-- <%=userId%> --%>";
		
		if(F.authCode.value.length == 0){
			makeRequest(rUrl);
		}
		
		for(var i=0; i < "<%-- <%=userEntity.getUser_email()%> --%>".split('@')[0].length;i++){
			asta = asta+"*";
		}
		
		firstStr = firstStr+asta;
		
		alert(firstStr+"@"+"<%-- <%=userEntity.getUser_email()%> --%>".split('@')[1]+" 으로 인증키를 발송하였습니다.");
		F.action = "Password.do";
		F.cmdFlag.value="scHint";
		F.cmd.value = "sendMail";
		F.submit();
		
	}
	
	function makeRequest(url) {
	    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	      httpRequest = new XMLHttpRequest();
	    } else if (window.ActiveXObject) { // IE
	      try {
	        httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
	      } 
	      catch (e) {
	        try {
	          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	        } 
	        catch (e) {}
	      }
	    }

	    if (!httpRequest) {
	      alert('Giving up :( Cannot create an XMLHTTP instance');
	      return false;
	    }
	    
	    httpRequest.open('GET', url, false);
	    httpRequest.onreadystatechange = alertContents;
	    httpRequest.send(null);
	  }

	/*
	 * 
	 responseText 로 받아온 텍스트 데이터에 개행 문자가 포함 되어 있어 개행 문자 처리(trim())
	 
	 */
	function alertContents() {
		var form = document.pwfaq;
		
		if (httpRequest.readyState == 4) {
	        if (httpRequest.status == 200) {
	        	//서버로부터 응답이 도착
	        	var rs = httpRequest.responseText.replace(/\r\n/g,"").trim();
	        	
	        	form.authCode.value = rs;
	        }
	    }
	}

	//개행 문자 처리
	String .prototype.trim=function(){
	    return this.replace(/^[\s\xA0]+/,"").replace(/[\s\xA0]+$/,"");
	}
	
</SCRIPT>
</head>

<!-- onload 이벤트  
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" overflow:hidden;" scroll="no" onload="javascript:window.resizeTo('470','420');">
 -->
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" overflow:hidden;" scroll="no">

<form commandName="EmailEntity" name="keySearch" method="post" >
	<input type="hidden" name="userId" value="<%= userId %>"> 
<%-- 	<input type="hidden" name="status" value="<%= status %>">  --%>

	<table width="450" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="margin:auto;margin-top: 100px;">
		<!-- 인증 키 찾기  UI -->
		<tr>
			<td height="40"><img src="/dmt2d/images/new/title_01.jpg" width="300" height="40" /></td>
		</tr>
		<tr>
			<td height="75"><img src="/dmt2d/images/new/comm_01.jpg" width="450" height="75" /></td>
		</tr>
		<tr>
			<td height="20" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td height="112" align="center">
			<!--내용테이블시작 -->
			<table width="430" border="0" cellspacing="0" cellpadding="0" margin="0">
				<tr>
					<td width="10"><img src="/dmt2d/images/new/border_01.gif"  width="10" height="10" /></td>
					<td width="80"><img src="/dmt2d/images/new/border_02.gif"  width="80" height="10" /></td>
					<td width="10"><img src="/dmt2d/images/new/border_02.gif"  width="10" height="10" /></td>
					<td width="320"><img src="/dmt2d/images/new/border_09.gif"  width="320" height="10" /></td>
					<td width="10"><img src="/dmt2d/images/new/border_10.gif" width="10" height="10"/></td>
				</tr>
				<tr>
					<td width="10" background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td width="80" height="30" align="center" bgcolor="#DBE9F1"><span class="style1">질문</span></td>
					<td width="10" align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td width="320" align="left" bgcolor="#FFFFFF" style="padding-left:10px;">	
						<input type="text" id="passHintSelectCode" name="passHintSelectCode"  
							 style=" width:300px;" value="<%= passHintSelectCode %>" readonly />
					</td>
					<td width="10" background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td height="1" bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
				</tr>
				<tr>
					<td background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td height="30" align="center" bgcolor="#DBE9F1" class="style1">답변</td>
					<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
						<input type="text" name="passAnswer" id="passAnswer"  style=" width:300px;" size="50" maxlength="50"/>
					</td>
					<td background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td height="1" bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
				</tr>
				<tr>
					<td background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td height="30" align="center" bgcolor="#DBE9F1" class="style1">e-mail</td>
					<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
						<input type="text" id="userEmail" name="userEmail" style=" width:300px;" size="50" maxlength="50" />
					</td>
					<td background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td width="10"><img src="/dmt2d/images/new/border_04.gif" width="10" height="10"/></td>
					<td width="80"><img src="/dmt2d/images/new/border_05.gif" width="80" height="10"/></td>
					<td width="10"><img src="/dmt2d/images/new/border_05.gif" width="10" height="10"/></td>
					<td width="320"><img src="/dmt2d/images/new/border_08.gif" width="320" height="10"/></td>
					<td width="10"><img src="/dmt2d/images/new/border_06.gif" width="10" height="10"/></td>
				</tr>
			</table>
			<!--내용테이블 끝 --></td>
		</tr>
		<!-- 에러 메시지 : 추후 처리 -->
<!-- 		<tr> -->
<%-- 			<td height="20" align="center">&nbsp; <% if(message != null && !"".equals(message)) {%> --%>
<%-- 			<font color="red"><%=message%></font> <%}%> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		
		<tr>
			<td height="20" align="center">&nbsp;</td>
		</tr>
		
		<!--확인취소버튼 -->
		<tr>
			<td height="40">
			<table width="100%">
				<tr>
					<td align="right" width="50%">
						<a href="#"><img src="/dmt2d/images/new/btn_confirm.jpg" width="97" height="27" onClick="javascript:save()" border="0" /></a>
					</td>
					<td align="left" width="50%">
						<a href="#"><img src="/dmt2d/images/new/btn_cancel.jpg" width="97" height="27"	onClick="javascript:goMain()" border="0" /></a>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td align="left">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>	
	</table>
</form>
</body>
</html>