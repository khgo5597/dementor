<!-- hintUpdate.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%
	// session에서 유저 아이디 가져오기
	String userId = (String)session.getAttribute("userId");
// 	System.out.println("userId:"+userId);
%> 

<html>
<head>
<title>그래픽인증 - 동국대</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
img{display:block;}
table,td{
margin:0px;
padding:0px;
border-collapse:collapse;
}
-->
</style>
<link rel="stylesheet" href="/dmt2d/css/style.css" type="text/css">
<script type="text/javascript" src="/dmt2d/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/dmt2d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
	
	function check(){
		// 확인해야됨
		var passHintSelectCode = document.setting.passHintSelectCode;
		if(passHintSelectCode.style.display == ''){
			if(!passHintSelectCode.value){
				alert("원하는 질문을 선택하세요.");
				passHintSelectCode.focus();
				return;
			}
		}else{
			alert('??????');
			var frs = document.setting.textfield2;
			if(frs.value == ""){
				alert("질문을 입력하세요.");
				frs.focus(); 
				return;
			}
		}
		
		var passAnswer = document.setting.passAnswer;
		if(passAnswer.value == ""){
			alert("답변을 입력해 주십시요.");
			passAnswer.focus();
			return;
		}
		
		var userEmail = document.setting.userEmail;
		if(userEmail.value == ""){
			alert("이메일을 입력하세요.");
			userEmail.focus();
			return;
		}else{
			var mail_exp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;
			if(!mail_exp.test(setting.userEmail.value)){
				alert("잘못된 이메일 형식입니다.");
				userEmail.focus();
				return;
			}
		}
	}
	
	// 저장
	function save(){
		
		// 유효성 체크
		check();
		
		// 저장
		var form = document.setting;
		var userId, passHintSelectCode, passAnswer, userEmail;
		userId = form.userId.value;
		passHintSelectCode = form.passHintSelectCode.value;
		passAnswer = form.passAnswer.value;
		userEmail = form.userEmail.value;
		
		$.ajax({
			type : "post",
			url : "/dmt2d/hintUpdate.do",
			data : {
				userId : userId,
				passHintSelectCode : passHintSelectCode,
				passAnswer : passAnswer,
				userEmail : userEmail
			},
			success : function(code) {
				if (code == "S") {
					alert("입력된 정보가 정상적으로 저장되었습니다.\n" + "인증 키 찾기 페이지로 이동합니다.");
					
					// 인증 키 찾기 페이지로 이동
					keySearch();
					
				} else {
					alert("오류가 발생하였습니다.\ncode : " + code);
					return;
				}
			},
			error : function(data) {
				alert("오류가 발생하였습니다.");
				return;
			}
		});
	}
	
	// 찾기 페이지 가기
	function keySearch(){
		document.location.href = "/dmt2d/keySearch.do";
	}
	
	
	function newPopupResize() {
	 	 window.resizeTo(100,100);
	   var dWidth = parseInt(document.body.scrollWidth);
	   var dHeight = parseInt(document.body.scrollHeight); 	 
	   var divEl = document.createElement('div');
	   divEl.style.left = '0px';
	   divEl.style.top = '0px';
	   divEl.style.width = '100%';
	   divEl.style.height = '100%';
		
	   document.body.appendChild(divEl);
	   if (navigator.appName.charAt(0) == "N") { 
		   dHeight -= 16;
	   }
	 	 //alert(dWidth + ":" + divEl.offsetWidth + ":"  +document.body.clientWidth + ":"  +document.body.offsetWidth);
	   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
	   
	   document.body.removeChild(divEl);  
	}
</SCRIPT>
</head>
<!-- 
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"
	onload="newPopupResize();" oncontextmenu='return false'
	ondragstart='return false' onselectstart='return false'>
 -->
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"
	oncontextmenu='return false'
	ondragstart='return false' onselectstart='return false'>
	
<form commandName="EmailEntity" name="setting" method="post">
	<input type="hidden" name="userId" value="<%=userId%>"> 

	<!--테이블현재 사이즈 w:450, h:307 --> 
	<table width="450" border="0" cellpadding="0" cellspacing="0" style="margin:auto;margin-top: 100px;">
		<tr>
			<td height="40"><img src="/dmt2d/images/new/title_06.jpg"
				width="300" height="40" /></td>
		</tr>
		<tr>
			<td height="75"><img src="/dmt2d/images/new/comm_06.jpg"
				width="450" height="75" /></td>
		</tr>
		<tr>
			<td height="20" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td height="112" align="center">
			<!--내용테이블시작 -->
			<table width="430" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"><img src="/dmt2d/images/new/border_01.gif" width="10" height="10" /></td>
					<td width="80"><img src="/dmt2d/images/new/border_02.gif" width="80" height="10" /></td>
					<td width="10"><img src="/dmt2d/images/new/border_02.gif"  width="10" height="10" /></td>
					<td width="320"><img src="/dmt2d/images/new/border_09.gif"  width="320" height="10" /></td>
					<td width="10"><img src="/dmt2d/images/new/border_10.gif" width="10" height="10" /></td>
				</tr>
	
				<tr>
					<td width="10" background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td width="80" height="30" align="center" bgcolor="#DBE9F1"><span >질문</span></td>
					<td width="10" align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td width="320" align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
						<SELECT id="passHintSelectCode" name="passHintSelectCode" class="textbox_1">
		 					<OPTION>원하는 질문을 선택하세요.</OPTION>
							<OPTION value="가장 기억에 남는 장소는?">가장 기억에 남는 장소는?</OPTION>
							<OPTION value="나의 좌우명은?">나의 좌우명은?</OPTION>
							<OPTION value="나의 보물 제1호는?">나의 보물 제1호는?</option>
							<OPTION value="가장 기억에 남는 선생님 성함은?">가장 기억에 남는 선생님 성함은?</OPTION>
							<OPTION value="다른 사람은 모르는 나만의 신체비밀은?">다른 사람은 모르는 나만의 신체비밀은?</OPTION>
							<OPTION value="오래도록 기억하고 싶은 날짜는">오래도록 기억하고 싶은 날짜는</OPTION>
							<OPTION value="받았던 선물 중 기억에 남는 독특한 선물?">받았던 선물 중 기억에 남는 독특한 선물?</OPTION>
							<OPTION value="가장 생각나는 친구 이름은?">가장 생각나는 친구 이름은?</OPTION>
							<OPTION value="인상 깊게 읽은 책 이름은?">인상 깊게 읽은 책 이름은?</OPTION>
							<OPTION value="읽은 책 중에서 좋아하는 구절은?">읽은 책 중에서 좋아하는 구절은?</OPTION>
							<OPTION value="내가 존경하는 인물은?">내가 존경하는 인물은?</OPTION>
							<OPTION value="다시 태어나면 되고 싶은 것은?">다시 태어나면 되고 싶은 것은?</option>
							<OPTION value="내가 좋아하는 만화 캐릭터는?">내가 좋아하는 만화 캐릭터는?</option>
							<OPTION value="초등학교 시절 나의 꿈은?">초등학교 시절 나의 꿈은?</OPTION>
							<OPTION value="내 핸드폰 3번에 등록된 사람은?">내 핸드폰 3번에 등록된 사람은?</OPTION>
							<OPTION value="나의 출신 초등학교는?">나의 출신 초등학교는?</OPTION>
							<OPTION value="우리집 애완동물의 이름은?">우리집 애완동물의 이름은?</option>
							<OPTION value="나의 노래방 애창곡은?">나의 노래방 애창곡은?</OPTION>
							<OPTION value="가장 감명깊게 본 영화는?">가장 감명깊게 본 영화는?</OPTION>
							<OPTION value="좋아하는 스포츠 팀 이름은?">좋아하는 스포츠 팀 이름은?</OPTION>
							<OPTION value="본인의 출생지는?">본인의 출생지는?</option>
						</SELECT>
					</td>
					<td width="10" background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td height="1" bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
				</tr>
				<tr>
					<td background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td height="30" align="center" bgcolor="#DBE9F1" >답변</td>
					<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
						<input type="text" id="passAnswer" name="passAnswer"" class="textbox_1" style=" width:300px;" size="50" maxlength="50" />
					</td>
					<td background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td height="1" bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
					<td bgcolor="#a8b2d7"></td>
				</tr>
				<tr>
					<td background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
					<td height="30" align="center" bgcolor="#DBE9F1" >e-mail</td>
					<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
					<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
						<input type="text" id="userEmail" name="userEmail" class="textbox_1" style=" width:300px;" size="50"	maxlength="50" value="" />
					</td>
					<td background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td width="10"><img src="/dmt2d/images/new/border_04.gif" width="10" height="10" /></td>
					<td width="80"><img src="/dmt2d/images/new/border_05.gif" width="80" height="10"/></td>
					<td width="10"><img src="/dmt2d/images/new/border_05.gif" width="10" height="10"/></td>
					<td width="300"><img src="/dmt2d/images/new/border_08.gif" width="320" height="10"/></td>
					<td width="10"><img src="/dmt2d/images/new/border_06.gif" width="10" height="10" /></td>
				</tr>
			</table>
			<!--내용테이블 끝 -->
			</td>
		</tr>
		<tr>
			<td height="20" align="center">&nbsp;</td>
		</tr>
	
		<!--확인취소버튼 -->
		<tr>
			<td height="40">
			<table width="100%">
				<tr>
					<td align="right" width="50%">
						<a href="#"><img src="/dmt2d/images/new/btn_confirm.jpg" width="97" height="27" onClick="javascript:save();" border="0" /></a> 
					</td>
					<td align="left" width="50%">
						<a href="#"><img src="/dmt2d/images/new/btn_cancel.jpg" width="97" height="27" border="0" onClick='Javascript:history.go(-1);' /></a>
					</td>
				</tr>
			</table>
		</tr>
	</table>
</body>
</html>
