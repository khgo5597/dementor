<!-- authCodeForm.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%-- <%@ page import="com.mininfo.user.model.UserHintEntity"%> --%>
<%-- <%@ page import= "com.mininfo.common.util.CommonUtil"%> --%>
<%-- <%@ page import="com.mininfo.common.model.ResultEntity"%> --%>
<%-- <%@ page import="com.mininfo.common.property.DementorProperty"%> --%>
<%-- <%@ page import="com.mininfo.common.code.CodeMessageHandler"%> --%>
<%!
	public static String stringValue(Object str){
		if(str == null) return "";
		return (String)str;
	}
%>
<%

// System.out.println("#################  mail_send ####################");

// 	DementorProperty DP = null;

// 	UserHintEntity userEntity = (UserHintEntity)request.getAttribute("USER");
// 	//String message = (String)request.getAttribute("MESSAGE");
// 	String status      = CommonUtil.nullToStr(request.getParameter("status"));
// 	String userId     = CommonUtil.nullToStr(request.getParameter("userId"));
// 	String sendingChk  = CommonUtil.nullToStr(request.getParameter("sending"));
// 	String sendType  = CommonUtil.nullToStr(request.getParameter("sendType"));
// 	String processCmd  = CommonUtil.nullToStr(request.getParameter("processCmd"));
	
	String userId = (String)request.getAttribute("userId");
// 	String status = (String)request.getAttribute("status");


// System.out.println(">>  authCodeForm  >>>>>> userId                "+userId);	
// System.out.println(">>  authCodeForm   >>>>>> status                "+status);

// 		String message = "";
// 		int resultCode = 0;
// 		int processCode = 0;
		
// 		if(request.getAttribute("RESULT") != null) {
// 			ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
// 			message = resultEntity.getResultMessage();
// 			resultCode = resultEntity.getResultCode();
// 			processCode = resultEntity.getProcessCode();
// 		}
		
// 		String msg1= CodeMessageHandler.getInstance().getCodeMessage("5020"); //메일로 받으신 인증코드를 입력하여 주세요.
// 		String msg2= CodeMessageHandler.getInstance().getCodeMessage("5021"); //인증키 입력
// 		String msg3= CodeMessageHandler.getInstance().getCodeMessage("5126"); //새로운 인증코드가 Mail로 발송 되었습니다.
%>
<html>
<head>
<title>그래픽인증 - 동국대</title>
<link rel="stylesheet" href="/dmt2d/css/style.css" type="text/css">
<script type="text/javascript" src="/dmt2d/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/dmt2d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">

	$(document).ready(function(){
		// 인증 코드 자동 포커싱
		$("#auth_code").focus(); 
	});
	
	// 뒤로가기
	function goMain(){
		history.go(-1);
	}
	
	/* 유효성 검사 */
	function check(){
		var form = document.authCodeForm;
		
		var authCode = form.auth_code;
		if(authCode.value == ""){
			alert('메일로 받으신 인증코드를 입력하여 주세요.');
			authCode.focus;
			return;
		}
	}
	
	// 확인
	function save(){
		// 유효성 검사
		check();
		
		// 메일 보내기
		var form = document.authCodeForm;
		var userId, authCode;
		userId = form.userId.value;
		authCode = form.auth_code.value;
		
		$.ajax({
			type : "post",
			url : "/dmt2d/authCodeCheck.do",
			data : {
				userId : userId,
				auth_code : authCode
			},
			success : function(code) {
				if(code == "S") {
					// 재설정 페이지로...
					alert("확인되었습니다.\n그래픽 인증 재설정 페이지로 이동합니다.");
					
					// 키 재설정 페이지로 이동
					// 우선은 아이디를 get 방식으로 보내게 해놨음
					document.location.href = "/dmt2d/setting.do?userid=" + userId;
					
				} else { 
					alert("오류가 발생하였습니다.\ncode : " + code);
					return;
				}
			},
			error : function(data) {
				alert("오류가 발생하였습니다.");
				return;
			}
		});
	}
</SCRIPT>
</head>

<%
// 	if(sendingChk.equals("true")){
%>
<!-- <body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"> -->
<%-- <%	} else { %> --%>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<%-- <%  } %> --%>
<form commandName="EmailEntity" name="authCodeForm" method="post" >
	<input type="hidden" name="userId"     value="<%=userId%>">
<%-- 	<input type="hidden" name="status"     value="<%=status%>"> --%>

	<table width="450" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:auto;margin-top: 100px;">
		<tr height="20%">
			<td></td>
		</tr>
		<tr height="30%">
			<td align="center" valign="top" style="padding-left:10px;" >
				<table width="420px" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="40" ><img src="/dmt2d/images/new/title_01.jpg"  /></td>
					</tr>
					<tr>
						<td height="75" ><img src="/dmt2d/images/new/comm_01.jpg"  /></td>
					</tr>
					<tr>
						<td style="text-align: center; color: red;">본인 메일 확인 후 인증키 값을 입력해 주세요.</td>
					</tr>
					<tr>
						<td height="20" ></td>
					</tr>
					<table width="430" valign="top" border="0" cellspacing="0" cellpadding="0" >	
						<tr>
							<td width="12"><img src="/dmt2d/images/new/border_01.gif" width="12" height="10" /></td>
							<td width="80"><img src="/dmt2d/images/new/border_02.gif" width="80" height="10" /></td>
							<td width="10"><img src="/dmt2d/images/new/border_02.gif" width="10" height="10" /></td>
							<td width="320"><img src="/dmt2d/images/new/border_09.gif" width="320" height="10" /></td>
							<td width="10"><img src="/dmt2d/images/new/border_10.gif" width="10" height="10" /></td>
						</tr>
						<tr>
							<td width="10"  background="/dmt2d/images/new/border_03.gif">&nbsp;</td>
							<td width="80"  height="30" align="center" bgcolor="#DBE9F1" >인증키</td>
							<td width="10"  align="center" bgcolor="#DBE9F1"></td>
							<td width="320" align="left" style="padding-left:10px;">
								<input type="text" id="auth_code" name="auth_code" size="40" maxlength="50" />
							</td>
							<td width="10"  background="/dmt2d/images/new/border_07.gif">&nbsp;</td>
						</tr>
						<tr>
							<td width="12" ><img src="/dmt2d/images/new/border_04.gif" width="12" height="10" /></td>
							<td width="80" ><img src="/dmt2d/images/new/border_05.gif" width="80" height="10" /></td>
							<td width="10" ><img src="/dmt2d/images/new/border_05.gif" width="10" height="10" /></td>
							<td width="320" ><img src="/dmt2d/images/new/border_08.gif" width="320" height="10" /></td>
							<td width="10" ><img src="/dmt2d/images/new/border_06.gif" width="10" height="10" /></td>
						</tr>
						<tr>
							<td colspan="5" height="20"></td>
						</tr>
					</table>
					<!--확인취소버튼 -->
					<tr>
						<td height="40" align="center" valign="top" >
							<a href="#"><img src="/dmt2d/images/new/btn_confirm.jpg" width="97" height="27" border="0" onClick="javascript:save();" /></a>&nbsp; &nbsp; 
							<a href="#"><img src="/dmt2d/images/new/btn_cancel.jpg" width="97" height="27" border="0" onClick="javascript:goMain();" /></a>
						</td>
					</tr>
		<!-- 			<tr> -->
		<%-- 				<td height="20" align="center" ><font color="red"><%=message %></font></td> --%>
		<!-- 			</tr>			 -->
				</table>
				<!--내용테이블 끝 -->
			</td>
		</tr>
		<tr>
			<td height="50%" >&nbsp;</td>
		</tr>
	</table>
</form>
</body>
</html>
