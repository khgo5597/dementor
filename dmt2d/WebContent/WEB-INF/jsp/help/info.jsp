<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
/**  
 * @Description : 그래픽인증 안내 페이지
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */
%>
<%
//    String userid = request.getParameter("userid");
//    if(userid=="" || userid == null) userid=(String)session.getAttribute("userId");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<title>그래픽인증 - 동국대학교</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no' />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<script type="text/javascript">
    var url_op="gw.dongguk.edu"; // "gw.dongguk.edu"
	document.domain = "dongguk.edu"; //운영버서 : gw.dongguk.edu
	var service = {
			
		// 테스트용 주석입니다.
		//키 설정 페이지 이동
		keySetting : function() {
			document.location.href = "/dmt2d/setting.do";
			
		},
		//해지 처리
		serviceClose : function() {
			var jsonParam = {
				data:{
					userid:"<%=(String)session.getAttribute("userId")%>"
				}	
			};
			
			$.ajax({
				url : "/dmt2d/userCheck.do",
				contentType : "text/plain;charset=UTF-8",
				dataType : "json",
				data : {userId:"<%=(String)session.getAttribute("userId")%>"},
				success : function(data) {
					var result = eval(data);
					if(result.resultCode) {
						if(confirm("그래픽인증 서비스를 해지하시겠습니까?")) {
							$.ajax({
								type : "POST",
								url : "/dmt2d/closeUser.do?data="+JSON.stringify(jsonParam),
								contentType : "text/plain;charset=UTF-8",
								dataType : "json",
								success : function(data) {
									var result = eval(data);
									if(result.code == "101000") {
										alert("그래픽인증 서비스가 해지되었습니다.");
									} else if(result.code == "102260") {
										alert("그래픽인증 서비스에 등록되어 있지 않습니다.\n다시 확인하시기 바랍니다.");
									} else {
										alert("그래픽인증 해지 처리 중 오류가 발생하였습니다.");
									}
								}
							});
						}
					} else {
						alert("그래픽인증 서비스에 등록되어 있지 않습니다.\n다시 확인하시기 바랍니다.");
					}
				}
			});
		},
		
		//인증요청
		display : function() {
			$.ajax({
				url : "/dmt2d/userCheck.do",
				contentType : "text/plain;charset=UTF-8",
				dataType : "json",
				data : {userId:"<%=(String)session.getAttribute("userId")%>"},
				success : function(data) {
					var result = eval(data);
					if(result.resultCode) {
						document.location.href = "/dmt2d/display.do";
					} else {
						alert("그래픽인증 서비스에 등록되어 있지 않습니다.\n다시 확인하시기 바랍니다.");
					}
				}
			});
		}
		
	};
</script>
</head>
<body style="margin:0 0 0 0">

	<div id="header" style="position:relative;">
		<%@ include file="/WEB-INF/jsp/layout/gnb.jsp"%>
		<div id="main" style="position:relative;padding:130px 0 0 0;width:100%;text-align:center;margin:0 auto;">
			<div id="content" style="position:relative;margin:0 auto;">
				
				<!--
				############################################################################################# 
				메인 컨탠츠 부분
			    #############################################################################################
			    -->
				<!-- 버튼 -->
				<div id="mainContent" style="position:relative;"> 
					<input type="button" value="인증하기" onclick="javascript:service.display();"/>
					<input type="button" value="서비스 키 설정" onclick="javascript:service.keySetting();"/>
					<input type="button" value="서비스 해지" onclick="javascript:service.serviceClose();"/>
				</div>
				
				<!--
				############################################################################################# 
				메인 컨탠츠 부분
			    #############################################################################################
			    -->
				
			</div>
		</div>
		<%--@ include file="/WEB-INF/jsp/layout/footer.jsp"--%>	 
	</div>
</body>
</html>