﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String userId = request.getParameter("userId");											// USER ID
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>고객사 로그인 처리</title>
<script type="text/javascript">
    function onWindowClose() {
		window.close();
	}
</script>
</head>
<body>
<form name="chkForm" method="post">
	<input type="hidden" name="userId" value="<%=userId%>" >
</form>
<%									
	String dmt2d_auth = request.getParameter("dmt2d_auth");
    session.setAttribute("dmt2d_AUTH",dmt2d_auth);
	
	if(dmt2d_auth.equals("certTrue")) {    
%>
	<script type="text/javascript">
	    alert("인증성공");
        document.title="certOK";
        window.opener='nothing';
        window.open('','_parent','');		

		setInterval("onWindowClose()", 1000);
        //window.close();
    </script>
    <input type="hidden" name="sendMessage" value="certTrue">
<%  } else { %>
	<script type="text/javascript">    
        alert("인증실패");
        document.title="certFail";
        window.opener='nothing';
        window.open('','_parent','');	

        setInterval("onWindowClose()", 1000);		
        //window.close();                                                                                                    
	</script>
	<input type="hidden" name="sendMessage" value="certFalse">
<%  } %>
</body>
</html>