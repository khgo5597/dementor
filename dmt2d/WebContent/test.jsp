<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
/**  
 * @Description : 그래픽인증 안내 페이지
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<title>그래픽인증 - 동국대학교</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name='viewport' content='width=480' />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<script type="text/javascript">
	$(document).bind("touchstart", function(e){
		
	});
	
	$(document).bind("touchmove", function(e){
		var event = e.originalEvent;
		
		$("#test").css("left", event.touches[0].screenX+2);
		$("#test").css("top", event.touches[0].screenY+2);
		
		e.preventDefault();	//	이벤트취소
	});
</script>
</head>
<body style="margin:0 0 0 0">
	<div id="test" style="position:absolute;width:100px;height:100px;border:1px solid #000"/>
</body>
</html>