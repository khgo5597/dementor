
//사용자 이미지 요청
function reqauthimgkey() {
    
    var jsonParam = {
        data:{
            userid  : userid,
            devtype : "0"
        }   
    };

    $.ajax({
        type : "POST",
        url : "/dmt2d/reqauthimgkey.do?data="+JSON.stringify(jsonParam),
        contentType : "text/plain;charset=UTF-8",
        dataType : "json",
        success : function(data) {
            var result = eval(data);

            //이미지 요청 완료시
            if(result.code == "101000") {
                dementorAuth.init(result.data.level, result.data.servertime, result.data.icons, "/dmt2d/images/" , result.data.userid);
                return false;
            } 
                        
            //이미지 요청 실패시
            switch(result.code) {
                case "102260":
                    alert("등록된 사용자가 아닙니다.");        
                    break;
                case "102250":
                    if(requestType == 'middle') {
                        alert("그래픽 인증 비밀번호 입력을 5 회 실패하셨습니다. 관리자에게 문의하여 주십시오.");
                    }
                    break;
                default:
                    alert("그래픽인증 로딩에 실패하였습니다.\n다시 시도해주세요.");
            
            } 
                                   
            if(requestType == 'middle') {
                onResultMiddleWare('certFalse');
            }
            else {
                onResultGroupWare(result.code);
            } 
        }
    });
}
    
//사용자 인증
function reqauth() {
//	alert("인증확인!");
    var authvalue = SHA_Encode("0404", authKey, servertime, userid);
    var jsonParam = {
        data:{
            userid:userid,
            devtype:"0",
            correntLvl:correntLvl,
            authvalue:authvalue,
            servertime:servertime
        }   
    };
        
    $.ajax({
        type : "POST",
        url : "/dmt2d/reqauth.do?data="+JSON.stringify(jsonParam),
        contentType : "text/plain;charset=UTF-8",
        dataType : "json",
        success : function(data) {   
            var result = eval(data);
            if(result.code == "101000") {
                
                alert("그래픽인증이 완료되었습니다.");
                if(requestType == 'middle') {
                    onResultMiddleWare('certTrue');
                }
                else {
                    onResultGroupWare(result.code);
                } 
                return false;
            } 

            //사용자 인증 실패시
            switch(result.code) {
                case "102260":
                    alert("등록된 사용자가 아닙니다.");   
                    if(requestType == '') {
                        alert(result.code);
                    }
                    break;
                case "102200":
                    alert("[" + userid + "] 인증에 실패하였습니다. :: " +result.code );
                    dementorAuth.reload();
                    reInit();
                    return false;
                case "102250":
                    if(requestType == 'middle') {
                        alert("그래픽 인증 비밀번호 입력을 5 회 실패하셨습니다. 관리자에게 문의하여 주십시오.");
                    }
                    break;
                default:
                    alert("그래픽인증 로딩에 실패하였습니다.\n다시 시도해주세요.");
            
            }  
            
            if(requestType == 'middle') {
                onResultMiddleWare('certFalse');
            }
            else {
                onResultGroupWare(result.code);
            } 
        }
    });
}
    
//재시작
function reInit() {
    reqauthimgkey();
}

//재발급
function reKey() {
    
}