
//IE 결과 처리
function onResultIE(resultCode) {
    window.returnValue = resultCode;
    //window.close();
}

//Chrome 및 Portal 결과처리
function onResultPage(resultCode) {
    
    var returnUrl = "";
    
    //리턴 Url 설정
//    if(url_op.indexOf("portal") > -1) {  //Portal
//        returnUrl = "https://" + url_op + "/member/graphic/GraphicResult.do"
//    }   
//    else {                               //그룹웨어
//        returnUrl = "https://" + url_op + "/myoffice/Common/GraphicResult.aspx"
//    }
//    
//    returnUrl += "?code=" + resultCode;
//    returnUrl += "&type=" + type;
    
    document.location.href = returnUrl;    
}

//학사행정 결과처리
function onResultMiddleWare(result) {    
    document.location.href = "/dmt2d/clientLogin.jsp?dmt2d_auth=" + result;
}

//그룹웨어 인증 결과 처리
function onResultGroupWare(resultCode) {
                    
    if(url_op.indexOf("portal") > -1) {     //Portal
        onResultPage(resultCode);
        return false;
    }
    else {                                  //그룹웨어
        if(navigator.appName.indexOf("Microsoft") > -1 || navigator.userAgent.indexOf("Trident") > 0){
            onResultIE(resultCode);
        }
        else{
            onResultPage(resultCode);
        } 
    }
    
}