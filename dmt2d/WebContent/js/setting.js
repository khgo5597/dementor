var service = {
    keyCateList : function() {
        var jsonParam = {
            data:{
                userid:userid,
                devkey:"",
                devname:"",
                devtype:"0",
                cmd:""
            }   
        };

        $.ajax({
            type : "POST",
            url : "/dmt2d/list.do?data="+JSON.stringify(jsonParam),
            contentType : "text/plain;charset=UTF-8",
            dataType : "json",
            success : function(data) {
                var result = eval(data);
                
                if(result.code == "101000") {
                    dementorSetting.init(result.data.category, "/dmt2d/images/");
                } else {
                    alert("그래픽인증 로딩에 실패하였습니다.\n다시 시도해주세요.");
                }
                return false;
            }
        });
    },

    //수정 요청
    keySetting : function() {
        for(var i=0;i<4;i++) {
            var keyValue = $(":input[name=key"+i+"]").val();
            if(keyValue == "0" || keyValue == "") {
                alert("등록할 키를 선택하세요.");
                return false;
            }
        }
        
        var jsonParam = {
            data:{
                userid:userid,
                devkey:"",
                devname:"",
                devtype:"0",
                cmd:"",
                hole:$(":input[name=key0]").val(),
                key1:$(":input[name=key1]").val(),
                key2:$(":input[name=key2]").val(),
                key3:$(":input[name=key3]").val()
            }   
        };
        
        $.ajax({
            type : "POST",
            url : "/dmt2d/keyset.do?data="+JSON.stringify(jsonParam),
            contentType : "text/plain;charset=UTF-8",
            dataType : "json",
            success : function(data) {
                var result = eval(data);
                
                if(result.code == "101000") {
                    alert("인증키 설정이 완료되었습니다.");
                    if(requestType != '') {
                        var url = "/dmt2d/display.do?userid=" + userid + "&requestType=" + requestType;
                        location.href = url;
                    }
                    else{
                        onResultGroupWare(result.code);
                    }                       
                } else {
                    alert("인증키 설정에 실패하였습니다.\n다시 시도해주세요.");
                    if(requestType == 'middle') {
                        donResultMiddleWare('certFalse');
                    } else{
                        onResultGroupWare(result.code);                
                    }                           
                }
            }
        });
    }
};