﻿/* 전역 변수 선언 */
var selectKey = "";
var moveKey = "";
var authKey = "";
var authCnt = 0;
var servertime = "";

var iconUrlArray = new Array(new Array(5), new Array(5), new Array(5),
		new Array(5), new Array(5));
var correntLvl = 0;
var userIcons;

var rowCnt = 0;
var colsCnt = 0;
var moveCnt = 0;

var imgUrlPath = "";
var initCnt = 0;

var totalSize = 0;
var iconSize = 0;

var moveX = 0;
var moveY = 0;

var serverImgUrl = "/dmt2d/images/";
var divHeightSize = 0;

var userId = "";

var dementorAuth = {
	// 초기 설정 함수(사용자 ID, 보안레벨, 아이콘 정보, 공통 이미지Url 경로)
	init : function(lvl, time, icons, imgUrl, userid) {
		// 메뉴, 드레그 이벤트 없애기
		$("#dementor").bind("contextmenu", function(event) {
			return false;
		});
		$("#dementor").bind("selectstart", function(event) {
			return false;
		});
		$("#dementor").bind("dragstart", function(event) {
			return false;
		});

		// 파이어폭스용 이벤트 생성
		if (navigator.userAgent.indexOf('Firefox') >= 0) {
			var eventNames = [ "mousedown", "mouseover", "mouseout",
					"mousemove", "mousedrag", "click", "dblclick", "keydown",
					"keypress", "keyup" ];

			for (var i = 0; i < eventNames.length; i++) {
				window.addEventListener(eventNames[i], function(e) {
					window.event = e;
				}, true);
			}
		}

		// 파라미터 전역 변수 셋팅
		imgUrlPath = imgUrl;
		servertime = time;
		userIcons = icons;
		userId = userid;

		if (initCnt <= 0) {
			// 기본 화면 그리기 함수 호출
			dementorAuth.defaultDraw();
			// 기본 이벤트 설정 함수 호출
			dementorAuth.defaultEventSet();
			correntLvl = lvl + 1;
		} else {
			lvl = correntLvl;
		}

		// 보안 레벨에 해당하는 아이콘 배치 함수 호출
		if (lvl == "0") {
			dementorAuth.display1();
			$("#dmt2dLvl").find("div:eq(0)").click();
		} else if (lvl == "1") {
			dementorAuth.display2();
			$("#dmt2dLvl").find("div:eq(1)").click();
		} else if (lvl == "2") {
			dementorAuth.display3();
			$("#dmt2dLvl").find("div:eq(2)").click();
		} else if (lvl == "3") {
			dementorAuth.display4();
			$("#dmt2dLvl").find("div:eq(3)").click();
		}

		$("input:[name=dementorDisplay]").focus();
		initCnt++;
	},

	// 기본 화면 그리기 함수
	defaultDraw : function() {
		// 기본 레이아웃 생성(배경)
		$("#dementor").append($("<div/>", {
			id : "dmt2dDisplay"
		}));
		$("#dmt2dDisplay")
				.attr(
						"style",
						"background:url('"
								+ imgUrlPath
								+ "auth/displayBg.png');width:343px;height:540px;padding:3px;position:relative;float:left;border:2px solid #D5D5D5;cursor:url('"
								+ imgUrlPath
								+ "auth/dementorCursor.cur'), default;");
		$("#dmt2dDisplay").css("border-radius", "5px");
		$("#dmt2dDisplay").css("background-repeat", "no-repeat");
		$("#dmt2dDisplay")
				.append(
						$(
								"<input/>",
								{
									type : "text",
									name : "dementorDisplay",
									style : "position:absolute;width:1px;height:1px;border:0px;top:355px;left:50px"
								}));

		// 사용자키 정보 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dKeyLayer"
		}));
		$("#dmt2dKeyLayer")
				.attr(
						"style",
						"position:relative;float:left;width:335px;height:330px;z-index:9999;top:4px;left:3px;border:1px solid #D5D5D5;");

		// 보안 1단계시 아이콘 MOVE 이미지 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dKeySelect"
		}));
		$("#dmt2dKeySelect")
				.attr(
						"style",
						"cursor:url('"
								+ imgUrlPath
								+ "auth/dementorCursor.cur'), default;background-color:#FFF;position:absolute;width:49px;height:48px;display:none;border:4px solid #4374D9;padding:3px;z-index:9999");
		$("#dmt2dKeySelect").css("opacity", "0.6");
		$("#dmt2dKeySelect").css("-moz-opacity", "0.6");
		$("#dmt2dKeySelect").css("-khtml-opacity", "0.6");
		$("#dmt2dKeySelect").css("-ms-filter",
				"progid:DXImageTransform.Microsoft.Alpha(Opacity=60)");
		$("#dmt2dKeySelect").css("filter", "alpha(opacity=60);");

		// 스텝 정보 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dStep"
		}));
		$("#dmt2dStep")
				.attr(
						"style",
						"position:absolute;background:url('"
								+ imgUrlPath
								+ "auth/stepBg.png');width:334px;height:18px;top:346px;left:8px;z-index:9998")
				.append($("<div/>"));
		$("#dmt2dStep").find("div").attr("style",
				"position:absolute;z-index:9999;top:6px;left:60px").append(
				$("<img/>"));
		$("#dmt2dStep").find("img")
				.attr("src", imgUrlPath + "auth/stepBar.png").attr("style",
						"width:0px;height:6px;");

		// 이동 횟수 표시 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dMoveCnt",
			text : "0"
		}));
		$("#dmt2dMoveCnt")
				.attr(
						"style",
						"position:absolute;z-index:9999;float:left;top:404px;left:29px;text-align:center;width:40px;");

		// 보안 단계 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dLvl"
		}));
		$("#dmt2dLvl")
				.attr(
						"style",
						"position:absolute;background:url('"
								+ imgUrlPath
								+ "auth/levelBg.png');width:72px;height:9px;z-index:9999;top:410px;left:262px;cursor:url('"
								+ imgUrlPath
								+ "auth/dementorCursor.cur'), default");
		$("#dmt2dLvl")
				.append($("<div/>"))
				.find("div:eq(0)")
				.attr("style",
						"position:absolute;padding-top:3px;top:0px;left:4px;width:16px;height:9px")
				.append($("<img/>"));
		$("#dmt2dLvl")
				.append($("<div/>"))
				.find("div:eq(1)")
				.attr("style",
						"position:absolute;padding-top:3px;top:0px;left:20px;width:16px;height:9px")
				.append($("<img/>"));
		$("#dmt2dLvl")
				.append($("<div/>"))
				.find("div:eq(2)")
				.attr("style",
						"position:absolute;padding-top:3px;top:0px;left:36px;width:16px;height:9px")
				.append($("<img/>"));
		$("#dmt2dLvl")
				.append($("<div/>"))
				.find("div:eq(3)")
				.attr("style",
						"position:absolute;padding-top:3px;top:0px;left:52px;width:16px;height:9px")
				.append($("<img/>"));
		$("#dmt2dLvl").find("img").each(
				function() {
					$(this).attr("src", imgUrlPath + "auth/levelBar.png").attr(
							"style", "width:0px;height:3px");
				});
		$("#dmt2dLvl").append($("<div/>")).find("div:eq(4)").attr("style",
				"position:absolute;top:-7px;left:0px").append($("<img/>"));
		$("#dmt2dLvl").find("img:eq(4)").attr("src",
				imgUrlPath + "auth/levelIcon.png");
		$("#dmt2dLvl").append($("<div/>")).find("div:eq(5)").attr("style",
				"position:absolute;top:-35px;left:-4px;width:100px");
		$("#dmt2dLvl").find("div:eq(5)").append($("<span>", {
			style : "font-weight:bold;font-size:10pt",
			text : "보안 "
		}));
		$("#dmt2dLvl").find("div:eq(5)").append($("<span>", {
			style : "font-weight:bold;font-size:16pt",
			text : correntLvl
		}));
		$("#dmt2dLvl").find("div:eq(5)").append($("<span>", {
			style : "font-weight:bold;font-size:10pt",
			text : " 단계"
		}));

		// 마우스 이동 버튼 레이어 생성
		$("#dmt2dDisplay")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dMoveBtn",
									style : "position:absolute;left:102px;top:370px;cursor:url('"
											+ imgUrlPath
											+ "auth/dementorCursor.cur'), default"
								}));
		// $("#dmt2dMoveBtn").append($("<table/>", {style:"border:0px;",
		// cellpadding:"1px",
		// style:"padding:0px;margin:0px;border-collapse:separate;cursor:url('"+imgUrlPath+"auth/dementorCursor.cur'),
		// default"}).append(tableTag));
		var tableTag = "<tr><td style='text-align:center;padding:1px 1px 1px 2px;margin:0px;' colspan='3'><img src='"
				+ imgUrlPath
				+ "auth/up_off.png' style='width:45px;height:45px;'/></td></tr>"
				+ "<tr>"
				+ "<td style='text-align:center;padding:1px;margin:0px;'><img src='"
				+ imgUrlPath
				+ "auth/left_off.png;' style='width:45px;height:45px;'/></td>"
				+ "<td style='text-align:center;padding:1px;margin:0px;'><img src='"
				+ imgUrlPath
				+ "auth/enter_off.png' style='width:43px;height:43px;'/></td>"
				+ "<td style='text-align:center;padding:1px;margin:0px;'><img src='"
				+ imgUrlPath
				+ "auth/right_off.png' style='width:43px;height:43px;'/></td>"
				+ "</tr>"
				+ "<tr><td style='text-align:center;padding:1px 1px 1px 2px' colspan='3'><img src='"
				+ imgUrlPath
				+ "auth/down_off.png' style='width:45px;height:45px;'/></td></tr>";
		// $("#dmt2dMoveBtn").append($("<table/>", {style:"border:0px;",
		// cellpadding:"1px",
		// style:"padding:0px;margin:0px;border-collapse:separate"}).append(tableTag));
		$("#dmt2dMoveBtn")
				.append(
						$(
								"<table/>",
								{
									style : "border:0px;",
									cellpadding : "1px",
									style : "padding:0px;margin:0px;border-collapse:separate;cursor:url('"
											+ imgUrlPath
											+ "auth/dementorCursor.cur'), default"
								}).append(tableTag));
		$("#dmt2dMoveBtn").append($("<div/>", {
			id : "multiCursor1",
			style : "position:absolute;display:none;z-index:9999"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/dementorCursor.png"
		})));
		$("#dmt2dMoveBtn").append($("<div/>", {
			id : "multiCursor2",
			style : "position:absolute;display:none;z-index:9999"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/dementorCursor.png"
		})));
		$("#dmt2dMoveBtn").append($("<div/>", {
			id : "multiCursor3",
			style : "position:absolute;display:none;z-index:9999"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/dementorCursor.png"
		})));

		// 버튼 레이어 생성
		$("#dmt2dDisplay").append($("<div/>", {
			id : "dmt2dBtn",
			style : "position:absolute;top:494px;left:14px"
		}));
		$("#dmt2dBtn").append($("<div/>", {
			id : "dmt2dBtn",
			style : "position:absolute;left:229px;"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/complate_off.png"
		})));
		$("#dmt2dBtn").append($("<div/>", {
			id : "dmt2dBtn",
			style : "position:absolute;"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/reload_off.png"
		})));

		// 도움말 생성 취소
		$("#dmt2dDisplay").append($("<div/>", {
			style : "position:absolute;left:357px;clear:both;top:25px;"
		}).append($("<img/>", {
			src : imgUrlPath + "auth/help.png",
			style : "width:390px;height:535px"
		})));

		// 인증키 찾기(email 발송)
		$("#dmt2dDisplay").append(
				$("<div/>", {
					style : "position:absolute;left:500px;clear:both;top:5px;"
				}).append(
						$("<a href='javascript:dementorAuth.keySearch();'>"
								+ "<img src='" + imgUrlPath
								+ "new/btn_reset.jpg' /></a>")));
	},

	// 기본 이벤트 셋팅 함수
	defaultEventSet : function() {
		$("#dmt2dLvl").find("div:eq(0)").click(function() {
			$("#dmt2dLvl").find("div:eq(4)").css("left", "0px");
			$("#dmt2dLvl").find("img:eq(0)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(1)").css("width", "0px");
			$("#dmt2dLvl").find("img:eq(2)").css("width", "0px");
			$("#dmt2dLvl").find("img:eq(3)").css("width", "0px");

			$("#dmt2dLvl").find("span:eq(1)").html("1");
			dementorAuth.display1();
			dementorAuth.reload();
		});

		$("#dmt2dLvl").find("div:eq(1)").click(function() {
			$("#dmt2dLvl").find("div:eq(4)").css("left", "16px");
			$("#dmt2dLvl").find("img:eq(0)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(1)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(2)").css("width", "0px");
			$("#dmt2dLvl").find("img:eq(3)").css("width", "0px");

			$("#dmt2dLvl").find("span:eq(1)").html("2");
			dementorAuth.display2();
			dementorAuth.reload();
		});

		$("#dmt2dLvl").find("div:eq(2)").click(function() {
			$("#dmt2dLvl").find("div:eq(4)").css("left", "32px");
			$("#dmt2dLvl").find("img:eq(0)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(1)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(2)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(3)").css("width", "0px");

			$("#dmt2dLvl").find("span:eq(1)").html("3");
			dementorAuth.display3();
			dementorAuth.reload();
		});

		$("#dmt2dLvl").find("div:eq(3)").click(function() {
			$("#dmt2dLvl").find("div:eq(4)").css("left", "48px");
			$("#dmt2dLvl").find("img:eq(0)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(1)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(2)").css("width", "16px");
			$("#dmt2dLvl").find("img:eq(3)").css("width", "15px");

			$("#dmt2dLvl").find("span:eq(1)").html("4");
			dementorAuth.display4();
			dementorAuth.reload();
		});

		$("#dmt2dMoveBtn").find("td").each(
				function(idx) {
					$(this).css(
							"cursor",
							"url('" + imgUrlPath
									+ "auth/dementorCursor.cur'), default");

					$(this).find("img").mouseover(
							function() {
								$(this).attr(
										"src",
										$(this).attr("src")
												.replace("off", "on"));
							});

					$(this).find("img").mouseout(
							function() {
								$(this).attr(
										"src",
										$(this).attr("src")
												.replace("on", "off"));
							});

					$(this).find("img").click(function() {
						if (idx == 0)
							dementorAuth.moveKeyDown("u");
						else if (idx == 1)
							dementorAuth.moveKeyDown("l");
						else if (idx == 2)
							dementorAuth.complateKey();
						else if (idx == 3)
							dementorAuth.moveKeyDown("r");
						else if (idx == 4)
							dementorAuth.moveKeyDown("d");
					});
				});

		$("#dmt2dBtn").find("div").each(function(idx) {
			// $(this).css("cursor", "pointer");

			$(this).find("img").mouseover(function() {
				$(this).attr("src", $(this).attr("src").replace("off", "on"));
			});

			$(this).find("img").mouseout(function() {
				$(this).attr("src", $(this).attr("src").replace("on", "off"));
			});

			$(this).find("img").click(function() {
				if (idx == 0) {
					if (authCnt < 3) {
						alert("스텝 완료 후 눌러주세요.");
						return;
					}

					reqauth();
				} else {
					dementorAuth.reInit();
				}
			});
		});

		$("input:[name=dementorDisplay]").keydown(function() {

			var code = event.which ? event.which : event.keyCode;

			if (code == "38")
				dementorAuth.moveKeyDown("u");
			else if (code == "37")
				dementorAuth.moveKeyDown("l");
			else if (code == "39")
				dementorAuth.moveKeyDown("r");
			else if (code == "40")
				dementorAuth.moveKeyDown("d");
			else if (code == "13")
				dementorAuth.complateKey();
		});

		$("#dementor").click(function() {
			$("input:[name=dementorDisplay]").focus();
		});
	},

	// 보안 1단계 아이콘 배치 함수
	display1 : function() {
		var iconHtml = "<table cellpadding='1' border='0' style='padding:1px 0 0 1px;border-collapse:separate'><tr>";

		var row = 0;
		var cols = 0;

		$
				.each(
						userIcons,
						function(idx, item) {
							cols = idx % 5;

							if (idx > 0 && cols == 0) {
								iconHtml += "</tr><tr>";
								row++;
							}

							iconHtml += "<td id='"
									+ row
									+ "_"
									+ cols
									+ "' style='background-color:#FFF;width:60px;height:59px;text-align:center;vertical-align:middle;border:1px solid #D5D5D5;cursor:url('"
									+ imgUrlPath
									+ "auth/dementorCursor.cur'), default;padding:1px;'"
									+ "onmouseover='javascript:this.style.backgroundColor=\"#F15F5F\"' onmouseout='javascript:this.style.backgroundColor=\"#FFF\"'"
									+ "onmousedown='javascript:dementorAuth.keyMouseMove(this,\""
									+ item.iconurl
									+ "\")'>"
									+ "<img src='"
									+ item.iconurl
									+ "' style='width:56px;height:55px'/></div>"
									+ "</td>";
						});

		iconHtml += "</tr></table>";
		$("#dmt2dKeyLayer").html(iconHtml);

		correntLvl = 0;

		$("#dmt2dMoveBtn").unbind("mouseover");
		$("#dmt2dMoveBtn").unbind("mouseout");
	},

	// 보안 2단계 아이콘 배치 함수
	display2 : function() {
		var iconHtml = "<table cellpadding='1' border='0' style='background-color:#FFF;padding:1px 0 0 1px;border-collapse:separate'><tr>";

		var row = 0;
		var cols = 0;

		var divTop = 35;
		var divLeft = 36;

		$
				.each(
						userIcons,
						function(idx, item) {
							cols = idx % 5;

							if (idx > 0 && cols == 0) {
								iconHtml += "</tr><tr>";
								divLeft = 35;
								divTop += 64;
								row++;
							}

							iconHtml += "<td id='"
									+ row
									+ "_"
									+ cols
									+ "' style='width:62px;height:61px;text-align:center;vertical-align:middle;padding:1px;'>"
									+ "<img src='"
									+ item.iconurl
									+ "' style='width:62px;height:61px'/>"
									+ "<div class='position:relative'><div style='position:absolute;top:"
									+ divTop
									+ "px;left:"
									+ divLeft
									+ "px;"
									+ "opacity:0.6;-moz-opacity:0.6;-khtml-opacity: 0.6;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=60);filter: alpha(opacity=60);'>"
									+ "<img src='" + item.iconurl
									+ "' style='width:30px;height:30px'/>"
									+ "<div></div>" + "</td>";

							divLeft += 66;

							iconUrlArray[row][cols] = item.iconurl;
						});

		iconHtml += "</tr></table>";
		$("#dmt2dKeyLayer").html(iconHtml);

		correntLvl = 1;

		$("#dmt2dMoveBtn").unbind("mouseover");
		$("#dmt2dMoveBtn").unbind("mouseout");
	},

	// 보안 3단계 아이콘 배치 함수
	display3 : function() {
		var iconHtml = "<table cellpadding='1' border='0' style='background-color:#FFF;padding:1px 0 0 1px;border-collapse:separate'><tr>";

		var row = 0;
		var cols = 0;

		$
				.each(
						userIcons,
						function(idx, item) {
							cols = idx % 5;

							if (idx > 0 && cols == 0) {
								iconHtml += "</tr><tr>";
								row++;
							}

							iconHtml += "<td id='"
									+ row
									+ "_"
									+ cols
									+ "' style='width:62px;height:61px;text-align:center;vertical-align:middle;padding:1px;'>"
									+ "<img src='"
									+ item.iconurl
									+ "' style='width:62px;height:61px'/></div>"
									+ "</td>";
						});

		iconHtml += "</tr></table>";
		$("#dmt2dKeyLayer").html(iconHtml);

		correntLvl = 2;

		$("#dmt2dMoveBtn").unbind("mouseover");
		$("#dmt2dMoveBtn").unbind("mouseout");
	},

	// 보안 4단계 아이콘 배치 함수
	display4 : function() {
		var iconHtml = "<table cellpadding='1' border='0' style='background-color:#FFF;padding:1px 0 0 1px;border-collapse:separate'><tr>";

		var row = 0;
		var cols = 0;

		$
				.each(
						userIcons,
						function(idx, item) {
							cols = idx % 5;

							if (idx > 0 && cols == 0) {
								iconHtml += "</tr><tr>";
								row++;
							}

							iconHtml += "<td id='"
									+ row
									+ "_"
									+ cols
									+ "' style='width:62px;height:61px;text-align:center;vertical-align:middle;padding:1px;'>"
									+ "<img src='"
									+ item.iconurl
									+ "' style='width:62px;height:61px'/></div>"
									+ "</td>";
						});

		iconHtml += "</tr></table>";
		$("#dmt2dKeyLayer").html(iconHtml);

		correntLvl = 3;

		$("#dmt2dMoveBtn").mouseover(
				function() {
					$("#multiCursor1").css("display", "block");
					$("#multiCursor2").css("display", "block");
					$("#multiCursor3").css("display", "block");

					$("#multiCursor1").css(
							"cursor",
							"url('" + imgUrlPath
									+ "auth/dementorCursor.cur'), default");
					$("#multiCursor2").css(
							"cursor",
							"url('" + imgUrlPath
									+ "auth/dementorCursor.cur'), default");
					$("#multiCursor3").css(
							"cursor",
							"url('" + imgUrlPath
									+ "auth/dementorCursor.cur'), default");

					$(this).find("img:eq(0)").attr(
							"src",
							$(this).find("img:eq(0)").attr("src").replace(
									"off", "on"));
					$(this).find("img:eq(1)").attr(
							"src",
							$(this).find("img:eq(1)").attr("src").replace(
									"off", "on"));
					$(this).find("img:eq(3)").attr(
							"src",
							$(this).find("img:eq(3)").attr("src").replace(
									"off", "on"));
					$(this).find("img:eq(4)").attr(
							"src",
							$(this).find("img:eq(4)").attr("src").replace(
									"off", "on"));
				});

		$("#dmt2dMoveBtn").mouseout(
				function() {
					$("#multiCursor1").css("display", "none");
					$("#multiCursor2").css("display", "none");
					$("#multiCursor3").css("display", "none");

					$(this).find("img:eq(0)").attr(
							"src",
							$(this).find("img:eq(0)").attr("src").replace("on",
									"off"));
					$(this).find("img:eq(1)").attr(
							"src",
							$(this).find("img:eq(1)").attr("src").replace("on",
									"off"));
					$(this).find("img:eq(3)").attr(
							"src",
							$(this).find("img:eq(3)").attr("src").replace("on",
									"off"));
					$(this).find("img:eq(4)").attr(
							"src",
							$(this).find("img:eq(4)").attr("src").replace("on",
									"off"));
				});

		$("#dmt2dMoveBtn").mousemove(
				function() {
					var btnOffset = $(this).offset();
					var defaultLeft = btnOffset.left;
					var defaultTop = btnOffset.top;

					$("#multiCursor1").css(
							"left",
							event.clientY + $(document).scrollTop()
									- defaultTop - 3);
					$("#multiCursor1").css("top",
							defaultLeft - event.clientX + 142);

					$("#multiCursor2").css(
							"left",
							defaultTop - event.clientY
									- $(document).scrollTop() + 142);
					$("#multiCursor2").css("top",
							event.clientX - defaultLeft - 3);

					$("#multiCursor3").css("left",
							defaultLeft - event.clientX + 142);
					$("#multiCursor3").css(
							"top",
							defaultTop - event.clientY
									- $(document).scrollTop() + 142);
				});

		$("#dmt2dMoveBtn").find("div").each(
				function(idx) {
					$(this).click(function() {
						dementorAuth.complateKey();
					});

					$(this).mouseover(
							function() {
								$("#dmt2dMoveBtn").find("img:eq(2)").attr(
										"src",
										$("#dmt2dMoveBtn").find("img:eq(2)")
												.attr("src").replace("off",
														"on"));
							});

					$(this).mouseout(
							function() {
								$("#dmt2dMoveBtn").find("img:eq(2)").attr(
										"src",
										$("#dmt2dMoveBtn").find("img:eq(2)")
												.attr("src").replace("on",
														"off"));
							});
				});
	},

	// 보안 1단계 시 아이콘 이동 이벤트 함수
	keyMouseMove : function(obj, iconUrl) {
		var row = 0;
		var cols = 0;

		selectKey = obj.id;

		var xy = $("#dmt2dKeyLayer").offset();

		var minX = xy.left + 8;
		var maxX = xy.left + $("#dmt2dKeyLayer").width()
				- $("#dmt2dKeySelect").width() + 3;

		var minY = xy.top + 10;
		var maxY = xy.top + $("#dmt2dKeyLayer").height()
				- $("#dmt2dKeySelect").height() + 2;

		if (minX >= event.clientX - 22)
			$("#dmt2dKeySelect").css("left", minX - xy.left);
		else if (maxX <= event.clientX - 22)
			$("#dmt2dKeySelect").css("left", maxX - xy.left);
		else
			$("#dmt2dKeySelect").css("left", event.clientX - 22 - xy.left);

		if (minY >= event.clientY - 17 + $(document).scrollTop())
			$("#dmt2dKeySelect").css("top", minY - xy.top);
		else if (maxY <= event.clientY - 17 + $(document).scrollTop())
			$("#dmt2dKeySelect").css("top", maxY - xy.top);
		else
			$("#dmt2dKeySelect").css("top",
					event.clientY - 17 - xy.top + $(document).scrollTop());

		$("#dmt2dKeySelect").css("display", "");
		$("#dmt2dKeySelect").css("background-image", "url('" + iconUrl + "')");
		$("#dmt2dKeySelect").css("background-position", "center center");

		$(obj).attr("borderColor", "#D5D5D5");

		$("#dmt2dDisplay")
				.mousemove(
						function() {
							row = 0;
							cols = 0;

							for (var idx = 0; idx < 25; idx++) {
								cols = idx % 5;

								if (idx > 0 && cols == 0)
									row++;

								var tdOffset = $("#" + row + "_" + cols)
										.offset();

								if ((tdOffset.left <= event.clientX && tdOffset.left + 63 >= event.clientX)
										&& (tdOffset.top <= event.clientY
												+ $(document).scrollTop() && tdOffset.top + 65 >= event.clientY
												+ $(document).scrollTop())) {

									$("#" + row + "_" + cols).css(
											"background-color", "#F15F5F");
									moveKey = row + "_" + cols;
								} else {
									$("#" + row + "_" + cols).css(
											"background-color", "#FFF");
								}
							}

							if (minX >= event.clientX - 22)
								$("#dmt2dKeySelect")
										.css("left", minX - xy.left);
							else if (maxX <= event.clientX - 22)
								$("#dmt2dKeySelect")
										.css("left", maxX - xy.left);
							else
								$("#dmt2dKeySelect").css("left",
										event.clientX - 22 - xy.left);

							if (minY >= event.clientY - 17
									+ $(document).scrollTop())
								$("#dmt2dKeySelect").css("top", minY - xy.top);
							else if (maxY <= event.clientY - 17
									+ $(document).scrollTop())
								$("#dmt2dKeySelect").css("top", maxY - xy.top);
							else
								$("#dmt2dKeySelect").css(
										"top",
										event.clientY - 17 - xy.top
												+ $(document).scrollTop());
						});

		$("#dmt2dKeySelect").mouseup(
				function() {
					$("#dmt2dKeySelect").css("display", "none");

					row = 0;
					cols = 0;

					for (var idx = 0; idx < 25; idx++) {
						cols = idx % 5;

						if (idx > 0 && cols == 0)
							row++;

						$("#" + row + "_" + cols).css("background-color",
								"#FFF");
					}

					$("#dmt2dKeySelect").unbind("mousemove");
					$("#dmt2dDisplay").unbind("mousemove");
					$(this).unbind("mouseup");

					if (authCnt >= 3) {
						alert("완료 버튼을 눌러주세요.");
						return;
					}

					if (authCnt < 3 && selectKey != "" && moveKey != ""
							&& selectKey != moveKey) {
						moveCnt++;
						dementorAuth.complateKey();
					}
				});

		$(document).mouseup(function() {
			$("#dmt2dKeySelect").css("display", "none");

			row = 0;
			cols = 0;

			for (var idx = 0; idx < 25; idx++) {
				cols = idx % 5;

				if (idx > 0 && cols == 0)
					row++;

				$("#" + row + "_" + cols).css("background-color", "#FFF");
			}

			$("#dmt2dKeySelect").unbind("mousemove");
			$("#dmt2dDisplay").unbind("mousemove");
			$(this).unbind("mouseup");

			selectKey = "";
			moveKey = "";
			moveCnt = 0;
		});
	},

	// 방향키를 이용한 아이콘 이동 시 위치 셋팅 함수
	moveKeyDown : function(type) {
		if (authCnt >= 3) {
			alert("완료 버튼을 눌러주세요.");
			return;
		}

		if (correntLvl == 1)
			dementorAuth.iconsMove(type);

		if (type == "u") {
			if (rowCnt == 0)
				rowCnt = 4;
			else
				rowCnt--;
		}

		if (type == "d") {
			if (rowCnt == 4)
				rowCnt = 0;
			else
				rowCnt++;
		}

		if (type == "l") {
			if (colsCnt == 0)
				colsCnt = 4;
			else
				colsCnt--;
		}

		if (type == "r") {
			if (colsCnt == 4)
				colsCnt = 0;
			else
				colsCnt++;
		}

		selectKey = "0_0";
		moveKey = rowCnt + "_" + colsCnt;

		moveCnt++;

		$("#dmt2dMoveCnt").html(moveCnt);
	},

	// 보안 2단계 시 아이콘 이동 표시 함수
	iconsMove : function(type) {
		if (type == "u") {
			var row = 0;
			var oldIconUrlArray = new Array(new Array(5), new Array(5),
					new Array(5), new Array(5), new Array(5));

			for (var i = 0; i < 5; i++) {
				for (var cols = 0; cols < 5; cols++) {
					if (i == 4)
						row = 0;
					else
						row = i + 1;

					var iconUrl = iconUrlArray[row][cols];

					$("#" + i + "_" + cols).find("img:eq(1)").attr("src",
							iconUrl);
					oldIconUrlArray[i][cols] = iconUrl;
				}
			}

			iconUrlArray = oldIconUrlArray;
		}

		if (type == "d") {
			var row = 0;
			var oldIconUrlArray = new Array(new Array(5), new Array(5),
					new Array(5), new Array(5), new Array(5));

			for (var i = 0; i < 5; i++) {
				for (var cols = 0; cols < 5; cols++) {
					if (i == 0)
						row = 4;
					else
						row = i - 1;

					var iconUrl = iconUrlArray[row][cols];

					$("#" + i + "_" + cols).find("img:eq(1)").attr("src",
							iconUrl);
					oldIconUrlArray[i][cols] = iconUrl;
				}
			}

			iconUrlArray = oldIconUrlArray;
		}

		if (type == "l") {
			var cols = 0;
			var oldIconUrlArray = new Array(new Array(5), new Array(5),
					new Array(5), new Array(5), new Array(5));

			for (var i = 0; i < 5; i++) {
				for (var j = 0; j < 5; j++) {
					if (j == 4)
						cols = 0;
					else
						cols = j + 1;

					var iconUrl = iconUrlArray[i][cols];

					$("#" + i + "_" + j).find("img:eq(1)").attr("src", iconUrl);
					oldIconUrlArray[i][j] = iconUrl;
				}
			}

			iconUrlArray = oldIconUrlArray;
		}

		if (type == "r") {
			var cols = 0;
			var oldIconUrlArray = new Array(new Array(5), new Array(5),
					new Array(5), new Array(5), new Array(5));

			for (var i = 0; i < 5; i++) {
				for (var j = 0; j < 5; j++) {
					if (j == 0)
						cols = 4;
					else
						cols = j - 1;

					var iconUrl = iconUrlArray[i][cols];

					$("#" + i + "_" + j).find("img:eq(1)").attr("src", iconUrl);
					oldIconUrlArray[i][j] = iconUrl;
				}
			}

			iconUrlArray = oldIconUrlArray;
		}
	},

	// 아이콘 이동 초기화
	iconsInit : function() {
		var row = 0;
		var cols = 0;

		$.each(userIcons, function(idx, item) {
			cols = idx % 5;

			if (idx > 0 && cols == 0) {
				row++;
			}

			$("#" + row + "_" + cols).find("img:eq(1)").attr("src",
					item.iconurl);
			iconUrlArray[row][cols] = item.iconurl;
		});
	},

	// 아이콘 이동 완료 함수
	complateKey : function() {
		if (authCnt >= 3)
			alert("완료 버튼을 눌러주세요.");

		if (authCnt < 3 && moveCnt > 0) {
			var selectRow = selectKey.split("_")[0];
			var selectCols = selectKey.split("_")[1];

			var moveRow = moveKey.split("_")[0];
			var moveCols = moveKey.split("_")[1];

			var moveRowCnt = selectRow - moveRow;
			var moveColsCnt = selectCols - moveCols;

			if (moveColsCnt >= 0) {
				for (var i = 0; i < moveColsCnt; i++)
					authKey += "l";
			} else {
				for (var i = selectCols; i > 0; i--)
					authKey += "l";

				for (var i = 0; i < 5 - moveCols; i++)
					authKey += "l";
			}

			if (moveRowCnt >= 0) {
				for (var i = 0; i < moveRowCnt; i++)
					authKey += "u";
			} else {
				for (var i = selectRow; i > 0; i--)
					authKey += "u";

				for (var i = 0; i < 5 - moveRow; i++)
					authKey += "u";
			}

			authKey += "e";
			authCnt++;
		}

		selectKey = "";
		moveKey = "";

		rowCnt = 0;
		colsCnt = 0;
		moveCnt = 0;

		$("#dmt2dMoveCnt").html(moveCnt);
		$("#dmt2dStep").find("img").css("width", 89 * authCnt + "px");

		if (correntLvl == 1)
			dementorAuth.iconsInit();
	},

	// 초기화 함수
	reload : function() {
		selectKey = "";
		moveKey = "";

		rowCnt = 0;
		colsCnt = 0;
		moveCnt = 0;

		authKey = "";
		authCnt = 0;

		$("#dmt2dMoveCnt").html(moveCnt);
		$("#dmt2dStep").find("img").css("width", "0px");

		if (correntLvl == 1)
			dementorAuth.iconsInit();
	},

	// 재시작 함수
	reInit : function() {
		selectKey = "";
		moveKey = "";
		authKey = "";
		authCnt = 0;
		servertime = "";

		iconUrlArray = new Array(new Array(5), new Array(5), new Array(5),
				new Array(5), new Array(5));
		correntLvl = 0;
		userIcons;

		rowCnt = 0;
		colsCnt = 0;
		moveCnt = 0;

		reInit();
	},

	// 인증키 찾기
	keySearch : function() {
		var jsonParam = {
			data : {
				userId : userId,
				devtype : "0"
			}
		};
		$.ajax({
			type : "POST",
			url : "/dmt2d/emailCheck.do?data=" + JSON.stringify(jsonParam),
			contentType : "text/plain;charset=UTF-8",
			dataType : "json",
			success : function(data) {
				var result = eval(data);
				if (result.code == "102250") { // 사용자 이메일 없을 경우
					alert("힌트 및 이메일이 등록되지 않았습니다.\n설정 페이지로 이동합니다.");
					document.location.href = "/dmt2d/hintUpdateView.do";
				} else { // 사용자 이메일이 등록된 경우
					alert("확인되었습니다.\n찾기 페이지로 이동합니다.");
					document.location.href = "/dmt2d/keySearch.do";
				}
			}
		});
	},
};

// 키 설정(홈페이지)
var dementorSetting = {
	// 초기 설정 함수(사용자 ID, 보안레벨, 아이콘 정보, 공통 이미지Url 경로)
	init : function(category, imgUrl) {
		// 메뉴, 드레그 이벤트 없애기
		$("#dementor").bind("contextmenu", function(event) {
			return false;
		});
		$("#dementor").bind("selectstart", function(event) {
			return false;
		});
		$("#dementor").bind("dragstart", function(event) {
			return false;
		});

		// 파이어폭스용 이벤트 생성
		if (navigator.userAgent.indexOf('Firefox') >= 0) {
			var eventNames = [ "mousedown", "mouseover", "mouseout",
					"mousemove", "mousedrag", "click", "dblclick", "keydown",
					"keypress", "keyup" ];

			for (var i = 0; i < eventNames.length; i++) {
				window.addEventListener(eventNames[i], function(e) {
					window.event = e;
				}, true);
			}
		}

		// 파라미터 전역 변수 셋팅
		imgUrlPath = imgUrl;

		// 기본 화면 그리기 함수 호출
		dementorSetting.defaultDraw();

		// 카테고리 설정
		dementorSetting.cateSetting(category);
	},
	// 기본 레이아웃 생성(배경)
	defaultDraw : function() {

		// 설명 문구
		$("#dementor")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dHelp",
									style : "position:relative;width:600px;margin:12px 0 12px 0;left:4px"
								}));
		$("#dmt2dHelp")
				.append(
						$(
								"<div/>",
								{
									style : "background:url("
											+ serverImgUrl
											+ "common/title1.png) no-repeat left bottom; width:234px; height:33px;"
								}));
		$("#dmt2dHelp")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dHelpText",
									style : "padding:13px 20px 8px 25px;list-style:none;margin:0px auto;"
								}));
		$("#dmt2dHelpText")
				.append(
						$(
								"<li/>",
								{
									style : "padding:0 0 3px 9px; font-size:12px;font-family:'돋움',Dotum,'굴림',Gulim,AppleGothic,Sans-serif;color:#646464;",
									text : "카테고리 별로 25개 이상의 아이콘이 제공되며 카테고리 선택에 따라 다른 아이콘이 나타납니다."
								}));
		$("#dmt2dHelpText")
				.append(
						$(
								"<li/>",
								{
									style : "padding:0 0 3px 9px; font-size:12px;font-family:'돋움',Dotum,'굴림',Gulim,AppleGothic,Sans-serif;color:#646464",
									text : "그래픽인증 서비스에서 인증키로 사용되는 아이콘 4개를 순서대로 클릭합니다."
								}));
		$("#dmt2dHelpText")
				.append(
						$(
								"<li/>",
								{
									style : "padding:0 0 3px 9px; font-size:12px;font-family:'돋움',Dotum,'굴림',Gulim,AppleGothic,Sans-serif;color:#646464",
									text : "선택된 아이콘은 하단에 나타나며 마우스를 해당 위치에 올려놓으면 선택된 아이콘이 보여집니다."
								}));

		$("#dementor")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dSetting",
									style : "position:relative;float:left;width:645px;height:430px;text-align:left;border:1px solid #BABABA;border-radius:5px;"
								}));
		$("#dmt2dSetting")
				.append(
						$(
								"<div/>",
								{
									style : "position:relative;padding:10px 10px 10px 0px;float:left;width:255px"
								}).append($("<img/>", {
							src : imgUrlPath + "keysetting/regtitle1.png"
						})));
		$("#dmt2dSetting").append($("<div/>", {
			style : "position:relative;padding:10px;float:left;"
		}).append($("<img/>", {
			src : imgUrlPath + "keysetting/regtitle2.png"
		})));
		$("#dmt2dSetting")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dCate",
									style : "position:relative;width:260px;height:120px;padding:0 5px;z-index:9999;"
								}));

		$("#dmt2dSetting").append($("<div/>", {
			id : "dmt2dInfo",
			style : "position:relative;float:left;top:66px;left:5px"
		}));
		$("#dmt2dInfo").append($("<img/>", {
			src : imgUrlPath + "keysetting/info01.png"
		})).append("<br/>");
		$("#dmt2dInfo").append($("<img/>", {
			src : imgUrlPath + "keysetting/bar03.png",
			style : "padding-top:5px;width:270px"
		}));

		$("#dmt2dSetting")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dIcon",
									style : "position:relative;height:342px;width:355px;overflow-y:hidden;top:-71px;left:8px;float: left;"
								}));
		$("#dmt2dIcon").append($("<img/>", {
			src : imgUrlPath + "keysetting/key_empty.gif",
			style : "height:342px;width:355px;"
		}));

		$("#dmt2dSetting").append($("<div/>", {
			id : "dmt2dKey",
			style : "position:absolute;left:5px;top:293px;width:265px;"
		}));
		$("#dmt2dKey").append($("<img/>", {
			src : imgUrlPath + "keysetting/hole_none.png",
			style : "cursor:pointer;width:64px;height:64px"
		}));
		$("#dmt2dKey").append($("<img/>", {
			src : imgUrlPath + "keysetting/key1_none.png",
			style : "cursor:pointer;margin-left:3px;width:64px;height:64px"
		}));
		$("#dmt2dKey").append($("<img/>", {
			src : imgUrlPath + "keysetting/key2_none.png",
			style : "cursor:pointer;margin-left:3px;width:64px;height:64px"
		}));
		$("#dmt2dKey").append($("<img/>", {
			src : imgUrlPath + "keysetting/key3_none.png",
			style : "cursor:pointer;margin-left:3px;width:64px;height:64px"
		}));

		$("#dmt2dSetting")
				.append(
						$(
								"<div/>",
								{
									id : "dmt2dBtn",
									style : "position:absolute;width:604px;height:25px;top:400px;text-align:center"
								}));
		$("#dmt2dBtn").append($("<img/>", {
			src : imgUrlPath + "keysetting/but_rekey.png",
			style : "cursor:pointer"
		}).click(function() {
			dementorSetting.keyReset();
		}));
		$("#dmt2dBtn").append($("<img/>", {
			src : imgUrlPath + "keysetting/but_end.png",
			style : "padding-left:5px;cursor:pointer"
		}).click(function() {
			service.keySetting();
		}));
		// 도움말
		$("#dmt2dSetting")
				.append(
						$(
								"<div/>",
								{
									style : "position:absolute;width:60px;height:9px;top:10px;left:599px;cursor:pointer"
								})
								.append(
										$(
												"<img/>",
												{
													src : imgUrlPath
															+ "auth/notice.png"
												})
												.click(
														function() {
															window
																	.open(
																			imgUrlPath
																					+ "help/helpSetting.gif",
																			"helpPopup",
																			"width=612, height=424, menubar=no, status=no, toolbar=no, scrollbars=no, resizable=no, location=no");
														})));

		$("#dementor").append($("<input/>", {
			type : "hidden",
			id : "keys",
			name : "key0",
			value : ""
		}));
		$("#dementor").append($("<input/>", {
			type : "hidden",
			id : "keys",
			name : "key1",
			value : ""
		}));
		$("#dementor").append($("<input/>", {
			type : "hidden",
			id : "keys",
			name : "key2",
			value : ""
		}));
		$("#dementor").append($("<input/>", {
			type : "hidden",
			id : "keys",
			name : "key3",
			value : ""
		}));
	},

	// 카테고리 셋팅
	cateSetting : function(category) {
		$
				.each(
						category,
						function(index, item) {
							$("#dmt2dCate")
									.append(
											$(
													"<img/>",
													{
														id : "dmt2dCate"
																+ item.categoryid,
														src : imgUrlPath
																+ "category/0/"
																+ item.categoryname
																+ ".png",
														style : "width:63px;height:63px;cursor:pointer;padding-left:7px;"
													}));
							$("#dmt2dCate").find("img:eq(" + index + ")")
									.click(
											function() {
												dementorSetting.cateSelect(
														item.iconitem,
														item.categoryid);
											});
						});
	},

	// 카테고리 선택
	cateSelect : function(iconList, categoryid) {
		var iconHtml = "<table style='border:0px;border-collapse:separate;border-spacing:2px;'><tr>";

		$
				.each(
						iconList,
						function(index, item) {
							if (index > 0 && index % 5 == 0)
								iconHtml += "</tr><tr>";

							iconHtml += "<td style='padding:2px;width:63px;height:53px;text-align:center;vertical-align:middle;border:1px solid #D5D5D5;cursor:pointer;background-color:#FFF'"
									+ "onclick='dementorSetting.iconSelect(\""
									+ item.iconurl
									+ "\",\""
									+ item.iconid
									+ "\")'>"
									+ "<img src='"
									+ imgUrlPath
									+ "icon_img/0/"
									+ item.iconurl
									+ "' style='width:60px;height:60px'/></td>";
						});

		iconHtml += "</tr></table>";
		$("#dmt2dIcon").html(iconHtml);
		$("#dmt2dIcon").css("overflow-y", "auto");
	},

	// 아이콘 선택
	iconSelect : function(iconUrl, iconId) {
		// 중복 선택인지 체크
		var cnt = 0;

		for (var i = 0; i < 4; i++) {
			var keyValue = $(":input[name=key" + i + "]").val();
			if (keyValue == iconId) {
				alert("이미 선택한 이미지 입니다.\n다른 이미지를 선택해 주세요.");
				return false;
			}
		}

		for (var i = 0; i < 2; i++) {
			var keyValue = $(":input[name=key" + i + "]").val();
			var nextValue = $(":input[name=key" + (i + 1) + "]").val();
			var nextValue2 = $(":input[name=key" + (i + 2) + "]").val();

			if (keyValue == "" || keyValue == null)
				keyValue = iconId;
			if (nextValue == "" || nextValue == null)
				nextValue = iconId;
			if (nextValue2 == "" || nextValue2 == null)
				nextValue2 = iconId;

			if (keyValue != "" && nextValue != "" && nextValue2 != "") {
				if (parseInt(keyValue) + 1 == parseInt(nextValue))
					cnt++;
				if (parseInt(nextValue) + 1 == nextValue2)
					cnt++;
			}

			if (cnt >= 2) {
				alert("연속된 3개의 이미지를 설정할 수 없습니다.\n다른 이미지를 선택해 주세요.");
				return false;
			} else {
				cnt = 0;
			}
		}

		// 비어 있는 키 셋팅
		$("#dmt2dKey")
				.find("img")
				.each(
						function(index) {
							var keyUrl = this.src;

							if (keyUrl.indexOf("none") > -1) {

								if (index < 3)
									$("#dmt2dInfo").find("img:eq(0)").attr(
											"src",
											imgUrlPath + "keysetting/info0"
													+ (index + 2) + ".png");

								$(":input[name=key" + index + "]").val(iconId);
								this.src = keyUrl.replace("none", "col");
								// 마우스 오버 이벤트
								$(this)
										.mouseover(
												function() {
													this.src = imgUrlPath
															+ "icon_img/0"
															+ iconUrl;
													this.style.backgroundColor = "#FFF";
												});
								// 마우스 아웃 이벤트
								$(this).mouseout(function() {
									this.src = keyUrl.replace("none", "col");
									this.style.backgroundColor = "";
								});
								// 마우스 클릭 이벤트
								$(this)
										.click(
												function() {
													if (confirm("key를 삭제 후 재설정 하시겠습니까?")) {
														this.src = keyUrl;
														$(
																":input[name=key"
																		+ index
																		+ "]")
																.val("");
														$("#dmt2dInfo")
																.find(
																		"img:eq(0)")
																.attr(
																		"src",
																		imgUrlPath
																				+ "keysetting/info0"
																				+ (index + 1)
																				+ ".png");

														$(this).unbind("click");
														$(this).unbind(
																"mouseover");
														$(this).unbind(
																"mouseout");
													}
												});

								return false;
							}
						});
	},

	// 초기화
	keyReset : function() {
		alert("선택한 모든 key를 리셋 후 재설정 합니다.");

		$("#dmt2dIcon").html('');
		$("#dmt2dIcon").append($("<img/>", {
			src : imgUrlPath + "keysetting/key_empty.gif",
			style : "height:306px;width:316px;"
		}));
		$("#dmt2dIcon").css("overflow-y", "");

		$("#dmt2dKey").find("img").each(
				function(index) {

					this.src = this.src.replace("col", "none");
					$(":input[name=key" + index + "]").val("");
					$("#dmt2dInfo").find("img:eq(0)").attr("src",
							imgUrlPath + "keysetting/info01.png");

					$(this).unbind("click");
					$(this).unbind("mouseover");
					$(this).unbind("mouseout");

				});
	}
};
