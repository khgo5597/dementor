﻿var imgUrlPath;
var currentCate;
var totalWidth;
var totalHeight;
var cateLeft;
var selectIconKey;
var stepMsg = new Array();

var servertime = "";

var iconUrlArray = new Array(new Array(5), new Array(5), new Array(5), new Array(5), new Array(5));
var correntLvl = 0;
var userIcons;

var selectKey = "";
var moveKey = "";
var authKey = "";
var authCnt = 0;
var initCnt = 0;

var rowCnt = 0;
var colsCnt = 0;
var moveCnt = 0; 

var selectCateArray = new Array();

//인증
var dementorAuth = {
    //초기 설정 함수(사용자 ID, 보안레벨, 아이콘 정보, 공통 이미지Url 경로)
    init : function(lvl, time, icons, imgUrl) {
        //메뉴, 드레그 이벤트 없애기
        $("#dementor").bind("contextmenu", function(event){return false;});
        $("#dementor").bind("selectstart", function(event){return false;});
        $("#dementor").bind("dragstart", function(event){return false;});
        
        stepMsg[0] = "열쇠1 아이콘을 입력해주세요.";
        stepMsg[1] = "열쇠2 아이콘을 입력해주세요.";
        stepMsg[2] = "열쇠3 아이콘을 입력해주세요.";
        
        //파라미터 전역 변수 셋팅
        imgUrlPath = imgUrl;        
        servertime = time;
        userIcons = icons;

        if(initCnt <= 0) {
            //기본 화면 그리기 함수 호출
            dementorAuth.defaultDraw();
            correntLvl = lvl;
        }
        
        dementorAuth.display();
        initCnt++;
        
        //도움말 생성 및 이벤트
        dementorAuth.authHelp();
    },
    
    //기본 레이아웃 생성(배경)
    defaultDraw : function(category) {
        
        totalWidth = $(window).width();
        totalHeight = $(window).height();

        //기본 레이어
        $("#dementor").append($("<div/>", {id:"dmt2dAuth", style:"width:100%;position:relative;"}));
                                
        //인증 스텝
        $("#dmt2dAuth").append($("<div/>", {id:"dmt2dStep", style:"position:relative;width:100%;text-align:center; padding-top:10px; padding-left:5px;"}));
                
        //아이콘 선택 스탭
        $("#dmt2dStep").append($("<img/>", {src:imgUrlPath+"keysetting/hole_col.png",  style:"width:16%; margin-right:1%;"}));
        $("#dmt2dStep").append($("<img/>", {src:imgUrlPath+"keysetting/key1_col.png",  style:"width:16%; margin-right:1%;"}));
        $("#dmt2dStep").append($("<img/>", {src:imgUrlPath+"keysetting/key2_none.png", style:"width:16%; margin-right:1%;"}));
        $("#dmt2dStep").append($("<img/>", {src:imgUrlPath+"keysetting/key3_none.png", style:"width:16%; margin-right:10%;"}));
        $("#dmt2dStep").append($("<img/>", {src:imgUrlPath+"auth/notice.png", style:"width:16%; padding:0px;"})
                .bind("touchend", function(){
                    $("#dmt2dAuthHelp").css("display","");
                    $("#dmt2dAuth").css("display","none");
                    $("#dmt2dAuthHelp").css("left","0");
                }));
        
        //아이콘 선택 영역
        $("#dmt2dAuth").append($("<div/>", {id:"dmt2dKeyLayer", style:"position:relative; width:"+totalWidth+"px; margin-top:10px;"}));
        $("#dmt2dAuth").append($("<div/>", {id:"dmt2dBtn", style:"position:relative;width:100%;height:25px;top:10px;text-align:center"}));
        $("#dmt2dBtn").append($("<img/>", {src:imgUrlPath+"auth/reload_off.png", style:"width:25%;"})
                .bind("touchend", function(){
                    dementorAuth.reload();                   
                }));
        $("#dmt2dBtn").append($("<img/>", {src:imgUrlPath+"auth/complate_off.png", style:"width:25%; padding-left:10px;"})
                .bind("touchend", function(){
                    reqauth();
                })); 
        
        //아이콘 MOVE 이미지 레이어 생성
        $("#dmt2dAuth").append($("<div/>", {id:"dmt2dKeySelect"}));
        $("#dmt2dKeySelect").attr("style", "position:absolute;width:"+(totalWidth/5-12)+"px;height:"+(totalWidth/5-12)+"px;display:none;padding:3px;z-index:9999");
        $("#dmt2dKeySelect").css("opacity", "0.6");
        $("#dmt2dKeySelect").css("-moz-opacity", "0.6");
        $("#dmt2dKeySelect").css("-khtml-opacity", "0.6");
        $("#dmt2dKeySelect").css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity=60)");
        $("#dmt2dKeySelect").css("filter", "alpha(opacity=60);");
        
        dementorSetting.fadeEvent(0);
        dementorSetting.fadeEvent(1);
    }, 
    
    //보안 1단계 아이콘 배치 함수
    display : function() {
        var row = 0;
        var cols = 0;
        
        $("#dmt2dKeyLayer").html("");
        $("#dmt2dKeyLayer").append($("<table/>", {id:"dmt2dKeyTable", style:"border:0;width:100%"}));
        $("#dmt2dKeyTable").append($("<tr/>", {id:"dmt2d_tr_"+row}));
        
        $.each(userIcons, function(idx, item){
            cols = idx%5;
            
            if(idx > 0 && cols == 0) {
                row++;
                $("#dmt2dKeyTable").append($("<tr/>", {id:"dmt2d_tr_"+row}));
            } 
            
            $("#dmt2d_tr_"+row).append($("<td/>", {id:row+"_"+cols, style:"width:"+(totalWidth/5)+"px;text-align:center;padding:2px 4px 3px 4px;"})
                    .append($("<div/>", {style:"width:"+(totalWidth/5-12)+"px;text-align:center; border:1px solid #D5D5D5;"})
                    .append($("<img/>", {src:item.iconurl, style:"width:"+(totalWidth/5-12)+"px"}))));
            
            $("#"+row+"_"+cols).bind('touchstart', function(e) {
                selectKey = $(this).attr("id");

                var event = e.originalEvent;
                
                var tdOffset = $(this).offset();
                var divOffset = $("#dmt2dAuth").offset();
                
                $('#dmt2dKeySelect').css("left", tdOffset.left - divOffset.left);
                $('#dmt2dKeySelect').css("top", tdOffset.top - divOffset.top);
                
                moveTop = event.touches[0].pageY;
                moveLeft = event.touches[0].pageX;
                
                iconTop = tdOffset.top;
                iconLeft = tdOffset.left;
                
                $("#dmt2dKeySelect").css("display", "");
                $("#dmt2dKeySelect").css("background-image", "url('"+item.iconurl+"')");
                $("#dmt2dKeySelect").css("background-position", "center center");
                $("#dmt2dKeySelect").css("background-repeat", "no-repeat");
                $("#dmt2dKeySelect").css("background-size", (totalWidth/5-12)+"px");
                
                e.preventDefault(); //  이벤트취소
            });
        });
        
        $("#dmt2dKeyTable").bind('touchmove', function(e) {
            var event = e.originalEvent;
            
            var divOffset = $("#dmt2dAuth").offset();
            
            var top = iconTop - (moveTop - event.touches[0].pageY) - divOffset.top;
            var left = iconLeft - (moveLeft - event.touches[0].pageX) - divOffset.left;
            
            $('#dmt2dKeySelect').css("left", left);
            $('#dmt2dKeySelect').css("top", top);
            
            moveX = event.touches[0].pageX+$(document).scrollLeft();
            moveY = event.touches[0].pageY+$(document).scrollTop();

            e.preventDefault(); //  이벤트취소
        });
        
        $("#dmt2dKeyTable").bind('touchend', function(e) {
            $("#dmt2dKeySelect").css("display", "none");

            var moveRow = 0;
            var moveCols = 0;

            for(var idx=0;idx<25;idx++) {
                moveCols = idx%5;
                
                if(idx > 0 && moveCols == 0)
                    moveRow++;
                
                var tdOffset = $("#"+moveRow+"_"+moveCols).offset();

                if((tdOffset.left <=  moveX && tdOffset.left+(totalWidth/5-12) >= moveX) &&
                        (tdOffset.top <= moveY && tdOffset.top+(totalWidth/5-12) >= moveY)) {
                    
                    moveKey = moveRow+"_"+moveCols;
                }
            }
            
            if(authCnt < 3 && selectKey != "" && moveKey != "" && selectKey != moveKey) {
                moveCnt++;
                dementorAuth.complateKey();
            } 

            if(authCnt==1) {
                dementorSetting.fadeStop(0);
                dementorSetting.fadeStop(1);
                $("#dmt2dStep").find("img:eq("+(authCnt+1)+")").attr("src", $("#dmt2dStep").find("img:eq("+(authCnt+1)+")").attr("src").replace("none", "col"));
                dementorSetting.fadeEvent(2);
            } else if (authCnt==2) {
                dementorSetting.fadeStop(2);
                $("#dmt2dStep").find("img:eq("+(authCnt+1)+")").attr("src", $("#dmt2dStep").find("img:eq("+(authCnt+1)+")").attr("src").replace("none", "col"));
                dementorSetting.fadeEvent(3);
            } else if (authCnt==3) {
                dementorSetting.fadeStop(3);
            }
            
            $("#dmt2dStepMsg").html(stepMsg[authCnt]);
            
            e.preventDefault(); //  이벤트취소
        });
        
        $(document).bind('touchend', function(e) {
            $("#dmt2dKeySelect").css("display", "none");
            
            selectKey = "";
            moveKey = "";
            
            e.preventDefault(); //  이벤트취소
        });
    },
    
    //아이콘 이동 완료 함수
    complateKey : function() {
        
        if(authCnt < 3 && moveCnt > 0) {
            var selectRow = selectKey.split("_")[0];
            var selectCols = selectKey.split("_")[1];
            
            var moveRow = moveKey.split("_")[0];
            var moveCols = moveKey.split("_")[1];
            
            var moveRowCnt = selectRow - moveRow;
            var moveColsCnt = selectCols - moveCols;
            
            if(moveColsCnt >= 0) {
                for(var i=0;i<moveColsCnt;i++)
                    authKey += "l";
            } else {
                for(var i=selectCols;i>0;i--)
                    authKey += "l";
                
                for(var i=0;i<5-moveCols;i++)
                    authKey += "l";
            }
            
            if(moveRowCnt >= 0) {
                for(var i=0;i<moveRowCnt;i++)
                    authKey += "u";
            } else {
                for(var i=selectRow;i>0;i--)
                    authKey += "u";
                
                for(var i=0;i<5-moveRow;i++)
                    authKey += "u";
            }
            
            authKey += "e";
            authCnt++;
        } 

        selectKey = "";
        moveKey = "";
        
        rowCnt = 0;
        colsCnt = 0;
    },
    
    //초기화 함수
    reload : function() {
        selectKey = "";
        moveKey = "";
        authKey = "";
        authCnt = 0;
        servertime = "";

        iconUrlArray = new Array(new Array(5), new Array(5), new Array(5), new Array(5), new Array(5));
        userIcons;

        rowCnt = 0;
        colsCnt = 0;
        
        $("#dmt2dStep").find("img:eq(1)").attr("src", imgUrlPath+"keysetting/key1_col.png");
        $("#dmt2dStep").find("img:eq(2)").attr("src", imgUrlPath+"keysetting/key2_none.png");
        $("#dmt2dStep").find("img:eq(3)").attr("src", imgUrlPath+"keysetting/key3_none.png");
        
        $("#dmt2dStepMsg").html(stepMsg[0]);
        
        dementorSetting.fadeStop(0);
        dementorSetting.fadeStop(1);
        dementorSetting.fadeStop(2);
        dementorSetting.fadeStop(3);
        
        dementorSetting.fadeEvent(0);
        dementorSetting.fadeEvent(1);
        
        reInit();
    },
    
    //도움말 생성 및 이벤트
    authHelp : function() {
        $("#dementor").append($("<div/>", {id:"dmt2dAuthHelp", style:"position:absolute;width:"+totalWidth+"px;height:"+totalHeight+"px;z-index:9999;display:none"}));
        
        $("#dmt2dAuthHelp").append($("<div/>", {style:"position:absolute;width:"+totalWidth+"px;height:"+totalHeight+"px; background-color:rgba(0, 0, 0, 0.7);"})
                .append($("<img/>", {src:imgUrlPath+"help/helpMobileAuth.gif", style:"width:"+totalWidth+"px; padding-top:10px;"}))
                .append($("<div/>", {style:"position:absolute;width:100%;height:40px;top:"+(totalHeight-40)+"px;z-index:9999;"})
                        .append($("<img/>", {src:imgUrlPath+"help/close_btn.png", style:"float:right;width:60px;margin:5px 10px 5px 5px"})
                                .bind("touchend", function(){
                                    $("#dmt2dAuthHelp").css("display", "none");
                                    $("#dmt2dAuth").css("display", "");
                                    $("#dmt2dAuthHelp").css("left", "0");
                                }))));
    }
};

//키 등록
var dementorSetting = {
    init : function(category, imgUrl) {
        //메뉴, 드레그 이벤트 없애기
        $("#dementor").bind("contextmenu", function(event){return false;});
        $("#dementor").bind("selectstart", function(event){return false;});
        $("#dementor").bind("dragstart", function(event){return false;});
        
        stepMsg[0] = "자물쇠 아이콘을 선택하세요.";
        stepMsg[1] = "열쇠1 아이콘을 선택하세요.";
        stepMsg[2] = "열쇠2 아이콘을 선택하세요.";
        stepMsg[3] = "열쇠3 아이콘을 선택하세요.";
        
        //파라미터 전역 변수 셋팅
        imgUrlPath = imgUrl;
        
        //기본 화면 그리기 함수 호출
        dementorSetting.defaultDraw();
        
        //카테고리 및 아이콘 생성 및 이벤트
        dementorSetting.cateIconSetting(category);
        
        //도움말 생성 및 이벤트
        dementorSetting.settingHelp();
    }, 

    //기본 레이아웃 생성(배경)
    defaultDraw : function(category) {
        
        totalWidth = $(window).width();
        totalHeight = $(window).height();
        
        //기본 레이어
        $("#dementor").append($("<div/>", {id:"dmt2dSetting", style:"width:100%;position:relative;overflow:hidden"}));
        
        //아이콘 선택 스탭
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dStep", style:"position:relative;padding-top:10px; padding-left:5%; width:100%;"})
                .append($("<img/>", {src:imgUrlPath+"keysetting/hole_col.png",  style:"width:16%; margin-right:1%;"})
                        .bind("touchend", function(){
                            dementorSetting.selectIconView(this, 1);
                        }))
                .append($("<img/>", {src:imgUrlPath+"keysetting/key1_none.png", style:"width:16%; margin-right:1%;"})
                        .bind("touchend", function(){
                            dementorSetting.selectIconView(this, 2);
                        }))
                .append($("<img/>", {src:imgUrlPath+"keysetting/key2_none.png", style:"width:16%; margin-right:1%;"})
                        .bind("touchend", function(){
                            dementorSetting.selectIconView(this, 3);
                        }))
                .append($("<img/>", {src:imgUrlPath+"keysetting/key3_none.png", style:"width:16%; margin-right:10%;"})
                        .bind("touchend", function(){
                            dementorSetting.selectIconView(this, 4);
                        }))
                .append($("<img/>", {src:imgUrlPath+"auth/notice.png", style:"width:16%; padding:0px;"})
                        .bind("touchend", function(){
                            $("#dmt2dSettingHelp").css("display", "");
                            $("#dmt2dSetting").css("display", "none");
                            $("#dmt2dSettingHelp").css("left", "0");
                        })));
                
        //아이콘 확인 레이어
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dSelectIcon1", style:"position:absolute;width:100%;z-index:9999; top:10px;left:5%;display:none;"+
            "width:16%;text-align:center; " +
            "-webkit-border-radius:16%;-moz-border-radius:16%;"})
                .append($("<img/>", {src:imgUrlPath+"keysetting/hole_none.png", style:"width:100%; vertical-align:middle;"})));
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dSelectIcon2", style:"position:absolute;width:100%;z-index:9999; top:10px;left:22%;display:none;"+
            "width:16%;text-align:center;" +
            "-webkit-border-radius:16%;-moz-border-radius:16%;"})
                .append($("<img/>", {src:imgUrlPath+"keysetting/key1_none.png", style:"width:100%; vertical-align:middle;"})));
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dSelectIcon3", style:"position:absolute;width:100%;z-index:9999; top:10px;left:39%;display:none;"+
            "width:16%;text-align:center;" +
            "-webkit-border-radius:16%;-moz-border-radius:16%;"})
                .append($("<img/>", {src:imgUrlPath+"keysetting/key2_none.png", style:"width:100%; vertical-align:middle;"})));
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dSelectIcon4", style:"position:absolute;width:100%;z-index:9999; top:10px;left:56%;display:none;"+
            "width:16%;text-align:center;" +
            "-webkit-border-radius:16%;-moz-border-radius:16%;"})
                .append($("<img/>", {src:imgUrlPath+"keysetting/key3_none.png", style:"width:100%; vertical-align:middle;"})));

        //아이콘 선택 스텝Bar
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dStepBar", style:"position:relative;width:100%;"}));
         
        //카테고리 선택 영역
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dCate", style:"background-size:100% " + ((totalWidth * 0.15) + 10) + "px;position:relative;width:100%;height:" + ((totalWidth * 0.15) + 10) + "px;text-align:center"}));
        

        $("#dmt2dCate").append($("<div/>", {id:"dmt2dCateBg", style:"width:100%;position:absolute;display:none"}));
                //.append($("<img/>", {src:imgUrlPath+"display/cat_blue.png", style:"width:76px;height:44px;margin-top:3px;"})));

        $("#dmt2dCate").append($("<div/>", {id:"dmt2dCateIcon", style:"position:absolute;width:100%;height:58px;z-index:9998"}));
        
        //아이콘 선택 영역
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dIcon", style:"position:relative;width:"+totalWidth+"px;overflow:hidden"}));
        
        $("#dmt2dIcon").css("height", totalWidth+"px");
        $("#dmt2dIcon").append($("<div/>", {id:"dmt2dIconBg", style:"position:absolute;overflow-x:hidden;width:100%;height:"+totalWidth+"px"}));
        
        //아이콘 선택 영역
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dKeyLayer", style:"position:relative;width:100%;z-index:9999;display:none"}));

        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dBtn", style:"position:relative;width:100%; height: 50px; top:10px;text-align:center"}));
        $("#dmt2dBtn").append($("<img/>", {src:imgUrlPath+"keysetting/btn_rekey_none.png", style:"width:25%;"})
                .bind("touchend", function(){
                    service.goBack();                           
                }));
        $("#dmt2dBtn").append($("<img/>", {src:imgUrlPath+"keysetting/btn_end_none.png", style:"width:25%; padding-left:10px;"})
                .bind("touchend", function(){
                    service.keySetting();
                })); 
        
        //아이콘 MOVE 이미지 레이어 생성
        $("#dmt2dSetting").append($("<div/>", {id:"dmt2dKeySelect"}));
        $("#dmt2dKeySelect").attr("style", "position:absolute;width:"+(totalWidth/5-12)+"px;height:"+(totalWidth/5-12)+"px;display:none;padding:3px;z-index:9998;");
        $("#dmt2dKeySelect").css("opacity", "0.6");
        $("#dmt2dKeySelect").css("-moz-opacity", "0.6");
        $("#dmt2dKeySelect").css("-khtml-opacity", "0.6");
        $("#dmt2dKeySelect").css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity=60)");
        $("#dmt2dKeySelect").css("filter", "alpha(opacity=60);");
        
        $("#dementor").append($("<input/>", {type:"hidden", id:"keys", name:"key0", value:""}));
        $("#dementor").append($("<input/>", {type:"hidden", id:"keys", name:"key1", value:""}));
        $("#dementor").append($("<input/>", {type:"hidden", id:"keys", name:"key2", value:""}));
        $("#dementor").append($("<input/>", {type:"hidden", id:"keys", name:"key3", value:""}));
        
        dementorSetting.fadeEvent(0);
        
    }, 
    
    //카테고리 및 아이콘 및 이벤트 생성
    cateIconSetting : function(category) {
        var dmt2dIconHeight = totalWidth;
        
		$("#dmt2dIcon").css("width", (totalWidth * category.length) + "px");
        $.each(category, function(index, item){
            
            //카테고리 생성
            $("#dmt2dCateIcon").append($("<img/>", {id:"dmt2dCate"+item.categoryid, src:imgUrlPath+"category/0/"+item.categoryname+".png", 
                style:"width:15%;height:" + (totalWidth * 0.15) + "px;margin-top:0px;padding:5px 5px 0 5px;"})
                .bind("touchend", function(){
                    
                    $("#dmt2dIconBg").animate({
                        left:-(totalWidth*index)
                    }, 200); 

                    $("#dmt2dCateIcon").find("img:eq(0)").attr("src", $("#dmt2dCateIcon").find("img:eq(0)").attr("src").replace("col", "none"));
                    $("#dmt2dCateIcon").find("img:eq(1)").attr("src", $("#dmt2dCateIcon").find("img:eq(1)").attr("src").replace("col", "none"));
                    $("#dmt2dCateIcon").find("img:eq(2)").attr("src", $("#dmt2dCateIcon").find("img:eq(2)").attr("src").replace("col", "none"));
                    
                    $("#dmt2dCateIcon").find("img:eq("+index+")").attr("src", $("#dmt2dCateIcon").find("img:eq("+index+")").attr("src").replace("none", "col"));
                    /*
                    $("#dmt2dCateBg").animate({
                        left:cateLeft+(76*index)
                    }, 100);
                    */
                }));
            
            $("#dmt2dCateIcon").find("img:eq(0)").attr("src", $("#dmt2dCateIcon").find("img:eq(0)").attr("src").replace("none", "col"));
            //var start = new Date().getTime();
            //while (new Date().getTime() < start + 4500);
            
            //카테고리 별 아이콘 생성
            $("#dmt2dIconBg").append($("<div>", {id:"dmt2dIcon"+item.categoryid, style:"position:absolute;width:"+totalWidth+"px;height:"+dmt2dIconHeight+"px;left:"+(totalWidth*index)+"px;overflow-x:hidden;"}));
            $("#dmt2dIcon"+item.categoryid).append($("<table/>", {id:"dmt2dIconTable"+item.categoryid, style:"border:0;width:100%"}));
            var row = 0
            var cols = 0;
            $("#dmt2dIconTable"+item.categoryid).append($("<tr/>", {id:"dmt2dIconTable"+item.categoryid+"_"+row}));
            
            $.each(item.iconitem, function(idx, icon){
                cols = idx%5;
                
                if(idx > 0 && cols == 0) {
                    row++;
                    $("#dmt2dIconTable"+item.categoryid).append($("<tr/>", {id:"dmt2dIconTable"+item.categoryid+"_"+row}));
                } 
                
                $("#dmt2dIconTable"+item.categoryid+"_"+row).append($("<td/>", {style:"width:"+(totalWidth/5)+"px;text-align:center;padding:2px 2px 1px 2px"})
                        .append($("<div/>", {style:"background-color:#FFFFFF;border:1px solid #D5D5D5;width:"+(totalWidth/5-9)+"px;text-align:center;"})
                        .append($("<img/>", {src:imgUrlPath+"icon_img/0"+icon.iconurl, style:"width:"+(totalWidth/5-9)+"px"})
                                .bind("click", function(){
                                    dementorSetting.iconSelect(icon.iconurl, icon.iconid, item.categoryid, category);
                                }))));
            });
            
        });
        
        var cateImgOffset = $("#dmt2dCateIcon").find("img:eq(0)").offset();
        $("#dmt2dCateBg").css("left", cateImgOffset.left+"px");
        
        var cateImgOffset = $("#dmt2dCateIcon").find("img:eq(0)").offset();
        $("#dmt2dCateBg").css("left", (cateImgOffset.left)+"px");
        $("#dmt2dCateBg").css("display", "");
        
        cateLeft = cateImgOffset.left;
        
        $("#dmt2dIconBg").bind("swiperight", function() {
            for(var i=0;i<3;i++) {
                if(($("#dmt2dCateIcon").find("img:eq("+i+")").attr("src").indexOf('col')) > 0) {
                    var imgEq = i;
                    dementorSetting.cateIconMoveEvent("left", imgEq);
                    break;
                }
            }   
        });
        
        $("#dmt2dIconBg").bind("swipeleft", function() {
            for(var i=0;i<3;i++) {
                if(($("#dmt2dCateIcon").find("img:eq("+i+")").attr("src").indexOf('col')) > 0) {
                    var imgEq = i;
                    dementorSetting.cateIconMoveEvent("right", imgEq);
                    break;
                }
            }       
        });
        
    }, 
    
    //카테고리 별 아이콘 이동
    cateIconMoveEvent : function(moveType, imgEq) {
        if(moveType == "right") {
            var iconBgLeft = ($("#dmt2dIconBg").offset()).left;
            var cateBgLeft = ($("#dmt2dCateBg").offset()).left;
            var nextImg = imgEq + 1;
            
            if(iconBgLeft <=  0 && iconBgLeft >= (totalWidth*-1)) {
                iconBgLeft = iconBgLeft-(totalWidth*1);
                cateBgLeft = cateBgLeft+(76*1);

                $("#dmt2dIconBg").animate({
                    left:iconBgLeft
                }, 200);
                
                $("#dmt2dCateIcon").find("img:eq("+imgEq+")").attr("src", $("#dmt2dCateIcon").find("img:eq("+imgEq+")").attr("src").replace("col", "none"));
                $("#dmt2dCateIcon").find("img:eq("+nextImg+")").attr("src", $("#dmt2dCateIcon").find("img:eq("+nextImg+")").attr("src").replace("none", "col"));
                
            }
                        
            $("#dmt2dIconBg").find("div").each(function(){
                $(this).scrollTop(0);
            });
        } else if(moveType == "left") {
            var iconBgLeft = ($("#dmt2dIconBg").offset()).left;
            var cateBgLeft = ($("#dmt2dCateBg").offset()).left;
            var prevImg = imgEq - 1;
            
            if(iconBgLeft <  0) {
                iconBgLeft = iconBgLeft+(totalWidth*1);
                cateBgLeft = cateBgLeft-(76*1);
                                
                $("#dmt2dIconBg").animate({
                    left:iconBgLeft
                }, 200);
                
                $("#dmt2dCateIcon").find("img:eq("+imgEq+")").attr("src", $("#dmt2dCateIcon").find("img:eq("+imgEq+")").attr("src").replace("col", "none"));
                $("#dmt2dCateIcon").find("img:eq("+prevImg+")").attr("src", $("#dmt2dCateIcon").find("img:eq("+prevImg+")").attr("src").replace("none", "col"));
                
            }
            
            $("#dmt2dIconBg").find("div").each(function(){
                $(this).scrollTop(0);
            });
        }
    },
        
    //도움말 생성 및 이벤트
    settingHelp : function() {
        $("#dementor").append($("<div/>", {id:"dmt2dSettingHelp", style:"position:absolute;width:"+(totalWidth*4)+"px;height:"+totalHeight+"px;z-index:9998;display:none"}));
        
        $("#dmt2dSettingHelp").append($("<div/>", {style:"position:absolute;left:0px;width:"+totalWidth+"px;height:"+totalHeight+"px; background-color:rgba(0, 0, 0, 0.7);"})
                .append($("<img/>", {src:imgUrlPath+"help/helpMobileSetting.gif", style:"width:"+totalWidth+"px; padding-top:10px;"}))
                .append($("<div/>", {style:"position:absolute;width:100%;height:40px;top:"+(totalHeight-40)+"px;z-index:9999;"})
                .append($("<img/>", {src:imgUrlPath+"help/close_btn.png", style:"float:right;width:60px;margin:5px 10px 5px 5px"})
                .bind("click", function(){
                    $("#dmt2dSettingHelp").css("display", "none");
                    $("#dmt2dSetting").css("display", "");
                    $("#dmt2dSettingHelp").css("left", "0");
                }))));
    },
    
    //아이콘 선택
    iconSelect : function(iconUrl, iconId, categoryId, category) {

        var cnt = 0;
        //중복 선택인지 체크
        for(var i=0;i<4;i++) {
            var keyValue = $(":input[name=key"+i+"]").val();
            if(keyValue == iconId) {
                alert("이미 선택한 이미지 입니다.\n다른 이미지를 선택해 주세요.");
                return false;
            }
        }

        for(var i=0;i<2;i++) {
            var keyValue = $(":input[name=key"+i+"]").val();
            var nextValue = $(":input[name=key"+(i+1)+"]").val();
            var nextValue2 = $(":input[name=key"+(i+2)+"]").val();
            
            if(keyValue == "" || keyValue == null) keyValue = iconId;
            if(nextValue == "" || nextValue == null) nextValue = iconId;
            if(nextValue2 == "" || nextValue2 == null) nextValue2 = iconId;
            
            if(keyValue != "" && nextValue != "" && nextValue2 != "") {
                if(parseInt(keyValue)+1 == parseInt(nextValue))
                    cnt++;
                if(parseInt(nextValue)+1 == nextValue2)
                    cnt++;
            }

            if(cnt >= 2) {
                alert("연속된 3개의 이미지를 설정할 수 없습니다.\n다른 이미지를 선택해 주세요.");
                return false;
            } else {
                cnt = 0;
            }
        }
        
        if(selectCateArray.length == 0) {
            selectCateArray[0] = categoryId;
        } else {
            var selectCate = false;
            for(var i=0;i<selectCateArray.length;i++) {
                if(selectCateArray[i] == categoryId) {
                    selectCate = true;
                    break;
                }   
            }
            
            if(!selectCate) 
                selectCateArray[selectCateArray.length] = categoryId;
        }
        
/*      for(var i=0;i<4;i++) {
//          if($(":input[name=key"+i+1+"]").val() == "" || $(":input[name=key"+i+1+"]").val() == null) {
                $(":input[name=key"+i+"]").val(iconId);
                $("#dmt2dStep").find("img:eq("+i+1+")").attr("src", $("#dmt2dStep").find("img:eq("+i+1+")").attr("src").replace("none", "col"));
                dementorSetting.fadeStop(i);                
                dementorSetting.fadeEvent(i+1);
                $("#dmt2dStep").find("img:eq("+i+")").attr("src", $("#dmt2dStep").find("img:eq("+i+")").attr("src").replace("none", "col"));
                $("#dmt2dSetting").find("h3").html(stepMsg[(i+1)]);
                $("#dmt2dStepBar").find("img").attr("src", imgUrlPath+"display/"+(i+1)+"step_line.png");
                
                $("#dmt2dSelectIcon"+(i)).find("img").attr("src", imgUrlPath+"icon_img/0/"+iconUrl);             
                break;
//          }
        }
*/

        
        for(var i=0;i<4;i++) {
            if($(":input[name=key"+i+"]").val() == "" || $(":input[name=key"+i+"]").val() == null) {
                var fade = i+1;
                $(":input[name=key"+i+"]").val(iconId);
                dementorSetting.fadeStop(i);
                if(fade<4){
                    $("#dmt2dStep").find("img:eq("+fade+")").attr("src", $("#dmt2dStep").find("img:eq("+fade+")").attr("src").replace("none", "col"));
                    dementorSetting.fadeEvent(i+1);
                }

                $("#dmt2dStepBar").find("img").attr("src", imgUrlPath+"display/"+(i+1)+"step_line.png");
                $("#dmt2dSetting").find("h3").html(stepMsg[(i+1)]);
                
                $("#dmt2dSelectIcon"+(i+1)).find("img").attr("src", imgUrlPath+"icon_img/0/"+iconUrl);
                break;
            }            
        }
    },
    
    //선택한 아이콘 확인
    selectIconView : function(obj, idx) {
        if(obj.src.indexOf("col") > -1) {
            $("#dmt2dSelectIcon"+idx).css("display", "");
            setTimeout(function(){
                $("#dmt2dSelectIcon"+idx).css("display", "none");
            }, 2000);
        }
    },
    
    //아이콘 fadeInOut 이벤트 처리
    fadeEvent : function (imgEq) {
        function fadeInOut(imgEq) {
            $("#dmt2dStep").find("img:eq("+imgEq+")").fadeTo(300, 1, function () {                   
                $("#dmt2dStep").find("img:eq("+imgEq+")").fadeTo(400, 0.3, function () {
                    $("#dmt2dStep").find("img:eq("+imgEq+")").fadeTo(400, 1, function () {                           
                        setTimeout(fadeInOut(imgEq), 50);                           
                    });                     
                });         
            });
        }       
        fadeInOut(imgEq);
    },
    
    fadeStop : function (imgEq) {
        $("#dmt2dStep").find("img:eq("+imgEq+")").fadeTo(0, 1, function (){},$("#dmt2dStep").find("img:eq("+imgEq+")").stop());
    }

};