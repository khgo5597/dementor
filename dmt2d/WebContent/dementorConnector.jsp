﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
/**  
 * @Description : 그래픽인증 안내 페이지
 * @Modification Information  
 * @
 * @  수정일      		 수정자                       수정내용
 * @ ----------     ---------     -------------------------------
 * @ 2014-12-13     개발팀                        최초생성
 * 
 * @author 개발팀
 * @since 2014-12-13
 * @version 1.0
 */
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<title>그래픽인증 - 동국대학교</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no' />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<%
   userid = request.getParameter("userId");
   //if(userid=="" || userid == null) userid=(String)session.getAttribute("userId");
%>
<script type="text/javascript">

	var service = {
		//키 설정 페이지 이동
		keySetting : function() {
		    var url = "/dmt2d/setting.do?userid=<%=userid%>&requestType=middle";
            location.href = url;
		},
		
		//키 설정 페이지 이동
		display : function() {
		    var url = "/dmt2d/display.do?userid=<%=userid%>&requestType=middle";
            location.href = url;
		},
		
		//인증요청
		userCheck : function() {
			$.ajax({
				url : "/dmt2d/userCheck.do",
				contentType : "text/plain;charset=UTF-8",
				dataType : "json",
				data : {userid:"<%=userid%>"},
				success : function(data) {
					var result = eval(data);
					if(result.resultCode) {
					    service.display();					    
					} else {
					    service.keySetting();
					}
				}
			});
		}
		
	};
    
    service.userCheck();
</script>
</head>
<body style="margin:0 0 0 0"><input type="hidden" name="sendMessage" id="hiddenSendMessage" value=""></body>
</html>