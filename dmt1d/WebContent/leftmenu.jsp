<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.Const"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>

<%
String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5003");
String msg2= CodeMessageHandler.getInstance().getCodeMessage("5004");
String msg3= CodeMessageHandler.getInstance().getCodeMessage("5005");
String msg4= CodeMessageHandler.getInstance().getCodeMessage("5006");
String msg5= CodeMessageHandler.getInstance().getCodeMessage("5007");
String msg6= CodeMessageHandler.getInstance().getCodeMessage("5008");
String msg7= CodeMessageHandler.getInstance().getCodeMessage("5009");
String msg8= CodeMessageHandler.getInstance().getCodeMessage("5010");
String msg9= CodeMessageHandler.getInstance().getCodeMessage("5011");
String msg10= CodeMessageHandler.getInstance().getCodeMessage("5012");
String msg11= CodeMessageHandler.getInstance().getCodeMessage("5013");
String msg12= CodeMessageHandler.getInstance().getCodeMessage("5014");
String msg13= CodeMessageHandler.getInstance().getCodeMessage("5015");
String msg14= CodeMessageHandler.getInstance().getCodeMessage("5016");
String msg15= CodeMessageHandler.getInstance().getCodeMessage("5135");
%>
<%-- Check session --%><%@include file="/jsp/common/anticache.jsp"%>
<%@include file="/jsp/common/userAttribute.jsp"%>
<%
int processCode = 0;
if(request.getAttribute("RESULT") != null) {
	ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
	processCode = resultEntity.getProcessCode();
}
%>
<script language="JavaScript">


	if("<%= dmt1d_AUTH%>" == "null") { 
	
		if("<%= processCode %>" == "<%= Const.KEY_EDIT %>") {
		
			var win = window.open("/dmt1d/cate/Cate.do?cmd=keyedit&status=client&userId=<%=USER_ID%>",'auth_level_2_reset','width=800, height=800, scrollbars=auto, resizable=no');
			win.moveTo(0,0);
			win.focus();
		
		} else {
			var win = window.open('about:blank','auth_level_2','width=351, height=535, scrollbars=auto, resizable=no');
			var F = document.form2;
			F.action = "/dmt1d/client/Client.do";
			F.target = "auth_level_2";
			F.cmd.value = "display";
			F.userId.value = "<%= USER_ID%>";
			F.submit();
			win.focus();
		}
	} else if ("<%= processCode %>" == "<%= Const.KEY_EDIT %>"){

			var win = window.open("/dmt1d/cate/Cate.do?cmd=keyedit&status=client&userId=<%=USER_ID%>",'auth_level_2_reset','width=800, height=800, scrollbars=auto, resizable=no');
			win.moveTo(0,0);
			win.focus();
	}


function goUserMenu(str) {
	if ( str == '1' ) {
		location.href="/dmt1d/user/Login.do?cmd=logoutAction&userId=<%=USER_ID%>";

		
	} else if( str == '2') {
		main.location.href="/dmt1d/user/User.do?cmd=userModifyForm&userId=<%=USER_ID%>";
	} else if( str == '3') {
		if(confirm('<%=USER_ID%>'+' <%=msg1%>')){
			location.href="/dmt1d/user/User.do?cmd=userOutAction&userId=<%=USER_ID%>";
	}
/*
	} else if( str == '4') {
		top.content.document.location.href="/dmt1d/client/Password.do?cmd=hintForm&userId=<%--=USER_ID--%>";
*/
	} else if( str == '5') {
		var win = window.open("/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>&status=1",'auth_level_2_reset','left=1, top=1, width=460, height=300, scrollbars=auto, resizable=no');
		win.moveTo(0,0);
		win.focus();
//		main.location.href="/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>";
	} else if( str == '6') {
		var win = window.open("/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>&status=2",'auth_level_2_reset','width=460, height=300, scrollbars=auto, resizable=no');
		win.moveTo(0,0);
		win.focus();
		
	} else if( str == '10') {
		top.content.document.location.href="/dmt1d/user/Login.do?cmd=loginForm";
	} else if( str == '11') {
		top.content.document.location.href="/dmt1d/user/User.do?cmd=userInForm";
	}
}

function goManagerMenu(str) {
	
	if ( str == '20' ) {
		main.location.href="/dmt1d/cate/Cate.do?cmd=list&status=admin";
	} else if( str == '21') {
		main.location.href="/dmt1d/cate/Cate.do?cmd=list&status=cateselect";
	} else if( str == '22') {
		main.location.href="/dmt1d/user/User.do?cmd=userList";
	} else if( str == '23') {
		top.content.document.location.href="/dmt1d/cate/Cate.do?cmd=icon";
	} else if( str == '24') {
		main.location.href="/dmt1d/cate/Cate.do?cmd=cateAdd";
	} else if( str == '30') {
		main.location.href="/dmt1d/monitoring/Monitoring.do?cmd=userOutList";
	} else if( str == '40') {
		main.location.href="/dmt1d/monitoring/Monitoring.do?cmd=accessMonthly";
	} else if( str == '41') {
		main.location.href="/dmt1d/monitoring/Monitoring.do?cmd=accessDaily";
	} else if( str == '42') {
		main.location.href="/dmt1d/monitoring/Monitoring.do?cmd=accessFailCount";
	}
}
</script>
<!----------------------left menu start------------------------------------------------------------->
                          <table width="152" border="0" cellspacing="0" cellpadding="0" valign="top">
                            <tr>
                              <td align="center">
							    <table width="152" height="80" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td align="center" background="/dmt1d/images/user/bg_logout.gif">
									  <table width="140" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td align="center">
										    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td height="20" align="center"><%=USER_ID%>&nbsp;<%=msg2%></td>
                                                <td width="60" align="left"><a href="#"><img src="/dmt1d/images/common/btn_logout.gif" height="16" border="0" onclick="javascript:goUserMenu(1);"/></a></td>
                                              </tr>
                                           </table>
										  </td>
                                        </tr>
                                        <tr>
                                          <td align="center">
										   <table border="0" cellpadding="0" cellspacing="0">
                                              <tr>
                                                <td width="76" height="20"><img src="/dmt1d/images/common/dementor.gif" width="76" height="16" border="0" /></td>
                                                <td class="bold style1"><%=msg3%></td>
                                              </tr>
                                           </table>
										  </td>
                                        </tr>
                                    </table>
									 </td>
                                  </tr>
                               </table>
							  </td>
                            </tr>
                            <tr>
                              <td height="5"></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top">
							    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="46" align="center"><img src="/dmt1d/images/user/title_user.gif" width="152" height="46" border="0" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goUserMenu(2);"><%=msg4%></a></td>
                                        </tr>
                                      </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
								  <% if(IS_ADMIN == false  && (dmt1d_AUTH == null)) { %>
								  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goUserMenu(3);"><%=msg5%></a></td>
                                        </tr>
                                      </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
								  <%}%>
								  <!--
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goUserMenu(5);"><%=msg6%></a></td>
                                        </tr>
                                      </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
								  -->
								  
									  	<!-- 
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goUserMenu(6);"><%=msg7%></a></td>
                                        </tr>
                                      </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  
                                         -->
                                </table>
							  </td>
                            </tr>
                            <tr>
                              <td height="5"></td>
                            </tr>
							<% if(IS_ADMIN  && (dmt1d_AUTH != null)) { %>
                            <tr>
                              <td>
							    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="46" align="center"><img src="/dmt1d/images/user/title_cate.gif" width="152" height="46" border="0" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(20);"><%=msg8%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                  <!-- 
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(24);"><%=msg15%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                   -->
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="100"><a href="javascript:goManagerMenu(21);"><%=msg9%> </a></td>
                                        </tr>
                                      </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                </table>
							  </td>
                            </tr>
                            <tr>
                              <td height="5"></td>
                            </tr>
                            <tr>
                              <td>
							    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="46" align="center"><img src="/dmt1d/images/user/title_mo.gif" width="152" height="46" border="0" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(41);"><%=msg10%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(40);"><%=msg11%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(42);"><%=msg12%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(22);" ><%=msg13%> </a></td>
                                        </tr>
                                     </table>
									</td>
                                  </tr>
                                  <!--  탈퇴 사용자 조회
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                                  <tr>
                                    <td height="20">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="18"><img src="/dmt1d/images/common/icon_title.gif" width="18" height="4" border="0" /></td>
                                          <td width="97"><a href="javascript:goManagerMenu(30);"><%=msg14%> </a></td>
                                        </tr>
                                    </table>
									</td>
                                  </tr>
                                   -->
                                  <tr>
                                    <td height="4"><img src="/dmt1d/images/common/line_dot.gif" width="160" height="2" /></td>
                                  </tr>
                              </table>
							  </td>
                            </tr>
							<% } %>
                          </table>
<!------------------left menu end----------------------------------------------------->
