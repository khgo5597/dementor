<!-- MessagePage -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import= "java.util.*"%>
<%@ page import= "java.io.*" %>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%
	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5113"); // 1단계 카테고리 선택에서 <br/> 원하시는 카테고리를 선택 하세요.
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5114"); // Hole key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg3= CodeMessageHandler.getInstance().getCodeMessage("5115"); // 1번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5116"); // 2번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5117"); // 3번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg6= CodeMessageHandler.getInstance().getCodeMessage("5118"); // 모든 key설정을 마치셨습니다.
%>
<HTML>
<HEAD>
<TITLE>::: DEMENTOR :::</TITLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
window.onload=fnInit;
function fnInit(){
		var val = window.dialogArguments;
		var Message;
		if(val == 6){
			Message = "<%=msg1%>";
		}else if(val == 0){
			Message = "<%=msg2%>";
		}else if(val == 1){
			Message = "<%=msg3%>";
		}else if(val == 2){
			Message = "<%=msg4%>";
		}else if(val == 3){
			Message = "<%=msg5%>";
		}else if(val == 4){
			Message = "<%=msg6%>";
		}
        oArgs.innerHTML=Message;
}

function newPopupResize() {
   window.moveTo(1,1);
   window.resizeTo(100,100);

   var dWidth = parseInt(document.body.scrollWidth);
   var dHeight = parseInt(document.body.scrollHeight); 	 
   var divEl = document.createElement('div');
   divEl.style.left = '0px';
   divEl.style.top = '0px';
   divEl.style.width = '100%';
   divEl.style.height = '100%';

   document.body.appendChild(divEl);
   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
   document.body.removeChild(divEl);

}
//-->
</SCRIPT>
</HEAD>

<BODY>
	<table width="300" height="220" border="0" cellspacing="0" cellpadding="0" onLoad="newPopupResize()" background="/dmt1d/images/common/step_bg.gif">
		<tr>
			<td width="300" height="210">
				<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td align="center">
							<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td colspan="3" height="30"><img src="/dmt1d/images/common/logo.gif" width="140" height="30" border="0"><img src="/dmt1d/images/common/title_help.gif" width="150" height="30" border="0"></td>
								</tr>
								<tr>
									<td width="15"></td>
									<td>         
										<span STYLE="color: #000000; font-weight: bold; font-size:15;" ID="oArgs"></span>
									</td>
									<td width="15"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="35" align="center">
							<input type="image" src="/dmt1d/images/common/btn_ok_2.gif" width="84" height="30"  value="Close" Onclick="javascript:self.close();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</BODY>
</HTML>
