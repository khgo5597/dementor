<%@ page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="com.mininfo.admin.monitoring.model.MonitoringEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ include file="/common/userAttribute.jsp"%>
<%
MonitoringEntity entity = (MonitoringEntity)request.getAttribute("ENTITY");
String[] countArray = entity.getCountArray();
int entityYear = entity.getReqYear();

String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5058");
String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5060");
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5061");
String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5070");
String msg5 = CodeMessageHandler.getInstance().getCodeMessage("5063");
String msg6 = CodeMessageHandler.getInstance().getCodeMessage("5059");
%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" >
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
<script language="JavaScript">
function goMonitoring(){
	var F = document.form1;
	F.action = "Monitoring.do";
	F.cmd.value = "accessMonthly";
	F.submit();
}
</script>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="form1" method="post">
<input type="hidden" name="cmd" value="accessMonthly">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td height="46" align="left"><img src="/dmt1d/images/admin/title_02.gif" width="600" height="46"></td>
	</tr>
	<tr>
	  <td height="10" align="left" valign="top">
	  </td>
	</tr>
	<tr>
	  <td height="45" align="left" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" width="700">
		<tr>
		  <td background="/dmt1d/images/common/root-left.gif" width="6" height="32"></td>
		  <td background="/dmt1d/images/common/root-bg.gif">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
				<td width="15" style="padding-left:10px"><img src="/dmt1d/images/common/root-icon.gif" width="6" height="14" border="0"></td>
				<td><%=entity.getReqYear()%><%=msg1%> (<%=msg2%> <%=entity.getTotalCount()%> <%=msg3%>)</td>
				<td width="60" align="right" style="padding-right:10px">&nbsp;</td>
				<td width="50" align="right" style="padding-right:10px">
					<select name="year" class="inputSup" id="year"> 
						<%	for(int i=2008;i<2016;i++) { 
								if(i == entityYear) {
						%>	                     
								<option value='<%= i %>' selected><%= i %><%=msg1%></option>
						<%   	} else {    %>
								<option value='<%= i %>'><%= i %><%=msg1%></option>
						<%      }
							} 
						%>                   
					</select>
				</td>
				<td width="70" align="right" style="padding-right:10px"><a href="#"><img src="/dmt1d/images/common/bu-search.gif" width="60" height="16" border="0" align="absmiddle" onClick="javascript:goMonitoring();"></a></td>
			  </tr>
			</table>
		  </td>
		  <td background="/dmt1d/images/common/root-right.gif" width="6" height="32"></td>
		</tr>
	  </table>
	  <tr>
		<td height="50" align="left">
		 <table border="0" cellpadding="0" cellspacing="0" width="700">
		  <tr>
			<td colspan="5" height="1" bgcolor="#D7D7D7"></td>
			</tr>
		  <tr align="center" bgcolor="#F7F7F7">
			<td height="25" width="350"><strong><%=msg4%></strong></td>
			<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td><b class="black"><%=msg5%></b></td>
			</tr>
		  <tr>
			<td height="1" colspan="5" bgcolor="#D7D7D7"></td>
		  </tr>
		  <% for (int i=0;i<countArray.length; i++) {  %>   
		  <tr>
			<td height="19" width="350"><div align="center"><%= i+1 %><%=msg6%></div></td>
			<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td height="25" align="center">(<%= countArray[i] %><%=msg3%>)</td> 
		  </tr> 
		  <tr>
			<td height="2" colspan="3" align="center" background="/dmt1d/images/common/line_dot.gif"></td>
			</tr>
		  <tr>
		  <% } %>
		  <tr>
			<td height="1" colspan="3" align="center" bgcolor="#D7D7D7"></td>
		  </tr>
		</table>
		</td>
	  </tr>
	 </table>
</form>
</body>
</html>
