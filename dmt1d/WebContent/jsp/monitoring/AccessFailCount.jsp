<%@ page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>

<%@ page import="java.util.List"%>
<%@ page import="com.mininfo.admin.monitoring.model.MonitoringEntity"%>
<%@ page import="com.mininfo.admin.monitoring.model.FailEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>

<%
MonitoringEntity entity = (MonitoringEntity)request.getAttribute("ENTITY");

List list = entity.getList();
FailEntity failEntity = null;

int entityYear = entity.getReqYear();
int entityMonth = entity.getReqMonth();

String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5058");
String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5059");
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5060");
String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5061");
String msg5 = CodeMessageHandler.getInstance().getCodeMessage("5065");
String msg6 = CodeMessageHandler.getInstance().getCodeMessage("5066");
String msg7 = CodeMessageHandler.getInstance().getCodeMessage("5067");
%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" >
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
<script language="JavaScript">
function goMonitoring(){
	var F = document.form1;
	F.action = "Monitoring.do";
	F.cmd.value = "accessFailCount";
	F.method= "post";
	F.target = "_self";
	F.submit();
}

function goDetailList(id){
	var win = window.open('','detailList','width=647, height=500, scrollbars=yes, resizable=no');
	var F = document.form1;
	F.action = "Monitoring.do";
	F.method="post";
	F.target = "detailList";
	F.cmd.value = "accessFailUserList";
	F.year.value = <%=entityYear%>;
	F.month.value = <%=entityMonth%>;
	F.userId.value = id;
	F.submit();
	win.focus();
}
</script>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="form1" method="post">
<input type="hidden" name="cmd" value="accessFailCount">
<input type="hidden" name="userId" value="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td height="46" align="left"><img src="/dmt1d/images/admin/title_03.gif" width="600" height="46"></td>
	</tr>
	<tr>
	  <td height="10" align="left" valign="top">
	  </td>
	</tr>
	<tr>
	  <td height="45" align="left" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" width="700">
		<tr>
		  <td background="/dmt1d/images/common/root-left.gif" width="6" height="32"></td>
		  <td background="/dmt1d/images/common/root-bg.gif">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
				<td width="15" style="padding-left:10px"><img src="/dmt1d/images/common/root-icon.gif" width="6" height="14" border="0"></td>
				<td><%=entity.getReqYear()%><%=msg1%>&nbsp; <%=entity.getReqMonth()%><%=msg2%>&nbsp;(<%=msg3%> <%=entity.getTotalCount()%> <%=msg4%>)</td>
				<td width="60" align="right" style="padding-right:10px">
					<select name="year" class="inputSup" id="year">     
						<%	for(int i=2008;i<2016;i++) { 
								if(i == entityYear) {
						%>	                     
								<option value='<%= i %>' selected><%= i %><%=msg1%></option>
						<%  	} else {    %>
								<option value='<%= i %>'><%= i %><%=msg1%></option>
						<%  	}
							} 
						%>                   
					</select>
				</td>
				<td width="50" align="right" style="padding-right:10px">
					<select name="month" class="inputSup">
						<%	for(int i=1;i<13;i++) { 
								if(i == entityMonth) {
						%>	                     
								<option value='<%= i %>' selected><%= i %><%=msg2%></option>
						<%  	} else {    %>
								<option value='<%= i %>'><%= i %><%=msg2%></option>
						<%  	}
							}
						%>                  
					</select>
				</td>
				<td width="70" align="right" style="padding-right:10px"><a href="#"><img src="/dmt1d/images/common/bu-search.gif" width="60" height="16" border="0" align="absmiddle" onClick="javascript:goMonitoring();"></a></td>
			  </tr>
			</table>
		  </td>
		  <td background="/dmt1d/images/common/root-right.gif" width="6" height="32"></td>
		</tr>
	  </table>
	  <tr>
		<td height="50" align="left">
		  <table border="0" cellpadding="0" cellspacing="0" width="700">
		  <tr>
			<td colspan="9" height="1" bgcolor="#D7D7D7"></td>
			</tr>
		  <tr align="center" bgcolor="#F7F7F7" class="black">
			<td height="25">ID</td>
			<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td><%=msg5%> </td>
			<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td><%=msg6%> IP </td>
			</tr>
		  <tr>
			<td height="1" colspan="9" bgcolor="#D7D7D7"></td>
		  </tr>
		  <%
		  	
		  	if(list.size() != 0){
		  	for (int i=0;i<list.size(); i++) {
				failEntity = (FailEntity) list.get(i);
		  %>  
		  <tr>
			<td height="25" align="center"><a href="javascript:goDetailList('<%=failEntity.getUser_id()%>');"><%=failEntity.getUser_id()%></a></td>
			<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td height="25"><div align="center"><%=failEntity.getCnt()%><%=msg6%></div></td>
			<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
			<td height="25" align="center"><%=failEntity.getAccess_ip()%></td>
		  </tr>
		  <tr>
			<td height="2" colspan="9" background="/dmt1d/images/common/line_dot.gif"></td>
		  </tr>
		  <% } 
		  	}else{
		  		 %>
		  <tr>
			<td height="25" align="center" colspan="9">검색결과가 없습니다</td>
		  </tr>
		  <tr>
			<td height="2" colspan="9" background="/dmt1d/images/common/line_dot.gif"></td>
		  </tr>
		  		  <%  		
		  	}
		  
		  %> 
			<td height="1" colspan="5" align="center" bgcolor="#D7D7D7"></td>
		  </tr>
		 </table>
		</td>
	  </tr>
	 </table>
</form>
</body>
</html>
