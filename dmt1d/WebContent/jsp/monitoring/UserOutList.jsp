<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="com.mininfo.common.util.CommonUtil"%>
<%@ page import="com.mininfo.admin.monitoring.util.PageEntity"%>
<%@ page import="com.mininfo.admin.monitoring.model.UserOutEntity"%>
<%@ page import="com.mininfo.admin.monitoring.model.UserOutListEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%
UserOutListEntity entity = (UserOutListEntity)request.getAttribute("ENTITY");
PageEntity PAGE = (PageEntity)request.getAttribute("PAGE");
List list = entity.getList();
UserOutEntity outEntity = null;

String entitySearch = entity.getSearch();
String entityStartYMD = entity.getStartYMD();
String entityEndYMD = entity.getEndYMD();
String entityUserId = CommonUtil.nullToStr(entity.getUser_id());

String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5076");
String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5077");
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5078");
String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5079");
String msg5 = CodeMessageHandler.getInstance().getCodeMessage("5080");
%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<script language="JavaScript" src="/dmt1d/js/cal/GCappearance.js"></script>
<script language="JavaScript" src="/dmt1d/js/cal/GurtCalendar.js"></script>
<link rel="stylesheet" href="/dmt1d/css/calendar.css">
<link rel="StyleSheet" HREF="/dmt1d/css/style.css" type="text/css">
<script language="JavaScript">

function goUserOutList(){
	var F = document.form1;
	F.action = "Monitoring.do";
	F.cmd.value = "userOutList";
	F.submit();
}

function goPageList(current_page){
	var F = document.form1;
	F.action = "Monitoring.do";
	F.cmd.value = "userOutList";
	F.current_page.value = current_page;
	F.submit();
}
</script>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="form1" method="post">
<input type="hidden" name="cmd" value="userOutList">
<input type="hidden" name="current_page" value="1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td height="46" align="left"><img src="/dmt1d/images/admin/title_05.gif" width="600" height="46"></td>
</tr>
<tr>
  <td height="10" align="left" valign="top">			  
  </td>
</tr>
<tr>
  <td height="75" align="left" valign="top">
	<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
	  <td background="/dmt1d/images/common/root_left2.gif" width="3" height="60"></td>
	  <td background="/dmt1d/images/common/root_bg2.gif">
		<table border="0" cellpadding="0" cellspacing="0" width="688">
		  <tr>
			<td >
			  <table width="588" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="30" align="center" class="black"><img src="/dmt1d/images/common/root-icon.gif" width="6" height="14" border="0"></td>
				  <td width="80" class="black"><%	if(!"id".equals(entitySearch)) { %>  
						<input name="search" type="radio" value="date" checked> <%=msg1%>
						<%	} else { %>
						<input name="search" type="radio" value="date"> <%=msg1%>
						<% } %>
				  </td>
				  <td width="120" align="left">
					<script language="JavaScript">
					<!--		
					var GC_SET_0 = {
					'appearance': GC_APPEARANCE,
					'dataArea' : 'startYMD',
					'date' : '<%=entityStartYMD%>'
					}
					new gCalendar(GC_SET_0);
					//-->
					</script>
				  </td>
				  <td width="20" align="center">~</td>
				  <td align="left">
					<script language="JavaScript">
					<!--		
					var GC_SET_0 = {
					'appearance': GC_APPEARANCE,
					'dataArea' : 'endYMD',
					'date' : '<%=entityEndYMD%>'
					}
					new gCalendar(GC_SET_0);
					//-->
					</script>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		  <tr>
			<td ><a href="#" onFocus="blur()"></a>
				<table width="588" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="30" align="center"><img src="/dmt1d/images/common/root-icon.gif" width="6" height="14" border="0"></td>
					<td width="80">
					<%	if("id".equals(entitySearch)) { %>     
					<input name="search" type="radio" value="id" checked><%=msg2%>
					<%	} else { %>    
					<input name="search" type="radio" value="id"><%=msg2%>
					<%	} %>
					</td>
					<td width="130"><input type="text" name="userId" class="textbox_1" id="idtxt" size="20" maxlength="50" value='<%=entityUserId%>'></td>
					<td width="10" align="left"></td>
					<td align="left"><a href="#"><img src="/dmt1d/images/common/bu-search.gif" width="60" height="16" border="0" align="absmiddle" onClick="javascript:goUserOutList();"></a></td>
				  </tr>
			   </table>
			  </td>
		  </tr>
		</table>
		</td>
	  <td width="6"  background="/dmt1d/images/common/root_right2.gif"></td>
	</tr>
  </table>
  <tr>
	<td height="50" align="left">
	 <table border="0" cellpadding="0" cellspacing="0" width="700">
	  <tr>
		<td colspan="9" height="1" bgcolor="#D7D7D7"></td>
	  </tr>
	  <tr align="center" bgcolor="#F7F7F7" class="black">
		<td width="40" height="25" align="center">No</td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td width="120" height="25" align="center">ID</td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td><%=msg3%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td><%=msg4%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td width="100"><%=msg5%></td>
		</tr>
	  <tr>
		<td height="1" colspan="9" bgcolor="#D7D7D7"></td>
	  </tr>
	  <%
	  if(list.size()<PAGE.end_idx) PAGE.end_idx = list.size();
	  if (PAGE.start_idx > 0)PAGE.start_idx -=1;
	  for(int i=PAGE.start_idx; i<PAGE.end_idx; i++){
		outEntity = (UserOutEntity)list.get(i);
	  %> 
	  <tr>
		<td height="25"  width="40" align="center"><%=(i+1)%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td height="25" align="center"><%=outEntity.getUser_id()%></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td height="25" align="center"><%=outEntity.getIn_date()%></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td height="25" align="center"><%=outEntity.getOut_date()%></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td height="25" align="center"><%=outEntity.getStatus()%></td>
	  </tr>
	  <tr>
		<td height="2" colspan="9" align="center" background="/dmt1d/images/common/line_dot.gif"></td>
		</tr>
	  <tr>
	  <% } %> 
	  <tr>
		<td height="1" colspan="9" align="center" bgcolor="#D7D7D7"></td>
	  </tr>
	  <tr>
		<td height="40" colspan="9" align="center">
		<%if(!"id".equals(entitySearch)) { %>  
		<table width="590" border="0" height="35" cellspacing="0" cellpadding="0">
			<tbody>
			<tr>
				<td height="80"><%@ include file="Page.jsp"%></td>
			</tr>
			</tbody>
		</table>
		<% } %>
		 </td>
	  </tr>
	 </table>
	</td>
  </tr>
 </table>
</form>				
</body>
</html>
