<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
String userId = request.getParameter("userId");						// USERID
String strServerIP = request.getServerName();                               // 서버 ip
String strServerPort = Integer.toString(request.getServerPort());         	// 서버 port
String stat = request.getParameter("stat")  == null ? "" : request.getParameter("stat");
String serverRootUrl = "http://"+ strServerIP +":"+ strServerPort +"/";  	// Root 경로

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/dmt1d/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/dmt1d/css/app.css" >
<title>:::: DEMENTOR ::::</title>
<script type="text/javascript">

function goNext() {
	location.href="dementor_1_1.jsp?userId=<%=userId%>";
}

function goGracNext() {
	var frm = document.form1;
	frm.action="/dmt1d/jsp/help/dementor_1_1.jsp";
	frm.submit();
}
</script>
</head>

<body style="margin:0 0 0 0" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="form1" method="post" action="/dmt1d/client/Client.do">
<input type="hidden" name="userId" value="<%=userId %>"/>
<input type="hidden" name="status" value="4">

<table width="350" height="510" border="0" cellpadding="0" cellspacing="0" background="/dmt1d/images/help/bg_help.png">
    <tr>
        <td align="center" valign="top">	
            <table width="340" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="238" height="40" align="right" valign="top"><img src="/dmt1d/images/help/title_help.png"></td>
<%
	if(!stat.equals("up")){
%>                    
                    <td width="45" valign="middle">
                        <a class="btn-u btn-u-dark-blue" href="/dmt1d/passTransfer.jsp?userId=<%=userId%>&status=1" target="_parent"><font color="white"><b>찾기</b></font></a>
                    </td>                    
                    <td width="45" valign="middle">                    
 <%--                   	<a class="btn-u btn-u-dark-blue" onclick="javascript:goUpd();"><font color="white"><b>변경</b></font></a>--%>
 	                <a class="btn-u btn-u-dark-blue" href="/dmt1d/dementorConnectorUpd.jsp?userId=<%=userId%>" target="_parent" ><font color="white"><b>변경</b></font></a>                    
                    </td>
<%	} else { %>
                    <td width="45" valign="middle">
                        &nbsp;&nbsp;&nbsp;
                    </td>                    
                    <td width="45" valign="middle">
 	                	&nbsp;&nbsp;&nbsp;
                    </td>
<%	} %>
                </tr>
                <tr>
                    <td height="402" colspan="3" align="center" valign="top"><br><img src="/dmt1d/images/help/help_img01.jpg"></td>
                </tr>
                <tr>
                    <td height="51" colspan="3" align="center" valign="top">
				<!-- START -->
                        <table width="305" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" align="center" style="padding-top:8px"><a href="#" onFocus="this.blur()"><img src="/dmt1d/images/help/bt_next.png" onclick="javascript:goNext();" border="0"></a></td>
                            </tr>
                        </table>
                <!-- END -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</body>
</html>