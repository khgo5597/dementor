<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
String userId = request.getParameter("userId");						// USERID
String strServerIP = request.getServerName();                               // 서버 ip
String strServerPort = Integer.toString(request.getServerPort());         	// 서버 port
String serverRootUrl = "http://"+ strServerIP +":"+ strServerPort +"/";  	// Root 경로
String stat = request.getParameter("stat")  == null ? "" : request.getParameter("stat");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/dmt1d/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/dmt1d/css/app.css" >
<script language='JavaScript'>
	function goPrev() {
		var frm = document.form1;
		frm.action="/dmt1d/jsp/help/dementor_1.jsp";
		frm.submit();
	}
	
	function goNext() {
		var frm = document.form1;
		frm.action="/dmt1d/jsp/help/dementor_1_2.jsp";
		frm.submit();
	}
	
</script>  
<title>:::: DEMENTOR ::::</title>
</head>
<body style="margin:0 0 0 0" >
<table width="350" height="510" border="0" cellpadding="0" cellspacing="0" background="/dmt1d/images/help/bg_help.png">
    <tr>
        <td align="center" valign="top">
        <table width="340" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="238" height="40" align="right" valign="top"><img src="/dmt1d/images/help/title_help.png"></td>
<%
	if(!stat.equals("up")){
%>                    
                    <td width="45" valign="middle">
                        <a class="btn-u btn-u-dark-blue" href="/dmt1d/passTransfer.jsp?userId=<%=userId%>&status=1" target="_parent"><font color="white"><b>찾기</b></font></a>
                    </td>                    
                    <td width="45" valign="middle">                    
 <%--                   	<a class="btn-u btn-u-dark-blue" onclick="javascript:goUpd();"><font color="white"><b>변경</b></font></a>--%>
 	                <a class="btn-u btn-u-dark-blue" href="/dmt1d/dementorConnectorUpd.jsp?userId=<%=userId%>" target="_parent" ><font color="white"><b>변경</b></font></a>                    
                    </td>
<%	} else { %>
                    <td width="45" valign="middle">
                        &nbsp;&nbsp;&nbsp;
                    </td>                    
                    <td width="45" valign="middle">
 	                	&nbsp;&nbsp;&nbsp;
                    </td>
<%	} %>
                </tr>   
            <tr>
                <td colspan="3" align="center" valign="top" style="padding:10 0 0 0"><img src="/dmt1d/images/help/Security_1Step.png"></td>
            </tr>
            <tr>
                <td colspan="3" align="center" valign="top"><embed src="/dmt1d/images/help/graphicISO_service_1.swf" height="318" width=300>
            <tr>
                <td height="51" colspan="3" align="center" valign="top">
                <!-- 버튼 START -->
                <table width="305" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="40" align="center" style="padding-top:8px">
                            <a href="#" onFocus="this.blur()"><img src="/dmt1d/images/help/bt_back.png" width="58" height="22" border="0" onclick="javascript:goPrev();"></a>
                            <img src="/dmt1d/images/help/spacer.png" width="9" height="1">
                            <a href="#" onFocus="this.blur()"><img src="/dmt1d/images/help/bt_next.png" onclick="javascript:goNext();" border="0"></a>
                        </td>
                    </tr>
                </table>
                <!-- 버튼 END -->
                </td>
            </tr>
        </table>
        </td>
    </tr>
</table>

<form name="form1" method="post">
<input type="hidden" name="userId" value="<%=userId %>"/>
</form>
</body>
</html>
