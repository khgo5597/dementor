<%@page contentType="text/html;charset=utf-8"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.Const"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@include file="/jsp/common/userAttribute.jsp"%>
<%
String message = "";
int resultCode = 0;
int processCode = 0;
if(request.getAttribute("RESULT") != null) {
	ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
	message = resultEntity.getResultMessage();
	resultCode = resultEntity.getResultCode();
	processCode = resultEntity.getProcessCode();
}
String msg = CodeMessageHandler.getInstance().getCodeMessage("5000"); 
String msg1 = CodeMessageHandler.getInstance().getCodeMessage("1101"); // 사용자 Key가 등록 되었습니다.
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" />
<title>::: DEMENTOR :::</title>


<script language="JavaScript">
function focu(){
	document.form1.btnok.focus();
}

function goUserMenu() {
	opener.location.href="/dmt1d/index.jsp";
	self.close();
}

function goLoin() {
	location.href="/dmt1d/index.jsp";
}

function goManagerMenu(str) {
	
	if (  str == '22') {
		location.href="/dmt1d/user/User.do?cmd=userList";
	}
	
}


</script>

</head>
<body BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="form1" method="post">
<table width="100%" height="335" border="0" cellspacing="1" cellpadding="1">
<tr>
	<td>
	<table width="350" border="0" align="center" cellspacing="1" cellpadding="1">
	  <tr>
	  	<% if(resultCode == Const.CODE_FAIL) { %>
	  	<td background="/dmt1d/images/image/failure.jpg" width="350" height="335">
	  	<% } else { %>
	  	<td background="/dmt1d/images/image/logo_bg.jpg" width="350" height="335">
	  	<% } %>
	    
	      <table width="270" border="0" align="center" cellpadding="1" cellspacing="1">
	      	<tr><td height="150"></td></tr> 
	        <tr>
	        	<td align="center" >
			  	<% if(resultCode == Const.CODE_FAIL) { %>
			  	<font color="FF7800"><B><%= message%></B></font>
			  	<% } else if(resultCode == Const.CODE_SUCCESS) { %>
			  	<font color="3F74D6"><B><%= message%></B></font>
			  	<% } else { %>        	
				<font color="3F74D6"><B><%=msg%></B></font>	
				<% } %>
				</td>
				<% if(msg1.equals(message)) { %>
					<script language='JavaScript'>
						// localhost 호출 주소 (수정 필요)
						top.opener.location.href = "/dmt1d/index.jsp";
						//window.close();
					</script>
				<% } %>
	        </tr>
	        <% if(processCode == Const.LOGIN_FAIL) { %>
	        <tr>
	        	<td align="center" >
	        		<a href="#"><img src="/dmt1d/images/common/bt_ok.gif" name="btnok" tabindex = '1' border="0" onClick="javascript:goUserMenu();"></a>
	        	</td>
	        </tr>
			<% }else if(processCode == Const.CODE_INDEX){ %>
			<tr>
				<td align="center">
	  				<a href="#"><img src="/dmt1d/images/common/bt_ok.gif" onfocus="this.blur()" border="0" onClick="javascript:goLoin();"></a>
	  			</td>
			</tr>
	        <% }else if(processCode == Const.CLOES){ %>
			<tr>
				<td align="center">
	  				<a href="#"><img src="/dmt1d/images/common/bt_ok.gif" width="40" height="22" onfocus="this.blur()" border="0" onClick="javascript:self.close();"></a>
	  			</td>
			</tr>
			<%} else {%>
			<tr>
				<td align="center">
	  				<a href="#"><img src="/dmt1d/images/common/bt_ok.gif" width="40" height="22" onfocus="this.blur()" border="0" onClick="javascript:self.close();"></a>
	  			</td>
			</tr>
			<%} %>
			
	      </table>
	   </td>
	  </tr>
	  <tr>
	  	<td align="center">	  		
<%--			<a href="#"><img src="/dmt1d/images/common/bt_ok.gif" width="40" height="22" onfocus="this.blur()" border="0" onClick="javascript:goManagerMenu(22);"></a> --%>
		</td>
	  </tr>
	</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
