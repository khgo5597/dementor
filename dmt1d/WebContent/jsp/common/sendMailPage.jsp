<%@page import="com.mininfo.user.dao.UserDAOImpl"%>
<%@page import="com.mininfo.user.dao.UserDAO"%>
<%@page import="com.mininfo.user.model.UserEntity"%>
<%@page import="com.mininfo.client.dao.MysqlclientDAO"%>
<%@page import="sun.reflect.ReflectionFactory.GetReflectionFactoryAction"%>
<%@page import="com.mininfo.common.property.DementorProperty"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%

MysqlclientDAO client = new MysqlclientDAO();
String userId    = request.getParameter("userId")   == null ? (String)request.getAttribute("userId") : request.getParameter("userId");			//사용자ID
String status    = request.getParameter("status")    == null ? (String)request.getAttribute("status") : request.getParameter("status");			
String sendType  = request.getParameter("sendType")  == null ? (String)request.getAttribute("sendType") : request.getParameter("sendType");		//전송형태
String cmdFlag   = request.getParameter("cmdFlag")   == null ? (String)request.getAttribute("cmdFlag") : request.getParameter("cmdFlag");		
String processCmd= request.getParameter("processCmd")== null ? (String)request.getAttribute("processCmd") : request.getParameter("processCmd");	
String authCode  = request.getParameter("authCode")  == null ? (String)request.getAttribute("authCode") : request.getParameter("authCode");		//인증코드
String userEmail = request.getParameter("userEmail") == null ? (String)request.getAttribute("userEmail") : request.getParameter("userEmail");	//사용자이메일


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>mailChk</title>
<link type="text/css" rel="stylesheet" href="/dmt1d/css/grac/default.css" />
<script language="javascript">

	window.onload=function() {	
		goSend();
	}

	function goSend(){
		
		sendmail.action = "/dmt1d/client/Password.do";
		sendmail.cmd.value = "mailSend";
		sendmail.submit();
		
	}
	
	function signGrac(){
		
		var sendmail = document.sendmail;
		sendmail.action = "/dmt1d/client/Password.do";
		sendmail.cmd.value = "gracSignSendMail";
		sendmail.submit();
		
	}

	
	function makeRequest(url) {
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
	    } else if (window.ActiveXObject) { // IE
	    	try {
	    		httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
	    	} catch (e) {
	    		try {
	    			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	    		} catch (e) {}
	    	}
	    }
	
	    if (!httpRequest) {
	    	alert('Giving up :( Cannot create an XMLHTTP instance');
	    	return false;
	    }
	    
	    httpRequest.open('GET', url, false);
	    httpRequest.onreadystatechange = alertContents;
	    httpRequest.send(null);
	}
	
	/*
	 * 
	 responseText 로 받아온 텍스트 데이터에 개행 문자가 포함 되어 있어 개행 문자 처리(trim())
	 
	 */
	function alertContents() {
		var form = document.sendmail;
		
		if (httpRequest.readyState == 4) {
	        if (httpRequest.status == 200) {
	        	//서버로부터 응답이 도착
	        	var rs = httpRequest.responseText.replace(/\r\n/g,"").trim();
	        	
	        	form.authCode.value = rs;
	        }
	    }
	}
	
	//개행 문자 처리
	String .prototype.trim=function(){
	    return this.replace(/^[\s\xA0]+/,"").replace(/[\s\xA0]+$/,"");
	}

</script>
</head>
<body style="cursor: wait">

<form name="sendmail" method="post" action="/dmt1d/client/Password.do">
<input type="hidden" name="cmd"         value=""> 
<input type="hidden" name="userId"     value="<%=userId%>"> 
<input type="hidden" name="status"      value="<%=status%>"> 
<input type="hidden" name="sendType"    value="<%=sendType %>">
<input type="hidden" name="cmdFlag"     value="<%=cmdFlag%>">
<input type="hidden" name="processCmd"  value="<%=processCmd%>">
<input type="hidden" name="authCode"  	value="<%=authCode%>">
<input type="hidden" name="userEmail" 	value="<%=userEmail%>">
</form>
</body>
</html>