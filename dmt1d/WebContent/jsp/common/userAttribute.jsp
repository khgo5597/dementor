<%@page import="com.mininfo.user.model.UserEntity" %>
<%
	boolean IS_ADMIN = false;//관리자 유무 체크
	boolean IS_LOGIN = false;//로그인 유무 체크
	String USER_ID = null;//사용자 아이디
	String USER_NAME = null;//사용자 이름
	String USER_EMAIL = null;//사용자 이메일 주소
	String dmt1d_AUTH = null;//디멘터 2차인증 성공한경우 Y값 넘겨줌

	UserEntity sessionUser = null;//세션내 사용자 객체
	
	if(session.getAttribute("USER") != null){
		sessionUser = (UserEntity)session.getAttribute("USER");
		IS_LOGIN = true;
		if("Y".equals(sessionUser.getAdmin_yn())) IS_ADMIN = true;
		USER_ID = sessionUser.getUser_id();
		USER_NAME = sessionUser.getUser_name();
		USER_EMAIL = sessionUser.getUser_email();
		
		if(session.getAttribute("dmt1d_AUTH") != null) {
			dmt1d_AUTH = (String)session.getAttribute("dmt1d_AUTH");
		}
	}
	
%>