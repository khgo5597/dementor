﻿<%@ page contentType="text/html; charset=UTF-8"  %>
<%@ page import= "com.mininfo.user.model.UserEntity"%>
<%@ page import= "com.mininfo.user.model.UserListEntity"%>
<%@ page import= "com.mininfo.user.dao.UserDAO"%>
<%@ page import= "com.mininfo.user.dao.UserDAOImpl"%>
<%@ page import= "com.mininfo.admin.monitoring.util.PageProcess"%>
<%@ page import= "java.util.*"%>
<%@ page import= "java.io.*" %>
<%@ page import="com.mininfo.admin.monitoring.util.PageEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%
	PageEntity PAGE = (PageEntity)request.getAttribute("PAGE");
	String radio = null;
	if(request.getAttribute("RADIO") != null){
		Object rad = request.getAttribute("RADIO");
		radio = (String)rad;
	}
	String searchynb = null;
	if(request.getAttribute("STATUSYNB") != null){
		Object rad2 = request.getAttribute("STATUSYNB");
		searchynb = (String)rad2;
	}
	
	
	List val = (List)request.getAttribute("USERS_LIST");

	if (request.getAttribute("USERS_LIST") == null) {
		UserDAO userService = new UserDAOImpl();
		UserListEntity userPage = new UserListEntity();
		
		val = userService.UserList(userPage);		
		
		request.setAttribute("RADIO", "id");
		request.setAttribute("STATUSYNB", null);
		
		PageProcess pageProcess = new PageProcess();
		//System.out.println(userPage);
		PAGE = pageProcess.calculatePage2(request, response, userPage);
	}
	
	UserEntity entity = null;
	

String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5085");
String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5086");
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5087");
String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5054");
String msg5 = CodeMessageHandler.getInstance().getCodeMessage("5088");
String msg6 = CodeMessageHandler.getInstance().getCodeMessage("5089");
String msg7 = CodeMessageHandler.getInstance().getCodeMessage("5109");
String msg8 = CodeMessageHandler.getInstance().getCodeMessage("5130");
String msg9 = CodeMessageHandler.getInstance().getCodeMessage("5065");
%>

<html>
<head>
<title>::: DEMENTOR ::: </title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" >
<SCRIPT LANGUAGE="JavaScript">
<!--
	function selected(val){
		var frm = document.form1.search;
		var frr = document.form1.userId;
		var frs = document.form1.status;
		var frc = document.form1.searchckd.value;

		if(val == "id"){
			frr.disabled ='';
			frs.disabled ='true';
			document.form1.search[0].checked=true;
			document.form1.searchckd.value = 'id';
		}else if(val == "st"){
			frr.disabled ='true';
			frs.disabled ='';
			document.form1.search[1].checked=true;
			document.form1.searchckd.value = 'st';
		}
		
	}

	function goPageList(current_page){
		var F = document.form1;
		F.action = "/dmt1d/user/User.do";
		F.cmd.value = "userList";
		F.current_page.value = current_page;
		F.submit();
	}

	function goUserOutList(){
		var F = document.form1;
		F.action = "/dmt1d/user/User.do";
		F.cmd.value = "userList";
		F.submit();
	}

	function statusEdit(val,val2,current_page){
		var F = document.form1;
		var tt = "";
		var cnt = 0;
		
        try { 
		   cnt = document.form1.count[val2%10].value;
        }catch(e){
		   cnt = document.form1.count.value;
        } 

		if(cnt == "" || cnt.length>2) return false;
	
		F.current_page.value = current_page;
		F.cmd.value = "userStatusEdit";
		F.action = "/dmt1d/user/User.do?userId="+val+"&status2="+tt+"&count2="+cnt;
		F.submit();
	}

	function keyDelete(val) {
		var F = document.form1;
		F.cmd.value = "userKeyDelete";
		F.action = "/dmt1d/user/User.do?userId="+val;
		F.submit();
	}
	
	function goExcel(){
		var F = document.form1;
		F.action = "/dmt1d/user/User.do";
		F.cmd.value = "excel";
		F.submit();	
	}
	
//-->
</SCRIPT>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" onload="javascript:selected('<%=radio%>');">
<form name="form1" method="post">
<input type="hidden" name="cmd" value="userList">
<input type="hidden" name="current_page" value="1">
<input type="hidden" name="searchckd">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td height="46" align="left"><img src="/dmt1d/images/admin/title_04.gif" width="600" height="46"></td>
</tr>
<tr>
  <td height="10" align="left" valign="top"></td>
</tr>
<tr>
  <td height="45" align="left" valign="top">
	<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
		<td background="/dmt1d/images/common/root-left.gif" width="6" height="32"></td>
		<td background="/dmt1d/images/common/root-bg.gif">
		<table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="30" align="center"><img src="/dmt1d/images/common/root-icon.gif" width="6" height="14" border="0"></td>
			<td width="80"><input name="search" type="radio" value="id" onClick="javascript:selected('id')">USER ID</td>
			<td width="130"><input type="text" name="userId" class="textbox_1" id="idtxt" size="20" maxlength="50"></td>
			<td width="10" align="left"><input name="search" type="radio" value="st" onClick="javascript:selected('st')"></td>
			<td>
				<select name="status" class="boardline" >
				  <%if("Y".equals(searchynb) || searchynb == null){%>
				  <option value="Y">Y</option>
				  <option value="N">N</option>
				  <option value="B">B</option>
				  <%}else if("N".equals(searchynb)){%>
				  <option value="N">N</option>
				  <option value="B">B</option>
				  <option value="Y">Y</option>
				  <%}else{%>
				  <option value="B">B</option>
				  <option value="N">N</option>
				  <option value="Y">Y</option>
				  <%}%>
				</select>			
			</td>
			<td align="center"><a href="#"><img src="/dmt1d/images/common/bu-search.gif" width="60" height="16" border="0" align="absmiddle" onClick="javascript:goUserOutList();"></a>
									   <a href="#"><img src="/dmt1d/images/button/ico_ldown.gif" border="0" align="absmiddle" onClick="javascript:goExcel();" alt="엑셀저장"></a>
			</td>
		  </tr>
		</table>
		</td>
		<td background="/dmt1d/images/common/root-right.gif" width="6" height="32"></td>
    </tr>
   </table>
 </td>
</tr>

<tr>
  <td height="10" align="left" valign="top">
  </td>
</tr>

  <tr>
	<td height="50" align="left">
	<table border="0" cellpadding="0" cellspacing="0" width="700">
	  <tr>
		<td colspan="15" height="1" bgcolor="#D7D7D7"></td>
		</tr>
	  <tr align="center" bgcolor="#F7F7F7" class="black">
		<td align="center">No</td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center">ID</td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg2%></td>
		<!-- 
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg3%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg4%></td>
		 -->
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg7%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg8%></td>		
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=msg9%></td>
		</tr>
	  <tr>
		<td height="1" colspan="15" bgcolor="#D7D7D7"></td>
	  </tr>
	  <%
		if(val.size() != 0 ){
			for(int i=0; i<val.size(); i++){
				entity = (UserEntity) val.get(i);
	  %>
	  
	  <tr>
		<td height="25" width="40" align="center"><%=(i+1)%></td>
		<td width="10"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td height="25" align="center"><a href="/dmt1d/user/User.do?cmd=userModifyForm&userId=<%=entity.getUser_id()%>"><%=entity.getUser_id()%></a></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><%=entity.getUser_email()%></td>
		<!--
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td style="padding-left:5px"><%=entity.getUser_question()%></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center" style="padding-left:5px"><%=entity.getUser_answer()%></td>
		-->
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><a href="#"><img src="/dmt1d/images/common/bt_change.gif" width="40" height="22" border="0" onClick="statusEdit('<%=entity.getUser_id()%>','<%=i%>',<%=PAGE.current_page%>);"></a></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>		
		<td align="center"><a href="#"><img src="/dmt1d/images/common/btn_delete2.gif" width="40" height="22" border="0" onClick="keyDelete('<%=entity.getUser_id()%>');"></a></td>
		<td style="padding-left:5px"><img src="/dmt1d/images/common/line_t.gif" width="2" height="10"></td>
		<td align="center"><input type="text" name="count" value="<%=entity.getKey_count()%>" size=3></td>
	  </tr>
	  <tr>
		<td height="2" colspan="15" background="/dmt1d/images/common/line_dot.gif"></td>
	  </tr>

	  <%	}
	    }
		entity = null;
	  %>
	  <tr>
		<td height="1" colspan="15" align="center" bgcolor="#D7D7D7"></td>
	  </tr>
	  <tr>
		<td height="40" colspan="15" align="center">
	 
		 </td>
	  </tr>
	  <tr align="center">
		<td height="40" colspan="15" align="center">
		
		<table width="590" border="0" height="35" cellspacing="0" cellpadding="0">
			<tbody>
			<tr>
				<td height="80"><%//@ include file="Page.jsp"%></td>
			</tr>
			</tbody>
		</table>
		
		 </td>
	  </tr>
	 </table>
	</td>
  </tr>
 </table>
</form>					
</body>
</html>
