<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%
	String message = "";
	if(request.getAttribute("RESULT") != null) {
		ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
		message = resultEntity.getResultMessage();
	}

	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5026");
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5001");
	String msg3= CodeMessageHandler.getInstance().getCodeMessage("5028");
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5029");
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5030");
	String msg6= CodeMessageHandler.getInstance().getCodeMessage("5031");
	String msg7= CodeMessageHandler.getInstance().getCodeMessage("5032");
	String msg8= CodeMessageHandler.getInstance().getCodeMessage("5033");
	String msg9= CodeMessageHandler.getInstance().getCodeMessage("5034");
	String msg10= CodeMessageHandler.getInstance().getCodeMessage("5035");
	String msg11= CodeMessageHandler.getInstance().getCodeMessage("5036");
	String msg12= CodeMessageHandler.getInstance().getCodeMessage("5037");
	String msg13= CodeMessageHandler.getInstance().getCodeMessage("5038");
	String msg14= CodeMessageHandler.getInstance().getCodeMessage("5039");
	String msg15= CodeMessageHandler.getInstance().getCodeMessage("5040");
	String msg16= CodeMessageHandler.getInstance().getCodeMessage("5041");
	String msg17= CodeMessageHandler.getInstance().getCodeMessage("5042");
	String msg18= CodeMessageHandler.getInstance().getCodeMessage("5043");
	String msg19= CodeMessageHandler.getInstance().getCodeMessage("5044");
	String msg20= CodeMessageHandler.getInstance().getCodeMessage("5045");
	String msg21= CodeMessageHandler.getInstance().getCodeMessage("5046");
	String msg22= CodeMessageHandler.getInstance().getCodeMessage("5047");
	String msg23= CodeMessageHandler.getInstance().getCodeMessage("5048");
	String msg24= CodeMessageHandler.getInstance().getCodeMessage("5049");
	String msg25= CodeMessageHandler.getInstance().getCodeMessage("5050");
	String msg26= CodeMessageHandler.getInstance().getCodeMessage("5051");
	String msg27= CodeMessageHandler.getInstance().getCodeMessage("5052");
	String msg28= CodeMessageHandler.getInstance().getCodeMessage("5053");
	String msg29= CodeMessageHandler.getInstance().getCodeMessage("5054");
	String msg30= CodeMessageHandler.getInstance().getCodeMessage("5055");
	String msg31= CodeMessageHandler.getInstance().getCodeMessage("5077");
%>
<html>
<head>
	<title>::: DEMENTOR :::</title>
	<link rel="stylesheet" href="/dmt1d/css/style.css" type="text/css">
	<SCRIPT LANGUAGE="JavaScript">
	function check(){
		var id = document.setting.userId;
		if(id.value == ""){
			alert("<%=msg2%>");
			id.focus();
			return;
		}

		var frr = document.setting.hin;
		if(frr.value == 0){
			alert("<%=msg1%>");
			frr.focus();
			return;
		}
	
		var frt = document.setting.answer;
		if(frt.value == ""){
			alert("<%=msg3%>");
			frt.focus();
			return;
		}
		
		var mail = document.setting.email;
		if(mail.value == ""){
			alert("<%=msg4%>");
			mail.focus();
			return;
		}
		
		var mail_exp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;
		if(!mail_exp.test(setting.email.value)){
			alert("<%=msg5%>");
			mail.focus();
			return;
		}

		var F = document.setting;
		F.submit();
	}

	function newPopupResize() {
	 	 window.moveTo(1,1);
	 	 window.resizeTo(100,100);
	   var dWidth = parseInt(document.body.scrollWidth);
	   var dHeight = parseInt(document.body.scrollHeight); 	 
	   var divEl = document.createElement('div');
	   divEl.style.left = '0px';
	   divEl.style.top = '0px';
	   divEl.style.width = '100%';
	   divEl.style.height = '100%';
		
	   document.body.appendChild(divEl);

	   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
	   document.body.removeChild(divEl);
	}

</SCRIPT>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" onload="newPopupResize();" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<form name="setting" action="/dmt1d/client/Password.do" method="post">
<input type="hidden" name="cmd" value="keySearch">
<input type="hidden" name="status" value="1">
<table width="450" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
	<td height="50" width="450"><img src="/dmt1d/images/user/pop_top_pw.gif" width="450" height="50" border="0" /></td>
  </tr>
  <tr>
	<td height="15" align="center"><% if(message != null) { %><font color="#FF6666"><%= message %></font><%}%>&nbsp;</td>
  </tr>
  <tr>
	<td height="30">
	<table width="450" height="44" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td background="/dmt1d/images/user/title_02_bg.gif">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="95" align="center"><%=msg31%></td>
				<td><input name="userId" type="text" class="textbox_1" size="50" maxlength="50" /></td>
			  </tr>
		  </table></td>
		</tr>
	</table>
	</td>
  </tr>
  <tr>
	<td height="5"></td>
  </tr>
  <tr>
	<td height="30">
	<table width="450" height="44" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td background="/dmt1d/images/user/title_02_bg.gif">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="95" align="center"><%=msg7%></td>
				<td><SELECT name="hin" class="textbox_1">
				  <OPTION value="<%=msg1%>" selected><%=msg1%></OPTION>
				  <OPTION value="<%=msg8%>"><%=msg8%></OPTION>
				  <OPTION value="<%=msg9%>"><%=msg9%></OPTION>
				  <option value="<%=msg10%>"><%=msg10%></option>
				  <OPTION value="<%=msg11%>"><%=msg11%></OPTION>
				  <OPTION value="<%=msg12%>"><%=msg12%></OPTION>
				  <OPTION value="<%=msg13%>"><%=msg13%></OPTION>
				  <OPTION value="<%=msg14%>"><%=msg14%></OPTION>
				  <OPTION value="<%=msg15%>"><%=msg15%></OPTION>
				  <OPTION value="<%=msg16%>"><%=msg16%></OPTION>
				  <OPTION value="<%=msg17%>"><%=msg17%></OPTION>
				  <OPTION value="<%=msg18%>"><%=msg18%></OPTION>
				  <option value="<%=msg19%>"><%=msg19%></option>
				  <option value="<%=msg20%>"><%=msg20%></option>
				  <OPTION value="<%=msg21%>"><%=msg21%></OPTION>
				  <OPTION value="<%=msg22%>"><%=msg22%></OPTION>
				  <OPTION value="<%=msg23%>"><%=msg23%></OPTION>
				  <option value="<%=msg24%>"><%=msg24%></option>
				  <OPTION value="<%=msg25%>"><%=msg25%></OPTION>
				  <OPTION value="<%=msg26%>"><%=msg26%></OPTION>
				  <OPTION value="<%=msg27%>"><%=msg27%></OPTION>
				  <option value="<%=msg28%>"><%=msg28%></option>
				</SELECT>
				</td>
			  </tr>
		  </table>
		  </td>
		</tr>
	</table></td>
  </tr>
  <tr>
	<td height="5"></td>
  </tr>
  <tr>
	<td height="30">
	<table width="450" height="44" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td background="/dmt1d/images/user/title_02_bg.gif">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="95" align="center"><%=msg29%></td>
				<td><input name="answer" type="text" class="textbox_1" size="50" maxlength="50" /></td>
			  </tr>
		  </table></td>
		</tr>
	</table></td>
  </tr>
  <tr>
	<td height="5"></td>
  </tr>
  <tr>
	<td height="30">
	<table width="450" height="44" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td background="/dmt1d/images/user/title_02_bg.gif">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="95" align="center"><%=msg30%> </td>
				<td><input name="email" type="text" class="textbox_1" size="50" maxlength="50" /></td>
			  </tr>
		  </table>
		  </td>
		</tr>
	</table></td>
  </tr>

  <tr>
	<td height="1"></td>
  </tr>
  <tr>
	<td height="40" align="center"><a href="#"><img src="/dmt1d/images/common/btn_enter.gif" width="80" height="26" border="0" onClick="javascript:check();"/></a>
	</td>
  </tr>
</table>
</body>
</form>
</html>
