<!-- me_check_id.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import = "java.util.*"%>
<%@ page import = "java.io.*"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%!
public static String stringValue(String str){
	if(str==null || str.trim().length() == 0) str="";
	return str;
}
%>
<%
String userId =stringValue(request.getParameter("userId"));
String checkId = (String)request.getAttribute("CHECK_ID");

String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5081");
String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5082");
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5083");
String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5084");
%>
<html>
<head>
	<title>::: DEMENTOR :::</title>
	<link rel="stylesheet" href="/dmt1d/css/style.css" type="text/css">
	<script type="text/javascript" src="/dmt1d/js/common.js"></script>
	<script type="text/javascript">
	<!--
	function useID() {
		var f = opener.document.form1.userId;
		var userId = document.form.userId.value;
		f.value = userId;
		self.close();
	}

	//-->
	</script>
</head>
<body style="margin:0;" onLoad="resizeTo(330,240);" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
<table width="330" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td >
	 <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="67"><img src="/dmt1d/images/user/pop_top_idCheck.gif" width="330" height="50"></td>
      </tr>
      <tr>
        <td align="center">
		  <table border="0" cellpadding="0" cellspacing="0" width="320">
          <tr>
            <td>
			  <form method=post name=form action="/dmt1d/user/User.do">
  			  <input type="hidden" name="cmd" value="checkId">
			  <table border="0" cellpadding="0" cellspacing="0" width="320">
                <tr>
                  <td width="30"></td>
                  <td width="120" align="right"><input type="text" name="userId" size="15" class="textbox_1" value="<%=userId%>" onKeyUp="checkValid(this, check_alnum, '<%=msg1%>');"></td>
                  <td width="15"></td>
                  <td><input type=image src="/dmt1d/images/common/btn_search.gif" width="80" height="26" border="0" align="absmiddle"></td>
                </tr>
              </table>
			  </form>
			</td>
          </tr>
          <tr>
            <td height="15"></td>
          </tr>
          <tr>
	      <%
			if(checkId.equals("null")){
			%>
					<td align="center"><%=msg2%></td>
			<%
			}else{
				if(checkId.equals("falue")){
			%>
					<td align="center" height=28 class="cont"><font color="#047B86" class="boldred"><%=msg3%></font>
					<a href="#"><img src="/dmt1d/images/common/btn_enter.gif" width="80" height="26" border="0" align=absmiddle onClick="javascript:useID();"></a>
			<%	}else{%>
					<td align="center" height=28 class="cont"><font color="#FF0000"><%=msg4%></font></td>
			<%
				}
			}
		  %>
          </tr>
         </table>
		</td>
      </tr>
     </table>
	</td>
  </tr>
</table>
</body>
</html>
