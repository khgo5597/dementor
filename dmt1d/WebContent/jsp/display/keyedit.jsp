<!-- keyedit.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.mininfo.admin.category.model.CategoryEntity"%>
<%@ page import="com.mininfo.admin.category.model.IconEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<% 
	DementorProperty DP = null;
	int iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
	List vec = (List)request.getAttribute("CATE_LIST");
	ArrayList keyvalue = (ArrayList)request.getAttribute("KEY_VALUE");
	String STATUS = stringValue(request.getAttribute("STATUS"));

	CategoryEntity entity2 = null;
%>

<%!
	public static String stringdefault(String src, String default_value){
        if(src == null || src.trim().length() == 0)
            return default_value;
        return src;
    }
%>
<%!
	public static String stringValue(Object str){
	  if(str == null) return "";
	  return (String)str;
	}
%>
<%	
	//String userId = stringValue(request.getParameter("userId"));
	String userId = (String)session.getAttribute("userId");
	String email = stringValue(request.getParameter("email"));
	String stat = stringValue(request.getParameter("status"));

	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5057"); // key를 삭제 후 재설정 하시겠습니까?
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5017"); // 저장된 카테고리가 없습니다.
	String msg3= CodeMessageHandler.getInstance().getCodeMessage("5107"); // 등록할 key를 선택하세요
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5108"); // 선택한 아이콘 확인은, 선택한 key위에 마우스를 올려 주세요.
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5112"); // 선택한 모든 key를 리셋 후 재설정 합니다.

	String msg6= CodeMessageHandler.getInstance().getCodeMessage("5113"); // 1단계 카테고리 선택에서 <br/> 원하시는 카테고리를 선택 하세요.
	String msg7= CodeMessageHandler.getInstance().getCodeMessage("5114"); // Hole key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg8= CodeMessageHandler.getInstance().getCodeMessage("5115"); // 1번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg9= CodeMessageHandler.getInstance().getCodeMessage("5116"); // 2번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	String msg10= CodeMessageHandler.getInstance().getCodeMessage("5117"); // 3번 key로 사용할 icon을 선택 하세요 <br/> 2단계 아이콘 선택에서 원하는 아이콘이 없다면 <br/> 다른 카테고리를 선택 하세요.
	//	String msg11= CodeMessageHandler.getInstance().getCodeMessage("5118"); // 모든 key설정을 마치셨습니다.
	String msg11= CodeMessageHandler.getInstance().getCodeMessage("5134"); // OK에 마우스를 올리면 선택한 아이콘이 보입니다.<br/>	클릭하면 삭제 후 재설정이 가능합니다.
%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(../images/new/set_bg.jpg);
}
-->
</style>
<script type="text/javascript" src="/dmt1d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
  <!--

	function select(obj){
		F=document.cate;
		icon.location.href="/dmt1d/cate/Cate.do?cmd=iconlist&status=client&cate="+obj;

		Msgselect();
	}

	function del(num){
		if(document.all.keys[num].value != ""){
			var temp = confirm("<%=msg1%>");
			if(temp == true){
				var blankimg = "";
				if (num == 0) {
					blankimg = "/dmt1d/images/new/key_02_h_off.gif";
				} else if (num == 1) {
					blankimg = "/dmt1d/images/new/key_02_1_off.gif";
				} else if (num == 2) {
					blankimg = "/dmt1d/images/new/key_02_2_off.gif";
				} else if (num == 3) {
					blankimg = "/dmt1d/images/new/key_02_3_off.gif";
				}
				document.all.setkey[num].src = blankimg; //"/dmt1d/images/common/step_off"+(num+1)+".gif";
				document.all.setkey[num].orgsrc =blankimg; //"/dmt1d/images/common/step_off"+(num+1)+".gif";
				document.all.keys[num].value = "";
				
			    var Message;
				if(num == 6){	
					Message = "<%=msg6%>";
				}else if(num == 0){
					document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title01.gif";
					Message = "<%=msg7%>";
				}else if(num == 1){
					document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title02.gif";
					Message = "<%=msg8%>";
				}else if(num == 2){
					document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title03.gif";
					Message = "<%=msg9%>";
				}else if(num == 3){
					document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title05.gif";
					Message = "<%=msg10%>";
				}else if(num == 4){
					document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title04.gif";
					Message = "<%=msg11%>";
				}
			    
			    var innerMsg = document.getElementById("innerMsg");
			   	innerMsg.innerHTML = Message;
			}
		}
	}

	function keyimg(){
	if(document.all.hole.value == "" || document.all.hole.value == null){
			alert("<%=msg3%>");
			return;
		}else if(document.all.key1.value == "" || document.all.key1.value == null){
			alert("<%=msg3%>");
			return;
		}
		<% if (iconKeyNumber == 3) { %>
		else if(document.all.key2.value == "" || document.all.key2.value == null){
			alert("<%=msg3%>");
			return;
		}
		<% } else if (iconKeyNumber == 4) { %>
		else if(document.all.key2.value == "" || document.all.key2.value == null){
			alert("<%=msg3%>");
			return;
		} else if(document.all.key3.value == "" || document.all.key3.value == null){
			alert("<%=msg3%>");
			return;
		}
		<%}%>

		//document.kees.action = "/dmt1d/client/Client.do?cmd=keyedit";
		document.kees.action="/dmt1d/client/Password.do";
		document.kees.submit();
	}

	function mover(idx)
	{
		var blankimg = "/dmt1d/images/common/step_off"+idx+".gif";

		
		var tmp = document.all.setkey[idx].src;
		document.all.setkey[idx].src = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = tmp;
	}

	function mout(idx)
	{
		var blankimg = "/dmt1d/images/common/step_off"+idx+".gif";

		
		var tmp = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = document.all.setkey[idx].src;
		document.all.setkey[idx].src = tmp;
	}

	function resett(){
		var blankimg = "";
		document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title01.gif";
		for(i=0;i<<%=iconKeyNumber%>;i++){
			if (i == 0) {
				blankimg = "/dmt1d/images/new/key_02_h_off.gif";
			} else if (i == 1) {
				blankimg = "/dmt1d/images/new/key_02_1_off.gif";
			} else if (i == 2) {
				blankimg = "/dmt1d/images/new/key_02_2_off.gif";
			} else if (i == 3) {
				blankimg = "/dmt1d/images/new/key_02_3_off.gif";
			}
			document.all.setkey[i].src = blankimg; //"/dmt1d/images/common/step_off"+(i+1)+".gif";
			document.all.setkey[i].orgsrc = blankimg; //"/dmt1d/images/common/step_off"+(i+1)+".gif";
			document.all.keys[i].value = "";
		}
		alert("<%=msg5%>");
	    var innerMsg = document.getElementById("innerMsg");
	   	innerMsg.innerHTML = "<%=msg6%>";
	}

	function helppage(msg){
		
	    var Message;
		if(msg == 6){	
			Message = "<%=msg6%>";
		}else if(msg == 0){
			Message = "<%=msg7%>";
		}else if(msg == 1){
			document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title02.gif";
			Message = "<%=msg8%>";
		}else if(msg == 2){
			document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title03.gif";
			Message = "<%=msg9%>";
		}else if(msg == 3){
			document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title05.gif";
			Message = "<%=msg10%>";
		}else if(msg == 4){
			document.getElementById("titleImg").src = "/dmt1d/images/new/key_comm02_title04.gif";
			Message = "<%=msg11%>";
		}
	    var innerMsg = document.getElementById("innerMsg");
	   	innerMsg.innerHTML = Message;
		//var win = showModalDialog("/dmt1d/MessagePage.jsp",msg,"dialogWidth:300px; dialogHeight:220px; center=yes; screenTop=yes; scroll=no; status=no; help=no;");
	}

	function Msgselect(){
		for(i=0;i<<%=iconKeyNumber%>;i++){
			if(document.kees.keys[i].value == ""){
				helppage(i);
				return;
			}
		}
		helppage(4);
	}
	
	function EndMsg(){
		document.getElementById("icon").contentWindow.document.body.onclick=function(){
			document.getElementById("icon").contentWindow.location.reload();
			var flag=false;
			for(i=0;i<<%=iconKeyNumber%>;i++){
				if(document.kees.keys[i].value == "")	flag=true;
			}
			if(flag==false)  helppage(4);		// 모든 키 설정 완료
		}
	}

	function newPopupResize() {
	 	 window.moveTo(10,0);
	 	 window.resizeTo(100,100);
	   var dWidth = parseInt(document.body.scrollWidth);
	   var dHeight = parseInt(document.body.scrollHeight); 	 
	   var divEl = document.createElement('div');
	   divEl.style.left = '0px';
	   divEl.style.top = '0px';
	   divEl.style.width = '100%';
	   divEl.style.height = '100%';
	 
	   document.body.appendChild(divEl);
	   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
	   document.body.removeChild(divEl);
	}

	function openManual() {
		popupScrollOpen('/dmt1d/help/set_manual.htm',"open_manual",'600','570');
		//window.open("/dmt1d/help/set_manual.htm","open_manual","width=600,height=570,scrollbars=yes,status=no");	
	}

  //-->
  </SCRIPT>
</head>
<body style="margin:0 0 0 0" onload="popupMove(800,730); JavaScript:helppage('4');">
<form name="key">
<table width="770" border="0" cellpadding="0" cellspacing="0">
  <tr>
	<td align="center"><table width="748" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="20" valign="bottom">&nbsp;</td>
			<td height="37" align="left" valign="bottom"><img src="../images/new/key_title.gif"/></td>
			<td align="right"><a href="#"><img src="../images/new/btn_set_manual.gif" width="122" height="22" border="0" onClick="openManual();" id="btn_set_manual" name="btn_set_manual"/></a></td>
			<td width="20" align="right">&nbsp;</td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td width="748" height="440" background="/dmt1d/images/new/key_pad_bg.jpg"><table width="100%" height="440" border="0" cellpadding="0" cellspacing="0">
	      <tr>
		    <td align="left" valign="top" width="300" style="padding-left:20px;"><table border="0" cellspacing="0" cellpadding="0">
	          <tr>
			    <td height="45" align="left"><img src="/dmt1d/images/new/key_title_sub01.gif" width="226" height="29" hspace="5" /></td>
			  </tr>
			  <tr>
			    <td align="left">
			      <table border="0" cellspacing="0" cellpadding="0">
			      <tr valign="top">
<%
					if(vec.size() != 0 ){
						for(int i=0; i<vec.size(); i++){
							entity2 = (CategoryEntity) vec.get(i);
%>
				    <td valign="top" style="padding:1 1 1 1" align="left">
				    <table width="63" height="63" border="0" cellpadding="0" cellspacing="0">
				      <tr>
				  	    <td align="center" valign="center">
				  		  <a href="#"><img src="..<%=entity2.getCategory_url()%>" width="63" height="63" border="0" onClick="javascript:select('<%=entity2.getCategory_id()%>')" alt="<%=entity2.getCategory_name()%>"></a>
				  	    </td>
				  	  </tr>
				    </table>
				    </td>
<%	
					if((i+1)%5 == 0){
%>
				  </tr>
				  <tr>
					<%}%>
				<%}
				}else{%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=msg2%>
				<%
				}
				entity2 = null;
				%>

					<td>
					</td>
				  </tr>
				  </table>
				</td>
			  </tr>
			  <tr>
				<td height="10" align="center">&nbsp;</td>
			  </tr>				
            </table>
            </td>
            <td width="30" align="center" valign="top" style="padding-top:40px;"><img src="../images/new/key_bar.gif" id="key_bar" name="key_bar"/></td>
            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="45" align="left"><img src="../images/new/key_title_sub02.gif" width="226" height="29" hspace="5" id="key_title_sub02" name="key_title_sub02"/></td>
              </tr>
              <tr>
                <td align="left">
  	              <iframe src="../cate/Cate.do?cmd=iconlist&status=client" style="border:none" allowTransparency="true" name="icon" frameborder="0" bordercolor="#FFFFFF" marginwidth="0" scrolling="auto" marginheight="0" width="325"  height="338" id="icon" onload="EndMsg();"></iframe>
                </td>
              </tr>
              <tr>
                <td align="center"><img src="../images/new/key_comm01.gif" width="321" height="23" id="key_comm01" name="key_comm01"/></td>
              </tr>
            </table></td>                            
          </tr>
        </table></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center">
	<table width="748" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="748" height="100" background="../images/new/key_pad_bg2.jpg">
	    <table width="100%" height="100" border="0" cellpadding="0" cellspacing="0">
	      <tr>
            <td align="center">
            <table border="0" cellspacing="0" cellpadding="0">
            <!-- 
              <tr>
                <td width="92" height="93" align="center" background="../images/new/key_02_bg1.gif"><img src="../images/new/key_02_h_off.gif" /></td>
                <td width="28"><img src="../images/new/key_02_bg_bar.gif" width="28" height="93" /></td>
                <td width="92" height="93" align="center" background="../images/new/key_02_bg1.gif"><img src="../images/new/key_02_1_ok.gif" /></td>
                <td width="8">&nbsp;</td>
                <td width="92" height="93" align="center" background="../images/new/key_02_bg1.gif"><img src="../images/new/icon/2.jpg" /></td>
              </tr>
            -->
              <tr>
                <td width="40%" align="left" style="padding-top:0px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="9"><img src="../images/new/key_comm02_bg.gif" width="9" id="key_comm02_bg" name="key_comm02_bg"/></td>
                    <td valign="top" background="../images/new/key_comm02_bg3.gif"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td height="30"><img src="../images/new/key_comm02_title01.gif" width="156" height="20" id="titleImg" name="titleImg"/></td>
                      </tr>
                      <tr>
                      	<td style="padding-left:20px;" class="text1"><div id="innerMsg"></div></td>
                      </tr>
                      <!-- 
                      <tr>
                        <td style="padding-left:20px;" class="text1">- 1단계에서 카테고리를 선택하세요.</td>
                      </tr>
                      <tr>
                        <td style="padding-left:20px;" class="tex2">- 원하는 아이콘이 없다면 다른 카테고리를 선택하세요.</td>
                      </tr>
                       -->
                    </table></td>
                    <td width="9"><img src="../images/new/key_comm02_bg2.gif" width="9" id="key_comm02_bg2" name="key_comm02_bg2"/></td>
                  </tr>
                </table>
                </td>
                <td width="20%"></td>
                <td height="93" align="center" onmouseover="mover(0)" onmouseout="mout(0)" onClick="javascript:del(0)" style="cursor:hand"><img src="../images/new/key_02_h_off.gif" name="setkey"/></td>
                <td width="28" align="center"><img src="/dmt1d/images/new/key01_1_bar.jpg"/></td>
                <td height="93" align="center" onmouseover="mover(1)" onmouseout="mout(1)" onClick="javascript:del(1)" style="cursor:hand"><img src="../images/new/key02_1_off.gif" name="setkey"/></td>
                <%if (iconKeyNumber == 3) { %>
                <td width="1" align="center"></td>
                <td height="93" align="center" onmouseover="mover(2)" onmouseout="mout(2)" onClick="javascript:del(2)" style="cursor:hand"><img src="../images/new/key02_2_off.gif" name="setkey"/></td>
                <%} else if (iconKeyNumber == 4) { %>
                <td width="1" align="center"></td>
                <td height="93" align="center" onmouseover="mover(2)" onmouseout="mout(2)" onClick="javascript:del(2)" style="cursor:hand"><img src="../images/new/key02_2_off.gif" name="setkey"/></td>
                <td width="1" align="center"></td>
                <td height="93" align="center" onmouseover="mover(3)" onmouseout="mout(3)" onClick="javascript:del(3)" style="cursor:hand"><img src="../images/new/key02_3_off.gif" name="setkey"/></td>
                <%} %>
                
		      </tr>
            </table>
            </td>
          </tr>              
	    </table>
	    </td>
	  </tr>
	  <tr>
        <td height="38" align="center" valign="middle">
          <a href="#"><img src="../images/new/key_btn_confirm.jpg" width="97" height="28" border="0" onClick="javascript:keyimg()" id="key_btn_confirm" name="key_btn_confirm"/></a>&nbsp; &nbsp; 
          <a href="#"><img src="../images/new/key_btn_reset.jpg" width="97" height="28" border="0" onClick="javascript:resett()" id="key_btn_reset" name="key_btn_reset" /></a></td>
        </tr>
	</table>
	</td>
  </tr>
</table>

</form>
<form name="kees" method=post>
	<input type=hidden name="hole" id="keys">
	<input type=hidden name="key1" id="keys">
	<% if(iconKeyNumber == 3) { %>
	<input type=hidden name="key2" id="keys">
	<% } else if(iconKeyNumber == 4) { %>
	<input type=hidden name="key2" id="keys">
	<input type=hidden name="key3" id="keys">
	<% } %>
	<input type=hidden name=cmd value="hintupdate">
	<input type=hidden name=userId value="<%=userId%>">
    <input type=hidden name=email value="<%=email%>"> 
    <input type=hidden name=status value="<%=stat%>">
</form>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
<!--
<%
	if(keyvalue.size() != 0 ){
		for(int i=0; i<iconKeyNumber; i++){

			IconEntity iconEntity = (IconEntity)keyvalue.get(i);
			String url = iconEntity.getIcon_url();
			int id = iconEntity.getIcon_id();
%>
		document.all.setkey[<%=i%>].orgsrc ="/dmt1d<%=url%>"; 
		<%
			if(i == 0) {
				url = "/dmt1d/images/new/key_02_h_ok.gif";
			} else if(i == 1) {
				url = "/dmt1d/images/new/key_02_1_ok.gif";
			} else if(i == 2) { 
				url = "/dmt1d/images/new/key_02_2_ok.gif";	
			} else if(i == 3) { 
				url = "/dmt1d/images/new/key_02_3_ok.gif";	
			}
		%>			
		
		document.all.setkey[<%=i%>].src = "<%=url%>"; //"/dmt1d/images/new/step_off"+(i+1)+".gif";
		
		document.all.keys[<%=i%>].value ="<%=id%>";
<%	
		}
	}
%>
//-->
</SCRIPT>
