<!-- keySerch.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.mininfo.admin.category.model.IconEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<% 
	DementorProperty DP = null;
	int iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
	ArrayList keyvalue = (ArrayList)request.getAttribute("KEY_VALUE");
	String userId = (String)session.getAttribute("userId");
%>
<%	
	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5057");
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5017");
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5108");
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5112");
%>
<%@include file="/jsp/common/userAttribute.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::DEMENTOR V.1.5:::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css">
<SCRIPT LANGUAGE="JavaScript">
<!--
	function mover(idx)
	{
		var blankimg = "/dmt1d/images/common/mouse_off.gif";

		
		var tmp = document.key.setkey[idx].src;
		document.all.setkey[idx].src = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = tmp;
	}

	function mout(idx)
	{
		var blankimg = "/dmt1d/images/common/mouse_off.gif";

		
		var tmp = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = document.all.setkey[idx].src;
		document.all.setkey[idx].src = tmp;
//		alert(document.key.setkey[idx].orgsrc+'==='+document.key.setkey[idx].src);
	}
	
	function newPopupResize() {
	 	 window.moveTo(10,0);
	 	 window.resizeTo(100,100);
	   var dWidth = parseInt(document.body.scrollWidth);
	   var dHeight = parseInt(document.body.scrollHeight); 	 
	   var divEl = document.createElement('div');
	   divEl.style.left = '0px';
	   divEl.style.top = '0px';
	   divEl.style.width = '100%';
	   divEl.style.height = '100%';
	 
	   document.body.appendChild(divEl);
	   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
	   document.body.removeChild(divEl);
	}

	function restart(){
		var win = window.open("/dmt1d/cate/Cate.do?cmd=keyedit&status=client&userId=<%=userId%>",'loginForm','left=1, top=1, width=990, height=730, scrollbars=auto, resizable=no');
		win.moveTo(0,0);
		win.focus();

		self.close();
	}
//-->
</SCRIPT>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"
	onload="newPopupResize();">
<form name="key">
<table width="380" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><!--암호찾기 부분 시작------------------------------------------------------------------------------------------------------------------------------->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="32">
				    <img src="/dmt1d/images/common/title_keysearch_01.gif" width="380" height="32" border="0">
				</td>
			</tr>
			<tr>
				<td height="110" align="center" valign="top" background="/dmt1d/images/common/title_keyset_02.gif">
				<table width="370" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="30" align="center">
						    <img src="/dmt1d/images/common/img_holekey.gif" width="80" height="22" border="0">
						</td>
						<td align="center">
						    <img src="/dmt1d/images/common/img_key_1.gif" width="80" height="22"	border="0">
						</td>
						<% if (iconKeyNumber == 3) { %>
						<td align="center">
						    <img src="/dmt1d/images/common/img_key_2.gif" width="80" height="22" border="0">
						</td>
						<% } else if (iconKeyNumber == 4) { %>
						<td align="center">
						    <img src="/dmt1d/images/common/img_key_2.gif" width="80" height="22" border="0">
						</td>
						<td align="center">
						    <img src="/dmt1d/images/common/img_key_3.gif" width="80" height="22" border="0">
						</td>
						<% } %>
					</tr>
					<tr>
						<td align="center">
						<table width="80" height="80" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" bgcolor="#CCCCCC">
								<table width="65" height="65" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
										<table width="65" height="65" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td align="center" valign="middle" bgcolor="#CCCCCC">
												<a href="#">
												    <img src="/dmt1d/images/common/mouse_off.gif" name="setkey" orgsrc="/dmt1d/images/common/step_off1.gif" width="64" height="64" border="0" onmouseover="mover(0)" onmouseout="mout(0)">
												</a>
												</td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
						<td align="center">
						<table width="80" height="80" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" bgcolor="#CCCCCC">
								<table width="65" height="65" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
										<table width="65" height="65" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td align="center" valign="middle" bgcolor="#CCCCCC">
												    <a href="#">
												        <img src="/dmt1d/images/common/mouse_off.gif" name="setkey" orgsrc="/dmt1d/images/common/step_off2.gif" width="64" height="64" border="0" onmouseover="mover(1)" onmouseout="mout(1)">
												    </a>
												</td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
						<% if (iconKeyNumber == 3) { %>
						<td align="center">
						<table width="80" height="80" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td align="center" bgcolor="#CCCCCC">
								<table width="65" height="65" border="0" cellpadding="0"
									cellspacing="0">
									<tr>
										<td>
										<table width="65" height="65" border="0" cellpadding="0"
											cellspacing="0">
											<tr>
												<td align="center" valign="middle" bgcolor="#CCCCCC"><a
													href="#"><img src="/dmt1d/images/common/mouse_off.gif"
													name="setkey" orgsrc="/dmt1d/images/common/step_off3.gif"
													width="64" height="64" border="0" onmouseover="mover(2)"
													onmouseout="mout(2)"></a></td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
						<%} else if (iconKeyNumber == 4) { %>
						<td align="center">
						<table width="80" height="80" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td align="center" bgcolor="#CCCCCC">
								<table width="65" height="65" border="0" cellpadding="0"
									cellspacing="0">
									<tr>
										<td>
										<table width="65" height="65" border="0" cellpadding="0"
											cellspacing="0">
											<tr>
												<td align="center" valign="middle" bgcolor="#CCCCCC"><a
													href="#"><img src="/dmt1d/images/common/mouse_off.gif"
													name="setkey" orgsrc="/dmt1d/images/common/step_off3.gif"
													width="64" height="64" border="0" onmouseover="mover(2)"
													onmouseout="mout(2)"></a></td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
						<td align="center">
						<table width="80" height="80" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td align="center" bgcolor="#CCCCCC">
								<table width="65" height="65" border="0" cellpadding="0"
									cellspacing="0">
									<tr>
										<td>
										<table width="65" height="65" border="0" cellpadding="0"
											cellspacing="0">
											<tr>
												<td align="center" valign="middle" bgcolor="#CCCCCC"><a
													href="#"><img src="/dmt1d/images/common/mouse_off.gif"
													name="setkey" orgsrc="/dmt1d/images/common/step_off4.gif"
													width="64" height="64" border="0" onmouseover="mover(3)"
													onmouseout="mout(3)"></a></td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
						<%} %>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="10"><img
					src="/dmt1d/images/common/title_keyset_03.gif" width="380"
					height="10" border="0"></td>
			</tr>
		</table>
		<!--암호찾기 부분 끝----------------------------------------------------------------------------------------------->
		</td>
	</tr>
	<tr>
		<td height="35" align="center">
		<table width="230" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center"><a href="#"><img
					src="/dmt1d/images/common/btn_ok_2.gif" width="84" height="30"
					border="0" onClick="javascript:self.close()"></a></td>
				<td align="center"><a href="#"><img
					src="/dmt1d/images/common/btn_reset.gif" width="84" height="30"
					border="0" onClick="javascript:restart()"></a></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</form>
<form name="kees" method=post>
<input type=hidden name=hole id="keys">
<input type=hidden name=key1 id="keys">
<input type=hidden name=key2 id="keys"> 
<input type=hidden name=key3 id="keys">
<input type=hidden name=userId id="userId">

</form>
</body>
</html>
<%
	if(keyvalue.size() != 0 ){
		for(int i=0; i<iconKeyNumber; i++){
%>
<SCRIPT LANGUAGE="JavaScript">
	<!--
		document.all.setkey[<%=i%>].orgsrc ="<%=(String)keyvalue.get(i)%>"; 
		document.all.setkey[<%=i%>].src ="/dmt1d/images/common/mouse_off.gif";
		document.all.keys[<%=i%>].value ="<%=(String)keyvalue.get(i)%>";
	//-->
	</SCRIPT>
<%	
		}
	}
%>
