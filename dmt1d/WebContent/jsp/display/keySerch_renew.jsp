<!-- keySerch.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.mininfo.admin.category.model.IconEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<% 
	DementorProperty DP = null;
	int iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
	ArrayList keyvalue = (ArrayList)request.getAttribute("KEY_VALUE");
	String userId = (String)session.getAttribute("userId");

%>
<%	// 메시지 입니다
	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5057");
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5017");
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5108");
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5112");
%>
<%@include file="/jsp/common/userAttribute.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::DEMENTOR V.1.7:::</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript" src="/dmt1d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function mover(idx)
	{
		var blankimg = "../images/new/";
		if (idx == 0) {
			blankimg+= "key01_h_off.jpg";
		} else if (idx == 1) {
			url += "key01_1_off.jpg";
		} else if (idx == 2) {
			url += "key01_2_off.jpg";
		} else if (idx == 3) {
			url += "key01_3_off.jpg";
		}

		var tmp = document.all.setkey[idx].src;
		document.all.setkey[idx].src = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = tmp;
		//alert(document.key.setkey[idx].orgsrc+'==='+document.key.setkey[idx].src);
}

	function mout(idx)
	{
		var blankimg = "../images/new/";
		if (idx == 0) {
			blankimg+= "key01_h_off.jpg";
		} else if (idx == 1) {
			url += "key01_1_off.jpg";
		} else if (idx == 2) {
			url += "key01_2_off.jpg";
		} else if (idx == 3) {
			url += "key01_3_off.jpg";
		}
		
		var tmp = document.all.setkey[idx].orgsrc;
		document.all.setkey[idx].orgsrc = document.all.setkey[idx].src;
		document.all.setkey[idx].src = tmp;
	}
	
	function newPopupResize() {
	 	 window.moveTo(10,0);
	 	 window.resizeTo(100,100);
	   var dWidth = parseInt(document.body.scrollWidth);
	   var dHeight = parseInt(document.body.scrollHeight); 	 
	   var divEl = document.createElement('div');
	   divEl.style.left = '0px';
	   divEl.style.top = '0px';
	   divEl.style.width = '100%';
	   divEl.style.height = '100%';
	 
	   document.body.appendChild(divEl);
	   window.resizeBy(dWidth - divEl.offsetWidth, dHeight - divEl.offsetHeight);
	   document.body.removeChild(divEl);
	}

	function restart(){
		var win = window.open("../cate/Cate.do?cmd=keyedit&status=client&userId=<%=userId%>",'loginForm','left=1, top=1, width=990, height=730, scrollbars=auto, resizable=no');
		win.moveTo(0,0);
		win.focus();

		self.close();
	}
//-->
</SCRIPT>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"
	onload="popupMove(560,460);">
<form name="key">
<table width="550" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="40"><img src="../images/new/title_05.jpg"
			width="300" height="40" /></td>
	</tr>
	<tr>
		<td height="75"><img src="../images/new/comm_05.jpg"
			width="450" height="75" /></td>
	</tr>
	<tr>
		<td height="20" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td height="112" align="center"><!--내용테이블시작 -->
		<table width="500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="14"><img src="../images/new/border_1_01.gif"
					width="14" height="133" /></td>
				<td align="center" bgcolor="#F3F3F3">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="92" height="93" align="center" background="../images/new/key01_bg.jpg">
							<img src="../images/new/key01_h_off.jpg" name="setkey" width="67" height="71" border="0" onmouseover="mover(0)"	onmouseout="mout(0)" />
						</td>
						<td width="28" align="center">
							<img src="../images/new/key01_1_bar.jpg" width="28" height="93" />
						</td>
						<td width="92" align="center" background="../images/new/key01_bg.jpg">
							<img src="../images/new/key01_1_off.jpg" name="setkey" width="67" height="71" onmouseover="mover(1)" onmouseout="mout(1)" />
						</td>
						<%if (iconKeyNumber == 3) { %>
						<td width="15" align="center">&nbsp;</td>
						<td width="92" align="center"
							background="../images/new/key01_bg.jpg"><img
							src="../images/new/key01_2_off.jpg" name="setkey" width="67"
							height="71" onmouseover="mover(2)" onmouseout="mout(2)" /></td>
						<%} else if (iconKeyNumber == 4) { %>
						<td width="15" align="center">&nbsp;</td>
						<td width="92" align="center"
							background="../images/new/key01_bg.jpg"><img
							src="../images/new/key01_2_off.jpg" name="setkey" width="67"
							height="71" onmouseover="mover(2)" onmouseout="mout(2)" /></td>
						<td width="15" align="center">&nbsp;</td>
						<td width="92" align="center"
							background="../images/new/key01_bg.jpg"><img
							src="../images/new/key01_3_off.jpg" name="setkey" width="67"
							height="71" onmouseover="mover(3)" onmouseout="mout(3)" /></td>
						<%} %>
					</tr>
				</table>
				</td>
				<td width="14"><img src="../images/new/border_1_02.gif"
					width="14" height="133" /></td>
			</tr>
		</table>
		<!--내용테이블 끝 --></td>
	</tr>
	<tr>
		<td height="20" align="center">&nbsp;</td>
	</tr>

	<!--확인취소버튼 -->
	<tr>
		<td height="40" align="center"><a href="#"><img
			src="../images/new/btn_confirm.jpg" width="97" height="27"
			border="0" onClick="javascript:self.close()" /></a>&nbsp; &nbsp; <a
			href="#"><img src="../images/new/btn_reset.jpg" width="97"
			height="27" border="0" onClick="javascript:restart()" /></a></td>
	</tr>
</table>
</form>
<form name="kees" method=post><input type=hidden name=hole
	id="keys"> <input type=hidden name=key1 id="keys"> <input
	type=hidden name=key2 id="keys"> <input type=hidden name=key3
	id="keys"></form>
</body>
</html>
<%
	if(keyvalue.size() != 0 ){
		for(int i=0; i<iconKeyNumber; i++){
%>
<SCRIPT LANGUAGE="JavaScript">
	<!--
		document.all.setkey[<%=i%>].orgsrc ="..<%=((IconEntity)keyvalue.get(i)).getIcon_url()%>";
		var url = "../images/new/";
		<%if(i == 0) { %>
			url += "key01_h_off.jpg";
		<%} else if(i == 1) { %>
			url += "key01_1_off.jpg";
		<%} else if(i == 2) { %>
			url += "key01_2_off.jpg";
		<%} else if(i == 3) { %>
			url += "key01_3_off.jpg";
		<%} %>
		document.all.setkey[<%=i%>].src = url;		
		document.all.keys[<%=i%>].value ="<%=((IconEntity)keyvalue.get(i)).getIcon_url()%>";
	//-->
	</SCRIPT>
<%	
		}
	}
%>
