<!-- authCodeForm.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.mininfo.user.model.UserHintEntity"%>
<%@ page import= "com.mininfo.common.util.CommonUtil"%>
<%@ page import="com.mininfo.common.model.ResultEntity"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<%@ page import="com.mininfo.common.code.CodeMessageHandler"%>
<%!
	public static String stringValue(Object str){
	  if(str == null) return "";
	  return (String)str;
	}
%>
<%

System.out.println("#################  mail_send ####################");

	DementorProperty DP = null;

	UserHintEntity userEntity = (UserHintEntity)request.getAttribute("USER");
	//String message = (String)request.getAttribute("MESSAGE");
	String status      = CommonUtil.nullToStr(request.getParameter("status"));
	String userId     = CommonUtil.nullToStr(request.getParameter("userId"));
	String sendingChk  = CommonUtil.nullToStr(request.getParameter("sending"));
	String sendType  = CommonUtil.nullToStr(request.getParameter("sendType"));
	String processCmd  = CommonUtil.nullToStr(request.getParameter("processCmd"));
	


System.out.println(">>  authCodeForm  >>>>>> userId                "+userId);	
System.out.println(">>  authCodeForm  >>>>>> status                "+status);
System.out.println(">>  authCodeForm  >>>>>> sendChk               "+sendingChk);
System.out.println(">>  authCodeForm  >>>>>> userEntity_userId     "+userEntity.getUser_id());
System.out.println(">>  authCodeForm  >>>>>> userEntity_answer     "+userEntity.getPass_answer());
System.out.println(">>  authCodeForm  >>>>>> userEntity_mail       "+userEntity.getUser_email());
		
		String message = "";
		int resultCode = 0;
		int processCode = 0;
		
		if(request.getAttribute("RESULT") != null) {
			ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
			message = resultEntity.getResultMessage();
			resultCode = resultEntity.getResultCode();
			processCode = resultEntity.getProcessCode();
		}
		
		String msg1= CodeMessageHandler.getInstance().getCodeMessage("5020");
		String msg2= CodeMessageHandler.getInstance().getCodeMessage("5021");
		String msg3= CodeMessageHandler.getInstance().getCodeMessage("5126");
%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" href="/dmt1d/css/style.css" type="text/css">
<script type="text/javascript" src="/dmt1d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">

var cnt;

function goLogin(){
	var F = document.form1;
	if(F.auth_code.value != ""){
		F.action = "Password.do";
		F.cmd.value = "authCodeAction";
		F.submit();
	} else{
		alert("<%=msg1%>");
		F.auth_code.focus();
		return;
	}
}

function search_pw(){
	alert("<%=msg3%>");
	form1.action = "Password.do";
	form1.submit();
}

function goMain()
{
	location.href="/dmt1d/dementorConnector.jsp?userId=<%=userId%>";
}

</SCRIPT>
</head>

<!-- 팝업을 사용하지 않음으로 변경
<body onload="popupMove(470,350)" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
 -->
<%
	if(sendingChk.equals("true")){
%>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<%	} else { %>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<%  } %>
<form name="form1" method="post" action="javascript:goLogin();">
<input type="hidden" name="userId"     value="<%=userId%>">
<input type="hidden" name="status"     value="<%=status%>">
<input type="hidden" name="textfield2" value="<%=userEntity.getPass_answer()%>">
<input type="hidden" name="mail"       value="<%=userEntity.getUser_email()%>">
<input type="hidden" name="cmd"         value="mailSend">
<%--<input type="hidden" name="cmd"         value="mailSend">--%> 
<input type="hidden" name="sendType"    value="<%=sendType %>">
<input type="hidden" name="cmdFlag"     value="">
<input type="hidden" name="processCmd"  value="<%=processCmd %>">
<input type="hidden" name="authCode"    value="">
<input type="hidden" name="sending"     value="true">

<table width="100%" height="100%" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" >
	<tr height="20%">
		<td></td>
	</tr>
	<tr height="30%">
		<td align="center" valign="top" style="padding-left:10px;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<table width="420px" border="0" cellspacing="0" cellpadding="0">
			
			<%if("1".equalsIgnoreCase(status) || "3".equalsIgnoreCase(status)){%>
			<tr>
				<td height="40" ><img src="/dmt1d/images/new/title_01.jpg"  /></td>
			</tr>
			<tr>
				<td height="75" ><img src="/dmt1d/images/new/comm_01.jpg"  /></td>
			</tr>

			<tr>
				<td style="text-align: center; color: red;">본인 메일 확인 후 인증키 값을 입력해 주세요.</td>
			</tr>

			<%}else if("2".equalsIgnoreCase(status) || "4".equalsIgnoreCase(status)){%>
			<tr>
				<td height="40" ><img src="/dmt1d/images/new/title_04.jpg"  /></td>
			</tr>
			<tr>
				<td height="75" ><img src="/dmt1d/images/new/comm_04.jpg"  /></td>
			</tr>
			<%}%>
			
			<tr>
				<td height="20" ></td>
			</tr>
			<table width="420px" valign="top" border="0" cellspacing="0" cellpadding="0" >	
				<tr>
				
					<td width="10px"><img src="/dmt1d/images/new/border_01.gif" width="10" height="10" /></td>
					<td width="80px"  background="/dmt1d/images/new/border_02.gif"></td>
					<td width="10px"  background="/dmt1d/images/new/border_02.gif"></td>
					<td width="310px" background="/dmt1d/images/new/border_09.gif"></td>
					<td width="10px"><img src="/dmt1d/images/new/border_10.gif" width="10" height="10" /></td>
					
				</tr>
				<tr>
					<td width="10px"  background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
					<td width="80px"  height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg2%></td>
					<td width="10px"  align="center" bgcolor="#DBE9F1"></td>
					<td width="310px" align="left" style="padding-left:10px;"><input name="auth_code" type="text" class="form1" id="user_a" size="40" maxlength="50" /></td>
					<td width="10px"  background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
				</tr>
				<tr>
					<td width="10px" ><img src="/dmt1d/images/new/border_04.gif" width="10" height="10" /></td>
					<td width="80px"  background="/dmt1d/images/new/border_05.gif"></td>
					<td width="10px"  background="/dmt1d/images/new/border_05.gif"></td>
					<td width="310px" background="/dmt1d/images/new/border_08.gif"></td>
					<td width="10px" ><img src="/dmt1d/images/new/border_06.gif" width="10" height="10" /></td>
				</tr>
			<tr>
				<td colspan="5" height="20">
				</td>
			</tr>
			</table>
			<!--확인취소버튼 -->
			<tr>
				<td height="40" align="center" valign="top" >
					<a href="#"><img src="/dmt1d/images/new/btn_confirm.jpg" width="97" height="27" border="0" onClick="javascript:goLogin();" /></a>&nbsp; &nbsp; 
					<a href="#"><img src="/dmt1d/images/new/btn_cancel.jpg" width="97" height="27" border="0" onClick="javascript:goMain();" /></a>
				</td>
			</tr>
			<tr>
				<td height="20" align="center" ><font color="red"><%=message %></font></td>
			</tr>			
		</table>

		<!--내용테이블 끝 --></td>
	</tr>
	<tr>
		<td height="50%" >&nbsp;</td>
	</tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>
