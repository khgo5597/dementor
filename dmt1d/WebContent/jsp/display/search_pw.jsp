<!-- search_pw.jsp -->
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="com.mininfo.user.model.UserHintEntity"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.client.dao.MysqlclientDAO"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<%@ page import= "com.mininfo.common.util.CommonUtil"%>
<%
	DementorProperty DP = null;
	String sendType = DP.getInstance().getAuthSendType();
	
	UserHintEntity userEntity = (UserHintEntity)request.getAttribute("USER");
	
	String userId     = CommonUtil.nullToStr(request.getParameter("userId"));
	String SENDTYPE   = CommonUtil.nullToStr(request.getParameter("SENDTYPE"));
	String processCmd = CommonUtil.nullToStr(request.getParameter("processCmd"));
	String status     = CommonUtil.nullToStr(request.getParameter("status"));
	
	//3항연산자 처리. request값이 없는경우 userEntity에서 값을 받아옴
	userId    = (userId    == "") ? userEntity.getUser_id() : userId;

	String message = "";
	int resultCode = 0;
	int processCode = 0;
	
	if(request.getAttribute("RESULT") != null)
	{
		ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
		message = resultEntity.getResultMessage();
		resultCode = resultEntity.getResultCode();
		processCode = resultEntity.getProcessCode();
	}

	String strServerIP = request.getServerName();//서버 ip
	String strServerPort = Integer.toString(request.getServerPort());// 서버 port
	String strServerScheme = request.getScheme();
	String serverRootUrl = strServerScheme+"://"+ strServerIP +":"+ strServerPort +"/";  // Root 경로
	
	MysqlclientDAO client = new MysqlclientDAO();
	int cnt = client.dmtloginCnt(userEntity.getUser_id());
	int MaxCount = DP.getInstance().getAccessCount();	
	
	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5028");
	String msg2= CodeMessageHandler.getInstance().getCodeMessage("5029");
	String msg3= CodeMessageHandler.getInstance().getCodeMessage("5030");
	String msg4= CodeMessageHandler.getInstance().getCodeMessage("5032");
	String msg5= CodeMessageHandler.getInstance().getCodeMessage("5054");
	String msg6= CodeMessageHandler.getInstance().getCodeMessage("5055");
	String msg7= CodeMessageHandler.getInstance().getCodeMessage("5124");
	String msg8= CodeMessageHandler.getInstance().getCodeMessage("5102");
	String msg9= CodeMessageHandler.getInstance().getCodeMessage("5133");

System.out.println(">> search_pw >>>>> userId     : "+userEntity.getUser_id());
System.out.println(">> search_pw >>>>> SENDTYPE   : "+SENDTYPE);	
System.out.println(">> search_pw >>>>> processCmd : "+processCmd);
System.out.println(">> search_pw >>>>> status 	  : "+status);
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>::: DEMENTOR [search_pw]:::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
img{display:block;}
table,td{
margin:0px;
padding:0px;
border-collapse:collapse;
}

</style>
<script type="text/javascript" src="/dmt1d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
	function check(){
		var pwf = document.pwfaq;
		var input = pwf.textfield2.value;
		var mailVal = null;
		
		if(pwf.mail != null) {
			mailVal = pwf.mail.value;
		} else {
			smsVal = pwf.sms.value;
		}
		
		if(input == ""){
			alert("<%=msg1%>");
			pwf.textfield2.focus();
			return;
		}
		if(mailVal == null) {
			var smsVal = pwf.sms.value;
			if (smsVal == ""){
				alert("<%=msg9%>");
				pwf.sms.focus();
				return;
			}
		} else {
			if(mailVal == ""){
				alert("<%=msg2%>");
				pwf.mail.focus();
				return;
			} else {
				var mail_exp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;
				if(!mail_exp.test(mailVal)){
					alert("<%=msg3%>");
					pwf.mail.focus();
					return;
				}
			}
		}
		
		//alert(mailVal+"<%=msg7%>");

		pwf.action = "Password.do";
		pwf.cmd.value = "sendMail";
		pwf.submit();	
	}
	
	function moveView(intWidth,intHeight){
		var bScrollbars;
        var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;

		if(intWidth >= screenWidth){ 
                intWidth = screenWidth - 20;
                bScrollbars = 1;
        }
        if(intHeight >= screenHeight){ 
                intHeight = screenHeight - 40;
                //intWidth = intWidth + 20;
                bScrollbars = 1;
        }

		var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;
		var intLeft = (screenWidth - intWidth) / 2;
		var intTop = (screenHeight - intHeight) / 2;

		if (navigator.appName.charAt(0) == "N") { 
				if (navigator.appVersion.charAt(0) == 2) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 3 ){ 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 4) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 5) { 
					intHeight += 6;
					intWidth -= 1;
				} 
		} 
		if (navigator.appName.charAt(0) == "M") {
				if (navigator.appVersion.charAt(0) == "2") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "3") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "4") {
						if(navigator.appVersion.indexOf("MSIE 5") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 6") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 7") != -1) {
						}
						if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
							intHeight -= 45;
						}
						if (navigator.appVersion.charAt(0) == "5") {
							intHeight -= 45;
						}
				}
		}

		window.moveTo(intLeft,intTop); 

		resizeTo(intWidth,intHeight);
	}
	
	function goMain()
	{
		location.href="/dmt1d/dementorConnector.jsp?userId=<%=userId%>";
	}
	
	function goSendPage() {
		var F = document.pwfaq;
		var firstStr = "<%=userEntity.getUser_email()%>".split('@')[0].substr(0,1);
		var asta = "";
		//인증코드 생성
		var rUrl="/dmt1d/client/Password.do?cmd=getAuthCode&userId=<%=userId%>";
		
		if(F.authCode.value.length == 0){
			makeRequest(rUrl);
		}
		
		for(var i=0; i < "<%=userEntity.getUser_email()%>".split('@')[0].length;i++){
			asta = asta+"*";
		}
		
		firstStr = firstStr+asta;
		alert(firstStr+"@"+"<%=userEntity.getUser_email()%>".split('@')[1]+" 으로 인증키를 발송하였습니다.");
		F.action = "/dmt1d/jsp/grac/sendMailPage.jsp";
		F.submit();
	}
	
	function goSend(strResult) {
		var F = document.pwfaq;
		var firstStr = "<%=userEntity.getUser_email()%>".split('@')[0].substr(0,1);
		var asta = "";
		//인증코드 생성
		var rUrl="/dmt1d/client/Password.do?cmd=getAuthCode&userId=<%=userId%>";
		
		if(F.authCode.value.length == 0){
			makeRequest(rUrl);
		}
		
		for(var i=0; i < "<%=userEntity.getUser_email()%>".split('@')[0].length;i++){
			asta = asta+"*";
		}
		
		firstStr = firstStr+asta;
		
		alert(firstStr+"@"+"<%=userEntity.getUser_email()%>".split('@')[1]+" 으로 인증키를 발송하였습니다.");
		F.action = "Password.do";
		F.cmdFlag.value="scHint";
		F.cmd.value = "sendMail";
		F.submit();
		
	}
	
	function makeRequest(url) {
	    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	      httpRequest = new XMLHttpRequest();
	    } else if (window.ActiveXObject) { // IE
	      try {
	        httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
	      } 
	      catch (e) {
	        try {
	          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	        } 
	        catch (e) {}
	      }
	    }

	    if (!httpRequest) {
	      alert('Giving up :( Cannot create an XMLHTTP instance');
	      return false;
	    }
	    
	    httpRequest.open('GET', url, false);
	    httpRequest.onreadystatechange = alertContents;
	    httpRequest.send(null);
	  }

	/*
	 * 
	 responseText 로 받아온 텍스트 데이터에 개행 문자가 포함 되어 있어 개행 문자 처리(trim())
	 
	 */
	function alertContents() {
		var form = document.pwfaq;
		
		if (httpRequest.readyState == 4) {
	        if (httpRequest.status == 200) {
	        	//서버로부터 응답이 도착
	        	var rs = httpRequest.responseText.replace(/\r\n/g,"").trim();
	        	
	        	form.authCode.value = rs;
	        }
	    }
	}

	//개행 문자 처리
	String .prototype.trim=function(){
	    return this.replace(/^[\s\xA0]+/,"").replace(/[\s\xA0]+$/,"");
	}
	
</SCRIPT>
</head>

<!-- onload 이벤트  
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" overflow:hidden;" scroll="no" onload="javascript:window.resizeTo('470','420');">
 -->
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" overflow:hidden;" scroll="no">
<form name="pwfaq" method="post" action="javascript:check();">
<input type="hidden" name="cmd"         value="sendMail"> 
<input type="hidden" name="userId"     value="<%=userEntity.getUser_id()%>"> 
<input type="hidden" name="status"      value="<%=status%>"> 
<input type="hidden" name="sendType"    value="<%=sendType %>">
<input type="hidden" name="cmdFlag"     value="">
<input type="hidden" name="processCmd"  value="<%=processCmd %>">
<input type="hidden" name="authCode"    value="">
<input type="hidden" name="userEmail"   value="<%=userEntity.getUser_email()%>">
<input type="hidden" name="sending"     value="true">

<table width="450" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" >
	<%if("1".equalsIgnoreCase(status) || "3".equalsIgnoreCase(status) || "4".equalsIgnoreCase(status)){%>
	<tr>
		<td height="40"><img src="/dmt1d/images/new/title_01.jpg" width="300" height="40" /></td>
	</tr>
	<tr>
		<td height="75"><img src="/dmt1d/images/new/comm_01.jpg" width="450" height="75" /></td>
	</tr>
	<%}else if("2".equalsIgnoreCase(status)){%>
	<tr>
		<td height="40"><img src="/dmt1d/images/new/title_04.jpg" width="300" height="40" /></td>
	</tr>
	<tr>
		<td height="75"><img src="/dmt1d/images/new/comm_04.jpg" width="450" height="75" /></td>
	</tr>	
	<%}%>
	<tr>
		<td height="20" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td height="112" align="center"><!--내용테이블시작 -->
		<table width="430" border="0" cellspacing="0" cellpadding="0" margin="0">
			<tr>
				<td width="10"><img src="/dmt1d/images/new/border_01.gif"  width="10" height="10" /></td>
				<td width="80"><img src="/dmt1d/images/new/border_02.gif"  width="80" height="10" /></td>
				<td width="10"><img src="/dmt1d/images/new/border_02.gif"  width="10" height="10" /></td>
				<td width="300"><img src="/dmt1d/images/new/border_09.gif"  width="320" height="10" /></td>
				<td width="10"><img src="/dmt1d/images/new/border_10.gif" width="10" height="10"/></td>
			</tr>
			<tr>
				<td width="10" background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<td width="80" height="30" align="center" bgcolor="#DBE9F1"><span class="style1"><%=msg4%></span></td>
				<td width="10" align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td width="300" align="left" bgcolor="#FFFFFF" style="padding-left:10px;">	<input name="select" type="text" class="form1" style=" width:300px;" value="<%=userEntity.getPass_hint_select_code()%>" readonly /></td>
				<td width="10" background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
			</tr>
			<tr>
				<td background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg5%></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;"><input name="textfield2" id="user_a" type="text" class="form1" style=" width:300px;"/></td>
				<td background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
			</tr>
			<tr>
				<td background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<% if ("SMS".equals(SENDTYPE)) { %>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg8 %></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" style="padding-left:10px;">
				<input name="sms" type="text" class="form1" style=" width:300px;"/></td>
				<% } else { %>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg6 %></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" bgcolor="#FFFFFF" style="padding-left:10px;">
					<input name="mail" type="text" class="form1" style=" width:300px;" /></td>
				<% } %>
				<td background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>
			<tr>
				<td width="10"><img src="/dmt1d/images/new/border_04.gif" width="10" height="10"/></td>
				<td width="80"><img src="/dmt1d/images/new/border_05.gif" width="80" height="10"/></td>
				<td width="10"><img src="/dmt1d/images/new/border_05.gif" width="10" height="10"/></td>
				<td width="300"><img src="/dmt1d/images/new/border_08.gif" width="320" height="10"/></td>
				<td width="10"><img src="/dmt1d/images/new/border_06.gif" width="10" height="10"/></td>
			</tr>
		</table>
		<!--내용테이블 끝 --></td>
	</tr>
	<tr>
		<td height="20" align="center">&nbsp; <% if(message != null && !"".equals(message)) {%>
		<font color="red"><%=message%></font> <%}%>
		</td>
	</tr>
	<!--확인취소버튼 -->
	<tr>
		<td height="40">
		<table width="100%">
			<tr>
				<td align="right" width="50%"><a href="#"><img src="/dmt1d/images/new/btn_confirm.jpg" width="97" height="27" onClick="javascript:check()" border="0" /></a></td>
				<td align="left" width="50%"><a href="#"><img src="/dmt1d/images/new/btn_cancel.jpg" width="97" height="27"	onClick="javascript:goMain()" border="0" /></a></td>
			</tr>
			<tr>
				<td align="right">&nbsp;</td>
				<td align="left">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>	
</table>
</form>
</body>
</html>