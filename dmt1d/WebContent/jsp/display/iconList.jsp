<!-- icon.jsp -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="com.mininfo.admin.category.model.IconEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@ page import="com.mininfo.common.property.DementorProperty"%>
<% 
	DementorProperty DP = null;
	int iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
	//String url = "http://" + request.getServerName() + "/dmt1d";

	ArrayList vec = (ArrayList)request.getAttribute("CATE_ICON_LIST");
	ArrayList vec2 = (ArrayList)request.getAttribute("CATE_ICON_LIST_ID");

		
	String msg= CodeMessageHandler.getInstance().getCodeMessage("5056"); // 이미 선택한 이미지 입니다.\n다른 이미지를 선택해 주세요.
	String msg1= CodeMessageHandler.getInstance().getCodeMessage("5132"); // 키등록이 완료되었습니다 확인 또는 재설정하시기 바랍니다

%>
<head>
<title>::: DEMENTOR :::</title>
<SCRIPT LANGUAGE="JavaScript">
  <!--
	var overimg = "";
	var blankimg = "";
	function keyedit(keyurl,num){
		
		for(k=0;k<<%=iconKeyNumber%>;k++){
			if(parent.document.all.setkey[k].orgsrc.lastIndexOf(fileNameSub(keyurl)) > 0){
				alert("<%=msg%>");
				return;
			}
			
		}
		
		for(i=0;i<<%=iconKeyNumber%>;i++){
			var keyUrl = parent.document.all.setkey[i].src;
			keyUrl = keyUrl.substring(keyUrl.indexOf("/dmt1d/images"),keyUrl.length);
			if (i == 0) {
				blankimg = "/dmt1d/images/new/key_02_h_off.gif";
				overimg = "/dmt1d/images/new/key_02_h_ok.gif";
			} else if (i == 1) {
				blankimg = "/dmt1d/images/new/key_02_1_off.gif";
				overimg = "/dmt1d/images/new/key_02_1_ok.gif";	
			} else if (i == 2) {
				blankimg = "/dmt1d/images/new/key_02_2_off.gif";
				overimg = "/dmt1d/images/new/key_02_2_ok.gif";	
			} else if (i == 3) {
				blankimg = "/dmt1d/images/new/key_02_3_off.gif";
				overimg = "/dmt1d/images/new/key_02_3_ok.gif";	
			}
			
			if(keyUrl == blankimg){
					parent.document.all.setkey[i].orgsrc = keyurl; 
					parent.document.all.setkey[i].src = overimg;
					parent.document.all.keys[i].value = num;
					parent.Msgselect();
					return;
			}
		}
	}

	function fileNameSub(url)
	{
		return url.substring(url.lastIndexOf('/')+1);
	}
  //-->
  </SCRIPT>
</head>

<body style="margin:0 0 0 0" oncontextmenu='return false'
	ondragstart='return false' onselectstart='return false'>
<form name="IconList">
<table width="320" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="3">&nbsp;</td>
		<td width="295" valign="top">
		<table border="0" cellpadding="0" cellspacing="0"
			style="padding-top:2">
			<tr style="padding-left: 2">
				<%
			if(vec.size() != 0 ){
				for(int i=0; i<vec.size(); i++){
					
			%>
				<td width="60" height="60" valign="top" align="left">
				<table width="60" height="60" border="0" cellpadding="1" cellspacing="1" bgcolor="cccccc">
					<tr>
						<td align="left" valign="top" bgcolor="#FFFFFF">
						<img src="/dmt1d<%=(String)vec.get(i)%>" border="0" name="icon" width="60" height="60"
							 value="<%=(String)vec2.get(i)%>" style="cursor:hand" 
							onClick="javascript:keyedit('/dmt1d<%=(String)vec.get(i)%>','<%=(String)vec2.get(i)%>')">
						</td>
					</tr>
				</table>
				</td>
				<%if((i+1)%5 == 0){%>
			</tr>
			<tr style="padding-left: 2">
				<%}%>
				<%}
			}else{
			%>
				<td width="295" height="145" align="center"><img
					src="/dmt1d/images/admin/record_icon.gif" width="190" height="20"></td>
				<%
			}

			%>

			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
