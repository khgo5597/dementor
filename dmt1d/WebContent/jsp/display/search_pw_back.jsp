<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="com.mininfo.user.model.UserHintEntity"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%!
	public static String stringValue(Object str){
	  if(str == null) return "";
	  return (String)str;
	}
%>
<%
UserHintEntity userEntity = (UserHintEntity)request.getAttribute("USER");
String STATUS = stringValue(request.getAttribute("STATUS"));
String SENDTYPE = stringValue(request.getAttribute("SENDTYPE"));
String message = "";
int resultCode = 0;
int processCode = 0;
if(request.getAttribute("RESULT") != null) {
	ResultEntity resultEntity = (ResultEntity)request.getAttribute("RESULT");
	message = resultEntity.getResultMessage();
	resultCode = resultEntity.getResultCode();
	processCode = resultEntity.getProcessCode();
}

String msg1= CodeMessageHandler.getInstance().getCodeMessage("5028");
String msg2= CodeMessageHandler.getInstance().getCodeMessage("5029");
String msg3= CodeMessageHandler.getInstance().getCodeMessage("5030");
String msg4= CodeMessageHandler.getInstance().getCodeMessage("5032");
String msg5= CodeMessageHandler.getInstance().getCodeMessage("5054");
String msg6= CodeMessageHandler.getInstance().getCodeMessage("5055");
String msg7= CodeMessageHandler.getInstance().getCodeMessage("5124");
String msg8= CodeMessageHandler.getInstance().getCodeMessage("5102");
String msg9= CodeMessageHandler.getInstance().getCodeMessage("5133");
%>
<html>
<head>
<title>::: DEMENTOR [search_pw]:::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" />
</style>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function check(){
		var pwf = document.pwfaq;
		var input = pwf.textfield2.value;
		var mailVal = null;
		
		if(pwf.mail != null) {
			mailVal = pwf.mail.value;
		} else {
			smsVal = pwf.sms.value;
		}
		
		if(input == ""){
			alert("<%=msg1%>");
			pwf.textfield2.focus();
			return;
		}
		if(mailVal == null) {
			var smsVal = pwf.sms.value;
			if (smsVal == ""){
				alert("<%=msg9%>");
				pwf.sms.focus();
				return;
			}
		} else {
			if(mailVal == ""){
				alert("<%=msg2%>");
				pwf.mail.focus();
				return;
			} else {
				var mail_exp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;
				if(!mail_exp.test(mailVal)){
					alert("<%=msg3%>");
					pwf.mail.focus();
					return;
				}
			}
		}
		
		//alert(mailVal+"<%=msg7%>");

		pwf.action = "Password.do";
		pwf.cmd.value = "sendMail";
		pwf.submit();	
	}

	function moveView(intWidth,intHeight){
		var bScrollbars;
        var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;

		if(intWidth >= screenWidth){ 
                intWidth = screenWidth - 20;
                bScrollbars = 1;
        }
        if(intHeight >= screenHeight){ 
                intHeight = screenHeight - 40;
                //intWidth = intWidth + 20;
                bScrollbars = 1;
        }

		var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;
		var intLeft = (screenWidth - intWidth) / 2;
		var intTop = (screenHeight - intHeight) / 2;

		if (navigator.appName.charAt(0) == "N") { 
				if (navigator.appVersion.charAt(0) == 2) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 3 ){ 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 4) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 5) { 
					intHeight += 6;
					intWidth -= 1;
				} 
		} 
		if (navigator.appName.charAt(0) == "M") {
				if (navigator.appVersion.charAt(0) == "2") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "3") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "4") {
						if(navigator.appVersion.indexOf("MSIE 5") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 6") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 7") != -1) {
						}
						if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
							intHeight -= 45;
						}
						if (navigator.appVersion.charAt(0) == "5") {
							intHeight -= 45;
						}
				}
		}

		window.moveTo(intLeft,intTop); 

		resizeTo(intWidth,intHeight);
	}
//-->
</SCRIPT>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<form name="pwfaq" method="post" action="javascript:check();"><input
	type="hidden" name="cmd" value="sendMail"> <input type="hidden"
	name="userId" value="<%=userEntity.getUser_id()%>"> <input
	type="hidden" name="status" value="<%=STATUS%>"> <input
	type="hidden" name="sendType" value="<%=SENDTYPE %>">
<table width="450" border="0" cellpadding="0" cellspacing="0">
	<%if("1".equalsIgnoreCase(STATUS) || "3".equalsIgnoreCase(STATUS)){%>
	<tr>
		<td height="40"><img src="/dmt1d/images/new/title_01.jpg"
			width="300" height="40" /></td>
	</tr>
	<tr>
		<td height="75"><img src="/dmt1d/images/new/comm_01.jpg"
			width="450" height="75" /></td>
	</tr>
	<%}else if("2".equalsIgnoreCase(STATUS)){%>
	<tr>
		<td height="40"><img src="/dmt1d/images/new/title_04.jpg"
			width="300" height="40" /></td>
	</tr>
	<tr>
		<td height="75"><img src="/dmt1d/images/new/comm_04.jpg"
			width="450" height="75" /></td>
	</tr>
	<%}%>
	<tr>
		<td height="20" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td height="112" align="center"><!--내용테이블시작 -->

		<table width="420" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="10"><img src="/dmt1d/images/new/border_01.gif"
					width="10" height="10" /></td>
				<td width="80" background="/dmt1d/images/new/border_02.gif"></td>
				<td width="10" background="/dmt1d/images/new/border_02.gif"></td>
				<td background="/dmt1d/images/new/border_09.gif"></td>
				<td width="10"><img src="/dmt1d/images/new/border_10.gif"
					width="10" height="10" /></td>
			</tr>

			<tr>
				<td background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<td height="30" align="center" bgcolor="#DBE9F1"><span
					class="style1"><%=msg4 %></span></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" style="padding-left:10px;"><select
					name="select" class="form1">
					<option><%=userEntity.getPass_hint_select_code()%></option>
				</select></td>
				<td background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
			</tr>
			<tr>
				<td background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg5 %></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" style="padding-left:10px;"><input
					name="textfield2" id="user_a" type="text" class="form1" size="35" /></td>
				<td background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
				<td bgcolor="#a8b2d7"></td>
			</tr>
			<tr>
				<td background="/dmt1d/images/new/border_03.gif">&nbsp;</td>
				<% if ("SMS".equals(SENDTYPE)) { %>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg8 %></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" style="padding-left:10px;"><input name="sms"
					type="text" class="form1" size="35" /></td>
				<% } else { %>
				<td height="30" align="center" bgcolor="#DBE9F1" class="style1"><%=msg6 %></td>
				<td align="center" bgcolor="#DBE9F1">&nbsp;</td>
				<td align="left" style="padding-left:10px;"><input name="mail"
					type="text" class="form1" size="35" /></td>
				<% } %>
				<td background="/dmt1d/images/new/border_07.gif">&nbsp;</td>
			</tr>

			<tr>
				<td width="10"><img src="/dmt1d/images/new/border_04.gif"
					width="10" height="10" /></td>
				<td background="/dmt1d/images/new/border_05.gif"></td>
				<td background="/dmt1d/images/new/border_05.gif"></td>
				<td background="/dmt1d/images/new/border_08.gif"></td>
				<td><img src="/dmt1d/images/new/border_06.gif" width="10"
					height="10" /></td>
			</tr>
		</table>

		<!--내용테이블 끝 --></td>
	</tr>
	<tr>
		<td height="20" align="center">&nbsp; <% if(message != null && !"".equals(message)) {%>
		<font color="red"><%=message%></font> <%}%>
		</td>
	</tr>
	<!--확인취소버튼 -->
	<tr>
		<td height="40" align="center"><a href="#"><img
			src="/dmt1d/images/new/btn_confirm.jpg" width="97" height="27"
			onClick="javascript:check()" border="0" /></a>&nbsp; &nbsp; <a href="#"><img
			src="/dmt1d/images/new/btn_cancel.jpg" width="97" height="27"
			onClick="javascript:self.close();" border="0" /></a></td>
	</tr>
</table>
</form>
</body>
</html>
