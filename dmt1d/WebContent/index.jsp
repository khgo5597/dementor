<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%@page import="com.mininfo.admin.category.DBencryption"%>
<%
	String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5001"); // 아이디를 입력해 주세요
	String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5002"); // 패스워드를 입력해 주세요
	String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5110"); // 유저ID
	String msg4 = CodeMessageHandler.getInstance().getCodeMessage("5111"); // 패스워드
%>
<html>
<head>
<script type="text/javascript" src="/dmt1d/js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
  window.name='dementor';
  // 페이지 로딩시 포커스를 id에 맞춘다. 
 	function focu(){
		document.form1.userId.focus();
    }

  	//확인 버튼 클릭시 form값 체크함수 
	function rgCheck(){ 
		var F = document.form1;
		var userId = F.userId.value;
		
		if(userId == ""){
			alert("<%=msg1%>"); // 아이디를 입력해 주세요
			F.userId.focus();
			return ; 
		}
		
		popupOpen('about:blank',"auth_level_1",'699','510'); //팝업오픈 (새빈창, 사용자레벨, 세로사이즈, 가로사이즈) 
		F.target = "auth_level_1"; //사용자 레벨 
		F.action="/dmt1d/dementorConnector.jsp"; //그래픽인증 설정페이지로 이동
		F.submit();
	}
  //-->
</SCRIPT>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" type="text/css" href="/dmt1d/css/style.css" />
</head>
<body onload="javascript:focu();" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'> 
<form name="form1" method="post" onSubmit="return rgCheck()">
<!-- <input type="hidden" name="cmd" value="loginAction">
<input type="hidden" name="page" value="main">
-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="middle">
	 <table width="960" height="600" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="middle">
		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" valign="middle">
				<table width="714" height="282" border="0" cellpadding="0" cellspacing="0" background="/dmt1d/images/user/bg_login.gif">
				  <tr>
					<td width="310" height="150">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="210">&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td>
							 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr><!-- 유저ID -->
								  <td height="25"><img src="/dmt1d/images/common/icon_px.gif" width="10" height="8" border="0" /><%=msg3%></td>
								  <td><input type="text" name="userId" class="textbox_2" size="8" class="textbox_2" size="8" maxlength="10" tabindex = '1' /></td>
								  <td><a href="javascript:rgCheck();"><img src="/dmt1d/images/common/btn_loginok.gif" width="42" height="42" border="0"/></a></td>
								</tr>
							</table>
							</td>
						  </tr>
						</table>
					</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td height="40">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
			  </td>
            </tr>
          </table>
		  </td>
      </tr>
    </table>
	</td>
  </tr>
</table>
</form>
</body>
</html>
