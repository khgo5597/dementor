<?xml version="1.0" encoding="utf-8"?>
<%@page contentType="text/xml;charset=UTF-8"%>
<jsp:useBean id="mkm" class="com.mininfo.engine.MindkeyMain" scope="session" />
<%

	String httpVersion=request.getProtocol();

	String browser = "";
	String userAgent = request.getHeader("User-Agent");
	System.out.println(userAgent);
	if (userAgent.indexOf("Trident/7.0") > 0) {
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Pragma","no-cache");
	} else {		
		response.setDateHeader("Expires",-1);
	}
	
	String userId = (String)session.getAttribute("userId");
	String iconurl[] = (String[])session.getAttribute("iconurl");
	// 세션 정보 삭제
	session.removeAttribute("userId");
	session.removeAttribute("iconurl");
%>
<iconList>
	<% for (int i = 0; i < iconurl.length; i++) { %>
		<icon id='<%=i %>' name="<%=i%>">..<%=iconurl[i] %></icon>
	<% } %>
</iconList>	