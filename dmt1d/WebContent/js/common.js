var check_alnum = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
var check_num = "0123456789";

function getSelected(obj_name) { //getSelected('ObjectName');
	var f = document.getElementsByName(obj_name);
	var val = '';
	for(var i=0;i<f.length;i++) {
		if(f[i].checked == true) val += (val != '') ? ","+f[i].value : f[i].value;
	}
	return val;
}

function checkCheckbox(obj, target_name) { //checkCheckbox(this, 'TargetName');
	var f = document.getElementsByName(target_name);
	for(var i=0;i<f.length;i++) f[i].checked = obj.checked;
}

function winOpen(url, name, scroll, resize) {
	if(url) window.open(url, name, 'width=100,height=100,scrollbars='+scroll+',resizable='+resize);
}

function checkValid(f, valid, msg) { //onKeyUp="return checkValid(this, 'asdfhjk', 'error message')"
	var obj_value = f.value;
	var not_good = false;
	for (var i=0;i<obj_value.length;i++) {
		if(valid.indexOf(obj_value.substring(i,i+1)) >= 0) continue;
		else {
			not_good = true;
			break;
		}
	}
	if(not_good == true) {
		alert(msg);
		f.value = '';
		f.focus();
	}
}

function tabEnter(field, event) { //onKeyUp="return tabEnter(this, event)"
	var keyCode = event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode);
	if(keyCode == 13) {
		for(var i=0;i<field.form.elements.length; i++) if (field == field.form.elements[i]) break;
		var idx = (i + 1) % field.form.elements.length;
		field.form.elements[idx].focus();
		return false;
	} else return false;
}

function checkJumin(field1, field2, msg, br1, br2, br3) {
	var sum = 0;
	var jumin1 = field1.value;
	var jumin2 = field2.value;

	if(!jumin1 || !jumin2) return false;
	
	sum += jumin1.charAt(0) * 2;
	sum += jumin1.charAt(1) * 3;
	sum += jumin1.charAt(2) * 4;
	sum += jumin1.charAt(3) * 5;
	sum += jumin1.charAt(4) * 6;
	sum += jumin1.charAt(5) * 7;
	
	sum += jumin2.charAt(0) * 8;
	sum += jumin2.charAt(1) * 9;
	sum += jumin2.charAt(2) * 2;
	sum += jumin2.charAt(3) * 3;
	sum += jumin2.charAt(4) * 4;
	sum += jumin2.charAt(5) * 5;
	
	check = (11 - sum%11) % 10;
	
	if(jumin2.charAt(6) != check || (jumin1.length != 6 && jumin2.length != 7)) {
		alert(msg);
		field1.value = '';
		field2.value = '';
		field1.focus();
		return false;
	}
	
	if(!(jumin2.charAt(0) == '1' || jumin2.charAt(0) == '2' || jumin2.charAt(0) == '3' || jumin2.charAt(0) == '4')) {
		alert(msg);
		field1.value = '';
		field2.value = '';
		field1.focus();
		return false;
	}
	if(br1 && br2 && br3) {
		tmp = jumin1.substring(0,2);
		br1.value = (parseInt(tmp) <= 10) ? "20"+tmp : "19"+tmp;
		br2.value = jumin1.substring(2,4);
		br3.value = jumin1.substring(4,6);
	}
	return true;
}

function getDays(year, month) {
	var nowDate = new Date();
	if(!year) year = nowDate.getYear();
	month = parseInt(month);

	var lastdate=new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if((0 == year%4 && 0 != year%100) || 0 == year%400) lastdate[1] = 29;

	return lastdate[month-1];
}

function getDaysOption(year, month, obj, value) {
	var days = getDays(year, month);
	var selected;

	obj.options.length = days;
	for(var i=0, j=1; i<days; i++, j++) {
		obj.options[i] = new Option(j, j);
		obj.options[i].selected = (value == j) ? true : false;
	}
}

function popupOpen(url,name,width,height) {
	var intWidth=500;
	var intHeight=450;
	var bScrollbars;
    var screenWidth = screen.availwidth;
    var screenHeight = screen.availheight;

	if(intWidth >= screenWidth){ 
               intWidth = screenWidth - 20;
               bScrollbars = 1;
       }
       if(intHeight >= screenHeight){ 
               intHeight = screenHeight - 40;
               //intWidth = intWidth + 20;
               bScrollbars = 1;
       }

	var screenWidth = screen.availwidth;
    var screenHeight = screen.availheight;
	var intLeft = (screenWidth - intWidth) / 2;
	var intTop = (screenHeight - intHeight) / 2;

	if (navigator.appName.charAt(0) == "N") { 
			if (navigator.appVersion.charAt(0) == 2) { 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 3 ){ 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 4) { 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 5) { 
				intHeight += 6;
				intWidth -= 1;
			} 
	} 
	if (navigator.appName.charAt(0) == "M") {
			if (navigator.appVersion.charAt(0) == "2") {
				intHeight -= 45;
			} 
			if (navigator.appVersion.charAt(0) == "3") {
				intHeight -= 45;
			} 
			if (navigator.appVersion.charAt(0) == "4") {
					if(navigator.appVersion.indexOf("MSIE 5") != -1) {
						intHeight -= 45;
					} 
					if(navigator.appVersion.indexOf("MSIE 6") != -1) {
						intHeight -= 45;
					} 
					if(navigator.appVersion.indexOf("MSIE 7") != -1) {
					}
					if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
						intHeight -= 45;
					}
					if (navigator.appVersion.charAt(0) == "5") {
						intHeight -= 45;
					}
			}
	}
	var win = window.open(url,name,'left='+intLeft+', top='+intTop+', width='+width+', height='+height+', scrollbars=auto, resizable=no');
	
	
	var b_ChkSP2 = false;
	b_ChkSP2 = (window.navigator.userAgent.indexOf("SV1") != -1);
	
	if(b_ChkSP2) {
		if(win == null) {
			alert("Due to the Windows XP Service Pack 2, Pop-up window is blocked.\nPlease disable pop-up blocking.");
		}
	}
	win.focus();
}
function popupScrollOpen(url,name,width,height) {
	var intWidth=500;
	var intHeight=450;
	var bScrollbars;
    var screenWidth = screen.availwidth;
    var screenHeight = screen.availheight;

	if(intWidth >= screenWidth){ 
               intWidth = screenWidth - 20;
               bScrollbars = 1;
       }
       if(intHeight >= screenHeight){ 
               intHeight = screenHeight - 40;
               //intWidth = intWidth + 20;
               bScrollbars = 1;
       }

	var screenWidth = screen.availwidth;
    var screenHeight = screen.availheight;
	var intLeft = (screenWidth - intWidth) / 2;
	var intTop = (screenHeight - intHeight) / 2;

	if (navigator.appName.charAt(0) == "N") { 
			if (navigator.appVersion.charAt(0) == 2) { 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 3 ){ 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 4) { 
				intHeight += 6;
				intWidth -= 1;
			} 
			if (navigator.appVersion.charAt(0) == 5) { 
				intHeight += 6;
				intWidth -= 1;
			} 
	} 
	if (navigator.appName.charAt(0) == "M") {
			if (navigator.appVersion.charAt(0) == "2") {
				intHeight -= 45;
			} 
			if (navigator.appVersion.charAt(0) == "3") {
				intHeight -= 45;
			} 
			if (navigator.appVersion.charAt(0) == "4") {
					if(navigator.appVersion.indexOf("MSIE 5") != -1) {
						intHeight -= 45;
					} 
					if(navigator.appVersion.indexOf("MSIE 6") != -1) {
						intHeight -= 45;
					} 
					if(navigator.appVersion.indexOf("MSIE 7") != -1) {
					}
					if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
						intHeight -= 45;
					}
					if (navigator.appVersion.charAt(0) == "5") {
						intHeight -= 45;
					}
			}
	}
	var win = window.open(url,name,'left='+intLeft+', top='+intTop+', width='+width+', height='+height+', scrollbars=yes,status=no');
	
	
	var b_ChkSP2 = false;
	b_ChkSP2 = (window.navigator.userAgent.indexOf("SV1") != -1);
	
	if(b_ChkSP2) {
		if(win == null) {
			alert("Due to the Windows XP Service Pack 2, Pop-up window is blocked.\nPlease disable pop-up blocking.");
		}
	}
	win.focus();
}

function popupMove(intWidth,intHeight) { 
		var bScrollbars;
        var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;

		if(intWidth >= screenWidth){ 
                intWidth = screenWidth - 20;
                bScrollbars = 1;
        }
        if(intHeight >= screenHeight){ 
                intHeight = screenHeight - 40;
                //intWidth = intWidth + 20;
                bScrollbars = 1;
        }

		var screenWidth = screen.availwidth;
        var screenHeight = screen.availheight;
		var intLeft = (screenWidth - intWidth) / 2;
		var intTop = (screenHeight - intHeight) / 2;

		if (navigator.appName.charAt(0) == "N") { 
				if (navigator.appVersion.charAt(0) == 2) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 3 ){ 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 4) { 
					intHeight += 6;
					intWidth -= 1;
				} 
				if (navigator.appVersion.charAt(0) == 5) { 
					intHeight += 6;
					intWidth -= 1;
				} 
		} 
		if (navigator.appName.charAt(0) == "M") {
				if (navigator.appVersion.charAt(0) == "2") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "3") {
					intHeight -= 45;
				} 
				if (navigator.appVersion.charAt(0) == "4") {
						if(navigator.appVersion.indexOf("MSIE 5") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 6") != -1) {
							intHeight -= 45;
						} 
						if(navigator.appVersion.indexOf("MSIE 7") != -1) {
						}
						if(navigator.appVersion.indexOf("MSIE 5") != -1 && navigator.appVersion.indexOf("MSIE 6") != -1 && navigator.appVersion.indexOf("MSIE 7") != -1){ 
							intHeight -= 45;
						}
						if (navigator.appVersion.charAt(0) == "5") {
							intHeight -= 45;
						}
				}
		}

		window.moveTo(intLeft,intTop); 		
		resizeTo(intWidth,intHeight);
}