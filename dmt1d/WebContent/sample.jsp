<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.mininfo.common.model.ResultEntity"%>
<%@page import="com.mininfo.common.Const"%>
<%@page import="com.mininfo.common.code.CodeMessageHandler"%>
<%
String msg1 = CodeMessageHandler.getInstance().getCodeMessage("5120"); //사용자관리
String msg2= CodeMessageHandler.getInstance().getCodeMessage("5121"); // 관리자 기능
String msg3 = CodeMessageHandler.getInstance().getCodeMessage("5006"); // 회원수정
String msg4= CodeMessageHandler.getInstance().getCodeMessage("5008"); // 암호찾기
String msg5 = CodeMessageHandler.getInstance().getCodeMessage("5009"); // 암호 재설정
String msg6= CodeMessageHandler.getInstance().getCodeMessage("5004"); // 님
String msg7= CodeMessageHandler.getInstance().getCodeMessage("5125"); // 관리자 기능입니다

String sDomain = request.getServerName() + ":" + Integer.toString(request.getServerPort());			// 도메인 및 포트 셋팅 - 20131224

/*
//20140421 관리자 권한 접속 IP 제한
String currentIP = request.getRemoteAddr();//현재 접속
/*
부산은행 관리자 권한 접근 가능 IP
130.36.36.115(개발)
130.36.36.135(검증)
130.36.34.55 / 130.36.34.56 (운영)

String passIP[] = {"130.36.36.115","130.36.36.135","130.36.34.55","130.36.34.56"};
int iOk = 0;

for(int i = 0; i < passIP.length; i++){
 if(passIP[i].equals(currentIP) ){
    iOk = 1;
    break;
 }
}

/*
if(iOk == 0){
 System.out.println("[허용되지 않은 아이피] : "+currentIP);
 response.sendRedirect("../dmt1d");
 
 return;
}
*/

%>
<%-- Check session --%>
<%@include file="common/anticache.jsp"%>
<%@include file="common/userAttribute.jsp"%>
<html>
<head>
<title>::: DEMENTOR :::</title>
<link rel="stylesheet" type="text/css" href="css/styleS.css" >
<script type="text/javascript" src="js/common.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function goUserMenu(str) {
		if ( str == '1' ) {
			location.href="http://<%=sDomain%>/dmt1d/user/Login.do?cmd=logoutAction&userId=<%=USER_ID%>";
		} else if( str == '5') {
			popupOpen("passTransfer.jsp?userId=<%=USER_ID%>",'auth_level_2_reset',450,300);
			//moveView(str);
			//win.moveTo(0,0);
			//win.focus();
	//		main.location.href="/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>";
		} else if( str == '4') {
			if("<%=dmt1d_AUTH%>" != "Y"){
				popupOpen('/dmt1d/user/Login.do?cmd=dmt1dCheck&userId=<%=USER_ID%>','loginForm',990,730);
				//var win = window.open("http://localhost:8080/dmt1d/user/Login.do?cmd=dmt1dCheck&userId=<%=USER_ID%>",'loginForm','left=1, top=1, width=990, height=730, scrollbars=auto, resizable=no');
				//win.moveTo(0,0);
				//win.focus();
			}else if("<%=IS_ADMIN%>" != "true"){
				alert("<%=msg7%>");
			}else{
				var win = window.open("http://<%=sDomain%>/dmt1d/user/Login.do?cmd=mainPage&USER_ID_S=<%=USER_ID%>",'adminForm','left=1, top=1, width=990, height=730, scrollbars=auto, resizable=no');
				win.moveTo(0,0);
				win.focus();
			}
		} else if( str == '3') {
					popupOpen("http://localhost:8080/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>&status=2","auth_level_2_reset",460,300);
				//var win = window.open("http://디멘터/dmt1d/client/Password.do?cmd=passwordSearchForm&userId=<%=USER_ID%>&status=2",'auth_level_2_reset','width=460, height=300, scrollbars=auto, resizable=no');
				//win.moveTo(0,0);
				//win.focus();
			
		
		} else if( str == '2') {
			main.location.href="http://<%=sDomain%>/dmt1d/userInfoModify.jsp?userId=<%=USER_ID%>";
		}
	}	

	function moveView(str){
		var screenWidth = screen.width;
	    var screenHeight = screen.height;
	    var w = 460;
	    var h = 300;
	       
		var intLeft = (screenWidth-w) / 2;
		var intTop = (screenHeight-h) / 2;	
	
		if(str == '5') {
			var win = window.open('passTransfer.jsp?userId=<%=USER_ID%>','auth_level_2_reset','left='+intLeft+', top='+intTop+', width='+w+', height='+h+', scrollbars=auto, resizable=no');
			win.focus();
		}
	}
-->
</SCRIPT>

</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" class="bg">
	 <table width="960" height="700" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="60" valign="top">
		 <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="56" align="left" valign="top">
			    <table width="100%" height="56" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="56"><a href="#"><img src="images/common/logo_sam.gif" width="208" height="30" border="0" /></a></td>
                    <td width="736"></td>
                  </tr>
              </table>
			  </td>
            </tr>
            <tr>
              <td height="4" bgcolor="#3399cc"></td>
            </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="180" height="10">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="180" align="left" valign="top" class="padd2">
			    <table width="180" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="4" bgcolor="#3399cc"></td>
                  </tr>
                  <tr>
                    <!------------로그인------------------------------ 
                      안녕하세요<a href="#"><img src="images/btn_login.gif" width="84" height="30" border="0"></a></td>
 --------------------------------------------------->
                    <td height="86" align="center" bgcolor="#3399CC"><a href="#"></a>
                        <table width="172" border="0" cellpadding="0" cellspacing="0" bordercolor="0">
                          <tr>
                            <td height="60" align="center" bgcolor="#FFFFFF"><%=USER_ID%>&nbsp;<%=msg6%>&nbsp;<a href="#"><img src="images/common/btn_logout.gif" height="16" border="0" align="absmiddle" onclick="javascript:goUserMenu(1);"></a></td>
                          </tr>
                          <tr>
                            <td height="1" align="center" bgcolor="#FFFFFF">
							  <table width="160" height="1" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td background="images/bg_dotline.gif"></td>
                                </tr>
                            </table>
							</td>
                          </tr>
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_to.gif" width="20" height="9" border="0" align="absmiddle"><span class="black"><a href="#"><%=msg1%></a></span></td>
                          </tr>
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_bullut.gif" width="31" height="9" border="0" align="absmiddle"><a href="javascript:goUserMenu(2);"><%=msg3%></a></td>
                          </tr>
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_bullut.gif" width="31" height="9" border="0"  align="absmiddle"><a href="javascript:goUserMenu(5);"><%=msg4%></a></td>
                          </tr>
                          <!-- 
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_bullut.gif" width="31" height="9" border="0" align="absmiddle"><a href="javascript:goUserMenu(3)"><%=msg5%></a></td>
                          </tr> 
                          -->
                          <%if(dmt1d_AUTH.equals("Y") && IS_ADMIN){ %>
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_to.gif" width="20" height="9" border="0" align="absmiddle"><span class="black"><a href="javascript:goUserMenu(4);"><%=msg2%></a></span></td>
                          </tr>
                          <%} %>
                          <!-- 
                          <tr>
                            <td height="30" bgcolor="#FFFFFF"><img src="images/common/icon_bullut.gif" width="31" height="9" border="0" align="absmiddle"><a href="javascript:goUserMenu(5);"><%=msg4%></a></td>
                          </tr> 
                          -->
                          <tr>
                            <td height="30" bgcolor="#FFFFFF">&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="30" bgcolor="#FFFFFF">&nbsp;</td>
                          </tr>
                      </table>
					  </td>
                  </tr>
                  <tr>
                    <td height="4" bgcolor="#3399CC"></td>
                  </tr>
              </table>
			  </td>
              <td align="left"><iframe allowTransparency="true" name="main" frameborder="0" bordercolor="#FFFFFF" marginwidth="0" scrolling="auto" marginheight="0" width="800" height="640" style="padding:5 0 0 2">
			</iframe></td>
            </tr>
        </table>
		</td>
      </tr>
    </table>
	</td>
  </tr>
</table>
</body>
</html>
