<%@page import="com.mininfo.client.dao.MysqlclientDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.mininfo.user.dao.UserDAOImpl"%>
<%@ page import="com.mininfo.user.dao.UserDAO"%>
<%@ page import="com.mininfo.user.model.UserEntity"%>
<%@ page import="com.mininfo.common.verify.VerifySign"%>
<%
	String userId = request.getParameter("userId");											// USER ID
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>고객사 로그인 처리</title>
<script>

	function chk_update() {
		document.chkForm.action = "/dmt1d/cate/Cate.do?cmd=keyedit&cmdType=M";
		document.chkForm.submit();
	}

</script>
</head>
<body>
<form name="chkForm" method="post">
	<input type="hidden" name="userId" value="<%=userId%>" >
</form>
<%
	String isOtkUse = "Y";	// OTK 사용 여부
	
	if ("Y".equals(isOtkUse)) {
		if (request.getParameter("otk") != null) {													// OTK 사용
			VerifySign verifySign = new VerifySign();												// 싸인 검증 클래스 생성
			String otk = request.getParameter("otk");												// OTK
			String urlId = request.getParameter("urlId");											// URL ID
			String userIp = request.getParameter("userIp");											// USER IP
			String strNow = request.getParameter("strNow");											// NOW
			String dmt1d_auth = request.getParameter("dmt1d_auth");									// auth
			String upChk = request.getParameter("upChk");									        // update 체크
			boolean signCheck = verifySign.isVerifySign(request, otk, urlId, userId,userIp, strNow);// 싸인 검증 메소드 호출
			session.setAttribute("dmt1d_AUTH",dmt1d_auth);
			System.out.println("signCheck : "+signCheck);
						
			if (!signCheck) {	// 싸인 검증
				// 싸인 검증 실패 (현재는 인증화면 재 호출)
%>
				<script>
				if("<%=dmt1d_auth%>" == "Y"){
					document.location.href = "index.jsp";
				} else {                    
                    alert("인증실패");
                    document.title="certFail";
<%--
                    window.open('/dmt1d/messageChk.jsp?sendMessage=certFalse','new','scrollbars=yes resizable=no width=420 height=300');
--%>
                    window.opener='nothing';
                    window.open('','_parent','');					
                    window.close();                                                                                                    
				}
				</script>
				<input type="hidden" name="sendMessage" value="certFalse">
<%
			} else {
				UserDAO userDAO = new UserDAOImpl();
				UserEntity userEntity = userDAO.getUserInfo(userId);
				MysqlclientDAO client = new MysqlclientDAO();
				client.dmtloginCntEdit(0,userId);	//인증 성공시 실패횟수 초기화 처리
				
				session.setAttribute("USER", userEntity);
%>
				<script>
				if("<%=dmt1d_auth%>" == "Y"){
                    document.location.href = "sample.jsp";
				} else {					
					if("<%=upChk%>"=="up"){
						document.chkForm.action = "/dmt1d/client/Client.do?cmd=getKey";
						document.chkForm.submit();		
					} else {
						alert("인증성공");
	                    document.title="certOK";
	                    window.opener='nothing';
	                    window.open('','_parent','');					
	                    window.close();
					}
				}
                    					
<%--
                    window.open('/dmt1d/messageChk.jsp?sendMessage=certTrue','new','scrollbars=yes resizable=no width=420 height=300');
--%>
				</script>
				<input type="hidden" name="sendMessage" value="certTrue">
<%
			}
		}
	}
%>
</body>
</html>