<%@ page contentType="text/html;charset=utf-8" import="com.mininfo.user.model.UserEntity,com.mininfo.common.verify.SiteVerify,com.mininfo.common.PageConst"%>
<%
	/* 관리자 페이지 접속 JSP */
	/* 작업자 : 박찬우 주임     */
	//String userId = (String)session.getAttribute("userId");
	String userId = "admin";
	if (userId != null) {
		UserEntity userEntity = new UserEntity();
	
		userEntity.setUser_id(userId);
		userEntity.setAdmin_yn("Y");
	
		session.setAttribute("USER", userEntity);
		session.setAttribute("dmt1d_AUTH", "Y");
		String param = "?cmd=mainPage&USER_ID_S="+userId;
		String selectdmtlogin="/dmt1d/user/Login.do" + param;
		SiteVerify siteVerify = new SiteVerify();
		if (!siteVerify.verify(request)) {
	response.sendRedirect(PageConst.failed);
		} else {
	response.sendRedirect(selectdmtlogin);
		}
	} else {
		// 오류시 이동 될 페이지 (수정 필요)
		response.sendRedirect("http://121.160.48.206:9090");
	}
%>
