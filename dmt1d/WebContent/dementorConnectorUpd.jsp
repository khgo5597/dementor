<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" import="java.text.SimpleDateFormat, java.util.Date, java.util.Enumeration, com.mininfo.common.crypto.SHA1Client, java.math.BigInteger, java.util.HashMap, com.mininfo.common.util.CommonUtil "%>
<%	
	// URL ID
	String urlId = "dementor";
	
	// 사용자 IP
	String userIp = request.getRemoteAddr();		
	
	// 현재 시간 얻기(밀리세컨드 단위)
	long now = System.currentTimeMillis();	
	SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	String strNow = sdfNow.format(new Date(now));
	String userId = request.getParameter("userId");
	//System.out.println(strNow);
	//System.out.println(userId);
	
	System.out.println("userIP : "+userIp+", userId : "+userId);
	
	SHA1Client sha1 = new SHA1Client(urlId, userId, userIp, strNow);
	// otk 얻기
	String otk = CommonUtil.byteArrayToHex(sha1.getOtk());
	// 접속시도자 정보 세션 저장
	HashMap userInfoMap = new HashMap();
	userInfoMap.put("urlId", urlId);
	userInfoMap.put("userId", userId);
	userInfoMap.put("userIp", userIp);
	userInfoMap.put("strNow", strNow);
	session.setAttribute("loginUserInfo", userInfoMap);
	
	// 디멘터 1.5 동작 여부 세션에 저장
	String dmt15 = request.getParameter("dmt15");
	session.setAttribute("dmt15", dmt15);
	
	System.out.println("otk : "+otk+", dmt15 : "+dmt15);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>클라이언트 - 디멘터 중간 단계</title>
</head>
<body>
<%
	if(userId!=null) {
%>
	<!-- 수정 필요 -->
	<form name="form1" method="post" action="/dmt1d/client/Client.do"> <!-- ClientController.class 호출  -->
		<input type="hidden" name="cmd" value="display">		
		<input type="hidden" name="page" value="main">
		<input type="hidden" name="urlId" value="<%=urlId %>"/>
		<input type="hidden" name="userId" value="<%=userId %>"/>
		<input type="hidden" name="userIp" value="<%=userIp %>"/>
		<input type="hidden" name="now" value="<%=strNow %>"/>
		<input type="hidden" name="otk" value="<%=otk %>"/>
		<!-- index window.name -->
		<input type="hidden" name="windowName" value="client"/>
		<input type="hidden" name="upChk" value="up"/>
	</form>
	<script>

			document.form1.submit();

	</script>
<%
	} else {
%>

	<!-- 수정 필요 -->
	<form name="form1" method="post" action="/dmt1d/client/Client.do"> <!-- ClientController.class 호출  -->
		<input type="hidden" name="cmd" value="display">		
		<input type="hidden" name="page" value="main">
		<input type="hidden" name="urlId" value="<%=urlId %>"/>
		<input type="hidden" name="userId" value="<%=userId %>"/>
		<input type="hidden" name="userIp" value="<%=userIp %>"/>
		<input type="hidden" name="now" value="<%=strNow %>"/>
		<input type="hidden" name="otk" value="<%=otk %>"/>
		<!-- index window.name -->
		<input type="hidden" name="windowName" value="client"/>
		<input type="hidden" name="upChk" value="up"/>
	</form>
	<script>

			alert("잘못된 접근 입니다.");

	</script>
<%
	}
%>
</body>
</html>
