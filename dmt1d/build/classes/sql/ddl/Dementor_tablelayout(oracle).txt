CREATE TABLE FRAME_MEMBER
(
  USER_ID        VARCHAR2(20 BYTE)              DEFAULT ''                    NOT NULL,
  USER_PASS      VARCHAR2(41 BYTE)              DEFAULT '',
  USER_NAME      VARCHAR2(20 BYTE)              DEFAULT '',
  USER_LUNAR     VARCHAR2(2 BYTE)               DEFAULT '',
  USER_BIRTH     VARCHAR2(20 BYTE)              DEFAULT '',
  USER_EMAIL     VARCHAR2(100 BYTE)             DEFAULT '',
  USER_PHONE     VARCHAR2(13 BYTE)              DEFAULT '',
  USER_MOBILE    VARCHAR2(13 BYTE)              DEFAULT '',
  USER_ZIP       VARCHAR2(7 BYTE)               DEFAULT '',
  USER_ADDRESS1  VARCHAR2(100 BYTE)             DEFAULT '',
  USER_ADDRESS2  VARCHAR2(100 BYTE)             DEFAULT '',
  USER_QUESTION  VARCHAR2(255 BYTE)             DEFAULT '',
  USER_ANSWER    VARCHAR2(255 BYTE)             DEFAULT '',
  USER_LASTDATE  DATE                           DEFAULT NULL,
  USER_REGDATE   DATE                           DEFAULT NULL,
  ADMIN_YN       VARCHAR2(2 BYTE)               DEFAULT 'N',
  USER_MAILING   VARCHAR2(2 BYTE)               DEFAULT NULL,
  USER_STATUS    VARCHAR2(2 BYTE)               DEFAULT 'N',
  ADMIN_PASS     VARCHAR2(100 BYTE)             DEFAULT NULL
);



CREATE TABLE L_ACCESS_LOG_TBL
(
  USER_ID        VARCHAR2(80 BYTE)              NOT NULL,
  ACCESS_DATE    DATE                           NOT NULL,
  ACCESS_IP      VARCHAR2(30 BYTE)              DEFAULT NULL,
  ACCESS_RESULT  VARCHAR2(1 BYTE)               DEFAULT NULL
);



CREATE TABLE L_ADV_LOG_TBL
(
  ADV_ID          NUMBER(10)                    NOT NULL,
  USER_ID         VARCHAR2(40 BYTE)             NOT NULL,
  ADV_CLICK_DATE  DATE                          NOT NULL
);



CREATE TABLE L_USER_DELETE_LOG_TBL
(
  USER_ID        VARCHAR2(80 BYTE)              DEFAULT ''                    NOT NULL,
  REG_DATE       DATE                           DEFAULT NULL,
  DELETE_DATE    DATE                           DEFAULT NULL,
  DELETE_STATUS  VARCHAR2(2 BYTE)               DEFAULT NULL
);



CREATE TABLE M_MANAGER_TBL
(
  MANAGER_ID      VARCHAR2(40 BYTE)             NOT NULL,
  MANAGER_LEVEL   CHAR(1 BYTE)                  NOT NULL,
  MANAGER_NAME    VARCHAR2(40 BYTE)             NOT NULL,
  MANAGER_PHONE   VARCHAR2(20 BYTE)             DEFAULT NULL,
  MANAGER_MOBILE  VARCHAR2(20 BYTE)             DEFAULT NULL,
  MANAGER_MAIL    VARCHAR2(20 BYTE)             DEFAULT NULL,
  MANAGER_PASS    VARCHAR2(40 BYTE)             NOT NULL
);



CREATE TABLE S_AUTH_CHECK
(
  USER_ID      VARCHAR2(20 BYTE)                DEFAULT ''                    NOT NULL,
  USER_AUTH    VARCHAR2(50 BYTE)                DEFAULT NULL,
  ACCESS_TIME  DATE                             DEFAULT NULL
);



CREATE TABLE S_AUTH_CHECK_TBL
(
  USER_CHECK  VARCHAR2(10 BYTE)                 DEFAULT ''                    NOT NULL,
  USER_ID     VARCHAR2(10 BYTE)                 DEFAULT ''                    NOT NULL,
  ANSWER_0    NUMBER(11)                        DEFAULT '0',
  ANSWER_1    NUMBER(11)                        DEFAULT '0',
  ANSWER_2    NUMBER(11)                        DEFAULT '0',
  ANSWER_3    NUMBER(11)                        DEFAULT '0',
  REF_DATE    VARCHAR2(257 BYTE)                DEFAULT NULL,
  TIMESTAMP   VARCHAR2(15 BYTE)                 DEFAULT ''                    NOT NULL
);



CREATE TABLE S_CATEGORY_ICON_TBL
(
  ICON_ID        NUMBER(10)                     NOT NULL,
  CATEGORY_ID    NUMBER(10)                     NOT NULL,
  ICON_NAME      VARCHAR2(100 BYTE)             NOT NULL,
  ICON_URL       VARCHAR2(120 BYTE)             NOT NULL,
  ICON_FILENAME  VARCHAR2(80 BYTE)              NOT NULL,
  ICON_STATUS    CHAR(1 BYTE)                   DEFAULT '0'                   NOT NULL,
  REG_DATE       DATE                           DEFAULT NULL
);



CREATE TABLE S_CATEGORY_TBL
(
  CATEGORY_ID        NUMBER(10)                 NOT NULL,
  CATEGORY_NAME      VARCHAR2(100 BYTE)         NOT NULL,
  CATEGORY_URL       VARCHAR2(120 BYTE)         NOT NULL,
  CATEGORY_FILENAME  VARCHAR2(80 BYTE)          NOT NULL,
  CATEGORY_STATUS    CHAR(1 BYTE)               DEFAULT '0'                   NOT NULL,
  REG_DATE           DATE                       DEFAULT NULL
);



CREATE TABLE S_SETTING_TBL
(
  MAIL_SERVER              VARCHAR2(100 BYTE)   NOT NULL,
  MAIL_PORT                VARCHAR2(10 BYTE)    DEFAULT ''                    NOT NULL,
  MAIL_FROM                VARCHAR2(100 BYTE)   NOT NULL,
  MAIL_FROM_NAME           VARCHAR2(100 BYTE)   NOT NULL,
  MAIL_SUBJECT             VARCHAR2(150 BYTE)   NOT NULL,
  MAIL_AUTH_YN             CHAR(1 BYTE)         DEFAULT 'N'                   NOT NULL,
  MAIL_AUTH_ID             VARCHAR2(50 BYTE)    DEFAULT NULL,
  MAIL_AUTH_PW             VARCHAR2(50 BYTE)    DEFAULT NULL,
  AUTH_CODE_EXPIRE_MINUTE  NUMBER(11)           DEFAULT '5'                   NOT NULL
);



CREATE TABLE U_USER_AUTH_CODE_TBL
(
  USER_ID      VARCHAR2(80 BYTE)                NOT NULL,
  CREATE_DATE  DATE                             NOT NULL,
  AUTH_CODE    VARCHAR2(20 BYTE)                NOT NULL
);



CREATE TABLE U_USER_KEY_TBL
(
  USER_ID      VARCHAR2(80 BYTE)                NOT NULL,
  KEY_COUNT    NUMBER(10)                       DEFAULT '0',
  KEY_HOLE     NUMBER(10)                       DEFAULT NULL,
  KEY_1        NUMBER(10)                       DEFAULT NULL,
  KEY_2        NUMBER(10)                       DEFAULT NULL,
  KEY_3        NUMBER(10)                       DEFAULT NULL,
  KEY_4        NUMBER(10)                       DEFAULT NULL,
  KEY_5        NUMBER(10)                       DEFAULT NULL,
  KEY_6        NUMBER(10)                       DEFAULT NULL,
  KEY_7        NUMBER(10)                       DEFAULT NULL,
  KEY_8        NUMBER(10)                       DEFAULT NULL,
  KEY_9        NUMBER(10)                       DEFAULT NULL,
  KEY_10       NUMBER(10)                       DEFAULT NULL,
  KEY_11       NUMBER(10)                       DEFAULT NULL,
  KEY_12       NUMBER(10)                       DEFAULT NULL,
  KEY_13       NUMBER(10)                       DEFAULT NULL,
  KEY_14       NUMBER(10)                       DEFAULT NULL,
  KEY_15       NUMBER(10)                       DEFAULT NULL,
  KEY_16       NUMBER(10)                       DEFAULT NULL,
  KEY_17       NUMBER(10)                       DEFAULT NULL,
  KEY_18       NUMBER(10)                       DEFAULT NULL,
  KEY_19       NUMBER(10)                       DEFAULT NULL,
  KEY_20       NUMBER(10)                       DEFAULT NULL,
  KEY_21       NUMBER(10)                       DEFAULT NULL,
  KEY_22       NUMBER(10)                       DEFAULT NULL,
  KEY_23       NUMBER(10)                       DEFAULT NULL,
  KEY_24       NUMBER(10)                       DEFAULT NULL,
  KEY_25       NUMBER(10)                       DEFAULT NULL,
  KEY_26       NUMBER(10)                       DEFAULT NULL,
  KEY_27       NUMBER(10)                       DEFAULT NULL,
  KEY_28       NUMBER(10)                       DEFAULT NULL,
  KEY_29       NUMBER(10)                       DEFAULT NULL,
  KEY_30       NUMBER(10)                       DEFAULT NULL,
  KEY_31       NUMBER(10)                       DEFAULT NULL,
  KEY_32       NUMBER(10)                       DEFAULT NULL,
  KEY_33       NUMBER(10)                       DEFAULT NULL,
  KEY_34       NUMBER(10)                       DEFAULT NULL,
  KEY_35       NUMBER(10)                       DEFAULT NULL,
  CREATE_DATE  DATE                             DEFAULT NULL,
  UPDATE_DATE  DATE                             DEFAULT NULL
);



CREATE TABLE U_USER_TBL
(
  USER_ID                VARCHAR2(80 BYTE)      NOT NULL,
  USER_EMAIL             VARCHAR2(80 BYTE)      NOT NULL,
  PASS_HINT_SELECT_CODE  VARCHAR2(40 BYTE)      DEFAULT ''                    NOT NULL,
  PASS_ANSWER            VARCHAR2(40 BYTE)      NOT NULL,
  USER_SMS               VARCHAR2(40 BYTE)      DEFAULT '',
  USER_LEVEL             VARCHAR2(2 BYTE)       DEFAULT NULL
);



ALTER TABLE FRAME_MEMBER ADD (
  PRIMARY KEY
 (USER_ID)
);


ALTER TABLE L_ACCESS_LOG_TBL ADD (
  PRIMARY KEY
 (USER_ID, ACCESS_DATE)
 );


ALTER TABLE L_ADV_LOG_TBL ADD (
  PRIMARY KEY
 (ADV_ID)
);


ALTER TABLE M_MANAGER_TBL ADD (
  PRIMARY KEY
 (MANAGER_ID)
);


ALTER TABLE S_AUTH_CHECK ADD (
  PRIMARY KEY
 (USER_ID)
);


ALTER TABLE S_AUTH_CHECK_TBL ADD (
  PRIMARY KEY
 (USER_CHECK, USER_ID, TIMESTAMP)
);


ALTER TABLE S_CATEGORY_ICON_TBL ADD (
  PRIMARY KEY
 (ICON_ID)
);


ALTER TABLE S_CATEGORY_TBL ADD (
  PRIMARY KEY
 (CATEGORY_ID)
);


ALTER TABLE U_USER_AUTH_CODE_TBL ADD (
  PRIMARY KEY
 (USER_ID, CREATE_DATE)
);


ALTER TABLE U_USER_KEY_TBL ADD (
  PRIMARY KEY
 (USER_ID)
);


ALTER TABLE U_USER_TBL ADD (
  PRIMARY KEY
 (USER_ID)
);



/*Data for the table `s_category_tbl` */

insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (1,'animal','/images/category/icon_animal.gif','icon_animal.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (2,'art','/images/category/icon_art.gif','icon_art.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (3,'baby','/images/category/icon_baby.gif','icon_baby.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (4,'building','/images/category/icon_building.gif','icon_building.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE)
values (5,'business','/images/category/icon_business.gif','icon_business.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (6,'clothing','/images/category/icon_clothing.gif','icon_clothing.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE)
values (7,'electron','/images/category/icon_electron.gif','icon_electron.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (8,'event','/images/category/icon_event.gif','icon_event.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (9,'finance','/images/category/icon_finance.gif','icon_finance.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (10,'food','/images/category/icon_food.gif','icon_food.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (11,'fruit','/images/category/icon_fruit.gif','icon_fruit.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (12,'furniture','/images/category/icon_furniture.gif','icon_furniture.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (13,'instrument','/images/category/icon_instrument.gif','icon_instrument.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (14,'letters','/images/category/icon_letters.gif','icon_letters.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (15,'life','/images/category/icon_life.gif','icon_life.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (16,'machine','/images/category/icon_machine.gif','icon_machine.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (17,'text','/images/category/icon_text.gif','icon_text.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (18,'plant','/images/category/icon_plant.gif','icon_plant.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (19,'science','/images/category/icon_science.gif','icon_science.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (20,'sports','/images/category/icon_sports.gif','icon_sports.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (21,'tradition','/images/category/icon_tradition.gif','icon_tradition.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (22,'traffic','/images/category/icon_traffic.gif','icon_traffic.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (23,'travel','/images/category/icon_travel.gif','icon_travel.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (24,'weather','/images/category/icon_weather.gif','icon_weather.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (25,'etc','/images/category/icon_etc.gif','icon_etc.gif','1',NULL);


insert into frame_member(USER_ID,USER_PASS,USER_NAME,ADMIN_YN,USER_STATUS) values ('admin','1','admin','Y','Y');

insert into s_setting_tbl 
	(MAIL_SERVER, 
	MAIL_PORT, 
	MAIL_FROM, 
	MAIL_FROM_NAME, 
	MAIL_SUBJECT, 
	MAIL_AUTH_YN, 
	MAIL_AUTH_ID, 
	MAIL_AUTH_PW, 
	AUTH_CODE_EXPIRE_MINUTE
	)
	values
	('mail.mininfo.co.kr', 
	'25', 
	'mininfo@mininfo.co.kr', 
	'manager', 
	'temporary authentication code', 
	'Y', 
	'webmaster@mininfo.co.kr', 
	'minfo', 
	5
	);
	
commit;