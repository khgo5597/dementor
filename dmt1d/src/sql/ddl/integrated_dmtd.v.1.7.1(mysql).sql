

--grant all privileges on *.* to 'dmta'@'localhost' identified by 'dmta' with grant option;



create database if not exists `dmt1d`;
USE `dmt1d`;


DROP TABLE IF EXISTS `frame_member`;

CREATE TABLE `frame_member` (
  `USER_ID` varchar(20) NOT NULL default '',
  `USER_PASS` varchar(41) default '',
  `USER_NAME` varchar(20) default '',
  `USER_LUNAR` varchar(2) default '',
  `USER_BIRTH` varchar(20) default '',
  `USER_EMAIL` varchar(100) default '',
  `USER_PHONE` varchar(13) default '',
  `USER_MOBILE` varchar(13) default '',
  `USER_ZIP` varchar(7) default '',
  `USER_ADDRESS1` varchar(100) default '',
  `USER_ADDRESS2` varchar(100) default '',
  `USER_QUESTION` varchar(255) default '',
  `USER_ANSWER` varchar(255) default '',
  `USER_LASTDATE` datetime default NULL,
  `USER_REGDATE` datetime default NULL,
  `ADMIN_YN` varchar(2) default 'N',
  `USER_MAILING` varchar(2) default NULL,
  `USER_STATUS` varchar(2) default 'N',
  PRIMARY KEY  (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `l_access_log_tbl`;

CREATE TABLE `l_access_log_tbl` (
  `USER_ID` varchar(80) NOT NULL,
  `ACCESS_DATE` datetime NOT NULL,
  `ACCESS_IP` varchar(30) default NULL,
  `ACCESS_RESULT` char(1) default NULL,
  PRIMARY KEY  (`USER_ID`,`ACCESS_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `l_user_delete_log_tbl`;

CREATE TABLE `l_user_delete_log_tbl` (
  `USER_ID` varchar(80) NOT NULL default '',
  `REG_DATE` datetime default NULL,
  `DELETE_DATE` datetime default NULL,
  `DELETE_STATUS` char(2) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `s_category_icon_tbl`;

CREATE TABLE `s_category_icon_tbl` (
  `ICON_ID` int(10) unsigned NOT NULL auto_increment,
  `CATEGORY_ID` int(10) unsigned NOT NULL,
  `ICON_NAME` varchar(100) NOT NULL,
  `ICON_URL` varchar(120) NOT NULL,
  `ICON_FILENAME` varchar(80) NOT NULL,
  `ICON_STATUS` char(1) NOT NULL default '0',
  `REG_DATE` datetime default NULL,
  PRIMARY KEY  (`ICON_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `s_category_tbl`;

CREATE TABLE `s_category_tbl` (
  `CATEGORY_ID` int(10) unsigned NOT NULL auto_increment,
  `CATEGORY_NAME` varchar(100) NOT NULL,
  `CATEGORY_URL` varchar(120) NOT NULL,
  `CATEGORY_FILENAME` varchar(80) NOT NULL,
  `CATEGORY_STATUS` char(1) NOT NULL default '0',
  `REG_DATE` datetime default NULL,
  PRIMARY KEY  (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `s_setting_tbl`;

CREATE TABLE `s_setting_tbl` (
  `MAIL_SERVER` varchar(100) NOT NULL,
  `MAIL_PORT` varchar(10) NOT NULL default '',
  `MAIL_FROM` varchar(100) NOT NULL,
  `MAIL_FROM_NAME` varchar(100) NOT NULL,
  `MAIL_SUBJECT` varchar(150) NOT NULL,
  `MAIL_AUTH_YN` char(1) NOT NULL default 'N',
  `MAIL_AUTH_ID` varchar(50) default NULL,
  `MAIL_AUTH_PW` varchar(50) default NULL,
  `AUTH_CODE_EXPIRE_MINUTE` int(11) NOT NULL default '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `u_user_auth_code_tbl`;

CREATE TABLE `u_user_auth_code_tbl` (
  `USER_ID` varchar(80) NOT NULL,
  `CREATE_DATE` datetime NOT NULL,
  `AUTH_CODE` varchar(20) NOT NULL,
  PRIMARY KEY  (`USER_ID`,`CREATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `u_user_key_tbl`;

CREATE TABLE `u_user_key_tbl` (
  `USER_ID` varchar(80) NOT NULL,
  `KEY_COUNT` int(10) unsigned default '0',
  `KEY_HOLE` int(10) unsigned default NULL,
  `KEY_1` int(10) unsigned default NULL,
  `KEY_2` int(10) unsigned default NULL,
  `KEY_3` int(10) unsigned default NULL,
  `KEY_4` int(10) unsigned default NULL,
  `KEY_5` int(10) unsigned default NULL,
  `KEY_6` int(10) unsigned default NULL,
  `KEY_7` int(10) unsigned default NULL,
  `KEY_8` int(10) unsigned default NULL,
  `KEY_9` int(10) unsigned default NULL,
  `KEY_10` int(10) unsigned default NULL,
  `KEY_11` int(10) unsigned default NULL,
  `KEY_12` int(10) unsigned default NULL,
  `KEY_13` int(10) unsigned default NULL,
  `KEY_14` int(10) unsigned default NULL,
  `KEY_15` int(10) unsigned default NULL,
  `KEY_16` int(10) unsigned default NULL,
  `KEY_17` int(10) unsigned default NULL,
  `KEY_18` int(10) unsigned default NULL,
  `KEY_19` int(10) unsigned default NULL,
  `KEY_20` int(10) unsigned default NULL,
  `KEY_21` int(10) unsigned default NULL,
  `KEY_22` int(10) unsigned default NULL,
  `KEY_23` int(10) unsigned default NULL,
  `KEY_24` int(10) unsigned default NULL,
  `KEY_25` int(10) unsigned default NULL,
  `KEY_26` int(10) unsigned default NULL,
  `KEY_27` int(10) unsigned default NULL,
  `KEY_28` int(10) unsigned default NULL,
  `KEY_29` int(10) unsigned default NULL,
  `KEY_30` int(10) unsigned default NULL,
  `KEY_31` int(10) unsigned default NULL,
  `KEY_32` int(10) unsigned default NULL,
  `KEY_33` int(10) unsigned default NULL,
  `KEY_34` int(10) unsigned default NULL,
  `KEY_35` int(10) unsigned default NULL,
  `CREATE_DATE` datetime default NULL,
  `UPDATE_DATE` datetime default NULL,
  PRIMARY KEY  (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `u_user_tbl`;

CREATE TABLE `u_user_tbl` (
  `USER_ID` varchar(80) NOT NULL,
  `USER_EMAIL` varchar(80) NOT NULL,
  `PASS_HINT_SELECT_CODE` varchar(40) NOT NULL default '',
  `PASS_ANSWER` varchar(40) NOT NULL,
  `USER_SMS` varchar(40) DEFAULT '',				-- add 20131224
  `USER_LEVEL` varchar(2) DEFAULT '0',				-- add 20131224
  PRIMARY KEY  (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Data for the table `s_category_tbl` */

insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (1,'animal','/images/category/icon_animal.gif','icon_animal.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (2,'art','/images/category/icon_art.gif','icon_art.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (3,'baby','/images/category/icon_baby.gif','icon_baby.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (4,'building','/images/category/icon_building.gif','icon_building.gif','1',NULL);
insert into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE)
values (5,'business','/images/category/icon_business.gif','icon_business.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (6,'clothing','/images/category/icon_clothing.gif','icon_clothing.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE)
values (7,'electron','/images/category/icon_electron.gif','icon_electron.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (8,'event','/images/category/icon_event.gif','icon_event.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (9,'finance','/images/category/icon_finance.gif','icon_finance.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (10,'food','/images/category/icon_food.gif','icon_food.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (11,'fruit','/images/category/icon_fruit.gif','icon_fruit.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (12,'furniture','/images/category/icon_furniture.gif','icon_furniture.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (13,'instrument','/images/category/icon_instrument.gif','icon_instrument.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (14,'letters','/images/category/icon_letters.gif','icon_letters.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (15,'life','/images/category/icon_life.gif','icon_life.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (16,'machine','/images/category/icon_machine.gif','icon_machine.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (17,'text','/images/category/icon_text.gif','icon_text.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (18,'plant','/images/category/icon_plant.gif','icon_plant.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (19,'science','/images/category/icon_science.gif','icon_science.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (20,'sports','/images/category/icon_sports.gif','icon_sports.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (21,'tradition','/images/category/icon_tradition.gif','icon_tradition.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (22,'traffic','/images/category/icon_traffic.gif','icon_traffic.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (23,'travel','/images/category/icon_travel.gif','icon_travel.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (24,'weather','/images/category/icon_weather.gif','icon_weather.gif','1',NULL);
insert  into s_category_tbl(CATEGORY_ID,CATEGORY_NAME,CATEGORY_URL,CATEGORY_FILENAME,CATEGORY_STATUS,REG_DATE) 
values (25,'etc','/images/category/icon_etc.gif','icon_etc.gif','1',NULL);


insert into frame_member(USER_ID,USER_PASS,USER_NAME,ADMIN_YN,USER_STATUS) values ('admin','1','admin','Y','Y');

insert into s_setting_tbl 
	(MAIL_SERVER, 
	MAIL_PORT, 
	MAIL_FROM, 
	MAIL_FROM_NAME, 
	MAIL_SUBJECT, 
	MAIL_AUTH_YN, 
	MAIL_AUTH_ID, 
	MAIL_AUTH_PW, 
	AUTH_CODE_EXPIRE_MINUTE
	)
	values
	('mail.mininfo.co.kr', 
	'25', 
	'mininfo@mininfo.co.kr', 
	'manager', 
	'temporary authentication code', 
	'Y', 
	'webmaster@mininfo.co.kr', 
	'minfo', 
	5
	);