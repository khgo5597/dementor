package com.mininfo.engine;
import java.security.MessageDigest;

import com.mininfo.common.property.DementorProperty;

/*
MindkeyMain 부분의 아래 두 함수에서만 Decoding, Diff사용
public boolean verifying(String value, String remoteAddr){
public boolean verifying(String value, HttpServletRequest req){

위 두함수에서
Decoding 삭제.
Diff삭제

위 두함수에서 
char[] clientx = (new Decoding(value,answer)).getOutput();
boolean returnBool = (new Diff(encryptedArrayKey,clientx,userSeed)).check();

-->

boolean returnBool = (new Diff(Diff.arrayKeyToStr(PermutaionServer.getAnswer()), value)).check();
*/

public class Diff {

	private String server;
	private String client;	
	
	private static int iconKeyNumber = 0;
	
	/*
	public static void main(String[] args) {
		int[] answer = {0, 1, 2, 3};
		String value = "896400c8e9aa725a28d4c7d92e67a9483412dae64906478cdd161fa62a2a2851";

		if ((new Diff(/Diff.arrayKeyToStr(answer), value)).check()) {
			System.out.println("일치");
		} else {
			System.out.println("불일치");
		}
	}
	*/
	
	Diff(){
		server="";
		client="";
	}

	Diff(String server, String client){					// server는 arrayKeyToStr호출
		
		this.server=server;
		this.client=client;

		//System.out.println("BeforeServer:" + server);
		//System.out.println("BeforeClient:" + client);
		//System.out.println("============================");
	}
	
	// 바이트 배열을 16진수 스트링으로 바꾼다.
	public static final String bytesToHex(byte[] a) {
	
		StringBuffer s = new StringBuffer();
		for(int i=0;i<a.length;++i) {
			s.append(Character.forDigit((a[i]>>4) & 0x0f, 16));
			s.append(Character.forDigit(a[i] & 0x0f, 16));
		}
		return s.toString();
	}

	public boolean check() {

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256"); 
			byte[] inputBytes = server.getBytes(); 
			byte[] resultBytes = null; 

			md.update(inputBytes); 
			resultBytes = md.digest(); 

			byte[] clientBytes = client.getBytes();	

			String resultString = bytesToHex(resultBytes);
			
			if (resultString.equals(client)) {
				return true;
			}	
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}

		return false;
	}

	public static String arrayKeyToStr(int[] answer, String userId, String date) {			// PermutaionServer의 getAnswer()호출.
		int[] arrayKey = new int[4];
		if (iconKeyNumber == 0) {
			try {
				DementorProperty DP = null;
				iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
			}catch(Exception e) {
				System.out.println(e.toString());
			}
		}
		int hLine = answer[0] / 5;
		arrayKey[0] = convertData(answer[0], hLine, answer[1]);		
		//arrayKey[0] = (answer[0]-answer[1]);
		if (iconKeyNumber == 3) {
			arrayKey[1] = convertData(answer[0], hLine, answer[2]);
			//arrayKey[1] = (answer[0]-answer[2]);
		}else if (iconKeyNumber == 4) {
			arrayKey[1] = convertData(answer[0], hLine, answer[2]);
			arrayKey[2] = convertData(answer[0], hLine, answer[3]);
			//arrayKey[1] = (answer[0]-answer[2]);
			//arrayKey[2] = (answer[0]-answer[3]);
		}
		arrayKey[3] = 0;
					

		String str = "";
		str += arrayKey[0];
		if (iconKeyNumber == 3) {
			str += arrayKey[1];		
		}else if (iconKeyNumber == 4) {
			str += arrayKey[1];		
			str += arrayKey[2];
		}
		str += arrayKey[3];

		str += date;		// 2009.3.10일 변경 된 내용
		str += userId;		// 2009.3.10일 변경 된 내용
		
		return str;
	}
	
	public static int convertData(int hole, int hLine, int key) {
		int kLine = key / 5;
		int upDown = hLine-kLine;
		int leftRight = (hole - key) - (upDown*5);
		
		return convertUpDown(upDown) + convertLeftRight(leftRight); 
	}
	
	public static int convertUpDown(int upDown) {
		if (upDown > 0 && upDown <= 4) {
			return ((upDown-1)*5) - 20;
		} else {
			return upDown * 5;
		}
	}
	
	public static int convertLeftRight(int leftRight) {		
		if (leftRight > 0 && leftRight <= 4) {
			return (leftRight-1) - 4;
		} else {
			return leftRight;
		}
	}

	public static int[] getMaxGapFromHole(int holeKey) {		//holeKey는 PermutationServer의 PermutationServer(int[] a, Seed s)생성자														//호출 이후 PermutationServer의 getAnswer()[0] 메소드를 호출하여 얻는다.
		if (iconKeyNumber == 0) {
			try {
				DementorProperty DP = null;
				iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
			}catch(Exception e) {
				System.out.println(e.toString());
			}
		}
		int[] gap = new int[4];													// left, right, top, down
		gap[0] = (holeKey % 5);
		gap[1] = 4 - (holeKey % 5);
		gap[2] = (holeKey / 5);
		if (iconKeyNumber == 4) {
			gap[3] = 4 - (holeKey / 5);
		}
		return gap;
	}
}