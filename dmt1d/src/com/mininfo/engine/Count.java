package com.mininfo.engine;
public class Count {
	private int num;
	private int max;
	Count(){
		num=0;
		max=0x7fffffff;
	}
	Count(int num,int max){
		this.num=num;
		this.max=max;
	}
	void setCount(int num){
		this.num=num;
	}
	void setMax(int max){
		this.max=max;
	}
	int getCount(){
		return num;
	}
	int getMax(){
		return max;
	}
	int next(){
		if(num==max)	return num=0;
		else	return ++num;
	}
}
