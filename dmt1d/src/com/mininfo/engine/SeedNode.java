package com.mininfo.engine;
public class SeedNode {
	private int count;
	private SeedElement data;
	private int height;
	private SeedNode left;
	private SeedNode right;
	
	SeedNode(){
		count=0;
		data=null;
		height=0;
	}
	
	SeedNode (int count, SeedElement data){
		this.count=count;
		this.data=data;
		this.height=0;
		this.left=null;
		this.right=null;
	}
	
	SeedNode(SeedNode n){
		this.count=n.count;
		this.data=n.data;
		this.height=n.height;
		this.left=n.left;
		this.right=n.right;
	}
	
	void copyData(SeedNode n){
		this.count=n.count;
		this.data=n.data;
	}
	
	void setCount(int count){
		this.count=count;
	}
	
	void setData(SeedElement data){
		this.data=data;
	}
	
	void setHeight(int height){
		this.height=height;
	}
	
	void setHeight(){
		if(left==null){
			if(right==null)	height=0;
			else	height=right.height+1;
		}
		else{
			if(right==null)	height=left.height+1;
			else	height= left.height>right.height ? (left.height+1) : (right.height+1);
		}
	}

	void setLeft(SeedNode left){
		this.left=left;
	}
	
	void setRight(SeedNode right){
		this.right=right;
	}
	
	int getCount(){
		return count;
	}
	
	SeedElement getData(){
		return data;
	}
	
	int getHeight(){
		return height;
	}
	
	SeedNode getLeft(){
		return left;
	}
	
	SeedNode getRight(){
		return right;
	}
	
	void increaseHeight(){
		height++;
	}

	void decreaseHeight(){
		height--;
	}
}
