package com.mininfo.engine;
public class AVLTree {
	private SeedNode root;
	
	AVLTree(){
		root=null;
	}
	
	public SeedNode find(int count){
		return find(count,root);
	}
	
	public SeedNode remove(int count){
		SeedNode temp=find(count);
		root=remove(count,root);
		return temp;
	}
	
	public void insert(SeedNode n){
		if(root==null)	root=n;
		else	insert(n,root);
	}
	
	private SeedNode insert(SeedNode n, SeedNode dest){
		if(dest==null)	dest=n;
		else if(n.getCount()<dest.getCount()){
			dest.setLeft(insert(n,dest.getLeft()));
			if(getHeight(dest.getLeft())-getHeight(dest.getRight())>=2){
				if(n.getCount()<dest.getLeft().getCount())
					dest=rotateWithLeftChild(dest);
				else
					dest=doubleWithLeftChild(dest);
			}
		}
		else if(n.getCount()>dest.getCount()){
			dest.setRight(insert(n,dest.getRight()));
			if(getHeight(dest.getRight())-getHeight(dest.getLeft())>=2){
				if(n.getCount()>dest.getRight().getCount())
					dest=rotateWithRightChild(dest);
				else
					dest=doubleWithRightChild(dest);
			}
		}
		else;
		dest.setHeight();
		return dest;
	}
	
	private SeedNode find(int count, SeedNode dest){
		if(dest==null)	return null;
		else if(count<dest.getCount())		return find(count,dest.getLeft());
		else if(count>dest.getCount())	return find(count,dest.getRight());
		else	return dest;
	}
	
	private SeedNode findMin(SeedNode dest){
		if(dest!=null){
			while(dest.getLeft()!=null)	dest=dest.getLeft();
		}
		return dest;
	}
	
	private SeedNode remove(int count, SeedNode dest){
		if(dest==null)	return dest;
		if(count<dest.getCount()){
			dest.setLeft(remove(count,dest.getLeft()));
			if(getHeight(dest.getRight())-getHeight(dest.getLeft())>=2){
				if(getHeight(dest.getRight().getRight())>getHeight(dest.getRight().getLeft()))
					dest=rotateWithRightChild(dest);
				else
					dest=doubleWithRightChild(dest);
			}
		}
		else if(count>dest.getCount()){
			dest.setRight(remove(count,dest.getRight()));
			if(getHeight(dest.getLeft())-getHeight(dest.getRight())>=2){
				if(getHeight(dest.getLeft().getLeft())>getHeight(dest.getLeft().getRight()))
					dest=rotateWithLeftChild(dest);
				else
					dest=doubleWithLeftChild(dest);
			}
		}
		else if(dest.getLeft()!=null && dest.getRight()!=null){
			dest.copyData(findMin(dest.getRight()));
			dest.setRight(remove(dest.getCount(),dest.getRight()));
		}
		else
			dest=(dest.getLeft()!=null)? dest.getLeft():dest.getRight();
		if(dest!=null)	dest.setHeight();
		return dest;
	}
	
	private static SeedNode rotateWithLeftChild(SeedNode n){
		SeedNode k1=n.getLeft();
		n.setLeft(k1.getRight());
		k1.setRight(n);
		n.setHeight();
		k1.setHeight();
		return k1;
	}
	
	private static SeedNode rotateWithRightChild(SeedNode n){
		SeedNode k1=n.getRight();
		n.setRight(k1.getLeft());
		k1.setLeft(n);
		n.setHeight();
		k1.setHeight();
		return k1;
	}
	
	private static SeedNode doubleWithLeftChild(SeedNode n){
		n.setLeft(rotateWithRightChild(n.getLeft()));
		return rotateWithLeftChild(n);
	}

	private static SeedNode doubleWithRightChild(SeedNode n){
		n.setRight(rotateWithLeftChild(n.getRight()));
		return rotateWithRightChild(n);
	}

	private static int getHeight(SeedNode n){
		return n==null ? -1 : n.getHeight();
	}
}
