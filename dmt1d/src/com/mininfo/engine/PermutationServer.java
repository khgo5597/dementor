package com.mininfo.engine;


public class PermutationServer {
	private Seed seed;
	private int size;
	private int[] array;
	private int[] key;
	private int[] arraykey;
	private int[] answer;
	/**
	 * key
	 * key[0] : hole
	 * key[1] : key 1
	 * key[2] : key 2
	 * key[3] : key 3
	 */
	
	PermutationServer(){		//Default Contructor
		size=1;
		
		//Allocating memory
		array=new int[size];
		seed=new Seed();
		key=new int[4];
		arraykey = new int[4]; //moon
		answer=new int[4];
		array[0]=0;
	}
	
	PermutationServer(int[] a, Seed s){
		int i;
		size=a.length;
		
		//Allocating memory
		array=new int[size];
		seed=s;
		key=new int[4];
		arraykey = new int[4]; //moon
		answer=new int[4];
		for(i=0;i<size;i++){
			array[i]=a[i];
		}
		nextArray();
	}
	
	private int[] nextArray(){
		int i,j;
		int k;
		int state;
		int index;
		int[] sd=new int[2];
		int[] temp=new int [size];
		byte[] seed=this.seed.getSeed();
		sd[0]=(((int)seed[0])<<24)+(((int)seed[1])<<16)+(((int)seed[2])<<8)+((int)seed[3]);
		sd[1]=(((int)seed[4])<<24)+(((int)seed[5])<<16)+(((int)seed[6])<<8)+((int)seed[7]);
		for(i=0;i<size;i++){
			temp[i]=array[i];
		}
		//update by sook
		key[0]=array[0];//moon
		key[1]=array[1];//moon
		key[2]=array[2];//moon
		key[3]=array[3];//moon
		
		/*
		key[1]=array[0];//moon
		key[2]=array[1];//moon
		key[3]=array[2];//moon
		key[0]=array[3];//moon
		 */
		state=0;
		for(i=0;i<size;i++,state=1-state){
			index=sd[state]%(size-i);
			index=(index<0 ? -index:index);
			array[i]=temp[index];
			for(j=index+1;j<(size-i);j++){
				temp[j-1]=temp[j];				
			}				
		}

		//moon
		for(k=0;k<size;k++){
			if(key[0]==array[k]){
				answer[0]=(byte)k;
			}
			else if(array[k]==key[1]){
				answer[1]=(byte)k;
			}
			else if(array[k]==key[2]){
				answer[2]=(byte)k;
			}
			else if(array[k]==key[3]){
				answer[3]=(byte)k;
			}
		}
	return array;
	}
	int[] getAnswer(){
		return answer;
		}
	int[] getKey(){
		return key;
	}
	
	int[] getArray(){
		return array;
	}

	char[] get16ByteKey(){
		char[] temp=new char[16];
		RijndaelEncrypt Encrypt;
		int i,j;
		for(i=0;i<4;i++){
			for(j=0;j<4;j++){
				temp[(i<<2)+j]=(char)((arraykey[i]>>(j<<3))&0xff);
			}
		}
		Encrypt=new RijndaelEncrypt(temp,seed.get256BitSeed());
		return Encrypt.Encrypt();
	}
}
