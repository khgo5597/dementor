package com.mininfo.engine;

public class RijndaelEncrypt extends Rijndael{
	RijndaelEncrypt(){
		super();
	}
	
	RijndaelEncrypt(char[] plain, char[] password){
		super();
		setPlaintext(plain);
		setPassword(password);
	}
	
	public void setPassword(char[] password){
		int i;
		keybits= password.length > key.length ? key.length:password.length;
		for(i=0;i<keybits;i++){
			key[i]=password[i];
			}
		if(keybits>24)		keybits=256;
		else if(keybits>16)	keybits=192;
		else				keybits=128;
		
	}
	
	public void setPlaintext(char[] plain){
		int i;
		for(i=0;i<16 && i<plain.length;i++){
			plaintext[i]=plain[i];
			}
	}
	
	public char[] Encrypt(){
		Encrypt(Setup());
		return ciphertext;
	}
	
	private void Encrypt(int nrounds){
		int offset;
		long s0, s1, s2, s3, t0, t1, t2, t3;
		s0=getU32(plaintext,0) ^ rk[0];
		s1=getU32(plaintext,4) ^ rk[1];
		s2=getU32(plaintext,8) ^ rk[2];
		s3=getU32(plaintext,12) ^ rk[3];
	    /* round 1: */
	    t0 = Te0[(int)( (s0 >> 24) & 0xff)] ^ Te1[(int) ((s1 >> 16) & 0xff)] ^ Te2[(int) ((s2 >>  8) & 0xff)] ^ Te3[(int) (s3 & 0xff)] ^ rk[ 4];
	    t1 = Te0[(int)( (s1 >> 24) & 0xff)] ^ Te1[(int) ((s2 >> 16) & 0xff)] ^ Te2[(int) ((s3 >>  8) & 0xff)] ^ Te3[(int) (s0 & 0xff)] ^ rk[ 5];
	    t2 = Te0[(int)( (s2 >> 24) & 0xff)] ^ Te1[(int) ((s3 >> 16) & 0xff)] ^ Te2[(int) ((s0 >>  8) & 0xff)] ^ Te3[(int) (s1 & 0xff)] ^ rk[ 6];
	    t3 = Te0[(int)( (s3 >> 24) & 0xff)] ^ Te1[(int) ((s0 >> 16) & 0xff)] ^ Te2[(int) ((s1 >>  8) & 0xff)] ^ Te3[(int) (s2 & 0xff)] ^ rk[ 7];
	    /* round 2: */
	    s0 = Te0[(int)( (t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)( 8)];
	    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)( 9)];
	    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(10)];
	    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(11)];
	    /* round 3: */
	    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(12)];
	    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(13)];
	    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(14)];
	    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(15)];
	    /* round 4: */
	    s0 = Te0[(int)((t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)(16)];
	    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)(17)];
	    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(18)];
	    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(19)];
	    /* round 5: */
	    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(20)];
	    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(21)];
	    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(22)];
	    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(23)];
	    /* round 6: */
	    s0 = Te0[(int)((t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)(24)];
	    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)(25)];
	    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(26)];
	    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(27)];
	    /* round 7: */
	    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(28)];
	    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(29)];
	    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(30)];
	    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(31)];
	    /* round 8: */
	    s0 = Te0[(int)((t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)(32)];
	    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)(33)];
	    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(34)];
	    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(35)];
	    /* round 9: */
	    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(36)];
	    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(37)];
	    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(38)];
	    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(39)];
	    if (nrounds > 10){
		    /* round 10: */
		    s0 = Te0[(int)((t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)(40)];
		    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)(41)];
		    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(42)];
		    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(43)];
		    /* round 11: */
		    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(44)];
		    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(45)];
		    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(46)];
		    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(47)];
		    if (nrounds > 12){
			    /* round 12: */
			    s0 = Te0[(int)((t0 >> 24) & 0xff)] ^ Te1[(int)((t1 >> 16) & 0xff)] ^ Te2[(int)((t2 >>  8) & 0xff)] ^ Te3[(int)(t3 & 0xff)] ^ rk[(int)(48)];
			    s1 = Te0[(int)((t1 >> 24) & 0xff)] ^ Te1[(int)((t2 >> 16) & 0xff)] ^ Te2[(int)((t3 >>  8) & 0xff)] ^ Te3[(int)(t0 & 0xff)] ^ rk[(int)(49)];
			    s2 = Te0[(int)((t2 >> 24) & 0xff)] ^ Te1[(int)((t3 >> 16) & 0xff)] ^ Te2[(int)((t0 >>  8) & 0xff)] ^ Te3[(int)(t1 & 0xff)] ^ rk[(int)(50)];
			    s3 = Te0[(int)((t3 >> 24) & 0xff)] ^ Te1[(int)((t0 >> 16) & 0xff)] ^ Te2[(int)((t1 >>  8) & 0xff)] ^ Te3[(int)(t2 & 0xff)] ^ rk[(int)(51)];
			    /* round 13: */
			    t0 = Te0[(int)((s0 >> 24) & 0xff)] ^ Te1[(int)((s1 >> 16) & 0xff)] ^ Te2[(int)((s2 >>  8) & 0xff)] ^ Te3[(int)(s3 & 0xff)] ^ rk[(int)(52)];
			    t1 = Te0[(int)((s1 >> 24) & 0xff)] ^ Te1[(int)((s2 >> 16) & 0xff)] ^ Te2[(int)((s3 >>  8) & 0xff)] ^ Te3[(int)(s0 & 0xff)] ^ rk[(int)(53)];
			    t2 = Te0[(int)((s2 >> 24) & 0xff)] ^ Te1[(int)((s3 >> 16) & 0xff)] ^ Te2[(int)((s0 >>  8) & 0xff)] ^ Te3[(int)(s1 & 0xff)] ^ rk[(int)(54)];
			    t3 = Te0[(int)((s3 >> 24) & 0xff)] ^ Te1[(int)((s0 >> 16) & 0xff)] ^ Te2[(int)((s1 >>  8) & 0xff)] ^ Te3[(int)(s2 & 0xff)] ^ rk[(int)(55)];
		    }
	    }
	    offset = nrounds << 2;
	    /*
	     * apply last round and
	     * map cipher state to byte array block:
	     */
	    s0 =
	    	(Te4[(int) ((t0 >> 24) & 0xff)] & 0xff000000) ^
			(Te4[(int) ((t1 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4[(int) ((t2 >>  8) & 0xff)] & 0x0000ff00) ^
			(Te4[(int) ((t3      ) & 0xff)] & 0x000000ff) ^
			rk[offset+0];
		putU32(ciphertext,0, s0);
		s1 =
			(Te4[(int) ((t1 >> 24) & 0xff)] & 0xff000000) ^
			(Te4[(int) ((t2 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4[(int) ((t3 >>  8) & 0xff)] & 0x0000ff00) ^
			(Te4[(int) ((t0      ) & 0xff)] & 0x000000ff) ^
			rk[offset+1];
		putU32(ciphertext,4, s1);
		s2 =
			(Te4[(int) ((t2 >> 24) & 0xff)] & 0xff000000) ^
			(Te4[(int) ((t3 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4[(int) ((t0 >>  8) & 0xff)] & 0x0000ff00) ^
			(Te4[(int) ((t1      ) & 0xff)] & 0x000000ff) ^
			rk[offset+2];
		putU32(ciphertext,8, s2);
		s3 =
			(Te4[(int) ((t3 >> 24) & 0xff)] & 0xff000000) ^
			(Te4[(int) ((t0 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4[(int) ((t1 >>  8) & 0xff)] & 0x0000ff00) ^
			(Te4[(int) ((t2      ) & 0xff)] & 0x000000ff) ^
			rk[offset+3];
		putU32(ciphertext,12, s3);
	}
}
