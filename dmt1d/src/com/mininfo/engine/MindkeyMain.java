package com.mininfo.engine;
/**
 * Mind Key JSP Version
 * mindkeyMain.java
 * 
 * Coded by Young Hoon Park
 * Seoul National University
 * Computer Network and Security Laboratory
 * 
 * Last Update : 2007/07/16 00:30
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.mininfo.client.dao.MysqlclientDAO;
import com.mininfo.client.dao.clientDAO;
import com.mininfo.client.model.UserCatgEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.admin.category.DBencryption;
import com.mininfo.admin.category.dao.CateDAO;
import com.mininfo.admin.category.dao.MysqlCateDAO;
import com.mininfo.admin.category.model.CategoryEntity;
import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.client.web.ClientController;
import com.mininfo.common.Const;
import com.mininfo.common.property.DementorProperty;
import com.mininfo.logger.dao.UserLogDao;
import com.mininfo.logger.dao.UserLogDaoImpl;
import com.mininfo.logger.model.AccessLogEntity;

public class MindkeyMain {
	private int step;
	private String userID;
	private int count;
	private Seed userSeed;
	private int[] imageAddress;
	private int[] answer;
	private final int numofImage=25;
	private String[] Iconurl;
	private String date = null;
//	private String str;

	private char[] encryptedArrayKey;

	public void mindkeyMainset(String ID){
		step=0;
		userID=ID;
		count=8;
		imageAddress=new int[numofImage];
		answer=null;

		getImageFromDB();
	}

	public int getImageAddress(int index){
		return imageAddress[index];
	}
	
	public String[] getImageAddressURL(){
		return Iconurl;
	}

	public boolean verifying(String value, String remoteAddr){
		
		UserLogDao dao = new UserLogDaoImpl();



		if (answer==null)	return false;
/*
		char[] clientx = (new Decoding(value,answer)).getOutput();
		boolean returnBool = (new Diff(encryptedArrayKey,clientx,userSeed)).check();
*/	
		boolean returnBool = (new Diff(Diff.arrayKeyToStr(answer,userID, date), value)).check();
		
		AccessLogEntity entity = new AccessLogEntity();
		entity.setUser_id(userID);
		entity.setAccess_ip(remoteAddr);
		
		if(returnBool){
			entity.setAccess_result(Const.ACCESS_SUCCESS);
		} else {
			entity.setAccess_result(Const.ACCESS_FAIL);
		}
		try {
			dao.insertAccessLog(entity);
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return returnBool;
	}
	
	public boolean verifying(String value, HttpServletRequest req, String date){
		this.date = date;
		
		UserLogDao dao = new UserLogDaoImpl();


		if (answer==null)	return false;
/*
		char[] clientx = (new Decoding(value,answer)).getOutput();
		boolean returnBool = (new Diff(encryptedArrayKey,clientx,userSeed)).check();
*/		
		boolean returnBool = (new Diff(Diff.arrayKeyToStr(answer, userID, date), value)).check();
		AccessLogEntity entity = new AccessLogEntity();
		entity.setUser_id(userID);
		entity.setAccess_ip(req.getRemoteAddr());
		
		if(returnBool){
			entity.setAccess_result(Const.ACCESS_SUCCESS);
			req.getSession(false).setAttribute("dmt1d_AUTH", "Y");
		} else {
			entity.setAccess_result(Const.ACCESS_FAIL);
			
			
			//DementorProperty.getInstance().isSessionRelease()
			try {
							
			if(DementorProperty.getInstance().isSessionRelease()){
				HttpSession session1 = req.getSession(false);
			
				
				if(session1.getAttribute("ACCESS_COUNT") == null){
					String count = "1";
					session1.setAttribute("ACCESS_COUNT", count);
					System.out.println("==== 1�� Ʋ������ �޽��� count : "+count);
				}else{
					String cnt = (String) session1.getAttribute("ACCESS_COUNT");
					System.out.println("========cnt : "+cnt);
					int count = Integer.parseInt(cnt);
					count = count+1;
					if(count < DementorProperty.getInstance().getAccessCount()){
						//count = count+1;	
						cnt = Integer.toString(count);
						session1.setAttribute("ACCESS_COUNT", cnt);
						System.out.println("==== 2�� Ʋ������ �޽��� count : " +count);
					}else{
						cnt = Integer.toString(count);
						session1.setAttribute("ACCESS_COUNT", cnt);
						System.out.println("==== 3�� Ʋ������ �޽��� count : " +count);
					}
					System.out.println("ACCESS_COUNT="+DementorProperty.getInstance().getAccessCount());
				}
				
			}else{
				
				clientDAO client = new MysqlclientDAO();
				
				int cnt = client.dmtloginCnt(userID);
				int MCnt = DementorProperty.getInstance().getAccessCount();
				if(cnt != MCnt){
					cnt = cnt+1;
					client.dmtloginCntEdit(cnt,userID);
				}
				
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			HttpSession session1 = req.getSession(false);
			
			String oldDate = (String)session1.getAttribute("ACCESS_DATE");
			if(oldDate == null || oldDate.equals("") || !entity.getAccess_date().equals(oldDate)) {
				dao.insertAccessLog(entity);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		HttpSession session1 = req.getSession(false);
		session1.setAttribute("ACCESS_DATE", entity.getAccess_date());
		return returnBool;
	}

	private void getImageFromDB(){
		if(step!=0) return;

		int i=0,j=0;
		int[] index=new int[numofImage];
		for(i=0;i<numofImage;i++)	index[i]=i;
		
		userSeed=new Seed(userID.toCharArray(),count); //byte���� �迭 [8]
		
		PermutationServer reordering=new PermutationServer(index,userSeed); 
		
		index=reordering.getArray(); //array[25]
		encryptedArrayKey=reordering.get16ByteKey();
		
		answer=reordering.getAnswer();
	
		ClientController ctcr = new ClientController();
		UserKeyEntity entity = null;
		
		List vc;
		try {
			vc = ctcr.getMemberKey(userID);
			
			entity=(UserKeyEntity)vc.get(0);
			
			//String Member_id;
			int ImageArr[] = new int[25];
	
	
			if(vc.size()>0){
				for(i=0; i<vc.size(); i++){
					entity=(UserKeyEntity)vc.get(i);
					ImageArr[0]=entity.getKey_hole();
					ImageArr[1]=entity.getKey_1();
					ImageArr[2]=entity.getKey_2();
					ImageArr[3]=entity.getKey_3();
					ImageArr[4]=entity.getKey_4();
					ImageArr[5]=entity.getKey_5();
					ImageArr[6]=entity.getKey_6();
					ImageArr[7]=entity.getKey_7();
					ImageArr[8]=entity.getKey_8();
					ImageArr[9]=entity.getKey_9();
					ImageArr[10]=entity.getKey_10();
					ImageArr[11]=entity.getKey_11();
					ImageArr[12]=entity.getKey_12();
					ImageArr[13]=entity.getKey_13();
					ImageArr[14]=entity.getKey_14();
					ImageArr[15]=entity.getKey_15();
					ImageArr[16]=entity.getKey_16();
					ImageArr[17]=entity.getKey_17();
					ImageArr[18]=entity.getKey_18();
					ImageArr[19]=entity.getKey_19();
					ImageArr[20]=entity.getKey_20();
					ImageArr[21]=entity.getKey_21();
					ImageArr[22]=entity.getKey_22();
					ImageArr[23]=entity.getKey_23();
					ImageArr[24]=entity.getKey_24();
				} // end for
			} // end if
			
			
			for(i=0;i<numofImage;i++){
				imageAddress[i]=ImageArr[index[i]];
				//System.out.println("imageAddress["+i+"]"+imageAddress[i]);
			}
			
			List memberkeyurl = ctcr.memberkeyurl(imageAddress);
			DBencryption encr = new DBencryption();
			
			Iconurl = new String[25];
			IconEntity Ientity = null;
			String dval ="";

			if(memberkeyurl.size()>0){
				for(i=0; i<memberkeyurl.size(); i++){
					Ientity = (IconEntity)memberkeyurl.get(i);
					for(j=0; j<memberkeyurl.size(); j++){
						if(imageAddress[j]==Ientity.getIcon_id()){
							dval = encr.db_decoding(Ientity.getIcon_url());
							Iconurl[j] = dval;
							//System.out.println("Iconurl["+j+"]"+Iconurl[j]);
						}
					} // end for j
				} // end for i
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		step=1;
	}
	
	public String getMaxGapFromHole2(){
		String str ="";
		
		int[] a = Diff.getMaxGapFromHole(answer[0]);
		str+= a[0];
		str+= a[1];
		str+= a[2];
		str+= a[3];

		return str;
	}

}
