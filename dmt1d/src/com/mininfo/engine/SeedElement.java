package com.mininfo.engine;
public class SeedElement {
	private Seed seed;
	private char[] encrypted_AK;
	private Count error;
	private int[] answer;
	
	SeedElement(){
		seed=null;
		encrypted_AK=new char[16];
		error=new Count(0,3);
		answer=new int[4];
	}
	
	SeedElement(Seed seed){
		this.seed=seed;
		encrypted_AK=new char[16];
		error=new Count(0,3);
		answer=new int[4];
	}
	
	SeedElement(Seed seed,char[] AK,int[] answer){
		int i=0;
		this.seed=seed;
		this.encrypted_AK=new char[16];
		error=new Count(0,3);
		this.answer=answer;
		for(i=0;i<16 && i<AK.length;i++){
			this.encrypted_AK[i]=AK[i];
		}
	}
	
	public void setSeed(Seed seed){
		this.seed=seed;
	}
	
	public void setAK(char[] AK){
		int i=0;
		this.encrypted_AK=new char[16];
		for(i=0;i<16 && i<AK.length;i++){
			this.encrypted_AK[i]=AK[i];
		}
	}
	
	public void setAnswer(int[] answer){
		this.answer=answer;
	}
	
	public Seed getSeed(){
		return seed;
	}
	
	public char[] getAK(){
		return encrypted_AK;
	}
	
	public int[] getAnswer(){
		return answer;
	}
	
	int getErrorCount(){
		return error.getCount();
	}
	
	int increaseErrorCount(){
		return error.next();
	}
}
