package com.mininfo.engine;

import java.util.Random;

public class Seed {
	private char[] ID;
	private byte[] seed;
	private int count;
	
	Seed(){		//Default Constructor
		ID=new char[80];
		seed=null;
		count=0;
	}
	
	Seed(char[] id, int count){	//Constructor
		int i;
		ID=new char[80];
		for(i=0;i<id.length;i++)	this.ID[i]=id[i];
		seed=null;
		this.count=count;
		generate();
	}
	
	public char[] getID(){
		return ID;
	}
	
	public byte[] getSeed(){
		return seed;
	}
	
	public String getTxData(){
		int i=0;
		char[] temp=new char[20];
		if(seed==null)	return null;
		for(i=0;i<8;i++){
			if(seed[i]<0){
				temp[i<<1]=(char)((int)seed[i]+128);
				temp[(i<<1)+1]=1;
			}
			else{
				temp[i<<1]=(char)seed[i];
				temp[(i<<1)+1]=0;
			}
		}
		temp[16]=(char)(count & 0xff);  //단자리 숫자로 처리
		temp[17]=(char)((count>>8) & 0xff);
		temp[18]=(char)((count>>16) & 0xff);
		temp[19]=(char)((count>>24) & 0xff);
		return String.valueOf(temp);
	}
	
	public char[] get256BitSeed(){
		int i=0;
		char[] temp=new char[32];
		if(seed==null){
			
			return null;
		}
		for(i=0;i<32;i++){
			if(i%4 ==0)	temp[i]=((char)seed[i>>2]);
			else temp[i]=0;
		}
		return temp;
	}
	
	private byte[] generate(){
		Random rand=new Random();
		int i,j,k,tmp = 0;
		int[] ID_tmp=new int[10];//용도확인
		seed=new byte[8];
		for(i=0;i<2;i++){
			for(j=0;j<10;j++)	ID_tmp[j]=0;
			tmp=rand.nextInt();//난순의 범위 2^32 --> -2^31~(2^31-1)
			for(j=0;j<10;j++){
				for(k=0;k<4;k++){
					ID_tmp[j]+=(((int)ID[(j<<3)+(k<<1)+i] & 0xff)<<(k<<3));
				}
			}
			for(j=0;j<10;j++)	tmp^=ID_tmp[j];
			for(j=0;j<4;j++){
				seed[(i<<2)+j]=(byte)((tmp>>(j<<3))&0xff);
			}
		}
		for(i=0;i<8;i++){
			seed[i] ^= (int)System.currentTimeMillis();
		}
		return seed;
	}
}
