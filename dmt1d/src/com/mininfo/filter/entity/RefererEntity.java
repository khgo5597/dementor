package com.mininfo.filter.entity;

import com.mininfo.common.model.BaseObject;

public class RefererEntity extends BaseObject {
	private int id;
	private String url;
	private String comment;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param rurll the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}
