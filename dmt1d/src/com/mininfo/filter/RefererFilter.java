package com.mininfo.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.*;

import com.mininfo.common.property.DementorProperty;

public class RefererFilter implements Filter {

    private static final String FAIL_PAGE = 
    	new DementorProperty("referer.properties").getProperty("referer.returnurl");
    
    private Logger log = Logger.getLogger(RefererFilter.class);
    
    protected FilterConfig filterConfig = null;
    protected ServletContext context = null;
    protected List permittedSiteTable = null;
    
    public void destroy() {
        this.filterConfig = null;

    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	HttpServletRequest req = (HttpServletRequest)request;
    	HttpServletResponse res = (HttpServletResponse)response;
    	String requestingHost = req.getRemoteHost();
    	String referringHost = getReferringHost(req.getHeader("Referer"));
    	
		log.info("requestingHost : " + requestingHost);
		log.info("referringHost : " + referringHost);

		String bannedSite = null;
    	boolean isPermitted = false;
    	
//        chain.doFilter(request, response);
        
        if (permittedSiteTable.contains(requestingHost)) {
        	bannedSite = requestingHost;
        	isPermitted = true;
        } else if (permittedSiteTable.contains(referringHost)) {
        	bannedSite = referringHost;
        	isPermitted = true;
        }
        if (isPermitted) {
        	chain.doFilter(request,response);
        } else {
    		log.info("FAIL_PAGE : " + FAIL_PAGE);
        	res.sendRedirect(FAIL_PAGE);
//        	showWarning(response, bannedSite);
        }
        
        
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    	this.filterConfig = filterConfig;
    	this.context = filterConfig.getServletContext();
//    	RefererURLIF refererURL = new RefererURLDAO();
    	try {
//			permittedSiteTable = refererURL.getRefererList();
    		permittedSiteTable = 
    	 		new DementorProperty("referer.properties").getPropertyList("referer.url");
    		Iterator iter = permittedSiteTable.iterator();
    		while(iter.hasNext()) {
    			
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    public FilterConfig getFilterConfig() {
        return filterConfig;
    }

    public void setFilterConfig(FilterConfig cfg) {
        filterConfig = cfg;
    }
    
    private String getReferringHost(String refererringURLString) {
    	try {
    		URL referringURL = new URL(refererringURLString);
    		return(referringURL.getHost());
    	} catch(MalformedURLException mue) { // Malformed or null
    		return(null);
    	}
    }
    private void showWarning(ServletResponse response,
    		String bannedSite)
    throws ServletException, IOException {
    	response.setContentType("text/html");
    	PrintWriter out = response.getWriter();
    	out.flush();
    	String docType =
    		"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
    		"Transitional//EN\">\n";
    	out.println
    	(docType +
    			"<HTML>\n" +
    			"<HEAD><TITLE>Access Prohibited</TITLE></HEAD>\n" +
    			"<BODY BGCOLOR=\"WHITE\">\n" +
    			"<H1>Access Prohibited</H1>\n" +
    			"Sorry, access from or via " + bannedSite + "\n" +
    			"is not allowed.\n" +
    	"</BODY></HTML>");
    }
}
