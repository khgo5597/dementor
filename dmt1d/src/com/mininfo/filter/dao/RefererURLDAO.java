package com.mininfo.filter.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import com.mininfo.common.dbconn.DBConnection;


public class RefererURLDAO extends DBConnection implements RefererURLIF{

	public Set getRefererList() throws Exception{
		Connection conn1 = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		Set refererList = new HashSet();
		//RefererEntity entity = null;
		try{
			conn1 = getConnect();
			ps1 = conn1.prepareStatement("select ID,URL,COMMENT from TB_REFERER");	
			rs1 =ps1.executeQuery();

			while(rs1.next()) {
//				entity = new RefererEntity();
//				entity.setId(rs1.getInt("ID"));
//				entity.setUrl(rs1.getString("URL"));
//				entity.setComment(rs1.getString("COMMENT"));
				refererList.add(rs1.getString("URL"));
			}
		}catch(Exception e){
			System.out.println("getRefererList()  : " +e.getMessage());
			e.printStackTrace();
		}finally{
			closeRS(rs1);
			closePS(ps1);
			closeCon(conn1);
		}
		return refererList;
	}
}