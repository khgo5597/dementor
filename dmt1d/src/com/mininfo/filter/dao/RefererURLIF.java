package com.mininfo.filter.dao;

import java.util.Set;

public interface RefererURLIF {
	
	public Set getRefererList() throws Exception;
	
}
