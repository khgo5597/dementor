package com.mininfo.client.dao;


import java.util.List;

import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.client.model.UserCatgEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.exception.DataAccessException;

public interface clientDAO {
	public void userkeyinsert(UserKeyEntity entity)throws DataAccessException;
	public void userkeyupdate(UserKeyEntity entity)throws DataAccessException;
	public boolean useridCheck(String userId)throws DataAccessException;
	public int iconlength()throws DataAccessException;
	public List memberkey(String userID)throws DataAccessException;
	public List memberkeyurl(int[] keyurl)throws DataAccessException;
	public int dmtloginCnt(String userId)throws DataAccessException;
	public void dmtloginCntEdit(int cnt,String userId)throws DataAccessException;
	public UserCatgEntity getUserCatgKey(String userID)throws DataAccessException;
	public List getIconCount(IconEntity icon)throws DataAccessException;
}
