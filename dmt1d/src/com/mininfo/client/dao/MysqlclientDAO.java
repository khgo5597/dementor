package com.mininfo.client.dao;

import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources; 
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.client.model.UserCatgEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.exception.DataAccessException;
public class MysqlclientDAO implements clientDAO{   
	protected final Log logger = LogFactory.getLog(getClass());
	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader (resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();
			
			throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}
	
	/* 
	 * ClientController [cmd : keysinsert] Use
	 * Client.xml insertUserKey3 
	 *  
	*/
	public void userkeyinsert(UserKeyEntity entity) throws DataAccessException {
		if( logger.isDebugEnabled() )
		{
			logger.info("insert()");
			logger.info("userkeyinsert()");
		}

		try{
			sqlMap.startTransaction();
			sqlMap.insert("insertUserKey3", entity);
			sqlMap.commitTransaction();
			
			return;
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * ClientController [cmd : keyedit] Use
	 * Client.xml updateUserKey3 
	 *  
	*/
	public void userkeyupdate(UserKeyEntity entity) throws DataAccessException {
		if( logger.isDebugEnabled() )
		{
			logger.info("update()");
			logger.info("userkeyupdate()");
		}

		try{
			sqlMap.startTransaction();
			sqlMap.update("updateUserKey3", entity);
			sqlMap.commitTransaction();
			
			return;
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * ClientController [cmd : display] Use
	 * ClientController [cmd : selection] Use
	 * LoginController [cmd : dmt1dCheck] Use
	 * Client.xml 
 	 * SELECT USER_ID FROM u_user_key_tbl WHERE USER_ID = #user_id#
	*/
	public boolean useridCheck(String userId) throws DataAccessException{
		if( logger.isDebugEnabled() )
		{
			logger.info("select()");
			logger.info("useridCheck2()");
		}
		UserKeyEntity entity = new UserKeyEntity();
		entity.setUser_id(userId);
		
		try{
			sqlMap.startTransaction();
			if(sqlMap.queryForObject("useridCheck2", entity) != null){
				sqlMap.commitTransaction();				
				return true;
			}else{ 				
				sqlMap.commitTransaction();
				return false;
			}
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * ClientController [cmd : keyedit] Use
	 * ClientController [cmd : keyinsert] Use
	 * Client.xml 
 	 *  SELECT COUNT(*) FROM s_category_icon_tbl
	*/
	public int iconlength() throws DataAccessException{
		if( logger.isDebugEnabled() )
		{
			logger.info("select()");
			logger.info("iconlength()");
		}

		try{
			sqlMap.startTransaction();
			int iconlength = ((Integer)(sqlMap.queryForObject("iconlength2")));
			sqlMap.commitTransaction();
			
			return iconlength;
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * ClientController [Method : getMemberKey] Use
	 * Client.xml getMemberKey2
	*/
	public List memberkey(String userId)throws DataAccessException{
		UserKeyEntity entity = new UserKeyEntity();
		entity.setUser_id(userId);
		List blist = null;
		try {
			sqlMap.startTransaction();
			blist = sqlMap.queryForList("getMemberKey2",entity);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		return blist;
	}
	
	/* 
	 * ClientController [Method : memberkeyurl] Use
	 * Client.xml getMemberKeyURL
	*/
	public List memberkeyurl(int[] keyurl)throws DataAccessException{
		UserKeyEntity entity = new UserKeyEntity();
		entity.setKeyurl(keyurl);
		List blist = null;
		try {
			sqlMap.startTransaction();
			blist = sqlMap.queryForList("getMemberKeyURL",entity);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}			
		return blist;
	}
	
	/* 
	 * jsp/user/userModify.jsp 296Line Use
	 * User.xml  
	 * SELECT * FROM u_user_tbl WHERE USER_ID = #user_id# 
	*/
	public List dmtHintWrite(String userId)throws DataAccessException{
		List blist = null;
		try {
			sqlMap.startTransaction();
			blist = sqlMap.queryForList("getDmtHintWrite",userId);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return blist;
	}
	
	/* 
	 * MindkeyMain Method[verifying(String value, HttpServletRequest req, String date)] Use
	 * Client.xml  
	 * SELECT KEY_COUNT FROM u_user_key_tbl WHERE USER_ID = #user_id#
	*/
	public int dmtloginCnt(String userId) throws DataAccessException{
		try{			
			sqlMap.startTransaction();
			String count = (String)sqlMap.queryForObject("getkeycount12",userId);
			sqlMap.commitTransaction();
			
			int MCnt = Integer.parseInt(count);
			System.out.println("MCnt========"+MCnt);
			return MCnt;
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * UserController Method[userStatusEditForm] Use
	 * MindkeyMain Method[verifying(String value, HttpServletRequest req, String date)] Use
	 * Client.xml  
	 * UPDATE u_user_key_tbl SET KEY_COUNT = #key_count# WHERE USER_ID = #user_id# 
	*/
	public void dmtloginCntEdit(int key_count, String userId) throws DataAccessException{
		try{
			UserKeyEntity entity = new UserKeyEntity();
			entity.setUser_id(userId);
			entity.setKey_count(key_count);
			sqlMap.startTransaction();
			sqlMap.update("getkeycountEdit23",entity);
			sqlMap.commitTransaction(); 
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	

	public UserCatgEntity getUserCatgKey(String userId) throws DataAccessException{
		UserCatgEntity entity = null;
		try {
			sqlMap.startTransaction();
			entity = (UserCatgEntity)sqlMap.queryForObject("getUserKeyCatg",userId);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		return entity;
	}
	
	/* 
	 * CategoryController [cmd : iconlist] Use
	 * Category.xml
	 *  SELECT ICON_ID, ICON_URL FROM s_category_icon_tbl WHERE CATEGORY_ID = #category_id#
	*/
	
			public List getIconCount(IconEntity icon) throws DataAccessException{
				List blist2 = null;
				try {
					sqlMap.startTransaction();
					blist2 = sqlMap.queryForList("getIconCount", icon);
					sqlMap.commitTransaction();
					
				} catch (SQLException e) {
					e.printStackTrace();
				} finally{
					try {
						sqlMap.endTransaction();
					}catch(SQLException sqlE) {
						logger.error(sqlE);
					}
				}
					return blist2;
			}
	
	
}
