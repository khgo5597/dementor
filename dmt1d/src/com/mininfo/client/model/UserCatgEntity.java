package com.mininfo.client.model;

import com.mininfo.common.model.BaseObject;

public class UserCatgEntity extends BaseObject{

	private static final long serialVersionUID = 1L;

	private int key_hole;
	private int key_1;
	private int key_2;
	private int key_3;
	private int key1_catg;
	private int key2_catg;
	private int key3_catg;
	private int hole_catg;
	
	public int getKey_hole() {
		return key_hole;
	}
	public void setKey_hole(int key_hole) {
		this.key_hole = key_hole;
	}
	public int getKey_1() {
		return key_1;
	}
	public void setKey_1(int key_1) {
		this.key_1 = key_1;
	}
	public int getKey_2() {
		return key_2;
	}
	public void setKey_2(int key_2) {
		this.key_2 = key_2;
	}
	public int getKey_3() {
		return key_3;
	}
	public void setKey_3(int key_3) {
		this.key_3 = key_3;
	}
	public int getKey1_catg() {
		return key1_catg;
	}
	public void setKey1_catg(int key1_catg) {
		this.key1_catg = key1_catg;
	}
	public int getKey2_catg() {
		return key2_catg;
	}
	public void setKey2_catg(int key2_catg) {
		this.key2_catg = key2_catg;
	}
	public int getKey3_catg() {
		return key3_catg;
	}
	public void setKey3_catg(int key3_catg) {
		this.key3_catg = key3_catg;
	}
	public int getHole_catg() {
		return hole_catg;
	}
	public void setHole_catg(int hole_catg) {
		this.hole_catg = hole_catg;
	}
}
