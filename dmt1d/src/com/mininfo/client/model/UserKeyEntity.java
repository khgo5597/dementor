package com.mininfo.client.model;

import com.mininfo.common.model.BaseObject;

public class UserKeyEntity extends BaseObject{
	
	private String user_id;
	private int key_count;
	private int key_hole;
	private int key_1;
	private int key_2;
	private int key_3;
	private int key_4;
	private int key_5;
	private int key_6;
	private int key_7;
	private int key_8;
	private int key_9;
	private int key_10;
	private int key_11;
	private int key_12;
	private int key_13;
	private int key_14;
	private int key_15;
	private int key_16;
	private int key_17;
	private int key_18;
	private int key_19;
	private int key_20;
	private int key_21;
	private int key_22;
	private int key_23;
	private int key_24;
	private int key_25;
	private int key_26;
	private int key_27;
	private int key_28;
	private int key_29;
	private int key_30;
	private int key_31;
	private int key_32;
	private int key_33;
	private int key_34;
	private int key_35;
	private int[] keyurl;
	private String create_date;
	private String update_date;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getKey_count() {
		return key_count;
	}
	public void setKey_count(int key_count) {
		this.key_count = key_count;
	}
	public int getKey_hole() {
		return key_hole;
	}
	public void setKey_hole(int key_hole) {
		this.key_hole = key_hole;
	}
	public int getKey_1() {
		return key_1;
	}
	public void setKey_1(int key_1) {
		this.key_1 = key_1;
	}
	public int getKey_2() {
		return key_2;
	}
	public void setKey_2(int key_2) {
		this.key_2 = key_2;
	}
	public int getKey_3() {
		return key_3;
	}
	public void setKey_3(int key_3) {
		this.key_3 = key_3;
	}
	public int getKey_4() {
		return key_4;
	}
	public void setKey_4(int key_4) {
		this.key_4 = key_4;
	}
	public int getKey_5() {
		return key_5;
	}
	public void setKey_5(int key_5) {
		this.key_5 = key_5;
	}
	public int getKey_6() {
		return key_6;
	}
	public void setKey_6(int key_6) {
		this.key_6 = key_6;
	}
	public int getKey_7() {
		return key_7;
	}
	public void setKey_7(int key_7) {
		this.key_7 = key_7;
	}
	public int getKey_8() {
		return key_8;
	}
	public void setKey_8(int key_8) {
		this.key_8 = key_8;
	}
	public int getKey_9() {
		return key_9;
	}
	public void setKey_9(int key_9) {
		this.key_9 = key_9;
	}
	public int getKey_10() {
		return key_10;
	}
	public void setKey_10(int key_10) {
		this.key_10 = key_10;
	}
	public int getKey_11() {
		return key_11;
	}
	public void setKey_11(int key_11) {
		this.key_11 = key_11;
	}
	public int getKey_12() {
		return key_12;
	}
	public void setKey_12(int key_12) {
		this.key_12 = key_12;
	}
	public int getKey_13() {
		return key_13;
	}
	public void setKey_13(int key_13) {
		this.key_13 = key_13;
	}
	public int getKey_14() {
		return key_14;
	}
	public void setKey_14(int key_14) {
		this.key_14 = key_14;
	}
	public int getKey_15() {
		return key_15;
	}
	public void setKey_15(int key_15) {
		this.key_15 = key_15;
	}
	public int getKey_16() {
		return key_16;
	}
	public void setKey_16(int key_16) {
		this.key_16 = key_16;
	}
	public int getKey_17() {
		return key_17;
	}
	public void setKey_17(int key_17) {
		this.key_17 = key_17;
	}
	public int getKey_18() {
		return key_18;
	}
	public void setKey_18(int key_18) {
		this.key_18 = key_18;
	}
	public int getKey_19() {
		return key_19;
	}
	public void setKey_19(int key_19) {
		this.key_19 = key_19;
	}
	public int getKey_20() {
		return key_20;
	}
	public void setKey_20(int key_20) {
		this.key_20 = key_20;
	}
	public int getKey_21() {
		return key_21;
	}
	public void setKey_21(int key_21) {
		this.key_21 = key_21;
	}
	public int getKey_22() {
		return key_22;
	}
	public void setKey_22(int key_22) {
		this.key_22 = key_22;
	}
	public int getKey_23() {
		return key_23;
	}
	public void setKey_23(int key_23) {
		this.key_23 = key_23;
	}
	public int getKey_24() {
		return key_24;
	}
	public void setKey_24(int key_24) {
		this.key_24 = key_24;
	}
	public int getKey_25() {
		return key_25;
	}
	public void setKey_25(int key_25) {
		this.key_25 = key_25;
	}
	public int getKey_26() {
		return key_26;
	}
	public void setKey_26(int key_26) {
		this.key_26 = key_26;
	}
	public int getKey_27() {
		return key_27;
	}
	public void setKey_27(int key_27) {
		this.key_27 = key_27;
	}
	public int getKey_28() {
		return key_28;
	}
	public void setKey_28(int key_28) {
		this.key_28 = key_28;
	}
	public int getKey_29() {
		return key_29;
	}
	public void setKey_29(int key_29) {
		this.key_29 = key_29;
	}
	public int getKey_30() {
		return key_30;
	}
	public void setKey_30(int key_30) {
		this.key_30 = key_30;
	}
	public int getKey_31() {
		return key_31;
	}
	public void setKey_31(int key_31) {
		this.key_31 = key_31;
	}
	public int getKey_32() {
		return key_32;
	}
	public void setKey_32(int key_32) {
		this.key_32 = key_32;
	}
	public int getKey_33() {
		return key_33;
	}
	public void setKey_33(int key_33) {
		this.key_33 = key_33;
	}
	public int getKey_34() {
		return key_34;
	}
	public void setKey_34(int key_34) {
		this.key_34 = key_34;
	}
	public int getKey_35() {
		return key_35;
	}
	public void setKey_35(int key_35) {
		this.key_35 = key_35;
	}
	public int[] getKeyurl() {
		return keyurl;
	}
	public void setKeyurl(int[] keyurl) {
		this.keyurl = keyurl;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
}
