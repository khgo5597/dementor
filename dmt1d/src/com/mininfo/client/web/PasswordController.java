package com.mininfo.client.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.setting.dao.SettingDao;
import com.mininfo.admin.setting.dao.SettingDaoImpl;
import com.mininfo.admin.setting.model.MailEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.SequenceGenerator;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.mail.MailSender;
import com.mininfo.common.mail.MailUtil;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.common.verify.SiteVerify;
import com.mininfo.user.dao.UserDAO;
import com.mininfo.user.dao.UserDAOImpl;
import com.mininfo.user.model.UserAuthCodeEntity;
import com.mininfo.user.model.UserHintEntity;

public class PasswordController extends HttpServlet {

	protected final Log logger = LogFactory.getLog(getClass());

	private String hintFormPage = PageConst.hintFormPage;	
	private String passSearchFormPage = PageConst.passSearchFormPage;
	private String authCodeFormPage = PageConst.authCodeFormPage;	
	private String authCodePage = "/jsp/display/authCodeForm.jsp";
	private String serchDPWPage = "/jsp/user/search_dmtPw.jsp";
	private String hintUpdatePage = "/jsp/display/hintUpd.jsp";
	private String mailPage = "/jsp/common/sendMailPage.jsp";	//占쏙옙占쏙옙 占쏙옙占쏙옙占쏙옙 호占쏙옙

	protected void service(HttpServletRequest req, HttpServletResponse res)	throws ServletException, IOException {
		String cmd = req.getParameter("cmd");
		String forwardURL = null;

		logger.info("cmd = " + cmd);

		try {
			if ("hintwrite".equalsIgnoreCase(cmd)) {
				forwardURL = hintwrite(req, res);
			} else if ("hintupdate".equalsIgnoreCase(cmd)) {
				forwardURL = hintupdate(req, res);
			} else if ("passwordSearchForm".equalsIgnoreCase(cmd)) {
				forwardURL = passwordSearchForm(req, res);
			} else if ("sendMail".equalsIgnoreCase(cmd)) {
				forwardURL = sendMail(req, res);
			} else if ("mailSend".equalsIgnoreCase(cmd)){
				forwardURL = mailSend(req, res);
			} else if ("authCodeAction".equalsIgnoreCase(cmd)) {
				forwardURL = authCodeAction(req, res);
			} else if ("searchDPassword".equalsIgnoreCase(cmd)) {
				forwardURL = searchDPassword(req, res);
			} else if ("keySearch".equalsIgnoreCase(cmd)) {
				forwardURL = keySearch(req, res);
			} else {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2001");
				StringBuffer sb = new StringBuffer()
				.append(cmd)
				.append(msg);

				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);					
				forwardURL = PageConst.contentsPage;
			}

			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
/* 
	public String hintForm(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		UserDAO userDao = new UserDAOImpl();
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);
		userEntity = userDao.findUser(userEntity);
		
		if(userEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2401");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			req.setAttribute("MESSAGE", sb.toString());
			return PageConst.userInFormPage;
		}
		
		if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2400");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		req.setAttribute("USER_ID", userId);
		return hintFormPage;
	}
*/	

	public String hintwrite(HttpServletRequest req,	HttpServletResponse res)throws Exception {
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		UserDAO userDao = new UserDAOImpl();
		String uemail = userDao.UserEmail(userId);
		
		req.setAttribute("USEREMAIL", uemail);
		return hintFormPage;
	}
	
	public String hintupdate(HttpServletRequest req, HttpServletResponse res)throws Exception {
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		UserDAO userDao = new UserDAOImpl();
		String uemail = userDao.UserEmail(userId);
		
		req.setAttribute("USEREMAIL", uemail);
		return hintUpdatePage;
	}

	public String searchDPassword(HttpServletRequest req,
			HttpServletResponse res)throws Exception {

		return serchDPWPage;
	}
	
	public String passwordSearchForm(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		// 占쏙옙占쏙옙트 占쏙옙占쏙옙 占쏙옙占쏙옙 占쏙옙 failed 占쏙옙占쏙옙占쏙옙占쏙옙 占싱듸옙
		SiteVerify site = new SiteVerify();
		
		if(!site.verify(req)) {
			return PageConst.failed;
		}
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		HttpSession session = req.getSession();
		
		if (userId != null) {
			// 占쏙옙占쏙옙占� 占쏙옙호 찾占쏙옙 占쏙옙占쏙옙 userId 占쏙옙占쏙옙 占쏙옙占쏙옙
			session.setAttribute("userId", userId);
		}
		userId = (String)session.getAttribute("userId");
		
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
			return PageConst.contentsPage;
		}
		
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
		
		if(userHintEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
			
		UserKeyEntity userKeyEntity = userDao.getUserKey(userId);
		if(userKeyEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				

			return PageConst.contentsPage;
		}
		
		if(CommonUtil.isEmpty(userHintEntity.getUser_email())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2501");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				

			return PageConst.contentsPage;
		}
		
		if(CommonUtil.isEmpty(userHintEntity.getPass_answer())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2502");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			
			return PageConst.contentsPage;
		}
		
		String sendType = CommonUtil.nullToStr(req.getParameter("sendType"));
		String processCmd = CommonUtil.nullToStr(req.getParameter("processCmd"));
		
		req.setAttribute("SENDTYPE", sendType);
		req.setAttribute("USER", userHintEntity);
		req.setAttribute("status", status);
		req.setAttribute("processCmd", processCmd);
		return passSearchFormPage;
	}
/*
	public String passwordSearchForm(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
			return PageConst.contentsPage;
		}
		
		logger.info("passwordSearchForm userId="+userId);
		
		UserDAO userDao = new UserDAOImpl();
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);
		userEntity = userDao.findUser(userEntity);
		
		if(userEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2505");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2506");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				

			return PageConst.contentsPage;
		}
		
		UserKeyEntity userKeyEntity = userDao.getUserKey(userId);
		if(userKeyEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				

			return PageConst.contentsPage;
		}
		
		if(CommonUtil.isEmpty(userEntity.getUser_email())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2501");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				

			return PageConst.contentsPage;
		}
		
		if(CommonUtil.isEmpty(userEntity.getUser_answer())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2502");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			
			return PageConst.contentsPage;
		}
		
		req.setAttribute("USER", userEntity);
		req.setAttribute("STATUS", status);
		return passSearchFormPage;
	}
*/	
	public void dmtHintset(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		UserDAO userDao = new UserDAOImpl();
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		//String hin = CommonUtil.nullToStr(new String(req.getParameter("hin").getBytes("8859_1"), "utf-8"));
		String hin = CommonUtil.nullToStr(req.getParameter("hin"));
		//String textfield3 = CommonUtil.nullToStr(new String(req.getParameter("textfield3").getBytes("8859_1"), "utf-8"));
		String textfield3 = CommonUtil.nullToStr(req.getParameter("textfield3"));
		String mail = CommonUtil.nullToStr(req.getParameter("email"));
		String sms = CommonUtil.nullToStr(req.getParameter("sms"));
		//logger.info(hin + " : dmHintset");
				
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
		}
		
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity.setUser_email(mail);
		userHintEntity.setUser_sms(sms);
		userHintEntity.setPass_hint_select_code(hin);
		userHintEntity.setPass_answer(textfield3);

		userDao.insertUserHint(userHintEntity);
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1100");
		StringBuffer sb = new StringBuffer()
		.append(userId)
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
		req.setAttribute("RESULT", resultEntity);			

	}
	
	public void dmtHintupdate(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		UserDAO userDao = new UserDAOImpl();
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		//String hin = CommonUtil.nullToStr(new String(req.getParameter("hin").getBytes("8859_1"), "utf-8"));
		String hin = CommonUtil.nullToStr(req.getParameter("hin"));
		//String textfield3 = CommonUtil.nullToStr(new String(req.getParameter("textfield3").getBytes("8859_1"), "utf-8"));
		String textfield3 = CommonUtil.nullToStr(req.getParameter("textfield3"));
		String mail = CommonUtil.nullToStr(req.getParameter("email"));
		String sms = CommonUtil.nullToStr(req.getParameter("sms"));
		//logger.info(hin + " : dmHintset");
				
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
		}
		
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity.setUser_email(mail);
		userHintEntity.setUser_sms(sms);
		userHintEntity.setPass_hint_select_code(hin);
		userHintEntity.setPass_answer(textfield3);

		userDao.updateUserHint(userHintEntity);
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1100");
		StringBuffer sb = new StringBuffer()
		.append(userId)
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
		req.setAttribute("RESULT", resultEntity);			

	}
/*
	public void hintAction(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		UserDAO userDao = new UserDAOImpl();
		
		String sele = CommonUtil.nullToStr(req.getParameter("sele"));
		String hin = CommonUtil.nullToStr(req.getParameter("hin"));
		String textfield2 = CommonUtil.nullToStr(req.getParameter("textfield2"));
		String textfield3 = CommonUtil.nullToStr(req.getParameter("textfield3"));
		String mail = CommonUtil.nullToStr(req.getParameter("email"));
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
		}
		
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);
		userEntity.setUser_answer(textfield3);
		userEntity.setUser_email(mail);
		
		if("sel".equals(sele)){
			userEntity.setUser_question(hin);
		} else {
			userEntity.setUser_question(textfield2);
		}


		userDao.updateUserHint(userEntity);
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1100");
		StringBuffer sb = new StringBuffer()
		.append(userId)
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
		req.setAttribute("RESULT", resultEntity);			

	}
*/	
	
	public String keySearch(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String hint = CommonUtil.nullToStr(req.getParameter("hin"));
		String answer = CommonUtil.nullToStr(req.getParameter("answer"));
		String email = CommonUtil.nullToStr(req.getParameter("email")).trim();
		String url = "http://" + req.getServerName() + ":" + req.getServerPort();
		
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
		
		if(userHintEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2503");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			return serchDPWPage;
		}
		
		if(!hint.equals(userHintEntity.getPass_hint_select_code())){
			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2515");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			return serchDPWPage;
		}
		
		if(!answer.equals(userHintEntity.getPass_answer())){
			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2507");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			return serchDPWPage;
		}
		
		if(!email.equals(userHintEntity.getUser_email())){
			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2508");
			StringBuffer sb = new StringBuffer()
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);			
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			return serchDPWPage;
		}
		
		SequenceGenerator strSeq = new SequenceGenerator(5, 10);
		
		String auth_code = strSeq.nextValue();
		String mail_server = "smtp.gmail.com";
		String mail_port = "465";
		String mail_to = email;
		String mail_from = "manager@mail.company.com";
		String mail_from_name = "manager";
		String mail_subject = "占쏙옙占쏙옙占� 占쏙옙占쏙옙 占쌘듸옙" + "dementor auth code";
		String mail_auth_yn = "N";
		String mail_auth_id = "";
		String mail_auth_pw = "";
		String mail_contets = getMailContents(url, email, auth_code);
		SettingDao settingDao = new SettingDaoImpl();
		MailEntity mailEntity = settingDao.getMailEntity();
		
		if(mailEntity != null){			
			mail_server = mailEntity.getMail_server(); 
			mail_port = mailEntity.getMail_port(); 
			mail_from = mailEntity.getMail_from(); 
			mail_from_name = mailEntity.getMail_from_name(); 
			mail_subject = mailEntity.getMail_subject(); 
			mail_auth_yn = mailEntity.getMail_auth_yn(); 
			mail_auth_id = mailEntity.getMail_auth_id(); 
			mail_auth_pw = mailEntity.getMail_auth_pw();
		}
		
		Properties pro = new Properties();
		pro.put("mail_server", mail_server);
		pro.put("mail_port", mail_port);
		pro.put("mail_to", mail_to);
		pro.put("mail_from", mail_from);
		pro.put("mail_from_name", mail_from_name);
		pro.put("mail_subject", mail_subject);
		pro.put("mail_auth_yn", mail_auth_yn);
		pro.put("mail_auth_id", mail_auth_id);
		pro.put("mail_auth_pw", mail_auth_pw);
		pro.put("mail_contets", mail_contets);
		
		MailUtil mailUtil = new MailUtil();

		if (logger.isDebugEnabled()) {
			logger.info("auth code mailContents : " + mail_contets);
		}
		try {
			mailUtil.sendMail(pro);
			
			UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
			authEntity.setUser_id(userId);
			authEntity.setAuth_code(auth_code);
			
			userDao.insertUserAuthCode(authEntity);
			
			logger.info("CREATE DATE ="+ new Date());
			
		} catch (Exception ex) {
			logger.info("sendMail Exception : "+ pro.toString());

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, ex.getMessage());	
			req.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
		}
		

		req.setAttribute("USER", userHintEntity);
		req.setAttribute("STATUS", status);
		return authCodeFormPage;
	}
/*
	public String sendMail(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String sendType = CommonUtil.nullToStr(req.getParameter("sendType"));
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		//String hin1 = CommonUtil.nullToStr(new String(req.getParameter("textfield2").getBytes("8859_1"), "utf-8"));
		String hin1 = CommonUtil.nullToStr(req.getParameter("textfield2"));
		String email = CommonUtil.nullToStr(req.getParameter("mail")).trim();
		String sms = CommonUtil.nullToStr(req.getParameter("sms")).trim();
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String url = "http://" + req.getServerName() + ":" + req.getServerPort();
		
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
		
		if(userHintEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2503");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		if(!hin1.equals(userHintEntity.getPass_answer())){
			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2507");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			req.setAttribute("SENDTYPE", sendType);
			
			return PageConst.passSearchFormPage;
		}
		if ("SMS".equals(sendType)) {
			if (!sms.equals(userDao.getUserSMS(userId))) {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("5132");
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(msg);
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);			
				req.setAttribute("USER", userHintEntity);
				req.setAttribute("STATUS", status);
				req.setAttribute("SENDTYPE", sendType);
				
				return PageConst.passSearchFormPage;
			}
		} else {
			if (!email.equals(userHintEntity.getUser_email())){			
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2508");
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(msg);
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);			
				req.setAttribute("USER", userHintEntity);
				req.setAttribute("STATUS", status);
				req.setAttribute("SENDTYPE", sendType);
				
				return PageConst.passSearchFormPage;
			}
		}
		
		SequenceGenerator strSeq = new SequenceGenerator(5, 10);
		
		String auth_code = strSeq.nextValue();
		String mail_server = "mail.kornet.net";
		String mail_port = "25";
		String mail_to = email;
		String mail_from = "manager@mail.company.com";
		String mail_from_name = "manager";
		String mail_subject = "dementor auth code";
		String mail_auth_yn = "N";
		String mail_auth_id = "manager";
		String mail_auth_pw = "manager";
		String mail_contets = getMailContents(url, email, auth_code);
		SettingDao settingDao = new SettingDaoImpl();
		MailEntity mailEntity = settingDao.getMailEntity();
		
		if(mailEntity != null){			
			mail_server = mailEntity.getMail_server(); 
			mail_port = mailEntity.getMail_port(); 
			mail_from = mailEntity.getMail_from(); 
			mail_from_name = mailEntity.getMail_from_name(); 
			mail_subject = mailEntity.getMail_subject(); 
			mail_auth_yn = mailEntity.getMail_auth_yn(); 
			mail_auth_id = mailEntity.getMail_auth_id(); 
			mail_auth_pw = mailEntity.getMail_auth_pw();
		}
		if ("MAIL_POP3".equals(sendType)) {
			// POP3 占쏙옙占쏙옙占쏙옙 占쏙옙占쏙옙 占쏙옙占쏙옙占쏙옙
			Properties pro = new Properties();
			pro.put("mail_server", mail_server);
			pro.put("mail_port", mail_port);
			pro.put("mail_to", mail_to);
			pro.put("mail_from", mail_from);
			pro.put("mail_from_name", mail_from_name);
			pro.put("mail_subject", mail_subject);
			pro.put("mail_auth_yn", mail_auth_yn);
			pro.put("mail_auth_id", mail_auth_id);
			pro.put("mail_auth_pw", mail_auth_pw);
			pro.put("mail_contets", mail_contets);
			
			MailUtil mailUtil = new MailUtil();
	
			if (logger.isDebugEnabled()) {
				logger.info("auth code mailContents : " + mail_contets);
			}
			
			try {
				//占싱몌옙占쏙옙 占쏙옙占쏙옙占쏙옙 占쏙옙占� 占쏙옙占쏙옙.
				mailUtil.sendMail(pro);
				
				UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
				authEntity.setUser_id(userId);
				authEntity.setAuth_code(auth_code);
				
				userDao.insertUserAuthCode(authEntity);
				
				logger.info("CREATE DATE ="+ new Date());
				
			} catch (Exception ex) {
				logger.info("sendMail Exception : "+ pro.toString());
	
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, ex.getMessage());	
				req.setAttribute("RESULT", resultEntity);			
				return PageConst.contentsPage;
			}
		} else if ("MAIL_DB".equals(sendType)) {
			String I_C_CD = "";					// 占쏙옙占쏙옙 (회占쏙옙占쌘듸옙)
			String I_SEND_TYPE = "MAIL";		// 占쏙옙占쏙옙 타占쏙옙
			String I_MAIL_TYPE = "01";			// 占쏙옙占쏙옙占쏙옙 占싣니몌옙 占쏙옙占쏙옙
			String I_TO_ADDR = mail_to;			// 占쏙옙화占쏙옙호
			String I_TO_USER_ID = userId;		// 占쏙옙占싱듸옙
			String I_TO_USER_NM = "";			// 占싱몌옙
			String I_VAL01 = mail_subject;		// 占쏙옙占쏙옙
			String I_VAL02 = mail_contets;		// 占쏙옙占쏙옙
			String I_PGM_ID = "";				// 호占쏙옙占쏙옙 占쏙옙占싸그뤄옙 ID
						
			if ("2".equals(status)) {
				I_PGM_ID = "replace";			// 占쏙옙호 占썹설占쏙옙
			} else if ("3".equals(status)) {
				I_PGM_ID = "search";			// 占쏙옙호 찾占쏙옙
			}
			Map sendParam = new HashMap();
			
			sendParam.put("I_C_CD",I_C_CD);
			sendParam.put("I_SEND_TYPE",I_SEND_TYPE);
			sendParam.put("I_MAIL_TYPE",I_MAIL_TYPE);
			sendParam.put("I_TO_ADDR",I_TO_ADDR);
			sendParam.put("I_TO_USER_ID",I_TO_USER_ID);
			sendParam.put("I_TO_USER_NM",I_TO_USER_NM);
			sendParam.put("I_VAL01",I_VAL01);
			sendParam.put("I_VAL02",I_VAL02);
			sendParam.put("I_PGM_ID",I_PGM_ID);
			sendParam.put("O_ERRORCODE",null);
			sendParam.put("O_ERRORMESG",null);
			
			UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
			authEntity.setUser_id(userId);
			authEntity.setAuth_code(auth_code);
			
			userDao.insertSendAuthCode(authEntity,sendParam);
		} else if ("SMS".equals(sendType)) {
			String I_C_CD = "";					// 占쏙옙占쏙옙 (회占쏙옙占쌘듸옙)
			String I_SEND_TYPE = "SMS";			// 占쏙옙占쏙옙 타占쏙옙
			String I_MAIL_TYPE = "";			// 占쏙옙占쏙옙占쏙옙 占싣니몌옙 占쏙옙占쏙옙
			String I_TO_ADDR = sms;				// 占쏙옙화占쏙옙호
			String I_TO_USER_ID = userId;		// 占쏙옙占싱듸옙
			String I_TO_USER_NM = "";			// 占싱몌옙
			String I_VAL01 = mail_subject;		// 占쏙옙占쏙옙
			String I_VAL02 = auth_code;			// 占쏙옙占쏙옙
			String I_PGM_ID = "";				// 호占쏙옙占쏙옙 占쏙옙占싸그뤄옙 ID
			if ("2".equals(status)) {
				I_PGM_ID = "replace";			// 占쏙옙호 占썹설占쏙옙
			} else if ("3".equals(status)) {
				I_PGM_ID = "search";			// 占쏙옙호 찾占쏙옙
			}
			
			Map sendParam = new HashMap();
			
			sendParam.put("I_C_CD",I_C_CD);
			sendParam.put("I_SEND_TYPE",I_SEND_TYPE);
			sendParam.put("I_MAIL_TYPE",I_MAIL_TYPE);
			sendParam.put("I_TO_ADDR",I_TO_ADDR);
			sendParam.put("I_TO_USER_ID",I_TO_USER_ID);
			sendParam.put("I_TO_USER_NM",I_TO_USER_NM);
			sendParam.put("I_VAL01",I_VAL01);
			sendParam.put("I_VAL02",I_VAL02);
			sendParam.put("I_PGM_ID",I_PGM_ID);
			sendParam.put("O_ERRORCODE",null);
			sendParam.put("O_ERRORMESG",null);
			
			UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
			authEntity.setUser_id(userId);
			authEntity.setAuth_code(auth_code);
			
			userDao.insertSendAuthCode(authEntity,sendParam);
		}

		req.setAttribute("USER", userHintEntity);
		req.setAttribute("STATUS", status);
		req.setAttribute("AUTH_CODE", auth_code);
		return authCodeFormPage;
	}
*/
/**
 * 占쏙옙占쏙옙 체크
 * @param req
 * @param res
 * @return
 * @throws Exception
 */
	public String sendMail(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String sendType = CommonUtil.nullToStr(req.getParameter("sendType"));
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		//String hin1 = CommonUtil.nullToStr(new String(req.getParameter("textfield2").getBytes("8859_1"), "utf-8"));
		String hin1 = CommonUtil.nullToStr(req.getParameter("textfield2"));
		String email = CommonUtil.nullToStr(req.getParameter("mail")).trim();
		String sms = CommonUtil.nullToStr(req.getParameter("sms")).trim();
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String processCmd  = CommonUtil.nullToStr(req.getParameter("processCmd"));
		String url = "http://" + req.getServerName() + ":" + req.getServerPort();
		String msgAuthCode = CodeMessageHandler.getInstance().getCodeMessage("5137");
		
		System.out.println(email + ":::" + userId + ":::" + status);
		
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
		
		SequenceGenerator strSeq = new SequenceGenerator(5, 10);
		String auth_code = strSeq.nextValue();
		String mail_server = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_port = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_to = email;
		String mail_from = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_from_name = "manager";
		String mail_subject = msgAuthCode+"-Dementor auth code";
		String mail_auth_yn = "N";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_auth_id = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_auth_pw = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_contets = getMailContents(url, email, auth_code);

		SettingDao settingDao = new SettingDaoImpl();
		MailEntity mailEntity = settingDao.getMailEntity();
		
		if(userHintEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2503");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		if(!hin1.equals(userHintEntity.getPass_answer())){
			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2507");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			req.setAttribute("SENDTYPE", sendType);
			
			return PageConst.passSearchFormPage;
		}
		
		if (!email.equals(userHintEntity.getUser_email())){			
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2508");
			StringBuffer sb = new StringBuffer().append(userId).append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);			
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			req.setAttribute("SENDTYPE", sendType);
			
			return PageConst.passSearchFormPage;
		}
		
		if(mailEntity != null){			
			mail_server = mailEntity.getMail_server(); 
			mail_port = mailEntity.getMail_port(); 
			mail_from = mailEntity.getMail_from(); 
			mail_from_name = mailEntity.getMail_from_name(); 
			mail_subject = mailEntity.getMail_subject(); 
			mail_auth_yn = mailEntity.getMail_auth_yn(); 
			mail_auth_id = mailEntity.getMail_auth_id(); 
			mail_auth_pw = mailEntity.getMail_auth_pw();
		}
		
		req.setAttribute("USER", userHintEntity);
		req.setAttribute("STATUS", status);
		req.setAttribute("AUTH_CODE", auth_code);
		return mailPage;
//		return mailSend(req, res);
	}
	
	/**
	 * 占쏙옙占쏙옙 占쏙옙占쏙옙占쏙옙
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	public String mailSend(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String sendType    = CommonUtil.nullToStr(req.getParameter("sendType"));
		String status      = CommonUtil.nullToStr(req.getParameter("status"));
		String hin1        = CommonUtil.nullToStr(req.getParameter("textfield2"));
		String email       = CommonUtil.nullToStr(req.getParameter("mail")).trim();
		String sms         = CommonUtil.nullToStr(req.getParameter("sms")).trim();
		String userId     = CommonUtil.nullToStr(req.getParameter("userId"));
		String processCmd  = CommonUtil.nullToStr(req.getParameter("processCmd"));
		String url         = "http://" + req.getServerName() + ":" + req.getServerPort();
		String msgAuthCode = CodeMessageHandler.getInstance().getCodeMessage("5137");

		System.out.println(email + ":::" + userId);
		
		System.out.println("url=> " + url);
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
		
		SequenceGenerator strSeq = new SequenceGenerator(5, 10);
		String auth_code = strSeq.nextValue();
		String mail_server = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_port = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_to = email;
		String mail_from = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_from_name = "manager";
		String mail_subject = "dementor";
		String mail_auth_yn = "N";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_auth_id = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_auth_pw = "";//占쏙옙占쏙옙占쏙옙 google SMTP占쏙옙 처占쏙옙
		String mail_contets = getMailContents(url, email, auth_code);

		Properties pro = new Properties();
		pro.put("mail_to", userHintEntity.getUser_email());
		pro.put("mail_from", "dementor@dementor.co.kr");
		pro.put("mail_from_name", mail_from_name);
		pro.put("mail_subject", mail_subject);
		pro.put("mail_contets", mail_contets);
		
		MailSender mailUtil = new MailSender();
		
		try {

			mailUtil.sendMail(pro);
			
			String strAuthCode = CommonUtil.nullToStr(req.getParameter("authCode"));
			
			UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
			authEntity.setUser_id(userId);
			authEntity.setAuth_code(auth_code);
			
			userDao.insertUserAuthCode(authEntity);	
			
		} catch (Exception ex) {
			logger.info("sendMail Exception : "+ pro.toString());

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, ex.getMessage());
			req.setAttribute("RESULT", resultEntity);
			return PageConst.contentsPage;
		}
		
		req.setAttribute("USER", userHintEntity);
		req.setAttribute("status", status);
		req.setAttribute("AUTH_CODE", auth_code);
		req.setAttribute("processCmd", processCmd);
		
		return authCodeFormPage;
	}
	
	private String authCodeAction(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		String auth_code = CommonUtil.nullToStr(req.getParameter("auth_code"));
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		System.out.println("userId:"+userId);
		System.out.println("auth_code:"+auth_code);
		
		HttpSession session = req.getSession();
		if(userId == null) {
			userId = (String)session.getAttribute("userId");		
		}
		String status = CommonUtil.nullToStr(req.getParameter("status"));
				
		UserDAO userDao = new UserDAOImpl();
		UserHintEntity userHintEntity = new UserHintEntity();
		userHintEntity.setUser_id(userId);
		userHintEntity = userDao.UserHint(userHintEntity);
				
		if(userHintEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2509");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		UserAuthCodeEntity authEntity = new UserAuthCodeEntity();
		authEntity.setUser_id(userId);
		
		authEntity = userDao.getUserAuthCode(authEntity);
		
		if(authEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2511");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		Date createDate = authEntity.getCreate_date();
		
		long createDateLong = createDate.getTime();
		long age = System.currentTimeMillis() - createDateLong;

				
		SettingDao settingDao = new SettingDaoImpl();
		int expire_minute = settingDao.getAuthCodeExpireMinte();
		
		if(expire_minute<0) expire_minute = 5;
		
        if (age > (expire_minute * 60 * 1000L)) {
        	String msg = CodeMessageHandler.getInstance().getCodeMessage("2512");
        	String msg2 = CodeMessageHandler.getInstance().getCodeMessage("2514");
        	StringBuffer sb = new StringBuffer()
        	.append(msg)        	
        	.append(expire_minute)
        	.append(msg2);
        	

        	ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);	
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			logger.info("fail= "+sb.toString());
			return authCodePage;
        }
        
		if(!auth_code.equals(authEntity.getAuth_code())){
			
			logger.info(userId + " input auth_code = "+ auth_code);
			logger.info(userId + " datebase auth_code = "+ authEntity.getAuth_code());
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2513");
			StringBuffer sb = new StringBuffer()
			.append(msg);
			

        	ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);	
			req.setAttribute("USER", userHintEntity);
			req.setAttribute("STATUS", status);
			
			logger.info("fail= "+sb.toString());
			
			return authCodePage;
		}
		
		if("3".equalsIgnoreCase(status)){
			return "/cate/Cate.do?cmd=keySerch&status=client&userId="+userId+"";
		}
			req.setAttribute("STATUS", status);
		
			return "/cate/Cate.do?cmd=keyedit&status=client&userId="+userId+"";
		
		
		/*
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1104");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
		resultEntity.setProcessCode(Const.KEY_EDIT);
		req.setAttribute("RESULT", resultEntity);
		return PageConst.contentsPage;
		*/
	}
	

	private String getMailContents(String url, String email, String authCode) throws Exception{
		String msg = CodeMessageHandler.getInstance().getCodeMessage("3001");
		StringBuffer sb = new StringBuffer();
		sb.append("<html>")
		.append("<head><title>::: DEMENTOR :::</title>")
		.append("<meta http-equiv='Content-Type' content='text/html; charset=utf_8'>")
		.append("</head>")
		.append("<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>")
		.append("<table id='Table_01' width='514' border='0' cellpadding='0' cellspacing='0'>")
		.append("<tr>")
		.append("<td valign='top'>")
		.append("<img src='")
		.append(url)
		.append("http://localhost:8080/dmt1d/images/email_img/mail_01.gif' width='514' height='198' alt=''>")
		.append("</td>")
		.append("</tr>")
		.append("<tr>")
		.append("<td height='112' valign='top'>")
		.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>")
		.append("<tr>")
		.append("<td width='84' height='112'>")
		.append("<img src='")
		.append(url)
		.append("http://localhost:8080/dmt1d/images/email_img/01mail_jp_02.gif' width='84' height='112' border='0'>")
		.append("</td>")
		.append("<td valign='middle'>")
		.append(""+msg+" ") 
		.append(authCode)
		.append("</td>")	
		.append("<td width='110'>")
		.append("<img src='")
		.append(url)
		.append("http://localhost:8080/dmt1d/images/email_img/01mail_jp_04.gif' width='110' height='112' border='0'>")
		.append("</td>")	
		.append("</tr>")		
		.append("</table>")
		.append("</td>")
		.append("</tr>")
		.append("<tr>")
		.append("<td height='26'>")
		.append("<img src='")
		.append(url)
		.append("http://localhost:8080/dmt1d/images/email_img/01mail_jp_09.gif' width='514' height='26'>")		
		.append("</td>")
		.append("</tr>")
		.append("</table>")				
		.append("</body>")
		.append("</html>");	
		
		return sb.toString();
	}
}
