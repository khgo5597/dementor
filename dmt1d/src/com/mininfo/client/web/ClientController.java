package com.mininfo.client.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.client.dao.MysqlclientDAO;
import com.mininfo.client.dao.clientDAO;
import com.mininfo.client.model.UserCatgEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.property.DementorProperty;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.common.verify.VerifySign;
import com.mininfo.user.model.UserEntity;



public class ClientController extends HttpServlet {

protected static final Log logger = LogFactory.getLog(ClientController.class);
	
	private String hint = "/jsp/admin/adminframe.jsp";
	private String test = "/jsp/admin/test.jsp";
	private String message = "/jsp/admin/message.jsp";
	private String keystting = "/cate/Cate.do?cmd=list&status=client";
	private String keyedit = "/cate/Cate.do?cmd=keyedit&status=client";
	private String display = "/jsp/display/display.jsp";
	private String displayUpd = "/jsp/display/displayUpd.jsp";
	private String lampview = "/jsp/display/lamp.jsp";
	
	private static int iconKeyNumber = 0;
	private static String isOtkUse = null;
	
	protected void service( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		String cmd = req.getParameter("cmd"); //display FROM dememtorConnector.jsp
		System.out.println("cmd:"+cmd);
		if (iconKeyNumber == 0 || isOtkUse == null) {
			DementorProperty DP = null;
			try {
				iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
				isOtkUse = DP.getInstance().getOtkUseYn();
			} catch(Exception e) {
				logger.info("iconKeyNumber Exception. ClientController.java");
			}
		}
		//System.out.println("cmd : "+cmd);
		logger.info("ClientController cmd="+cmd);
		String forwardURL = null;
		try {
			if ("keyset".equalsIgnoreCase(cmd)) {
				forwardURL = keysinsert(req,res);
			} else if ("selection".equalsIgnoreCase(cmd)){
				forwardURL = selection(req,res);
			} else if ("getKey".equalsIgnoreCase(cmd)){
				forwardURL = getKey(req,res);;
			} else if ("keyedit".equalsIgnoreCase(cmd)){ 
				forwardURL = keyedit(req,res);
			} else if ("display".equalsIgnoreCase(cmd)){
				forwardURL = display(req,res);
				StringBuffer sb = new StringBuffer(forwardURL);
				// page ���쇰�명�� 異�媛�
				String page = req.getParameter("page");
				sb.append("?page="+page);
				forwardURL = sb.toString();
				System.out.println("forwardURL : "+forwardURL);
			} else if ("lampview".equalsIgnoreCase(cmd)){
				forwardURL = lampview(req,res);
			} else if ("message".equalsIgnoreCase(cmd)){
				forwardURL = message(req,res);
			}
			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);
		} catch(NullPointerException nullP) {
			logger.info("ClientController.java : 89");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *  
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	public String getKey(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		
		String userId = request.getParameter("userId");
		
		session.setAttribute("userId", userId);
		return keyedit;
		
	}
	
	public String selection(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		clientDAO keyService = new MysqlclientDAO();
		String userId = request.getParameter("userId");
		String status = request.getParameter("status");
		
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
			resultEntity.setProcessCode(Const.CLOES);
			request.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
			
		}		
		
		boolean check = keyService.useridCheck(userId);
		if(check) {
			if("edit".equalsIgnoreCase(status)){
				return keyedit;
			} else {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2200");
				StringBuffer sb = new StringBuffer()
				.append(msg);

				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
				resultEntity.setProcessCode(Const.CLOES);
				request.setAttribute("RESULT", resultEntity);			
				return PageConst.contentsPage;
			}
		} else {
			if("edit".equalsIgnoreCase(status)){
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2300");
				StringBuffer sb = new StringBuffer()
				.append(msg);

				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
				resultEntity.setProcessCode(Const.CLOES);
				request.setAttribute("RESULT", resultEntity);			
				return PageConst.contentsPage;
			}else{
				
				return keystting;
			}	
		}		
	}
	
	/**
	 * �� �깅�
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public String keysinsert(HttpServletRequest request, HttpServletResponse response) throws Exception {
		clientDAO keyService = new MysqlclientDAO();
		
		UserKeyEntity entity = new UserKeyEntity();

		String userId = request.getParameter("userId");
		
		int hole = Integer.parseInt(request.getParameter("hole"));
		int key1 = Integer.parseInt(request.getParameter("key1"));
		int key2 = 0, key3 = 0;
		if (iconKeyNumber == 3) {
			key2 = Integer.parseInt(request.getParameter("key2"));
		} else if (iconKeyNumber == 4) {
			key2 = Integer.parseInt(request.getParameter("key2"));
			key3 = Integer.parseInt(request.getParameter("key3"));
		}
		
//		int iconlength = keyService.iconlength();
//		logger.info("iconlength : "+iconlength);		
		
//		int icnt = iconlength;
		int userViewSize = 25;
		
//		int val[]=new int[userViewSize];
		
		ArrayList<Integer> key = new ArrayList<Integer>(25); //�ㅼ�����λ�� �ㅺ�
		key.add(hole);
		key.add(key1);
		key.add(key2);
		key.add(key3);
		
//		ArrayList keysArray = new ArrayList();
		
		entity.setUser_id(userId);
		entity.setKey_hole(hole);
		entity.setKey_1(key1);
		if (iconKeyNumber == 3) {
			entity.setKey_2(key2);
		} else if (iconKeyNumber == 4) {
			entity.setKey_2(key2);
			entity.setKey_3(key3);
		}
/*	
		keysArray.add(0,entity.getKey_hole());
		keysArray.add(1,entity.getKey_1());
		if (iconKeyNumber == 3) {
			keysArray.add(2,entity.getKey_2());
		} else if (iconKeyNumber == 4) {
			keysArray.add(2,entity.getKey_2());
			keysArray.add(3,entity.getKey_3());
		}


		Random random=new Random();

		int soureArrSize = 0, selectedIndex;
		for (int z= 2; z<userViewSize; z++) {
			soureArrSize = sourceArray.size();
			selectedIndex = random.nextInt(soureArrSize);
			val[z] = ((Integer)sourceArray.get(selectedIndex)).intValue();
			sourceArray.remove(selectedIndex);
		}
		if (iconKeyNumber == 2) {
			entity.setKey_2(val[2]);
			entity.setKey_3(val[3]);
		} else if (iconKeyNumber == 3) {
			entity.setKey_3(val[3]);
		}
*/		
		IconEntity icon = new IconEntity();		
		
		icon.setKey_hole(entity.getKey_hole());
		icon.setKey_1(entity.getKey_1());
		icon.setKey_2(entity.getKey_2());
		icon.setKey_3(entity.getKey_3());
		
		List<IconEntity> csteArray = keyService.getIconCount(icon);
		
		Random random = new Random();
		
		while(csteArray.size()!=4){ //移댄��怨�由� ���대��瑜� 4媛�源�吏� ����
			boolean result = false;
			int i=random.nextInt(24)+1;
			for(int x=0;x<csteArray.size();x++) {
				icon = (IconEntity)csteArray.get(x);
				int iconId = icon.getCategory_id();
				
				if(i==iconId) {
					result = true;
				}
			}
			
			if(!result){
				icon = new IconEntity();
				icon.setCategory_id(i);
				icon.setIcon_count(0);
				csteArray.add(icon);
			}
		}
		
		int[]start_num ={1, 26,51};
		int[]end_num   ={25,50,75};
		
		ArrayList<Integer> cateArray = new ArrayList<Integer>(4);
		ArrayList<Integer> count = new ArrayList<Integer>(4);
		
		for(int i=0;i<csteArray.size();i++) {
			icon = (IconEntity)csteArray.get(i);
			cateArray.add(icon.getCategory_id());
			count.add(icon.getIcon_count());
		}
		
		int j=0;
		
		while(j<75){  //count.get(0)=2
			boolean result1 = false;
			int i = random.nextInt(74)+1; // 1~625
			for(int x=0 ; x<key.size(); x++){ 
				if(i==key.get(x)){
					result1 = true; 					
				}
			}
			if(!result1) {
				key.add(i);
				count.set(0, count.get(0)+1);
			}
			
			
			/*	
			if(!result1 && i>start_num[cateArray.get(0)-1] && i<end_num[cateArray.get(0)-1] && count.get(0) < 25){				
				
				count.set(0, count.get(0)+1);				
			}
				
			if(!result1 && i>start_num[cateArray.get(1)-1] && i<end_num[cateArray.get(1)-1] && count.get(1) < 6){				
				key.add(i);
				count.set(1, count.get(1)+1);
			}
			
			if(!result1 && i>start_num[cateArray.get(2)-1] && i<end_num[cateArray.get(2)-1] && count.get(2) < 6){				
				key.add(i);
				count.set(2, count.get(2)+1);
			}
			
			
			
			if(!result1 && i>start_num[cateArray.get(3)-1] && i<end_num[cateArray.get(3)-1] && count.get(3) < 6){				
				key.add(i);
				count.set(3, count.get(3)+1);
			}*/
			j++;
		}

		/*
		entity.setKey_4(val[4]);
		entity.setKey_5(val[5]);
		entity.setKey_6(val[6]);
		entity.setKey_7(val[7]);
		entity.setKey_8(val[8]);
		entity.setKey_9(val[9]);
		entity.setKey_10(val[10]);
		entity.setKey_11(val[11]);
		entity.setKey_12(val[12]);
		entity.setKey_13(val[13]);
		entity.setKey_14(val[14]);
		entity.setKey_15(val[15]);
		entity.setKey_16(val[16]);
		entity.setKey_17(val[17]);
		entity.setKey_18(val[18]);
		entity.setKey_19(val[19]);
		entity.setKey_20(val[20]);
		entity.setKey_21(val[21]);
		entity.setKey_22(val[22]);
		entity.setKey_23(val[23]);
		entity.setKey_24(val[24]);
		*/	
		
		entity.setKey_4(key.get(4));
		entity.setKey_5(key.get(5));
		entity.setKey_6(key.get(6));
		entity.setKey_7(key.get(7));
		entity.setKey_8(key.get(8));
		entity.setKey_9(key.get(9));
		entity.setKey_10(key.get(10));
		entity.setKey_11(key.get(11));
		entity.setKey_12(key.get(12));
		entity.setKey_13(key.get(13));
		entity.setKey_14(key.get(14));
		entity.setKey_15(key.get(15));
		entity.setKey_16(key.get(16));
		entity.setKey_17(key.get(17));
		entity.setKey_18(key.get(18));
		entity.setKey_19(key.get(19));
		entity.setKey_20(key.get(20));
		entity.setKey_21(key.get(21));
		entity.setKey_22(key.get(22));
		entity.setKey_23(key.get(23));
		entity.setKey_24(key.get(24));
		
		for(int i=0;i<key.size();i++) { 
			System.out.println("key"+(i+1)+"==="+key.get(i));
		}
		
		keyService.userkeyinsert(entity);
		
		PasswordController pcontroller = new PasswordController();
		pcontroller.dmtHintset(request,response);
		
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1101");
		StringBuffer sb = new StringBuffer()
		.append(msg);
		
		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
//		return PageConst.contentsPage;
		return PageConst.dementorConnector + "?userId=" + userId;
	}

	/**
	 * �ㅻ�寃�	
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public String keyedit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		clientDAO keyService = new MysqlclientDAO();
		
		UserKeyEntity entity = new UserKeyEntity();

		String userId = request.getParameter("userId");
		
		int hole = Integer.parseInt(request.getParameter("hole"));
		int key1 = Integer.parseInt(request.getParameter("key1"));
		int key2 = 0, key3 = 0;
		if (iconKeyNumber == 3) {
			key2 = Integer.parseInt(request.getParameter("key2"));
		} else if (iconKeyNumber == 4) {
			key2 = Integer.parseInt(request.getParameter("key2"));
			key3 = Integer.parseInt(request.getParameter("key3"));
		}
		
		int userViewSize = 25;
		
		ArrayList<Integer> key = new ArrayList<Integer>(25); //�ㅼ�����λ�� �ㅺ�
		key.add(hole);
		key.add(key1);
		key.add(key2);
		key.add(key3);
				
//		ArrayList keysArray = new ArrayList();
		
		entity.setUser_id(userId);
		entity.setKey_hole(hole);
		entity.setKey_1(key1);
		if (iconKeyNumber == 3) {
			entity.setKey_2(key2);
		} else if (iconKeyNumber == 4) {
			entity.setKey_2(key2);
			entity.setKey_3(key3);
		}
/*
		keysArray.add(0,entity.getKey_hole());
		keysArray.add(1,entity.getKey_1());
		if (iconKeyNumber == 3) {
			keysArray.add(2,entity.getKey_2());
		} else if (iconKeyNumber == 4) {
			keysArray.add(2,entity.getKey_2());
			keysArray.add(3,entity.getKey_3());
		}
		ArrayList sourceArray = new ArrayList();
		
		for (int z= 1; z<=icnt; z++) {
			if (!keysArray.contains(z))
				sourceArray.add(z);
		}

		Random random=new Random();

		int soureArrSize = 0, selectedIndex;
		for (int z= 2; z<userViewSize; z++) {
			soureArrSize = sourceArray.size();
			selectedIndex = random.nextInt(soureArrSize);
			val[z] = ((Integer)sourceArray.get(selectedIndex)).intValue();
			sourceArray.remove(selectedIndex);
		}
		if (iconKeyNumber == 2) {
			entity.setKey_2(val[2]);
			entity.setKey_3(val[3]);
		} else if (iconKeyNumber == 3) {
			entity.setKey_3(val[3]);
		}
*/	

		IconEntity icon = new IconEntity();		
		
		icon.setKey_hole(entity.getKey_hole());
		icon.setKey_1(entity.getKey_1());
		icon.setKey_2(entity.getKey_2());
		icon.setKey_3(entity.getKey_3());
		
		List<IconEntity> csteArray = keyService.getIconCount(icon);
		
		Random random = new Random();
		
		while(csteArray.size()!=4){ //移댄��怨�由� ���대��瑜� 4媛�源�吏� ����
			boolean result = false;
			int i=random.nextInt(24)+1;
			for(int x=0;x<csteArray.size();x++) {
				icon = (IconEntity)csteArray.get(x);
				int iconId = icon.getCategory_id();
				
				if(i==iconId) {
					result = true;
				}
			}
			
			if(!result){
				icon = new IconEntity();
				icon.setCategory_id(i);
				icon.setIcon_count(0);
				csteArray.add(icon);
			}
		}
		
		int[]start_num ={1, 26,51};
		int[]end_num   ={25,50,75};
		
		ArrayList<Integer> cateArray = new ArrayList<Integer>(4);
		ArrayList<Integer> count = new ArrayList<Integer>(4);
		
		for(int i=0;i<csteArray.size();i++) {
			icon = (IconEntity)csteArray.get(i);
			cateArray.add(icon.getCategory_id());
			count.add(icon.getIcon_count());
		}
		
		int j=0;
		
		while(j<75){  //count.get(0)=2
			boolean result1 = false;
			int i = random.nextInt(74)+1; // 1~625
			for(int x=0 ; x<key.size(); x++){ 
				if(i==key.get(x)){
					result1 = true; 
				}
			}
			
			if(!result1){				
				key.add(i);
				count.set(0, count.get(0)+1);
			}
			
/*			
			if(!result1 && i>start_num[cateArray.get(0)-1] && i<end_num[cateArray.get(0)-1] && count.get(0) < 7){				
				key.add(i);
				count.set(0, count.get(0)+1);
			}
			
			if(!result1 && i>start_num[cateArray.get(1)-1] && i<end_num[cateArray.get(1)-1] && count.get(1) < 6){				
				key.add(i);
				count.set(1, count.get(1)+1);
			}
			
			if(!result1 && i>start_num[cateArray.get(2)-1] && i<end_num[cateArray.get(2)-1] && count.get(2) < 6){				
				key.add(i);
				count.set(2, count.get(2)+1);
			}
			
			if(!result1 && i>start_num[cateArray.get(3)-1] && i<end_num[cateArray.get(3)-1] && count.get(3) < 6){				
				key.add(i);
				count.set(3, count.get(3)+1);
			}
			*/
			
			j++;
		}
/*		
		entity.setKey_4(val[4]);
		entity.setKey_5(val[5]);
		entity.setKey_6(val[6]);
		entity.setKey_7(val[7]);
		entity.setKey_8(val[8]);
		entity.setKey_9(val[9]);
		entity.setKey_10(val[10]);
		entity.setKey_11(val[11]);
		entity.setKey_12(val[12]);
		entity.setKey_13(val[13]);
		entity.setKey_14(val[14]);
		entity.setKey_15(val[15]);
		entity.setKey_16(val[16]);
		entity.setKey_17(val[17]);
		entity.setKey_18(val[18]);
		entity.setKey_19(val[19]);
		entity.setKey_20(val[20]);
		entity.setKey_21(val[21]);
		entity.setKey_22(val[22]);
		entity.setKey_23(val[23]);
		entity.setKey_24(val[24]);
*/			

		entity.setKey_4(key.get(4));
		entity.setKey_5(key.get(5));
		entity.setKey_6(key.get(6));
		entity.setKey_7(key.get(7));
		entity.setKey_8(key.get(8));
		entity.setKey_9(key.get(9));
		entity.setKey_10(key.get(10));
		entity.setKey_11(key.get(11));
		entity.setKey_12(key.get(12));
		entity.setKey_13(key.get(13));
		entity.setKey_14(key.get(14));
		entity.setKey_15(key.get(15));
		entity.setKey_16(key.get(16));
		entity.setKey_17(key.get(17));
		entity.setKey_18(key.get(18));
		entity.setKey_19(key.get(19));
		entity.setKey_20(key.get(20));
		entity.setKey_21(key.get(21));
		entity.setKey_22(key.get(22));
		entity.setKey_23(key.get(23));
		entity.setKey_24(key.get(24));
		
		for(int i=0;i<key.size();i++) { 
			System.out.println("key"+(i+1)+"==="+key.get(i));
		}
		
		keyService.userkeyupdate(entity);
		
		PasswordController pcontroller = new PasswordController();
		pcontroller.dmtHintupdate(request,response);
		
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1102");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		
		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
//		return PageConst.contentsPage;
		return PageConst.dementorConnector + "?userId=" + userId;
	}
	
/*	
	public String keyedit(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		clientDAO keyService = new MysqlclientDAO();
		
		UserKeyEntity entity = new UserKeyEntity();

		String userId = request.getParameter("userId");
		
		int hole = Integer.parseInt(request.getParameter("hole"));
		int key1 = Integer.parseInt(request.getParameter("key1"));
		int key2 = 0, key3 = 0;
		if (iconKeyNumber == 3) {
			key2 = Integer.parseInt(request.getParameter("key2"));
		} else if (iconKeyNumber == 4) {
			key2 = Integer.parseInt(request.getParameter("key2"));
			key3 = Integer.parseInt(request.getParameter("key3"));
		}
		
		logger.info("hole : "+hole);
		logger.info("key1 : "+key1);
		if (iconKeyNumber == 3) {
			logger.info("key2 : "+key2);
		} else if (iconKeyNumber == 4) {
			logger.info("key2 : "+key2);
			logger.info("key3 : "+key3);
		}
		int iconlength = keyService.iconlength();
		int icnt = iconlength;
		int userViewSize = 25;
		int val[]=new int[userViewSize];
		
		ArrayList keysArray = new ArrayList();
		
		entity.setUser_id(userId);
		entity.setKey_hole(hole);
		entity.setKey_1(key1);
		if (iconKeyNumber == 3) {
			entity.setKey_2(key2);
		} else if (iconKeyNumber == 4) {
			entity.setKey_2(key2);
			entity.setKey_3(key3);
		}

		keysArray.add(0,entity.getKey_hole());
		keysArray.add(1,entity.getKey_1());
		if (iconKeyNumber == 3) {
			keysArray.add(2,entity.getKey_2());
		} else if (iconKeyNumber == 4) {
			keysArray.add(2,entity.getKey_2());
			keysArray.add(3,entity.getKey_3());
		}
		ArrayList sourceArray = new ArrayList();
		
		for (int z= 1; z<=icnt; z++) {
			if (!keysArray.contains(z))
				sourceArray.add(z);
		}

		Random random=new Random();

		int soureArrSize = 0, selectedIndex;
		for (int z= 2; z<userViewSize; z++) {
			soureArrSize = sourceArray.size();
			selectedIndex = random.nextInt(soureArrSize);
			val[z] = ((Integer)sourceArray.get(selectedIndex)).intValue();
			sourceArray.remove(selectedIndex);
		}
		if (iconKeyNumber == 2) {
			entity.setKey_2(val[2]);
			entity.setKey_3(val[3]);
		} else if (iconKeyNumber == 3) {
			entity.setKey_3(val[3]);
		}
		entity.setKey_4(val[4]);
		entity.setKey_5(val[5]);
		entity.setKey_6(val[6]);
		entity.setKey_7(val[7]);
		entity.setKey_8(val[8]);
		entity.setKey_9(val[9]);
		entity.setKey_10(val[10]);
		entity.setKey_11(val[11]);
		entity.setKey_12(val[12]);
		entity.setKey_13(val[13]);
		entity.setKey_14(val[14]);
		entity.setKey_15(val[15]);
		entity.setKey_16(val[16]);
		entity.setKey_17(val[17]);
		entity.setKey_18(val[18]);
		entity.setKey_19(val[19]);
		entity.setKey_20(val[20]);
		entity.setKey_21(val[21]);
		entity.setKey_22(val[22]);
		entity.setKey_23(val[23]);
		entity.setKey_24(val[24]);
			
		keyService.userkeyupdate(entity);
		
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1102");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		
		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
//		return PageConst.contentsPage;
		return PageConst.dementorConnector + "?userId=" + userId;
	}
*/	
	public boolean signVerify(HttpServletRequest request) throws SQLException, Exception {
		VerifySign verifySign = new VerifySign();										// 사용자 ID , SESSION , OTK 비교 클래스
		String otk = request.getParameter("otk");										// OTK
		String urlId = request.getParameter("urlId");									// URL ID
		String userId = request.getParameter("userId");									// USER ID
		String userIp = request.getParameter("userIp");									// USER IP
		String now = request.getParameter("now");										// NOW
		return verifySign.isVerifySign(request, otk, urlId, userId,userIp, now);		// 사용자 ID , SESSION, otk얻기, 동일 여부체크
	}
	
	public String display(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Get으로 데이터를 넘길경우 연결실패
		if(request.getMethod().equalsIgnoreCase("GET")) {
			return PageConst.failed;
		}
		if ("Y".equals(isOtkUse)) { //사용자여부 Y 
			if (request.getParameter("otk") != null) {					// OTK가 없을경우 
				if (!signVerify(request)) {								// 사용자 ID , SESSION , OTK 비교해서 다를걍우 
					final Log logger = LogFactory.getLog(getClass());	// 
					logger.info("signVerify error!");
					return PageConst.failed;
				}
			}
		}
		
		clientDAO keyService = new MysqlclientDAO();
		String userId = request.getParameter("userId");
		String urlId = request.getParameter("urlId");									// URL ID
		String level = request.getParameter("level");
		String upChk = request.getParameter("upChk");
		System.out.println("level===="+level);
		System.out.println("cmd===="+request.getParameter("cmd"));
		System.out.println("page===="+request.getParameter("page"));
		HttpSession session = request.getSession();
		if (urlId != null) {
			session.setAttribute("urlId",urlId);
		}
		if(request.getParameter("page") != null) {
			String page = request.getParameter("page");		//  page = main
			if(page.equals("main")) {
				// �몄�댄�� 寃�利� �ㅽ�� �� failed ���댁�濡� �대��
				//SiteVerify site = new SiteVerify();
				//if(!site.verify(request)) {
					//return PageConst.failed;
				//}
				System.out.println("keyService.useridCheck(userId)#############"+keyService.useridCheck(userId));
				if(!keyService.useridCheck(userId)){
					request.setAttribute("USERID", userId);
					request.setAttribute("URL", request.getRequestURL().toString());
					return PageConst.showhelp; //?
				}
			}else if(page.equals("modify")){
				// main�� ���� ������蹂� ������ �듯�� �ㅼ�댁�ㅻ�� referer�� ���� 耳��댁��
				//SiteVerify site = new SiteVerify();
				//if(!site.verify(request,"REFERER")) {
				//	return PageConst.failed;
				//}
			} 
		} else {
			// ���� ���� �ㅽ�⑥��, �ㅼ�명��
			//SiteVerify site = new SiteVerify();
			//if(!site.verify(request)) {
			//	return PageConst.failed;
			//}
		}
		
		
		if(CommonUtil.isEmpty(userId)){ // �ъ�⑹�� 議댁�ъ��臾�
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2100");
			StringBuffer sb = new StringBuffer().append(userId).append(msg);
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			request.setAttribute("RESULT", resultEntity);	
			resultEntity.setProcessCode(Const.CLOES);
			return PageConst.contentsPage;
		}

		if(keyService.useridCheck(userId)){ // �ъ�⑹�� �� 媛� ��臾�
			if(upChk.equals("up")){
				request.setAttribute("upChk", upChk);
				session.setAttribute("LEVEL", level); //�몄���� level 媛��� 吏���
				return displayUpd;
			} else {
				//alert(keys+'='+document.correct.usr_correct.value);	
				//user.setUser_id(userid);
				session.setAttribute("LEVEL", level); //�몄���� level 媛��� 吏���
				return display;
			}
		} else {
			//String msg = CodeMessageHandler.getInstance().getCodeMessage("9000");
			//request.setAttribute("MESSAGE", msg);			
			return keystting;
		}
	}
	
	public List getMemberKey(String userId) throws Exception {
		
		clientDAO keyService = new MysqlclientDAO();
		List memberkey = keyService.memberkey(userId);
		
		return memberkey;
	}
	
	public UserCatgEntity getUserCatgKey(String userId) throws Exception {
		
		clientDAO keyService = new MysqlclientDAO();
		UserCatgEntity getUserCatgKey = keyService.getUserCatgKey(userId);
		
		return getUserCatgKey;
	}
	
	public List memberkeyurl(int[] keyurl) throws Exception {
		
		clientDAO keyService = new MysqlclientDAO();
		List memberkeyurl = keyService.memberkeyurl(keyurl);
		
		return memberkeyurl;
	}
	
	public String lampview(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return lampview;
	}
	
	public String message(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String Count = request.getParameter("cnt");
		int cnt = DementorProperty.getInstance().getAccessCount();
		String MCnt = Integer.toString(cnt);
		String msg = CodeMessageHandler.getInstance().getCodeMessage("5122");
		String msg2 = CodeMessageHandler.getInstance().getCodeMessage("5123");
		StringBuffer sb = new StringBuffer()
		.append(msg+" ")
		.append(MCnt)
		.append(msg2);
		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);	
				
		return PageConst.contentsPage;
		
	}
	
	public String message(HttpServletRequest request,
			HttpServletResponse response, String msg) throws Exception {
		StringBuffer sb = new StringBuffer()
		.append(msg);
		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);	
		return PageConst.contentsPage;		
	}
	
}
