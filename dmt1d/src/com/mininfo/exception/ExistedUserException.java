package com.mininfo.exception;

public class ExistedUserException extends Exception {

    public ExistedUserException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ExistedUserException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public ExistedUserException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ExistedUserException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }
}
