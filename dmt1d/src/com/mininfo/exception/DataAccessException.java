package com.mininfo.exception;

public class DataAccessException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 
     */
    public DataAccessException() {
        super();
    }

    /**
     * @param message
     */
    public DataAccessException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public DataAccessException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
