package com.mininfo.exception;

public class PasswordMismatchException extends Exception {

    public PasswordMismatchException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public PasswordMismatchException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public PasswordMismatchException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public PasswordMismatchException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
