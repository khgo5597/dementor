package com.mininfo.user.web;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.client.dao.MysqlclientDAO;
import com.mininfo.client.dao.clientDAO;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.property.DementorProperty;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.user.dao.UserDAO;
import com.mininfo.user.dao.UserDAOImpl;
import com.mininfo.user.model.UserEntity;

public class LoginController extends HttpServlet {
	
	protected final Log logger = LogFactory.getLog(getClass());

	private String loginPage = "/index.jsp";
	
	// main 占쌨소듸옙 占쏙옙활
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		String cmd = req.getParameter("cmd");
		
		logger.info("LoginController cmd="+cmd);
		String forwardURL = null;
		try {
			
			if ("loginAction".equalsIgnoreCase(cmd)) {
				forwardURL = login(req, res); // 로그인처리
			} else if ("logoutAction".equalsIgnoreCase(cmd)) {
				forwardURL = logout(req, res); // 로그아웃 처리
			} else if ("mainPage".equalsIgnoreCase(cmd)) {
				forwardURL = mainPage(req, res); // /main.jsp 파일 호출
			} else if ("logindmt1d".equalsIgnoreCase(cmd)) {
				forwardURL = logindmt(req, res); // ?
			} else if ("dmt1dCheck".equalsIgnoreCase(cmd)){
				forwardURL = dmt1dCheck(req, res); // ?
			} else {
				forwardURL = loginForm(req, res);
			}
			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String loginForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return loginPage;
	}
	
	public String mainPage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return PageConst.main;
	}
	
	public String logindmt(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String userId = CommonUtil.nullToStr(request.getParameter("userId"));
		
		String selectdmt = "/client/Client.do?cmd=display&userId="+userId+"";
		return selectdmt;
	}

	public String login(HttpServletRequest req, HttpServletResponse res) throws Exception {
		System.out.println("login start########");
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String user_pass = CommonUtil.nullToStr(req.getParameter("user_pass"));
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);

		try {
			UserDAO userDAO = new UserDAOImpl();
			userEntity = userDAO.findUser(userEntity);
			
			if(userEntity == null){ // 사용자
				
				String msg = CodeMessageHandler.getInstance().getCodeMessage("4000"); //사용자는 등록되어 있지 않습니다<br/>아이디를 확인 해주시기 바랍니다
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(" ")
				.append(msg);

				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
				resultEntity.setProcessCode(Const.CLOES);
				req.setAttribute("RESULT", resultEntity);					
				return PageConst.contentsPage;
			} 
			
			if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
				String msg = CodeMessageHandler.getInstance().getCodeMessage("4001"); // 사용자는 이미 탈퇴 처리 되었습니다.<br/>가입 탈퇴한  사용자는 접속 할 수 없습니다.
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(" ")
				.append(msg);
				
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
				resultEntity.setProcessCode(Const.CLOES);
				req.setAttribute("RESULT", resultEntity);					
				return PageConst.contentsPage;
			}
			
			if(!userEntity.isMatchPassword(user_pass)) {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("4002"); // 사용자의 비밀번호가 일치하지 않습니다.<br/>비밀번호를 확인 해주시기 바랍니다.
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(" ")
				.append(msg);
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
				resultEntity.setProcessCode(Const.CLOES);
				req.setAttribute("RESULT", resultEntity);	
						
				return PageConst.contentsPage;				
			}
											
			HttpSession session = req.getSession(true);
			session.setAttribute("USER", userEntity);
			
			//String selectdmt = "/client/Client.do?cmd=display&userId="+userId+"";
			//return selectdmt;
		
			/*
			 * 코드수정 2008년 11월 17일 작성자 박찬우주임
			String selectdmt = "/client/Client.do?cmd=display&userId="+userId+"&page=main"; // client.web.ClientController.display
			HashMap paramMap = null; // HashMap형 변수 생성
			HashMap map=null;
			if(selectdmt.indexOf("?")!=-1){ // Get 파라미터 값이 있는지?여부
				String params=selectdmt.substring(selectdmt.indexOf("?")+1,selectdmt.length()); // params변수에 Get파라미터 값을 어사인시킨다
				if(params.indexOf("&")==-1){ // &표시가 있는지 확인
					paramMap = new HashMap(); // HashMap형 생성자 pa
					String[] tempParam=params.split("="); // =을 중심으로 key값과 value 값을 배열에 담는다
					paramMap.put(tempParam[0], tempParam[1]); // HashMap 변수 pa에 어사인 시킨다
					req.setAttribute("clientParam",paramMap);
				}else{ // & 표시 값이 더 있다면
					paramMap = new HashMap(); // HashMap형 생성자 pa
					String[] params2=params.split("&"); // &을 기준으로 나누어 배열변수에 어사인 시킨다
					for(int i=0; i<params2.length; i++){ // &파라미트의 값만큼 for 문을 돌린다
						String[] tempParam=params2[i].split("="); // =을 중심으로 key값과 value값을 배열에 담는다
						for(int j=0; j<tempParam.length; j++){
							paramMap.put(tempParam[0], tempParam[1]); // HashMap 변수 pa에 어사인 시킨다
							req.setAttribute("clientParam",paramMap);
						}
					}
				}
			}
			
			 */
			String param = "?cmd=display&userId="+userId+"&page=main";
			String selectdmtlogin=PageConst.transfer + param;

			return selectdmtlogin;
			/*
			String msg = CodeMessageHandler.getInstance().getCodeMessage("1200");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(" ")
			.append(msg);
			ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
			req.setAttribute("RESULT", resultEntity);					
			return PageConst.select;
			*/
						
		} catch (Exception e){
			throw e;
		}
	}
	
	public String dmt1dCheck(HttpServletRequest req, HttpServletResponse res) throws Exception {

			String userId = CommonUtil.nullToStr(req.getParameter("userId"));
			
			UserEntity userEntity = new UserEntity();
			userEntity.setUser_id(userId);

			try {
				UserDAO userDAO = new UserDAOImpl();
				userEntity = userDAO.findUser(userEntity);
				
				if(userEntity == null){
					
					String msg = CodeMessageHandler.getInstance().getCodeMessage("4000"); // 사용자는 등록되어 있지 않습니다<br/>아이디를 확인 해주시기 바랍니다
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(" ")
					.append(msg);

					ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
					resultEntity.setProcessCode(Const.CLOES);
					req.setAttribute("RESULT", resultEntity);					
					return PageConst.contentsPage;
				} 
				
				if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
					String msg = CodeMessageHandler.getInstance().getCodeMessage("4001"); // 사용자는 이미 탈퇴 처리 되었습니다.<br/>가입 탈퇴한  사용자는 접속 할 수 없습니다.
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(" ")
					.append(msg);
					
					ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
					resultEntity.setProcessCode(Const.CLOES);
					req.setAttribute("RESULT", resultEntity);					
					return PageConst.contentsPage;
				}
								
												
					HttpSession session = req.getSession(true);
					session.setAttribute("USER", userEntity);
				
					//String selectdmt = "/client/Client.do?cmd=display&userId="+userId+"";
					//return selectdmt;
				clientDAO keyService = new MysqlclientDAO();
				
				if(!keyService.useridCheck(userId)){
					req.setAttribute("USERID", userId);
					return PageConst.showhelp;
				}else{
					String selectdmt = "/client/Client.do?cmd=display&userId="+userId+"";
					return selectdmt;
				}
					/*
					String msg = CodeMessageHandler.getInstance().getCodeMessage("1200");
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(" ")
					.append(msg);
					ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
					req.setAttribute("RESULT", resultEntity);					
					return PageConst.select;
					*/
							
			} catch (Exception e){
				throw e;
			}
		}
	
	public String logout(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			
		HttpSession session = request.getSession(false);
			if(session != null){
				session.invalidate();
			}
		response.setHeader("Cache-Control","no-store"); //HTTP 1.1
		response.setHeader("Pragma","no-cache"); //HTTP 1.0
		response.setDateHeader ("Expires", 0);
		return PageConst.index;
	}

}
