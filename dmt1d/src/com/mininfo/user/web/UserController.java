package com.mininfo.user.web; 

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.monitoring.util.PageEntity;
import com.mininfo.admin.monitoring.util.PageProcess;
import com.mininfo.client.dao.MysqlclientDAO;
import com.mininfo.client.dao.clientDAO;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.logger.dao.UserLogDao;
import com.mininfo.logger.dao.UserLogDaoImpl;
import com.mininfo.logger.model.UserOutLogEntity;
import com.mininfo.user.dao.UserDAO;
import com.mininfo.user.dao.UserDAOImpl;
import com.mininfo.user.model.UserEntity;
import com.mininfo.user.model.UserHintEntity;
import com.mininfo.user.model.UserListEntity;


public class UserController extends HttpServlet {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private String userInFormPage = "/jsp/user/userWrite.jsp";
	private String ckeckIdFormPage = "/jsp/user/idCheck.jsp";
	private String modifyFormPage = "/jsp/user/userModify.jsp";
	private String usersList = "/jsp/user/usersList.jsp";
	private String excel = "/jsp/user/excel.jsp";

	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		String cmd = req.getParameter("cmd");
		String forwardURL = null;
					
		try {
			if("userInForm".equalsIgnoreCase(cmd)){
				forwardURL = userInForm(req,res);
			} else if("userInAction".equalsIgnoreCase(cmd)){
				forwardURL = userInProcess(req,res);
			} else if ("checkId".equalsIgnoreCase(cmd)){
				forwardURL = checkUserId(req,res);
			} else if ("userModifyForm".equalsIgnoreCase(cmd)){
				forwardURL = userUpdateForm(req,res);
			} else if ("userModifyAction".equalsIgnoreCase(cmd)){
				forwardURL = userUpdateProcess(req,res);
			} else if ("userOutAction".equalsIgnoreCase(cmd)){
				forwardURL = userOutProcess(req,res);
			} else if ("userList".equalsIgnoreCase(cmd)){ 
				forwardURL = userList(req,res);
			} else if ("userStatusEdit".equalsIgnoreCase(cmd)){ 
				forwardURL = userStatusEditForm(req,res);
			} else if ("dmtHintSet".equalsIgnoreCase(cmd)){ 
				forwardURL = dmtHintSet(req,res);
			} else if ("userKeyDelete".equalsIgnoreCase(cmd)){ 
				forwardURL = userKeyDelete(req,res);
			} else if ("excel".equalsIgnoreCase(cmd)){ 
				forwardURL = excel(req,res);
			} else {		
				String resultMessage = "UNKNOWN REQUEST";
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, resultMessage);	
				req.setAttribute("RESULT", resultEntity);
				forwardURL = PageConst.contentsPage;
			}
			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);
			
		} catch (Exception e) {
			    e.printStackTrace();
		}	
	}
	
	public String userInForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return userInFormPage;
	}


	public String ckeckIdForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return ckeckIdFormPage;
	}
	

	public String userUpdateForm(HttpServletRequest req, HttpServletResponse res) throws Exception {


		UserDAO userService = new UserDAOImpl();
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		//System.out.println(userId);
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);
		
		//UserDAO userDAO = new UserDAOImpl();
		//userEntity = userDAO.findUser(userEntity);
		/*if(userEntity == null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("4200");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
		}
		
		if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("4201");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);	
			
			return PageConst.contentsPage;
		}
		*/
		req.setAttribute("USER", userEntity);		
		
		return modifyFormPage;
	}
	

	public String userInProcess(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		UserDAO userDao = new UserDAOImpl();
		
		UserRequestProcess process = new UserRequestProcess();
		UserEntity userEntity = process.requestInEntity(req, res);
		
		if(CommonUtil.isEmpty(userEntity.getUser_id())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userEntity.getUser_id()+msg);	
			req.setAttribute("RESULT", resultEntity);	
			
			return PageConst.contentsPage;
		}
		
		StringBuffer sb = new StringBuffer();
		
		if(userDao.findUser(userEntity) != null){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("4100");
			sb.append(userEntity.getUser_id())
			.append(msg);
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
			resultEntity.setProcessCode(Const.CODE_INDEX);
			req.setAttribute("RESULT", resultEntity);		
			return PageConst.contentsPage;			
			
		} else {
			userDao.insertUser(userEntity);
			String msg = CodeMessageHandler.getInstance().getCodeMessage("1202");
			sb.append(userEntity.getUser_id())
			.append(msg);
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
			resultEntity.setProcessCode(Const.CODE_INDEX);
			req.setAttribute("RESULT", resultEntity);		
			return PageConst.contentsPage;			
		}
	}


	public String checkUserId(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		UserEntity user = new UserEntity();
		
		user.setUser_id(CommonUtil.nullToStr(req.getParameter("userId")));
		if((user.getUser_id())==null || (user.getUser_id()) == ""){
			req.setAttribute("CHECK_ID", "null");
			return ckeckIdForm(req, res);
		}
		
		UserDAO userService = new UserDAOImpl();
		
		boolean isExistId = userService.isExistId(user);
		
		if (isExistId){
			req.setAttribute("CHECK_ID", "true");
		}else{
			req.setAttribute("CHECK_ID", "falue");
		}
		return ckeckIdForm(req, res);
	}
	

	public String userUpdateProcess(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		UserDAO userService = new UserDAOImpl();
		
		UserRequestProcess process = new UserRequestProcess();
		UserEntity userEntity =process.requestEntity(req, res);
		int result = userService.updateUser(userEntity);
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1203");
		StringBuffer sb = new StringBuffer()
		.append(userEntity.getUser_id())
		.append(msg);


		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
		req.setAttribute("RESULT", resultEntity);		
		return PageConst.contentsPage;
	}

	public String userOutProcess(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		
		if(CommonUtil.isEmpty(userId)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2003");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
			req.setAttribute("RESULT", resultEntity);	
			return PageConst.contentsPage;
		}
		
		UserEntity userEntity = new UserEntity();
		userEntity.setUser_id(userId);
		
		UserDAO userDAO = new UserDAOImpl();
		userEntity = userDAO.findUser(userEntity);
			
		logger.info("userEntity="+userEntity);
		
		if (userEntity == null) {
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2603");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		if(Const.USER_STATUS_OUT.equals(userEntity.getUser_status())){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2600");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}
		
		userEntity.setUser_status(Const.USER_STATUS_OUT);
		if(!userDAO.removeUser(userEntity)){
			String msg = CodeMessageHandler.getInstance().getCodeMessage("2601");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
			req.setAttribute("RESULT", resultEntity);				
			return PageConst.contentsPage;
		}

		UserLogDao dao = new UserLogDaoImpl();
		
		UserOutLogEntity entity = new UserOutLogEntity();
		entity.setUser_id(userId);
		
//		if(dao.isAlreadyOutUser(entity)){
//			String msg = CodeMessageHandler.getInstance().getCodeMessage("2602");
//			StringBuffer sb = new StringBuffer()
//			.append(userId)
//			.append(msg);

//			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
//			req.setAttribute("RESULT", resultEntity);				
//			return PageConst.contentsPage;
//		}

		if(dao.insertUserOutLog(entity)) {
			String msg = CodeMessageHandler.getInstance().getCodeMessage("1103");
			StringBuffer sb = new StringBuffer()
			.append(userId)
			.append(msg);
			
			HttpSession session = req.getSession(false);
			session.invalidate();
			
			ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
			resultEntity.setProcessCode(Const.LOGIN_FAIL);
			req.setAttribute("RESULT", resultEntity);				
			
			return PageConst.contentsPage;
		} else {
			String msg = CodeMessageHandler.getInstance().getCodeMessage("1103");
			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId +msg);	
			req.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
		}
	}
	
	public String userList(HttpServletRequest req,
			HttpServletResponse res) throws Exception{
		
		int startIndex = 0;
		int endIndex = 0;
		//current_page = CommonUtil.strToInt(req.getParameter("current_page"), 1);
		String searchckd = CommonUtil.nullToStr(req.getParameter("searchckd"));
		String searchId = CommonUtil.nullToStr(req.getParameter("userId"));
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		String search = CommonUtil.nullToStr(req.getParameter("search"));
		UserListEntity userPage = new UserListEntity();
		
		if(searchId != ""){
			userPage.setUser_id(searchId);
		}else if("st".equalsIgnoreCase(search)){
			userPage.setUser_status(status);
			userPage.setSearch(search);
		}
		
		if(search == "" || "id".equalsIgnoreCase(search)){
			req.setAttribute("RADIO", "id");
		}else if("st".equalsIgnoreCase(search)){
			req.setAttribute("RADIO", "st");
			req.setAttribute("STATUSYNB", status);
		}
		PageProcess pageProcess = new PageProcess();
		//System.out.println(userPage);
		PageEntity page = pageProcess.calculatePage2(req, res, userPage);
		
		userPage.setStartIndex(page.getStart_idx());
		userPage.setEndIndex(page.getEnd_idx());
		
		req.setAttribute("PAGE", page);
		
		UserDAO userService = new UserDAOImpl();
		java.util.List userList = null;
		userList = userService.UserList(userPage);
				
		req.setAttribute("USERS_LIST", userList);
		return usersList;
	}
	
	
	public String userKeyDelete(HttpServletRequest req,
			HttpServletResponse res) throws Exception{
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));

		UserDAO userService = new UserDAOImpl();
		userService.userKeyDelete(userId);
		userId="";
		return usersList;
	}
	
	public String userStatusEditForm(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String user_status = CommonUtil.nullToStr(req.getParameter("status2"));
		int cnt = CommonUtil.strToInt(req.getParameter("count2"));
		
		clientDAO client = new MysqlclientDAO();
		client.dmtloginCntEdit(cnt,userId);
		
		int isExistId = 0;
		UserEntity user = new UserEntity();
		user.setUser_id(userId);
		user.setUser_status(user_status);
						
		if("N".equalsIgnoreCase(user_status)  || "Y".equalsIgnoreCase(user_status)){
			if(CommonUtil.isEmpty(userId)){
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2003"); // 입력값이 유효하지 않습니다
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, userId+msg);	
				req.setAttribute("RESULT", resultEntity);	
				return PageConst.contentsPage;
			}
			
			UserEntity userEntity = new UserEntity();
			userEntity.setUser_id(userId);
			
			UserDAO userDAO = new UserDAOImpl();
			userEntity = userDAO.findUser(userEntity);
				
			logger.info("userEntity="+userEntity);
			
			if (userEntity == null) {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2603"); // 사용자의 탈퇴 요청하신 아이디는 존재하지 않습니다.
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(msg);			
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);				
				return PageConst.contentsPage;
			}
						
			if(user_status.equals(userEntity.getUser_status())){
				if("N".equals(user_status)){
					String msg = CodeMessageHandler.getInstance().getCodeMessage("2600"); // 사용자는 이미 탈퇴 처리가 완료된 사용자 입니다.<br/>관리자에게 문의하시기 바랍니다.
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(msg);
					
					ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
					req.setAttribute("RESULT", resultEntity);				
					return PageConst.contentsPage;
				}else if("Y".equals(user_status)){
					String msg = CodeMessageHandler.getInstance().getCodeMessage("2600"); // 사용자는 이미 탈퇴 처리가 완료된 사용자 입니다.<br/>관리자에게 문의하시기 바랍니다.
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(msg);
					
					ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
					req.setAttribute("RESULT", resultEntity);				
					return PageConst.contentsPage;
				}
			}
			
			userEntity.setUser_status(user_status);
			if(!userDAO.removeUser(userEntity)){
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2601"); // 사용자 탈퇴작업이 완료되지 않았습니다 관리자에게 작업로그를 확인해보시기 바랍니다
				StringBuffer sb = new StringBuffer()
				.append(userId)
				.append(msg);

				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);				
				return PageConst.contentsPage;
			}

			UserLogDao dao = new UserLogDaoImpl();
			
			UserOutLogEntity entity = new UserOutLogEntity();
			entity.setUser_id(userId);
			entity.setDelete_status(user_status);
			
//			if(dao.isAlreadyOutUser(entity)){
//				String msg = CodeMessageHandler.getInstance().getCodeMessage("2602");
//				StringBuffer sb = new StringBuffer()
//				.append(userId)
//				.append(msg);
//
//				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
//				req.setAttribute("RESULT", resultEntity);				
//				return PageConst.contentsPage;
//			}

			if(dao.insertUserOutLog(entity)) {
				if("N".equals(user_status)){
					String msg = CodeMessageHandler.getInstance().getCodeMessage("1103");
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(msg);
					
					HttpSession session = req.getSession(false);
					session.invalidate();
					
					ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
					req.setAttribute("RESULT", resultEntity);				
					
					return PageConst.contentsPage;
				}else if("Y".equals(user_status)){
					String msg = CodeMessageHandler.getInstance().getCodeMessage("1103");
					StringBuffer sb = new StringBuffer()
					.append(userId)
					.append(msg);
					
					HttpSession session = req.getSession(false);
					session.invalidate();
					
					ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());
					req.setAttribute("RESULT", resultEntity);				
					
					return PageConst.contentsPage;
				}
			}
		}
			
		UserDAO userService = new UserDAOImpl();
		
		isExistId = userService.statusEdit(user);
//		if (isExistId == 0){
//			String msg = CodeMessageHandler.getInstance().getCodeMessage("2602");
//			StringBuffer sb = new StringBuffer()
//			.append(userId)
//			.append(msg);

//			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
//			req.setAttribute("RESULT", resultEntity);				
//			return PageConst.contentsPage;
//		}		
		return usersList;
	}
	
	public String dmtHintSet(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String question = CommonUtil.nullToStr(req.getParameter("question3"));
		String answer = CommonUtil.nullToStr(req.getParameter("user_answer2"));
		String hint_mail = CommonUtil.nullToStr(req.getParameter("hintmail"));
		if("".equals(userId)) {
			userId = (String)req.getSession().getAttribute("userId");
			if("".equals(userId) || userId == null) {
				UserEntity userEntity = new UserEntity();
				HttpSession session = req.getSession();
				userEntity = (UserEntity)session.getAttribute("USER");
				userId = userEntity.getUser_id();
			}
		}
		UserHintEntity HEntity = new UserHintEntity();
		HEntity.setUser_id(userId);
		HEntity.setPass_hint_select_code(question);
		HEntity.setPass_answer(answer);
		HEntity.setUser_email(hint_mail);
		
		UserDAO userService = new UserDAOImpl();
					
		userService.dmtHintUpDate(HEntity);
		
		String msg = CodeMessageHandler.getInstance().getCodeMessage("1203");
		StringBuffer sb = new StringBuffer()
		.append(userId)
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_SUCCESS, sb.toString());	
		req.setAttribute("RESULT", resultEntity);		
		return PageConst.contentsPage;
	}
	
	public String excel(HttpServletRequest req,
			HttpServletResponse res) throws Exception{
		
		int startIndex = 0;
		int endIndex = 0;
		//current_page = CommonUtil.strToInt(req.getParameter("current_page"), 1);
		String searchckd = CommonUtil.nullToStr(req.getParameter("searchckd"));
		String searchId = CommonUtil.nullToStr(req.getParameter("userId"));
		String status = CommonUtil.nullToStr(req.getParameter("status"));
		String search = CommonUtil.nullToStr(req.getParameter("search"));
		UserListEntity userPage = new UserListEntity();
		
		if(searchId != ""){
			userPage.setUser_id(searchId);
		}else if("st".equalsIgnoreCase(search)){
			userPage.setUser_status(status);
			userPage.setSearch(search);
		}
		
		if(search == "" || "id".equalsIgnoreCase(search)){
			req.setAttribute("RADIO", "id");
		}else if("st".equalsIgnoreCase(search)){
			req.setAttribute("RADIO", "st");
			req.setAttribute("STATUSYNB", status);
		}
		PageProcess pageProcess = new PageProcess();
		//System.out.println(userPage);
		PageEntity page = pageProcess.calculatePage2(req, res, userPage);
		
		userPage.setStartIndex(page.getStart_idx());
		userPage.setEndIndex(page.getEnd_idx());
		
		req.setAttribute("PAGE", page);
		
		UserDAO userService = new UserDAOImpl();
		java.util.List userList = null;
		userList = userService.UserList(userPage);
				
		req.setAttribute("USERS_LIST", userList);
		return excel;
	}
	
}
