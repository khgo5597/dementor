package com.mininfo.user.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.common.util.CommonUtil;
import com.mininfo.user.model.UserEntity;

public class UserRequestProcess {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	public UserEntity requestEntity(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		UserEntity userEntity = new UserEntity();
		
		userEntity.setUser_id(CommonUtil.nullToStr(req.getParameter("userId")));
		userEntity.setUser_pass(CommonUtil.nullToStr(req.getParameter("user_pass")));
		userEntity.setUser_lunar(CommonUtil.nullToStr(req.getParameter("user_lunar")));
		StringBuffer sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("year")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("month")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("day")));
		userEntity.setUser_birth(sb.toString());
		
		userEntity.setUser_email(CommonUtil.nullToStr(req.getParameter("user_email")));
		
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone2")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone3")));
		userEntity.setUser_phone(sb.toString());
		
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile2")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile3")));		
		userEntity.setUser_mobile(sb.toString());
		
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_zip1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_zip2")));
		userEntity.setUser_zip(sb.toString());
		
		userEntity.setUser_address1(CommonUtil.nullToStr(req.getParameter("user_address1")));
		userEntity.setUser_address2(CommonUtil.nullToStr(req.getParameter("user_address2")));
		userEntity.setUser_question(CommonUtil.nullToStr(req.getParameter("user_question")));
		userEntity.setUser_answer(CommonUtil.nullToStr(req.getParameter("user_answer")));
		userEntity.setUser_mailing(CommonUtil.nullToStr(req.getParameter("user_mailing")));
		
		return userEntity;
	}

	public UserEntity requestInEntity(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		UserEntity userEntity = new UserEntity();

		userEntity.setUser_id(CommonUtil.nullToStr(req.getParameter("userId")));
		userEntity.setUser_pass(CommonUtil.nullToStr(req.getParameter("user_pass")));
		userEntity.setUser_name(CommonUtil.nullToStr(req.getParameter("user_name")));
		userEntity.setUser_lunar(CommonUtil.nullToStr(req.getParameter("user_lunar")));
		
		StringBuffer sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("year")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("month")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("day")));

		logger.info("birth= "+ sb.toString());
		userEntity.setUser_birth(sb.toString());

		userEntity.setUser_email(req.getParameter("user_email"));
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone2")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_phone3")));
		userEntity.setUser_phone(sb.toString());
		
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile2")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_mobile3")));		
		userEntity.setUser_mobile(sb.toString());
		
		sb = new StringBuffer();
		sb.append(CommonUtil.nullToStr(req.getParameter("user_zip1")));
		sb.append("-");
		sb.append(CommonUtil.nullToStr(req.getParameter("user_zip2")));
		userEntity.setUser_zip(sb.toString());
		
		userEntity.setUser_address1(CommonUtil.nullToStr(req.getParameter("user_address1")));
		userEntity.setUser_address2(CommonUtil.nullToStr(req.getParameter("user_address2")));
		
		userEntity.setUser_question(CommonUtil.nullToStr(req.getParameter("user_question")));
		userEntity.setUser_answer(CommonUtil.nullToStr(req.getParameter("user_answer")));
		userEntity.setUser_mailing(CommonUtil.nullToStr(req.getParameter("user_mailing")));
		userEntity.setUser_status("Y");
		userEntity.setAdmin_yn("N");
		
		return userEntity;
	}
}
