package com.mininfo.user.dao;

import java.io.Reader;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.exception.DataAccessException;
import com.mininfo.user.model.UserAuthCodeEntity;
import com.mininfo.user.model.UserEntity;
import com.mininfo.user.model.UserHintEntity;
import com.mininfo.user.model.UserListEntity;

public class UserDAOImpl implements UserDAO {

	private final Log logger = LogFactory.getLog(getClass());

	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();

			throw new RuntimeException(
					"Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}

	/* 
	 * UserController [cmd : userOutAction] Use
	 * UserController [cmd : userStatusEdit] Use
	 * insertUserOutLog :  INSERT INTO l_user_delete_log_tbl ( USER_ID, REG_DATE, DELETE_DATE, DELETE_STATUS ) VALUES (#user_id#, (SELECT USER_REGDATE FROM frame_member WHERE USER_ID=#user_id#), now(), #delete_status#) 
	*/
	public void insertUser(UserEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			sqlMap.insert("insertUser", userEntity);
			sqlMap.commitTransaction();
			 
			logger.debug("test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			 
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * UserController [cmd : userModifyAction] Use
	 * User.xml updateUser 
	*/
	public int updateUser(UserEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			int result = sqlMap.update("updateUser", userEntity);
			sqlMap.commitTransaction();
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * UserController [cmd : userOutAction] Use
	 * UserController [cmd : userStatusEdit] Use
	 * outUser : UPDATE frame_member SET USER_STATUS = #user_status# WHERE USER_ID = #user_id# 
	*/
	public boolean removeUser(UserEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			sqlMap.update("outUser", userEntity);
			sqlMap.commitTransaction();
			return true;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * LoginController [cmd : dmt1dCheck] Use
	 * LoginController [cmd : loginAction] Use
	 * getUser :  SELECT * FROM frame_member WHERE USER_ID = #user_id#  
	*/
	public UserEntity findUser(UserEntity userEntity)
			throws DataAccessException {

		try {
			logger.info("findUser input = "+userEntity.toString());
			sqlMap.startTransaction();
			userEntity = (UserEntity) sqlMap.queryForObject("getUser", userEntity);
			sqlMap.commitTransaction();
			return userEntity;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [cmd : authCodeAction] Use
	 * PasswordController [cmd : keySearch] Use
	 * PasswordController [cmd : passwordSearchForm] Use
	 * PasswordController [cmd : sendMail] Use
	 * getHintUser :   SELECT * FROM u_user_tbl WHERE USER_ID = #user_id#   
	*/	
	public UserHintEntity UserHint(UserHintEntity userHintEntity)
		throws DataAccessException {
	
		try {
			
			logger.info("findUser input = "+userHintEntity.toString());
			sqlMap.startTransaction();
			userHintEntity = (UserHintEntity) sqlMap.queryForObject("getHintUser", userHintEntity);
			sqlMap.commitTransaction();
			return userHintEntity;		
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [method : dmtHintset] Use
	 * insertUserHint : INSERT INTO u_user_tbl (USER_ID,USER_EMAIL,USER_SMS,PASS_HINT_SELECT_CODE,PASS_ANSWER) VALUES(#user_id# , #user_email# , #user_sms# , #pass_hint_select_code# , #pass_answer#) 
	*/	
	public void insertUserHint(UserHintEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			sqlMap.insert("insertUserHint", userEntity);
			sqlMap.commitTransaction();
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [method : dmtHintupdate] Use
	 * insertUserHint : INSERT INTO u_user_tbl (USER_ID,USER_EMAIL,USER_SMS,PASS_HINT_SELECT_CODE,PASS_ANSWER) VALUES(#user_id# , #user_email# , #user_sms# , #pass_hint_select_code# , #pass_answer#) 
	 */	
	public void updateUserHint(UserHintEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			sqlMap.update("updateUserHint", userEntity);
			sqlMap.commitTransaction();			
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * did not used 
		
	public int updateUserHint(UserEntity userEntity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			int result = sqlMap.update("updateUserHint", userEntity);
			sqlMap.commitTransaction();
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	*/
	
	/* 
	 * PasswordController [cmd : keySearch] Use
	 * PasswordController [cmd : sendMail] Use
	 * insertUserAuthCode :  INSERT INTO u_user_auth_code_tbl (USER_ID, CREATE_DATE, AUTH_CODE) VALUES ( #user_id#, now(), #auth_code#) 
	*/	
	public void insertUserAuthCode(UserAuthCodeEntity authEntity)
			throws DataAccessException {

		try {
			sqlMap.startTransaction();
			sqlMap.insert("insertUserAuthCode", authEntity);
			sqlMap.commitTransaction();
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [cmd : authCodeAction] Use
	 * selectUserAuthCode : SELECT USER_ID, CREATE_DATE, AUTH_CODE FROM u_user_auth_code_tbl WHERE USER_ID = #user_id# ORDER BY CREATE_DATE DESC limit 1 
	*/	
	public UserAuthCodeEntity getUserAuthCode(UserAuthCodeEntity authEntity) {

		try {
			sqlMap.startTransaction();
			UserAuthCodeEntity userAuthCodeEntity = (UserAuthCodeEntity) sqlMap.queryForObject("selectUserAuthCode",authEntity);
			sqlMap.commitTransaction();
			return userAuthCodeEntity;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [cmd : passwordSearchForm] Use
	 * getUserKeys : SELECT KEY_HOLE, KEY_1, KEY_2, KEY_3 FROM u_user_key_tbl WHERE USER_ID=#user_id# 
	*/	
	public UserKeyEntity getUserKey(String userId) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			UserKeyEntity userKeyEntity = (UserKeyEntity)sqlMap.queryForObject("getUserKeys", userId);
			sqlMap.commitTransaction();
			return userKeyEntity;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * UserController [cmd : checkId] Use
	 * IdCheck : SELECT USER_ID FROM frame_member WHERE USER_ID = #user_id#  
	*/
	public boolean isExistId(UserEntity user) throws DataAccessException {


		try {
			sqlMap.startTransaction();
			UserEntity reternId = (UserEntity) sqlMap.queryForObject("IdCheck", user);

			sqlMap.commitTransaction();
			if (reternId != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}

	}

	/* 
	 * UserController [cmd : userList] Use
	 * User.xml getUserListID  
	 * User.xml getUserListST
	 * User.xml getUserList
	*/
	public List UserList(UserListEntity entity) throws DataAccessException {
		List alist = null;
		try {
			sqlMap.startTransaction();
			if(entity.getUser_id() != "" && entity.getUser_id() != null){
				alist = sqlMap.queryForList("getUserListID", entity);
			}else if(entity.getUser_status() != "" && entity.getUser_status() != null){
				alist = sqlMap.queryForList("getUserListST", entity);
			}else{
				alist = sqlMap.queryForList("getUserList", entity);
			}

			sqlMap.commitTransaction();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		return alist;
	}

	/* 
	 * PasswordController [cmd : hintwrite] Use
	 * UserEmail : SELECT USER_EMAIL FROM u_user_tbl WHERE USER_ID = #user_id#   
	*/
	public String UserEmail(String userId )throws DataAccessException {
		try{
			//UserEntity uentity = new UserEntity();
			//uentity.setUser_id(userId);
			sqlMap.startTransaction();
			String useremail = (String)sqlMap.queryForObject("UserEmail",userId);

			sqlMap.commitTransaction();
			return useremail;
			
		}catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * PageProcess [method : calculatePage2] Use
	 * getDateUserIDListCount : SELECT count(*) FROM u_user_tbl WHERE USER_ID = #user_id#  
	 * getDateUserSTListCount : SELECT count(*) FROM u_user_tbl WHERE USER_STATUS = #user_status# 
	 * getDateUserListCount :  SELECT count(*) FROM u_user_tbl S,u_user_key_tbl D where S.USER_ID = D.USER_ID  
	*/
	public int getUserListSearchCount(UserListEntity entity)throws DataAccessException {
		try{
			int totalCount =0;
			sqlMap.startTransaction();
			if(entity.getUser_id() != null){				
				totalCount = ((Integer)(sqlMap.queryForObject("getDateUserIDListCount", entity))).intValue();
			}else if(entity.getUser_status() != null){
				
				totalCount = ((Integer)(sqlMap.queryForObject("getDateUserSTListCount", entity))).intValue();
			}else{
				
				totalCount = ((Integer)(sqlMap.queryForObject("getDateUserListCount"))).intValue();
			}

			sqlMap.commitTransaction();
			return totalCount;
		}catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * UserController [cmd : userStatusEdit] Use
	 * StatusEdit : UPDATE frame_member SET USER_STATUS = #user_status# WHERE USER_ID = #user_id#  
	*/
	public int statusEdit(UserEntity entity) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			int result = sqlMap.update("StatusEdit", entity);
			sqlMap.commitTransaction();
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * PasswordController [cmd : dmtHintset] Use
	 * getDmtHintWrite : SELECT * FROM u_user_tbl WHERE USER_ID = #user_id#   
	 * dmtHintEdit : UPDATE u_user_tbl SET PASS_HINT_SELECT_CODE = #pass_hint_select_code#, PASS_ANSWER = #pass_answer#, USER_EMAIL = #user_email# WHERE USER_ID = #user_id#
	 * insertUserHint :  INSERT INTO u_user_tbl (USER_ID,USER_EMAIL,USER_SMS,PASS_HINT_SELECT_CODE,PASS_ANSWER) VALUES(#user_id# , #user_email# , #user_sms# , #pass_hint_select_code# , #pass_answer#)   
	*/
	public void dmtHintUpDate(UserHintEntity entity) throws DataAccessException {

		try {
			UserHintEntity hintEntity = new UserHintEntity();
			int totalCount =0;
			String userId = entity.getUser_id();
			sqlMap.startTransaction();
			hintEntity = (UserHintEntity) sqlMap.queryForObject("getDmtHintWrite", userId);			
			if(hintEntity != null){
				sqlMap.update("dmtHintEdit", entity);
			}else{
				sqlMap.insert("insertUserHint", entity);
			}
			

			sqlMap.commitTransaction();
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * UserController [cmd : userKeyDelete] Use
	 * UserKeyDelete : DELETE FROM u_user_key_tbl WHERE USER_ID = #user_id#
	 * UserKeyDelete2 : DELETE FROM u_user_tbl WHERE USER_ID = #user_id#   
	 */
	// 키 삭제시 제거
	public int userKeyDelete(String userId) throws DataAccessException {

		try {
			sqlMap.startTransaction();
			int result = sqlMap.delete("UserKeyDelete", userId);
			if(result != 0) {
				result = sqlMap.delete("UserKeyDelete2", userId);
			}
			sqlMap.commitTransaction();
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * PasswordController [cmd : sendMail] Use
	 * insertUserAuthCode : INSERT INTO u_user_auth_code_tbl (USER_ID, CREATE_DATE, AUTH_CODE) VALUES ( #user_id#, now(), #auth_code#)
	 * insertSendAuthCode : User.xml { call OP_DMT_SEND_MAIL_SMS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) } 
	*/
	public void insertSendAuthCode(UserAuthCodeEntity authEntity, Map sendParam) throws DataAccessException, SQLException {
		try {
			sqlMap.startTransaction();
			sqlMap.insert("insertUserAuthCode", authEntity);
			//sqlMap.insert("insertSendAuthCode", sendParam);
			sqlMap.commitTransaction();
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally {
			sqlMap.endTransaction();
		}
	}

	/* 
	 * PasswordController [cmd : sendMail] Use
	 * getUserSMS : SELECT USER_SMS FROM u_user_tbl WHERE USER_ID = #user_id# 
	*/
	public String getUserSMS(String userId) throws DataAccessException {
		try{
			sqlMap.startTransaction();
			String userSMS = (String)sqlMap.queryForObject("getUserSMS",userId);
			sqlMap.commitTransaction();
			return userSMS;			
		}catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * clientLogin.jsp Use
	 * User.xml getUserInfo 
	*/
	public UserEntity getUserInfo(String userId) throws DataAccessException {
		try{
			sqlMap.startTransaction();
			UserEntity userEntity = (UserEntity)sqlMap.queryForObject("getUserInfo",userId);
			sqlMap.commitTransaction();
			return userEntity;			
		}catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	//@Override//레벨값을 가져오는 클래스
	public String getLevel(String userId) throws DataAccessException {
		try {
			sqlMap.startTransaction();
			String result = (String) sqlMap.queryForObject("getLevel", userId); //리턴형이 int이므로 String로 형변환
		 sqlMap.commitTransaction();
			return result; 
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		
	}
	
	//@Override//레벨값을 업데이트하는 클래스
	public int updateLevel(UserEntity entity) throws DataAccessException {
		try {
			sqlMap.startTransaction();
			int result = sqlMap.update("updateLevel", entity); // uesr_id, user_level을 엔터티에 담아 업데이트
			sqlMap.commitTransaction();
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}


	/* 
	 * UserController [cmd : userList] Use
	 * User.xml getUserListID  
	 * User.xml getUserListST
	 * User.xml getUserList
	*/
	public List ExcelList(UserListEntity entity) throws DataAccessException {
		List alist = null;
		try {
			sqlMap.startTransaction();
			if(entity.getUser_id() != "" && entity.getUser_id() != null){
				alist = sqlMap.queryForList("getUserListID", entity);
			}else if(entity.getUser_status() != "" && entity.getUser_status() != null){
				alist = sqlMap.queryForList("getUserListST", entity);
			}else{
				alist = sqlMap.queryForList("getUserList", entity);
			}

			sqlMap.commitTransaction();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		return alist;
	}
	
}
