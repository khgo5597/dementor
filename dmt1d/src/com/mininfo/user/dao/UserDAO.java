package com.mininfo.user.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.exception.DataAccessException;
import com.mininfo.user.model.UserAuthCodeEntity;
import com.mininfo.user.model.UserEntity;
import com.mininfo.user.model.UserHintEntity;
import com.mininfo.user.model.UserListEntity;

public interface UserDAO {
	
	public void insertUser(UserEntity user) throws DataAccessException;
	public int updateUser(UserEntity user) throws DataAccessException;
	public boolean removeUser(UserEntity entity) throws DataAccessException;
	public UserEntity findUser(UserEntity entity) throws DataAccessException;
	public UserHintEntity UserHint(UserHintEntity entity) throws DataAccessException;
	public void insertUserHint(UserHintEntity entity) throws DataAccessException;
	public void updateUserHint(UserHintEntity entity) throws DataAccessException;
	public void insertUserAuthCode(UserAuthCodeEntity authEntity) throws DataAccessException;
	public void insertSendAuthCode(UserAuthCodeEntity authEntity, Map sendParam) throws DataAccessException, SQLException;
	public UserAuthCodeEntity getUserAuthCode(UserAuthCodeEntity authEntity) throws DataAccessException;
	public UserKeyEntity getUserKey(String userId) throws DataAccessException;
	public boolean isExistId(UserEntity user) throws DataAccessException;
	public List UserList(UserListEntity userPage) throws DataAccessException;
	public String UserEmail(String userId) throws DataAccessException;
	public int getUserListSearchCount(UserListEntity entity) throws DataAccessException;
	public int statusEdit(UserEntity entity) throws DataAccessException;
	public void dmtHintUpDate(UserHintEntity entity) throws DataAccessException;
	public int userKeyDelete(String userId) throws DataAccessException;
	public String getUserSMS(String userId) throws DataAccessException;
	public UserEntity getUserInfo(String userId) throws DataAccessException;
	public String getLevel(String userId) throws DataAccessException;
	public int updateLevel(UserEntity entity) throws DataAccessException;

}