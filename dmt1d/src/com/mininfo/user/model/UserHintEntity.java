package com.mininfo.user.model;

import com.mininfo.common.model.BaseObject;

public class UserHintEntity extends BaseObject implements java.io.Serializable {

	public String user_id = null;
	public String user_email = null;
	public String user_sms = null;
	public String pass_hint_select_code = null;
	public String pass_answer = null;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getPass_hint_select_code() {
		return pass_hint_select_code;
	}
	public void setPass_hint_select_code(String pass_hint_select_code) {
		this.pass_hint_select_code = pass_hint_select_code;
	}
	public String getPass_answer() {
		return pass_answer;
	}
	public void setPass_answer(String pass_answer) {
		this.pass_answer = pass_answer;
	}
	public String getUser_sms() {
		return user_sms;
	}
	public void setUser_sms(String user_sms) {
		this.user_sms = user_sms;
	}
}
