package com.mininfo.user.model;

import java.util.StringTokenizer;

import com.mininfo.common.model.BaseObject;
import com.mininfo.common.util.CommonUtil;


public class UserEntity extends BaseObject implements java.io.Serializable {
	
	
	public String user_id = null;
	public String user_pass = null;
	public String user_name = null;
	public String user_lunar = null;	
	public String user_birth = null;	
	public String user_email = null;	
	public String user_phone = null;	
	public String user_mobile = null;	
	public String user_zip = null;		
	public String user_address1 = null;	
	public String user_address2 = null;	
	public String user_question = null;	
	public String user_answer = null;	
	public String user_lastdate = null;	
	public String user_regdate = null;	
	public String admin_yn = null;	
	public String user_mailing = null;	
	public String user_status = null;
	public int key_count = 0;
	public int user_level = 0;
	

	public int getUser_level() {
		return user_level;
	}

	public void setUser_level(int user_level) {
		this.user_level = user_level;
	}

	public UserEntity(){
	}
	
    public boolean isMatchPassword(String inputPassword){
    	if ( null == user_pass ) 
    		return false;
        if ( user_pass.equals(inputPassword)){
            return true;
        } else {
            return false;
        }
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[user_id : ");
        sb.append(user_id);
        sb.append("][user_name : ");
        sb.append(user_name);
        sb.append("][user_email : ");
        sb.append(user_email);
        sb.append("][user_answer : ");
        sb.append(user_answer);
        sb.append("]")
        ;
        return sb.toString();
    }
	public String getUser_phone1(){
		
		if("".equals(CommonUtil.nullToStr(user_phone))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_phone, "-");
		
		if(st.countTokens()>=2)
			return st.nextToken();
		return "";	
	}
	
	public String getUser_phone2(){
		
		if("".equals(CommonUtil.nullToStr(user_phone))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_phone, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}

	public String getUser_phone3(){
		
		if("".equals(CommonUtil.nullToStr(user_phone))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_phone, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}
	
	public String getUser_mobile1(){
		
		if("".equals(CommonUtil.nullToStr(user_mobile))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_mobile, "-");
		
		if(st.countTokens()>=2)
			return st.nextToken();
		return "";	
	}
	
	public String getUser_mobile2(){
		
		if("".equals(CommonUtil.nullToStr(user_mobile))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_mobile, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}

	public String getUser_mobile3(){
		
		if("".equals(CommonUtil.nullToStr(user_mobile))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_mobile, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}
	
	public String getUser_birth1(){
		
		if("".equals(CommonUtil.nullToStr(user_birth))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_birth, "-");
		
		if(st.countTokens()>=2)
			return st.nextToken();
		return "";	
	}
	
	public String getUser_birth2(){
		
		if("".equals(CommonUtil.nullToStr(user_birth))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_birth, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}

	public String getUser_birth3(){
		
		if("".equals(CommonUtil.nullToStr(user_birth))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_birth, "-");
		
		if(st.countTokens()>=2){
			st.nextToken();
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}
	
	public String getUser_zip1(){
		
		if("".equals(CommonUtil.nullToStr(user_zip))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_zip, "-");
		
		if(st.countTokens()>=1)
			return st.nextToken();
		return "";	
	}
	
	public String getUser_zip2(){
		
		if("".equals(CommonUtil.nullToStr(user_zip))){
			return "";
		}
		
		StringTokenizer st = new StringTokenizer(user_zip, "-");
		
		if(st.countTokens()>=1){
			st.nextToken();
			return st.nextToken();
		}
		return "";	
	}
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_pass() {
		return user_pass;
	}
	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_lunar() {
		return user_lunar;
	}
	public void setUser_lunar(String user_lunar) {
		this.user_lunar = user_lunar;
	}
	public String getUser_birth() {
		return user_birth;
	}
	public void setUser_birth(String user_birth) {
		this.user_birth = user_birth;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_mobile() {
		return user_mobile;
	}
	public void setUser_mobile(String user_mobile) {
		this.user_mobile = user_mobile;
	}
	public String getUser_zip() {
		return user_zip;
	}
	public void setUser_zip(String user_zip) {
		this.user_zip = user_zip;
	}
	public String getUser_address1() {
		return user_address1;
	}
	public void setUser_address1(String user_address1) {
		this.user_address1 = user_address1;
	}
	public String getUser_address2() {
		return user_address2;
	}
	public void setUser_address2(String user_address2) {
		this.user_address2 = user_address2;
	}
	public String getUser_question() {
		return user_question;
	}
	public void setUser_question(String user_question) {
		this.user_question = user_question;
	}
	public String getUser_answer() {
		return user_answer;
	}
	public void setUser_answer(String user_answer) {
		this.user_answer = user_answer;
	}
	public String getUser_lastdate() {
		return user_lastdate;
	}
	public void setUser_lastdate(String user_lastdate) {
		this.user_lastdate = user_lastdate;
	}
	public String getUser_regdate() {
		return user_regdate;
	}
	public void setUser_regdate(String user_regdate) {
		this.user_regdate = user_regdate;
	}
	public String getAdmin_yn() {
		return admin_yn;
	}
	public void setAdmin_yn(String admin_yn) {
		this.admin_yn = admin_yn;
	}
	public String getUser_mailing() {
		return user_mailing;
	}
	public void setUser_mailing(String user_mailing) {
		this.user_mailing = user_mailing;
	}
	public String getUser_status() {
		return user_status;
	}
	public void setUser_status(String user_status) {
		this.user_status = user_status;
	}

	public int getKey_count() {
		return key_count;
	}

	public void setKey_count(int key_count) {
		this.key_count = key_count;
	}
}
