package com.mininfo.admin.setting.model;


public class MailEntity {

	private String mail_server; 
	private String mail_port; 
	private String mail_from; 
	private String mail_from_name; 
	private String mail_subject; 
	private String mail_auth_yn; 
	private String mail_auth_id; 
	private String mail_auth_pw;
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		sb.append("[mail_server : ").append(mail_server).append("] ")
		.append("[mail_port : ").append(mail_port).append(" ] ")
		.append("[mail_from : ").append(mail_from).append(" ] ")
		.append("[mail_from_name : ").append(mail_from_name).append(" ] ")
		.append("[mail_subject : ").append(mail_subject).append(" ] ")
		.append("[mail_auth_yn : ").append(mail_auth_yn).append(" ] ")
		.append("[mail_auth_id : ").append(mail_auth_id).append(" ] ")
		.append("[mail_auth_pw : ").append(mail_auth_pw).append(" ] ");
		
		return sb.toString();
	}
	
	public String getMail_server() {
		return mail_server;
	}
	public void setMail_server(String mail_server) {
		this.mail_server = mail_server;
	}
	public String getMail_port() {
		return mail_port;
	}
	public void setMail_port(String mail_port) {
		this.mail_port = mail_port;
	}
	public String getMail_from() {
		return mail_from;
	}
	public void setMail_from(String mail_from) {
		this.mail_from = mail_from;
	}
	public String getMail_from_name() {
		return mail_from_name;
	}
	public void setMail_from_name(String mail_from_name) {
		this.mail_from_name = mail_from_name;
	}
	public String getMail_subject() {
		return mail_subject;
	}
	public void setMail_subject(String mail_subject) {
		this.mail_subject = mail_subject;
	}
	public String getMail_auth_yn() {
		return mail_auth_yn;
	}
	public void setMail_auth_yn(String mail_auth_yn) {
		this.mail_auth_yn = mail_auth_yn;
	}
	public String getMail_auth_id() {
		return mail_auth_id;
	}
	public void setMail_auth_id(String mail_auth_id) {
		this.mail_auth_id = mail_auth_id;
	}
	public String getMail_auth_pw() {
		return mail_auth_pw;
	}
	public void setMail_auth_pw(String mail_auth_pw) {
		this.mail_auth_pw = mail_auth_pw;
	}
}
