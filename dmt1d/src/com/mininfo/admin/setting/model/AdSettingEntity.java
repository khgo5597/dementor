package com.mininfo.admin.setting.model;


public class AdSettingEntity {
	
	private String window_close_yn;
	private int banner_rolling_cycle;
	private int movie_rolling_cycle;

	public String getWindow_close_yn() {
		return window_close_yn;
	}
	public void setWindow_close_yn(String window_close_yn) {
		this.window_close_yn = window_close_yn;
	}
	public int getBanner_rolling_cycle() {
		return banner_rolling_cycle;
	}
	public void setBanner_rolling_cycle(int banner_rolling_cycle) {
		this.banner_rolling_cycle = banner_rolling_cycle;
	}
	public int getMovie_rolling_cycle() {
		return movie_rolling_cycle;
	}
	public void setMovie_rolling_cycle(int movie_rolling_cycle) {
		this.movie_rolling_cycle = movie_rolling_cycle;
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		sb.append("[window_close_yn : ").append(window_close_yn).append("] ")
		.append("[banner_rolling_cycle : ").append(banner_rolling_cycle).append(" ] ")
		.append("[movie_rolling_cycle : ").append(movie_rolling_cycle).append(" ] ");
		
		return sb.toString();
	}
}
