package com.mininfo.admin.setting.model;


public class OpSettingEntity {
	
	private String mouse_use_yn;
	private String pass_icon_set;
	private String user_board_url;
	private int auth_code_expire;
	
	public String getMouse_use_yn() {
		return mouse_use_yn;
	}
	public void setMouse_use_yn(String mouse_use_yn) {
		this.mouse_use_yn = mouse_use_yn;
	}
	public String getPass_icon_set() {
		return pass_icon_set;
	}
	public void setPass_icon_set(String pass_icon_set) {
		this.pass_icon_set = pass_icon_set;
	}
	public String getUser_board_url() {
		return user_board_url;
	}
	public void setUser_board_url(String user_board_url) {
		this.user_board_url = user_board_url;
	}
	public int getAuth_code_expire() {
		return auth_code_expire;
	}
	public void setAuth_code_expire(int auth_code_expire) {
		this.auth_code_expire = auth_code_expire;
	}
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		sb.append("[mouse_use_yn : ").append(mouse_use_yn).append("] ")
		.append("[pass_icon_set : ").append(pass_icon_set).append(" ] ")
		.append("[user_board_url : ").append(user_board_url).append(" ] ");
		
		return sb.toString();
	}
}