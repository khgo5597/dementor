package com.mininfo.admin.setting.dao;

import com.mininfo.admin.setting.model.AdSettingEntity;
import com.mininfo.admin.setting.model.MailEntity;
import com.mininfo.admin.setting.model.OpSettingEntity;
 
public interface SettingDao {
	
	public void modifyAdSetting(AdSettingEntity entity) throws Exception;
	public void modifyOpSetting(OpSettingEntity entity) throws Exception;
	public void opUpdateSetting(OpSettingEntity entity) throws Exception;
	
	
	public AdSettingEntity getAdSetting() throws Exception;
	public OpSettingEntity getOpSetting() throws Exception;
	public MailEntity getMailEntity() throws Exception;
	public int getAuthCodeExpireMinte() throws Exception;
}
