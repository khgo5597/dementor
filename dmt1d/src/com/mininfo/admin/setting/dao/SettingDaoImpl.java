package com.mininfo.admin.setting.dao;

import java.io.Reader;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.admin.setting.dao.SettingDao;
import com.mininfo.admin.setting.model.AdSettingEntity;
import com.mininfo.admin.setting.model.MailEntity;
import com.mininfo.admin.setting.model.OpSettingEntity;
import com.mininfo.exception.DataAccessException;
import com.mininfo.user.model.UserEntity;

public class SettingDaoImpl implements SettingDao {

	protected final Log logger = LogFactory.getLog(getClass());

	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();

			throw new RuntimeException(
					"Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}

	/* 
	 * PasswordController [cmd : keySearch] Use
	 * PasswordController [cmd : sendMail] Use
	 * getMailSetting : SELECT  MAIL_SERVER, MAIL_PORT, MAIL_FROM, MAIL_FROM_NAME, MAIL_SUBJECT, MAIL_AUTH_YN, MAIL_AUTH_ID, MAIL_AUTH_PW FROM s_setting_tbl
	*/
	public MailEntity getMailEntity() throws Exception {
		
		try {
			sqlMap.startTransaction();
			MailEntity mail = (MailEntity)sqlMap.queryForObject("getMailSetting");
			sqlMap.commitTransaction();
			
			return mail;

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * did not used
	*/
	public AdSettingEntity getAdSetting(AdSettingEntity entity)
			throws Exception {

		try {
			sqlMap.startTransaction();
			AdSettingEntity resultEntity = (AdSettingEntity) (sqlMap
					.queryForObject("getSetting", entity));
			sqlMap.commitTransaction();
			
			return resultEntity;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}

	}
	
	/* 
	 * SettingController [cmd : AdModify] Use
	 * getMailSetting :  UPDATE s_setting_tbl SET WINDOW_CLOSE_YN = #window_close_yn#,BANNER_ROLLING_CYCLE = #banner_rolling_cycle#,MOVIE_ROLLING_CYCLE = #movie_rolling_cycle#
	*/	
	public void modifyAdSetting(AdSettingEntity entity) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("modifyBoard() ");
		}
		try {
			sqlMap.startTransaction();
			sqlMap.update("AdSettingEntity", entity);

			if (logger.isDebugEnabled()) {
				logger.info("modifyBoard()");
			}
			
			sqlMap.commitTransaction();
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * did not used
	*/
	public OpSettingEntity getOpSetting(OpSettingEntity entity)
			throws Exception {

		try {
			sqlMap.startTransaction();
			OpSettingEntity resultEntity = (OpSettingEntity) (sqlMap
					.queryForObject("getSetting", entity));
			
			sqlMap.commitTransaction();
			return resultEntity;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}

	}

	/* 
	 * did not used
	*/
	public void modifOpSetting(OpSettingEntity entity) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("modifyBoard() ");
		}
		try {
			sqlMap.startTransaction();
			sqlMap.update("OpSettingEntity", entity);

			if (logger.isDebugEnabled()) {
				logger.info("modifyBoard()");
			}
			
			sqlMap.commitTransaction();
			return;

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * PasswordController [cmd : authCodeAction] Use
	 * getAuthCodeExpireMinte :   SELECT  AUTH_CODE_EXPIRE_MINUTE FROM s_setting_tbl 
	*/		
	public int getAuthCodeExpireMinte() throws Exception {
		try {
			sqlMap.startTransaction();
			int minute = ((Integer)(sqlMap.queryForObject("getAuthCodeExpireMinte"))).intValue();			
			sqlMap.commitTransaction();
			return minute;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}	

	public AdSettingEntity getAdSetting() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public OpSettingEntity getOpSetting() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void modifyOpSetting(OpSettingEntity entity) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void opUpdateSetting(OpSettingEntity entity) throws Exception {
		// TODO Auto-generated method stub
		
	}
}