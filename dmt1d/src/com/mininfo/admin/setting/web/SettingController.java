package com.mininfo.admin.setting.web;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.setting.dao.SettingDao;
import com.mininfo.admin.setting.dao.SettingDaoImpl;
import com.mininfo.admin.setting.model.AdSettingEntity;
import com.mininfo.admin.setting.model.OpSettingEntity;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.util.CommonUtil;


public class SettingController extends HttpServlet {
	
	protected static final Log logger = LogFactory.getLog(SettingController.class);
	
	private String updatead = "/jsp/setting/AdvertisingUpdate.jsp";
	private String updateop = "/jsp/setting/OperatingUpdate.jsp";
	

	
	Map fieldMap = null;
	Map fileMap = null;
	
	ServletFileUpload upload = null;
	DiskFileItemFactory factory = null;
	List items = null; //FileItem 
	Iterator iter = null;
	FileItem item = null;
	File tempDir = null;
	
	boolean isMultipart = false;
	

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String cmd = "";

		isMultipart = ServletFileUpload.isMultipartContent(req);
		
		if (isMultipart) { 
			fieldMap = new LinkedHashMap(); 
			factory = new DiskFileItemFactory();
			upload = new ServletFileUpload(factory);
			upload.setHeaderEncoding("KSC5601");
			  
			Vector tempVec = null;
			try {
				items = upload.parseRequest(req);
				
			} catch (FileUploadException e1) {
				e1.printStackTrace();
			}
			
			iter = items.iterator();
			while (iter.hasNext()) {
				
			  item = (FileItem) iter.next();
			  if (item.isFormField()) {
			    try {
					tempVec = processFormField(item);
					if("cmd".equalsIgnoreCase(item.getFieldName()))
						cmd = item.getString();

				} catch (Exception e) {
					e.printStackTrace();
				}
			    fieldMap.put(tempVec.get(0),tempVec.get(1));
			  } 
			}
		} else {
			cmd = req.getParameter("cmd");
		}
		
		String forwardJsp = null;
		
		try {
			
			if (null == cmd || ("AdModifyForm".equals(cmd))){
				forwardJsp = modifyAdForm(req,resp);
			} else if ("AdModify".equals(cmd)){
				forwardJsp = modifyAd(req,resp);
			} else if ("OpModifyForm".equals(cmd)){
				forwardJsp = modifyOpForm(req,resp);
			} else if ("OpModify".equals(cmd)){
				forwardJsp = modifyOp(req,resp);
			} else {
				StringBuffer sb = new StringBuffer()
				.append(cmd)
				.append(" 는 정의되지 않은 서비스 입니다.")
				.append(" <br>")
				.append(" 확인해 주시기 바랍니다.");
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);					
				forwardJsp = PageConst.contentsPage;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		RequestDispatcher disp = req.getRequestDispatcher(forwardJsp);
		disp.forward(req, resp);
	}
	
	public String modifyOp(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("entering 'modify' method...");
		}
	
	isMultipart = ServletFileUpload.isMultipartContent(request);
	SettingRequestProcess requestProcess = new SettingRequestProcess();	
	
	if (isMultipart) { 
		fieldMap = new LinkedHashMap();//field 
		fileMap = new LinkedHashMap();//file 
		factory = new DiskFileItemFactory();
		upload = new ServletFileUpload(factory);
		Vector tempVec = null;
		
		iter = items.iterator();
		while (iter.hasNext()) {
		  item = (FileItem) iter.next();
		  if (item.isFormField()) {
		    tempVec = processFormField(item);
		    fieldMap.put(tempVec.get(0),tempVec.get(1));
		  }
		}
	}
	
	OpSettingEntity entity = requestProcess.mapOpEntity(fieldMap);
	
	SettingDao dao = new SettingDaoImpl();	
	dao.modifyOpSetting(entity);

	return updateop;
}
	
	

	
	public String modifyAd(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("entering 'modify' method...");
		}
		
		isMultipart = ServletFileUpload.isMultipartContent(request);

			SettingRequestProcess requestProcess = new SettingRequestProcess();	
			
		if (isMultipart) { 
			fieldMap = new LinkedHashMap();//field 
			fileMap = new LinkedHashMap();//file 
			factory = new DiskFileItemFactory();
			upload = new ServletFileUpload(factory);
			  
			Vector tempVec = null;
			
			iter = items.iterator();
			while (iter.hasNext()) {
			  item = (FileItem) iter.next();
			  if (item.isFormField()) {
			    tempVec = processFormField(item);
			    fieldMap.put(tempVec.get(0),tempVec.get(1));
			  }
			}
		}

		
		AdSettingEntity entity = requestProcess.mapAdEntity(fieldMap);
		
		SettingDao dao = new SettingDaoImpl();	
		dao.modifyAdSetting(entity);

		return updatead;
	}
	
	private String list(HttpServletRequest request, HttpServletResponse response) {
		return null;
	}

	private Vector processUploadedFile(FileItem item2) {
		return null;
	}

	public String modifyAdForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("entering 'modifyForm' method...");
		}
	
		SettingDao dao = new SettingDaoImpl();
		AdSettingEntity entity = dao.getAdSetting();
		request.setAttribute("SETTING_ENTITY", entity);
		return updatead;
		

	}
	
	public String modifyOpForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.info("entering 'modifyForm' method...");
		}
	
		SettingDao dao = new SettingDaoImpl();
		OpSettingEntity entity = dao.getOpSetting();
		request.setAttribute("SETTING_ENTITY", entity);
		return updateop;
		

	}
	
	private Vector processFormField(FileItem item) throws Exception{
		  String name = item.getFieldName();
		  String value = item.getString();
		  
		  value = CommonUtil.nullToStr(value);
		  
		  Vector vec = new Vector();
		  vec.addElement(new String(name.toLowerCase().getBytes("ISO-8859-1"),"KSC5601"));
		  vec.addElement(new String(value.getBytes("ISO-8859-1"),"KSC5601"));		  
		  //vec.addElement(new String(name.toLowerCase().getBytes("ISO-8859-1"),"UTF-8"));
		  //vec.addElement(new String(value.getBytes("ISO-8859-1"),"UTF-8"));  
		  
		  
		  return vec;
		}
	
	
}