package com.mininfo.admin.setting.web;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.admin.setting.model.AdSettingEntity;
import com.mininfo.admin.setting.model.OpSettingEntity;

public class SettingRequestProcess {
	
	
	public AdSettingEntity mapAdEntity(Map map) throws Exception {
		
		AdSettingEntity entity = new AdSettingEntity();
		entity.setWindow_close_yn(CommonUtil.nullToStr((String)map.get("window_close_yn")));
		entity.setBanner_rolling_cycle(CommonUtil.strToInt((String)map.get("banner_rolling_cycle")));
		entity.setMovie_rolling_cycle(CommonUtil.strToInt((String)map.get("movie_rolling_cycle")));
		return entity;
		
	}

	public OpSettingEntity mapOpEntity(Map map) throws Exception {
		
		OpSettingEntity entity = new OpSettingEntity();
		entity.setMouse_use_yn(CommonUtil.nullToStr((String)map.get("mouse_use_yn")));
		entity.setPass_icon_set(CommonUtil.nullToStr((String)map.get("pass_icon_set")));
		entity.setUser_board_url(CommonUtil.nullToStr((String)map.get("user_board_url")));
		return entity;
		
	}
}



