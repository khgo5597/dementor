package com.mininfo.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.common.PageConst;
public class FrameController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected static final Log logger = LogFactory.getLog(FrameController.class);
		
		private String adminIndex = "/jsp/admin/contents.jsp";
		private String topmenu = "/jsp/admin/topmenu.jsp";
		private String leftmenu = "/jsp/admin/leftmenu.jsp";	
		
		protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
			String cmd = req.getParameter("cmd");
	
			String forwardURL = null;
			try {
				if (null == cmd || "contents".equalsIgnoreCase(cmd)){
					forwardURL = adminIndex(req,res);
				} else if ("topmenu".equalsIgnoreCase(cmd)){
					forwardURL = topmenu(req,res);
				} else if ("leftmenu".equalsIgnoreCase(cmd)){
					forwardURL = leftmenu(req,res);
				} 
	
				RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
				disp.forward(req, res);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		public String adminIndex(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
						
			if(request.getSession(false).getAttribute("USER") == null){
				return PageConst.loginFormPage;
			} else {
				return adminIndex;
			}
		}
		
		public String topmenu(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			
			String status = request.getParameter("status");
			return topmenu;
		}
		
		public String leftmenu(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			
			String status = request.getParameter("status");
			return leftmenu;
		}
}
