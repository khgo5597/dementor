package com.mininfo.admin.category.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mininfo.admin.category.model.CategoryEntity;
import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.exception.DataAccessException;

public interface CateDAO {
	public List cateList()throws DataAccessException;
	public List cateList2()throws DataAccessException;
	public List selectCate()throws DataAccessException;
	public List cateIconList()throws DataAccessException;
	public List cateIconList2(CategoryEntity icon)throws DataAccessException;
	public List getUserKeys(String userId)throws DataAccessException;
	//public void insertCate(CategoryEntity entity, Map fileMap) throws DataAccessException;
	public void cateSelectSet(CategoryEntity entity) throws DataAccessException;
	public boolean dbEncoding(int num) throws DataAccessException;
	public void insertCategoryIconList() throws Exception;
}