package com.mininfo.admin.category.dao;

import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.admin.category.DBencryption;
import com.mininfo.admin.category.model.CategoryEntity;
import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.client.model.UserKeyEntity;
import com.mininfo.common.property.DementorProperty;
import com.mininfo.exception.DataAccessException;

public class MysqlCateDAO implements CateDAO{
	protected final Log logger = LogFactory.getLog(getClass());
	private boolean iconSet = false;
	private static final SqlMapClient sqlMap;
	private int iconKeyNumber = 0;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader (resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();
			
			throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}
	
	
	/*
	public void insertCate(CategoryEntity entity, Map fileMap) throws DataAccessException
	{
		if( logger.isDebugEnabled() )
		{
			logger.info("insert()");
			logger.info("insertCate()");
		}

		try{
			sqlMap.startTransaction();
			sqlMap.insert("insertDataBoard", entity);
			
			Set fileMapSet = fileMap.keySet();
			Iterator fileMapKeys = fileMapSet.iterator();
			String fileName = null;
			Vector fileValue = null;
						
			while (fileMapKeys.hasNext()) {
				fileName = (String)fileMapKeys.next();
				fileValue = (Vector)fileMap.get(fileName);
				insertFileInfo(entity,  fileValue);
			}
			
			sqlMap.commitTransaction();
			return;
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		}finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	private void insertFileInfo(CategoryEntity entity, Vector fileValue) throws Exception {
		FileItem fi = null;
		return;
	}
	
	*/
	
	/* 
	 * CategoryController [cmd : cateList] Use
	 * Category.xml
	 * SELECT CATEGORY_URL,CATEGORY_ID, CATEGORY_NAME FROM s_category_tbl
	*/
	public List cateList() throws DataAccessException{
		List alist = null;
		try {
			sqlMap.startTransaction();
			alist = sqlMap.queryForList("getCate");
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return alist;
	}
	
	/* 
	 * CategoryController [cmd : cateList] Use
	 * Category.xml
	 * SELECT CATEGORY_URL,CATEGORY_ID, CATEGORY_NAME FROM s_category_tbl WHERE CATEGORY_STATUS = '0'
	*/
	public List cateList2() throws DataAccessException{
		List alist = null;
		try {
			sqlMap.startTransaction();
			alist = sqlMap.queryForList("getCateList2");
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return alist;
	}
	
	/* 
	 * CategoryController [cmd : cateList] Use
	 * CategoryController [cmd : keyedit] Use
	 * Category.xml
	 * SELECT CATEGORY_URL,CATEGORY_ID, CATEGORY_NAME FROM s_category_tbl WHERE CATEGORY_STATUS = '1' 
	*/
	public List selectCate() throws DataAccessException{
		List alist = null;
		try {
			sqlMap.startTransaction();
			alist = sqlMap.queryForList("getCateSelectList");
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return alist;
	}
	/* 
	 * CategoryController [cmd : icon] Use
	 * Category.xml
	 * SELECT ICON_ID, ICON_URL FROM s_category_icon_tbl  
	*/
	public List cateIconList() throws DataAccessException{
		List blist = null;
		try {
			sqlMap.startTransaction();
			blist = sqlMap.queryForList("getCateIcon1");
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return blist;
	}
	/* 
	 * CategoryController [cmd : iconlist] Use
	 * Category.xml
	 *  SELECT ICON_ID, ICON_URL FROM s_category_icon_tbl WHERE CATEGORY_ID = #category_id#
	*/
	
	public List cateIconList2(CategoryEntity icon) throws DataAccessException{
		List blist2 = null;
		try {
			sqlMap.startTransaction();
			blist2 = sqlMap.queryForList("getCateIcon2", icon);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return blist2;
	}
	/*
	public List getuserkey(String user_id) throws DataAccessException{
		
		UserKeyEntity entity = new UserKeyEntity();
		entity.setUser_id(user_id);
		List alist = null;
		
		try {
			sqlMap.startTransaction();
			alist = sqlMap.queryForList("getuserkey2",entity);
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return alist;
	}
	*/
	
	/* 
	 * CategoryController [cmd : keyedit] Use
	 * CategoryController [cmd : keySerch] Use
	 * Client.xml getMemberKey2
	 * Client.xml
	 *  SELECT ICON_ID,ICON_URL FROM s_category_icon_tbl WHERE ICON_ID = #value# 
	*/
	public List getUserKeys(String userId)  throws DataAccessException{
		if (iconKeyNumber == 0) {
			try {
				DementorProperty DP = null;
				iconKeyNumber = Integer.parseInt(DP.getInstance().getIconKeyNumber());
			}catch(Exception e) {
				System.out.println(e.toString());
			} 
		}
		
		UserKeyEntity entity = new UserKeyEntity();
		entity.setUser_id(userId);
		
		List alist = new ArrayList();
		
		try {
			sqlMap.startTransaction();
			entity = (UserKeyEntity)sqlMap.queryForObject("getMemberKey2",entity);
			
			if(entity == null) return null;
			
			int hole_key = entity.getKey_hole();
			int key_1 = entity.getKey_1();
			int key_2 = entity.getKey_2();
			
			alist.add((IconEntity)sqlMap.queryForObject("getMemberKeyIcon",new Integer(hole_key)));
			alist.add((IconEntity)sqlMap.queryForObject("getMemberKeyIcon",new Integer(key_1)));
			alist.add((IconEntity)sqlMap.queryForObject("getMemberKeyIcon",new Integer(key_2)));
			if (iconKeyNumber == 4) {
				int key_3 = entity.getKey_3();
				alist.add((IconEntity)sqlMap.queryForObject("getMemberKeyIcon",new Integer(key_3)));
			}
			
			sqlMap.commitTransaction();
			
			return alist;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
			
		return alist;
	}
	
	/* 
	 * CategoryController [cmd : selectset] Use
	 * Category.xml 
	 *  UPDATE s_category_tbl SET CATEGORY_STATUS=#category_status# WHERE CATEGORY_ID = $cateselect[]$ 
	*/
	public void cateSelectSet(CategoryEntity entity) throws DataAccessException {
		
		try {
			sqlMap.startTransaction();
			sqlMap.update("updateCateSelect", entity);
			sqlMap.commitTransaction();
			
			return;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * CategoryController [cmd : dbEncoding] Use
	 * Category.xml 
	 * getdbIcon : SELECT ICON_ID, ICON_NAME, ICON_URL, ICON_FILENAME FROM s_category_icon_tbl WHERE ICON_ID = #icon_id#
	 * updateDBicon : UPDATE s_category_icon_tbl SET ICON_NAME=#icon_name#, ICON_URL=#icon_url#, ICON_FILENAME=#icon_filename#	WHERE ICON_ID = #icon_id# 
	 *  
	*/	
	public boolean dbEncoding(int num) throws DataAccessException{
		boolean test = false;
		try{
			IconEntity entity = new IconEntity();
			entity.setIcon_id(num);
		
			sqlMap.startTransaction();
			entity = (IconEntity)sqlMap.queryForObject("getdbIcon", entity);
			
			int IconId = entity.getIcon_id();
			String IconName = entity.getIcon_name();
			String IconUrl = entity.getIcon_url();
			String IconFileName = entity.getIcon_filename();
						
			DBencryption encr = new DBencryption();
			
			IconName = encr.db_encoding(IconName);
			IconUrl = encr.db_encoding(IconUrl);
			IconFileName = encr.db_encoding(IconFileName);
				
			/*
			IconName = encr.db_decoding(IconName);
			IconUrl = encr.db_decoding(IconUrl);
			IconFileName = encr.db_decoding(IconFileName);
			
			System.out.println("IconName = "+IconName);
			System.out.println("IconUrl = "+IconUrl);
			System.out.println("IconFileName = "+IconFileName);
			*/
			entity.setIcon_name(IconName);
			entity.setIcon_url(IconUrl);			
			entity.setIcon_filename(IconFileName);
						
			sqlMap.update("updateDBicon", entity);
		
		
			test = true;
			
			
			sqlMap.commitTransaction();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
		
		return test;

	}
	
	/* 
	 * CodeMessageHandler [method : getCodeMessage] Use
	 * Category.xml 
	 * getCategoryIconCount : SELECT  count(*) FROM s_category_icon_tbl 
	 * getCate : SELECT CATEGORY_URL,CATEGORY_ID, CATEGORY_NAME FROM s_category_tbl
	 * Category.xml insertCategoryIcon 
	 *  
	*/
	public void insertCategoryIconList() throws DataAccessException {
		if( logger.isDebugEnabled() ) {
			logger.info("insert insertCategory()");
		}
		try {
			
			if(iconSet) return;
			sqlMap.startTransaction();
			int categoryIconCnt = ((Integer)sqlMap.queryForObject("getCategoryIconCount"));
			
			if(categoryIconCnt<1){
				
				int icon_id = 1;
				
				List alist = sqlMap.queryForList("getCate");
				
				for(int i=0;i<alist.size();i++){
					
					CategoryEntity ce = (CategoryEntity)alist.get(i);
					int category_id = ce.getCategory_id();
					String category_name = ce.getCategory_name();
					
					for(int j=1;j<26;j++){
						
						String icon_name = DBencryption.db_encoding(category_name+"_"+j);
						String icon_filename = DBencryption.db_encoding(category_name+"_"+j+".png");
						String icon_url = DBencryption.db_encoding("/images/icon_img/"+category_name+"/"+category_name+"_"+j+".png");
						
						IconEntity ie = new IconEntity();
						ie.setIcon_id(icon_id);
						ie.setCategory_id(category_id);
						ie.setIcon_name(icon_name);
						ie.setIcon_url(icon_url);
						ie.setIcon_filename(icon_filename);
						ie.setIcon_status("0");
						sqlMap.insert("insertCategoryIcon", ie);
						
						icon_id++;
					}
				}
				iconSet = true;
			}
			sqlMap.commitTransaction();
			
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}		
	
}

