package com.mininfo.admin.category.model;

import com.mininfo.common.model.BaseObject;

public class CategoryEntity extends BaseObject{

	private int category_id;
	private int count;
	private	String category_name;
	private String category_url;
	private String category_filename;
	private String category_status;
	private String reg_date;
	private int[] cateselect;
	
	public CategoryEntity(){}
	
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getCategory_url() {
		return category_url;
	}
	public void setCategory_url(String category_url) {
		this.category_url = category_url;
	}
	public String getCategory_filename() {
		return category_filename;
	}
	public void setCategory_filename(String category_filename) {
		this.category_filename = category_filename;
	}
	public String getCategory_status() {
		return category_status;
	}
	public void setCategory_status(String category_status) {
		this.category_status = category_status;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

	public int[] getCateselect() {
		return cateselect;
	}

	public void setCateselect(int[] cateselect) {
		this.cateselect = cateselect;
	}
	
	public void delCateselect(int[] cateselect) {
		this.cateselect = cateselect;
	}
}
