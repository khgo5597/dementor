package com.mininfo.admin.category.model;

import com.mininfo.common.model.BaseObject;

public class IconEntity extends BaseObject{
	public IconEntity(){}
	
	private int icon_id;
	private int icon_count;
	private int key_hole;
	private int key_1;
	private int key_2;
	private int key_3;
	private int category_id;
	private int level;
	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getKey_hole() {
		return key_hole;
	}

	public void setKey_hole(int key_hole) {
		this.key_hole = key_hole;
	}

	public int getKey_1() {
		return key_1;
	}

	public void setKey_1(int key_1) {
		this.key_1 = key_1;
	}

	public int getKey_2() {
		return key_2;
	}

	public void setKey_2(int key_2) {
		this.key_2 = key_2;
	}

	public int getKey_3() {
		return key_3;
	}

	public void setKey_3(int key_3) {
		this.key_3 = key_3;
	}

	private String icon_name;
	private String icon_url;
	private String icon_filename;
	public int getIcon_count() {
		return icon_count;
	}

	public void setIcon_count(int icon_count) {
		this.icon_count = icon_count;
	}

	private String icon_status;
	private String reg_date;
	
	
	public IconEntity(int icon_id, String icon_url){
		this.icon_id = icon_id;
		this.icon_url = icon_url;
	}
	
	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

	public int getIcon_id() {
		return icon_id;
	}

	public void setIcon_id(int icon_id) {
		this.icon_id = icon_id;
	}

	public String getIcon_name() {
		return icon_name;
	}

	public void setIcon_name(String icon_name) {
		this.icon_name = icon_name;
	}

	public String getIcon_url() {
		return icon_url;
	}

	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}

	public String getIcon_filename() {
		return icon_filename;
	}

	public void setIcon_filename(String icon_filename) {
		this.icon_filename = icon_filename;
	}

	public String getIcon_status() {
		return icon_status;
	}

	public void setIcon_status(String icon_status) {
		this.icon_status = icon_status;
	}
}
