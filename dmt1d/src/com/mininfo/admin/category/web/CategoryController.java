package com.mininfo.admin.category.web;

import java.io.IOException;
import java.io.File;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.category.DBencryption;
import com.mininfo.admin.category.dao.CateDAO;
import com.mininfo.admin.category.dao.MysqlCateDAO;
import com.mininfo.admin.category.model.CategoryEntity;
import com.mininfo.admin.category.model.IconEntity;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.util.CommonUtil;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

/**
 * @param args
 */
public class CategoryController extends HttpServlet{
	
	protected static final Log logger = LogFactory.getLog(CategoryController.class);
	
	private String categoryWrite = "/jsp/category/categoryWrite.jsp";
	private String categoryList = "/jsp/category/categoryList.jsp";
	private String iconList = "/jsp/category/iconList.jsp";
	private String keysetting = "/jsp/display/keysetting.jsp";
	private String keyedit = "/jsp/display/keyedit.jsp";
	private String keySerch = "/jsp/display/keySerch_renew.jsp";
	private String clientIcon = "/jsp/display/iconList.jsp";
	private String stat = "/jsp/display/icon.jsp";
	private String stat2 = "/jsp/category/icon2.jsp";
	private String hintpage = "/jsp/display/hint.jsp";
	private String cateselect = "/jsp/category/cateselect.jsp";
	private String cateAdd = "/jsp/category/cateAdd.jsp";
	private String cateselect2 ="/cate/Cate.do?cmd=list&status=cateselect";
	private String message = "/jsp/admin/message.jsp";
	protected void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		String cmd = req.getParameter("cmd");
		String forwardURL = null;
		try {
			if (null == cmd || "writeForm".equalsIgnoreCase(cmd)){
				forwardURL = cateWrite(req,res);
			} else if ("list".equalsIgnoreCase(cmd)){
				forwardURL = cateList(req,res);
			} else if ("iconlist".equalsIgnoreCase(cmd)){
				forwardURL = cateIconList(req,res);
			} else if ("icon".equalsIgnoreCase(cmd)){
				forwardURL = icon(req,res);
			} else if ("keyedit".equalsIgnoreCase(cmd)){
				forwardURL = keyedit(req,res);
			} else if ("keySerch".equalsIgnoreCase(cmd)){
				forwardURL = keySerch(req,res);
			} else if ("selectset".equalsIgnoreCase(cmd)){
				forwardURL = selectset(req,res);
			} else if ("selectDel".equalsIgnoreCase(cmd)){
				forwardURL = selectDel(req,res);
			} else if ("dbEncoding".equalsIgnoreCase(cmd)){
				forwardURL = dbEncoding(req,res);
			} else if ("cateAdd".equalsIgnoreCase(cmd)){
				forwardURL = cateAdd(req,res);
			}

			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public String cateWrite(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return categoryWrite;
	}

	public String cateList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();
		
		String status = request.getParameter("status");
		// status에 파라미터가 있는 경우
		if(status.indexOf('?') != -1) {
			status = status.substring(0,status.indexOf('?'));
		}
		java.util.List url = null;
				
		if("admin".equalsIgnoreCase(status)){

			url = cateService.cateList();
			request.setAttribute("CATE_LIST", url);
			
			return categoryList;
		}else if("client".equalsIgnoreCase(status)){

			url = cateService.selectCate();
			request.setAttribute("CATE_LIST", url);
			
			return keysetting;
		}else if("cateselect".equalsIgnoreCase(status)){
			java.util.List url2 = null;
			url = cateService.cateList2();
			url2 = cateService.selectCate();
			request.setAttribute("CATE_LIST", url);
			request.setAttribute("SELECT_CATE", url2);
			return cateselect;
		}
		String msg = CodeMessageHandler.getInstance().getCodeMessage("3000");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
		return PageConst.contentsPage;
	}
	
	public String keyedit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		CateDAO cateService = new MysqlCateDAO();
		String userId    = request.getParameter("userId");
		HttpSession session = request.getSession();
		String processCmd   = request.getParameter("processCmd");
				
		session.setAttribute("userId", userId);
				
logger.debug("## session   ----> "+ session.getAttribute("userId"));
logger.debug("## userId    ----> "+ userId);
		
		if (userId != null && session.getAttribute("userId") == null) {
			String msg = CodeMessageHandler.getInstance().getCodeMessage("5024");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
			resultEntity.setProcessCode(Const.CLOES);
			request.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
		}
		userId = (String)session.getAttribute("userId");
		
		ArrayList keys = new ArrayList();
		DBencryption encr = new DBencryption();
		IconEntity Ientity = null;
		String key ="";
		java.util.List url = cateService.selectCate();
		java.util.List setkey = cateService.getUserKeys(userId);
//logger.debug("## setkey  ----> "+ setkey);		
		if(setkey != null){
			request.setAttribute("CATE_LIST", url);
			
			for(int i=0; i<setkey.size(); i++){
				Ientity = (IconEntity)setkey.get(i);
				key = encr.db_decoding(Ientity.getIcon_url());
				Ientity.setIcon_url(key);
				keys.add(Ientity);
			}
			
			request.setAttribute("KEY_VALUE", keys);
			request.setAttribute("processCmd", processCmd);
			
			return keyedit;
		}
		String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
		return PageConst.contentsPage;

	}
	
/*	
	public String keyedit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();
		
		String userId = request.getParameter("userId");
		HttpSession session = request.getSession();		
		if (userId != null && session.getAttribute("userId") == null) {
			String msg = CodeMessageHandler.getInstance().getCodeMessage("5024");
			StringBuffer sb = new StringBuffer()
			.append(msg);

			ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
			resultEntity.setProcessCode(Const.CLOES);
			request.setAttribute("RESULT", resultEntity);			
			return PageConst.contentsPage;
		}
		userId = (String)session.getAttribute("userId");
		
		ArrayList keys = new ArrayList();
		DBencryption encr = new DBencryption();
		IconEntity Ientity = null;
		String key ="";
		java.util.List url = cateService.selectCate();
		java.util.List setkey = cateService.getUserKeys(userId);
		
		if(setkey != null){
			request.setAttribute("CATE_LIST", url);
			
			for(int i=0; i<setkey.size(); i++){
				Ientity = (IconEntity)setkey.get(i);
				key = encr.db_decoding(Ientity.getIcon_url());
				Ientity.setIcon_url(key);
				keys.add(Ientity);
			}
			
			request.setAttribute("KEY_VALUE", keys);
			
			return keyedit;
		}
		String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
		return PageConst.contentsPage;

	}
*/
	
	public String keySerch(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();
		
		String userId = request.getParameter("userId");
		//ArrayList<String> keys = new ArrayList<String>();
		ArrayList<IconEntity> keys = new ArrayList<IconEntity>();
		//java.util.List url = cateService.selectCate();
		DBencryption encr = new DBencryption();
		IconEntity Ientity = null;
		
		
		java.util.List setkey = cateService.getUserKeys(userId);
		String key ="";
		
		if(setkey != null){
			for(int i=0; i<setkey.size(); i++){
				Ientity = (IconEntity)setkey.get(i);
				
				key = encr.db_decoding(Ientity.getIcon_url());
				Ientity.setIcon_url(key);
				keys.add(Ientity);
				//keys.add(key);
			}
			//request.setAttribute("CATE_LIST", url);
			request.setAttribute("KEY_VALUE", keys);
			return keySerch;
		}
		String msg = CodeMessageHandler.getInstance().getCodeMessage("2500");
		StringBuffer sb = new StringBuffer()
		.append(msg);

		ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());
		resultEntity.setProcessCode(Const.CLOES);
		request.setAttribute("RESULT", resultEntity);			
		return PageConst.contentsPage;

	}

	public String icon(HttpServletRequest request,
			HttpServletResponse Response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();

		java.util.List iconUrl = null;
		
		IconEntity Ientity = null;
		iconUrl = cateService.cateIconList();
	
		request.setAttribute("CATE_ICON_LIST", iconUrl);
		
		return iconList;
	}
	
	public String cateIconList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		DBencryption encr = new DBencryption();
		CateDAO cateService = new MysqlCateDAO();
		java.util.List iconUrl = null;
		
		ArrayList<String> iconUrl2 = new ArrayList<String>();
		ArrayList<String> iconUrl3 = new ArrayList<String>();
				
		IconEntity Ientity = null;
		String iconU ="";
		String iconId ="";
				
		CategoryEntity icon = new CategoryEntity();
		
		String aa=request.getParameter("cate");
		if(aa != null){
			icon.setCategory_id(Integer.parseInt(request.getParameter("cate")));
			String status = request.getParameter("status");
			
			iconUrl = cateService.cateIconList2(icon);
			for(int i=0; i<iconUrl.size(); i++){
				Ientity = (IconEntity)iconUrl.get(i);
				iconU = encr.db_decoding(Ientity.getIcon_url());
				iconUrl2.add(iconU);
				
				Ientity = (IconEntity)iconUrl.get(i);
				iconId = Integer.toString(Ientity.getIcon_id());
				iconUrl3.add(iconId);
				
			}
			
			request.setAttribute("CATE_ICON_LIST", iconUrl2);
			request.setAttribute("CATE_ICON_LIST_ID", iconUrl3);
				
			if("admin".equalsIgnoreCase(status)){
				return iconList;
			}else if("client".equalsIgnoreCase(status)){
				return clientIcon;
			}
		}else if(aa == null){
			String status = request.getParameter("status");
			if("admin".equalsIgnoreCase(status)){
				return stat2;
			}else if("client".equalsIgnoreCase(status)){
				return stat;
			}
		}
		return iconList;
	}

	public String selectset(HttpServletRequest request,
			HttpServletResponse Response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();
		CategoryEntity cate = new CategoryEntity();
		
		String ckcate[] = request.getParameterValues("ckbox");
		int catecheck[] = null;
		
		int c = ckcate.length;
		catecheck = new int[c];
		
		for(int i=0; i < ckcate.length; i++){
			int kk = Integer.parseInt(ckcate[i]);
			catecheck[i] = kk;
		}
		cate.setCateselect(catecheck);
		cate.setCategory_status("1");
		cateService.cateSelectSet(cate);
				
		return cateselect2;
	}
	
	public String selectDel(HttpServletRequest request,
			HttpServletResponse Response) throws Exception {
		CateDAO cateService = new MysqlCateDAO();
		CategoryEntity cate = new CategoryEntity();
		
		String ckcate[] = request.getParameterValues("ckbox2");
		int catecheck[] = null;
		
		int c = ckcate.length;
		catecheck = new int[c];
		
		for(int i=0; i < ckcate.length; i++){
			int kk = Integer.parseInt(ckcate[i]);
			catecheck[i] = kk;
		}
		cate.setCateselect(catecheck);
		cate.setCategory_status("0");
		cateService.cateSelectSet(cate);
		
		return cateselect2;
	}
	
	public String dbEncoding(HttpServletRequest request,
			HttpServletResponse Response) throws Exception{
		String num = request.getParameter("num");
		int i = Integer.parseInt(num);

		CateDAO cateService = new MysqlCateDAO();
		boolean ecd = false;
		
		//for(i=3;i>20;i++){
			ecd = cateService.dbEncoding(i);
		
		//}
		String DBecd = "/test.jsp?number="+num+"";
		
		return DBecd;
	}
	
	public String cateAdd(HttpServletRequest request,
			HttpServletResponse Response) throws Exception{
		return cateAdd;
	}	
}












