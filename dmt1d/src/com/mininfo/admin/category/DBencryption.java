package com.mininfo.admin.category;
public class DBencryption {
/*
	public static void main(String args[]) {
		
		
		String src = "asfsdajflasd_jfdklasjlfjdal5/.45";

		String en = db_encoding(src);

		System.out.println("encoding:" + en);

		String de = db_decoding(en);

		System.out.println("decoding:" + de);

	}
*/
	public static char[][] lookupTable = {
		{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
			'/', '.', '_'
		},
		
		{'b', '3', 'y', 'H', 'k', 'g', '9', 'O', 'o', 'R',
			'T', 'Y', 'G', 'Z', '0', 'n', 't', 'p', 'J', '4', 'U', '8', 'c', '5', 'A', 'B', 'F', 'm', 'q', 'v', 'S', 'N', 'e', 'Q', 'K', 'x',
			'W', '6', 'a', 'C', 'D', 'E', 'z', '1', 'w', 'i', 'j', '7', 'M', 'I', '/', '.', 'd', 'f', 'h', 'l', 'r', 'L', 'P', '2', 'V', 'X', 
			's', 'u', '_'
		}
	};

	private static int getIndex(char s, char[] table) {
		for (int i = 0; i < table.length; i++) {
			if (s == table[i]) {
				return i;
			}
		}
		return -1;
	}
	
	
	public static String db_encoding(String src) {
		char[] temp = src.toCharArray();
		char[] result = new char[temp.length];

		for (int i = 0; i < temp.length; i++) {
			int j = getIndex(temp[i], lookupTable[0]);
			result[i] = lookupTable[1][j];
		}

		for (int i = 0; i < result.length; i++) {
			result[i] = (char)(result[i] ^ 8);
		}

		return String.valueOf(result);
	}

	public static String db_decoding(String src) {
		char[] temp = src.toCharArray();
		char[] result = new char[temp.length];

		for (int i = 0; i < temp.length; i++) {
			temp[i] = (char)(temp[i] ^ 8);
		}

		for (int i = 0; i < temp.length; i++) {
			int j = getIndex(temp[i], lookupTable[1]);
			result[i] = lookupTable[0][j];
		}

		return String.valueOf(result);
	}
}