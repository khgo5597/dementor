package com.mininfo.admin.monitoring.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.monitoring.dao.MonitoringDao;
import com.mininfo.admin.monitoring.dao.MonitoringDaoImpl;
import com.mininfo.admin.monitoring.model.AccessUserEntity;
import com.mininfo.admin.monitoring.model.MonitoringEntity;
import com.mininfo.admin.monitoring.model.UserOutListEntity;
import com.mininfo.admin.monitoring.util.PageEntity;
import com.mininfo.admin.monitoring.util.PageProcess;
import com.mininfo.common.Const;
import com.mininfo.common.PageConst;
import com.mininfo.common.code.CodeMessageHandler;
import com.mininfo.common.model.ResultEntity;
import com.mininfo.common.util.CommonUtil;

public class MonitoringController extends HttpServlet {
	

	private static final long serialVersionUID = 1L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	private String accessMonthlyPage = "/jsp/monitoring/AccessMonthly.jsp";
	private String accessDailyPage = "/jsp/monitoring/AccessDaily.jsp";
	private String accessHourlyPage = "/jsp/monitoring/AccessHourly.jsp";
	private String accessWeeklyPage = "/jsp/monitoring/AccessWeekly.jsp";
	private String accessFailCountPage = "/jsp/monitoring/AccessFailCount.jsp";
	private String accessFailUserListPage = "/jsp/monitoring/AccessFailUserList.jsp";
	private String userOutListPage = "/jsp/monitoring/UserOutList.jsp";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String cmd = req.getParameter("cmd");
		String forwardURL = null;
		try {
			logger.info("cmd=" + cmd);
			if ("accessMonthly".equalsIgnoreCase(cmd)) {
				forwardURL = accessMonthly(req, res);
			} else if ("accessDaily".equalsIgnoreCase(cmd)) {
				forwardURL = accessDaily(req, res);
			} else if ("accessHourly".equalsIgnoreCase(cmd)) {
				forwardURL = accessHourly(req, res);
			} else if ("accessWeekly".equalsIgnoreCase(cmd)) {
				forwardURL = accessWeekly(req, res);
			} else if ("accessFailCount".equalsIgnoreCase(cmd)) {
				forwardURL = accessFailCount(req, res);
			} else if ("accessFailUserList".equalsIgnoreCase(cmd)) {
				forwardURL = accessFailUserList(req, res);
			} else if ("userOutList".equalsIgnoreCase(cmd)) {
				forwardURL = userOutList(req, res);
			} else {
				String msg = CodeMessageHandler.getInstance().getCodeMessage("2001");
				StringBuffer sb = new StringBuffer()
				.append(cmd)
				.append(msg);
				ResultEntity resultEntity = new ResultEntity(Const.CODE_FAIL, sb.toString());	
				req.setAttribute("RESULT", resultEntity);					
				forwardURL = PageConst.contentsPage;
			}
			logger.info("forwardURL=" + forwardURL);
			RequestDispatcher disp = req.getRequestDispatcher(forwardURL);
			disp.forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String accessMonthly(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		int year = CommonUtil.strToInt(req.getParameter("year"));
		int month = CommonUtil.strToInt(req.getParameter("month"));
		
		MonitoringEntity entity = new MonitoringEntity();
		entity.setReqYear(year);
		entity.setReqMonth(month);
		
		//today
		GregorianCalendar cal = new GregorianCalendar();
				
		if(entity.getReqYear()<=0) {
			entity.setReqYear(cal.get(Calendar.YEAR));
		}
		
		entity.initSearchDate();

		MonitoringDao dao = new MonitoringDaoImpl();
		entity = dao.getMonthly(entity);
		
		req.setAttribute("ENTITY", entity);
		
		return accessMonthlyPage;
	}

	public String accessDaily(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		int year = CommonUtil.strToInt(req.getParameter("year"));
		int month = CommonUtil.strToInt(req.getParameter("month"));

		MonitoringEntity entity = new MonitoringEntity();
		entity.setReqYear(year);
		entity.setReqMonth(month);
		
		//today
		GregorianCalendar cal = new GregorianCalendar();
				
		if(entity.getReqYear()<=0) {
			entity.setReqYear(cal.get(Calendar.YEAR));
		}
		
		if(entity.getReqMonth()<=0){
			entity.setReqMonth((cal.get(Calendar.MONTH)+1));
		}

		entity.initSearchDate();
		
		logger.info("2 entity="+entity);
		
		MonitoringDao dao = new MonitoringDaoImpl();
		entity = dao.getDaily(entity);
		
		req.setAttribute("ENTITY", entity);
		return accessDailyPage;
	}
	
	public String accessHourly(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		return accessHourlyPage;
	}
	
	public String accessWeekly(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		return accessWeeklyPage;
	}
	
	public String accessFailCount(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		int year = CommonUtil.strToInt(req.getParameter("year"));
		int month = CommonUtil.strToInt(req.getParameter("month"));

		MonitoringEntity entity = new MonitoringEntity();
		entity.setReqYear(year);
		entity.setReqMonth(month);
		
		//today
		GregorianCalendar cal = new GregorianCalendar();
				
		if(entity.getReqYear()<=0) {
			entity.setReqYear(cal.get(Calendar.YEAR));
		}
		
		if(entity.getReqMonth()<=0){
			entity.setReqMonth((cal.get(Calendar.MONTH)+1));
		}

		entity.initSearchDate();
		
		MonitoringDao dao = new MonitoringDaoImpl();
		entity = dao.getFailCount(entity);
		
		logger.info("Controller Return Entity="+entity);
		
		req.setAttribute("ENTITY", entity);
		return accessFailCountPage;
	}
	
	public String accessFailUserList(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		int year = CommonUtil.strToInt(req.getParameter("year"));
		int month = CommonUtil.strToInt(req.getParameter("month"));
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));

		AccessUserEntity entity = new AccessUserEntity();
		entity.setReqYear(year);
		entity.setReqMonth(month);
		entity.setUser_id(userId);
		
		//today
		GregorianCalendar cal = new GregorianCalendar();
				
		if(entity.getReqYear()<=0) {
			entity.setReqYear(cal.get(Calendar.YEAR));
		}
		
		if(entity.getReqMonth()<=0){
			entity.setReqMonth((cal.get(Calendar.MONTH)+1));
		}

		entity.initSearchDate();
		
		MonitoringDao dao = new MonitoringDaoImpl();
		entity = dao.getDetailFailList(entity);
		
		logger.info("accessFailUserList Controller Return Entity="+entity);
		
		req.setAttribute("ENTITY", entity);
	
		return accessFailUserListPage;
	}
	
	public PageEntity getPageEntity(HttpServletRequest request,
			HttpServletResponse response){
		
		PageEntity pageEntity = new PageEntity();
		int current_page = CommonUtil.strToInt(request.getParameter("current_page"), 1);
		pageEntity.setCurrent_page(current_page);
		
		return pageEntity;
	}
	
	public String userOutList(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		String search = CommonUtil.nullToStr(req.getParameter("search"));
		String startYMD = CommonUtil.nullToStr(req.getParameter("startYMD"));
		String endYMD = CommonUtil.nullToStr(req.getParameter("endYMD"));
		String userId = CommonUtil.nullToStr(req.getParameter("userId"));

		UserOutListEntity entity = new UserOutListEntity();
		entity.setSearch(search);
		entity.setStartYMD(startYMD);
		entity.setEndYMD(endYMD);
		
		if("id".equals(search)){
			entity.setUser_id(userId);
		}

		PageProcess pageProcess = new PageProcess();
		PageEntity page = pageProcess.calculatePage(req, res, entity);
		req.setAttribute("PAGE", page);
		
		entity.setStartIndex(page.getStart_idx());
		entity.setEndIndex(page.getEnd_idx());
		
		//today
		DecimalFormat df = new DecimalFormat("00");
		GregorianCalendar cal = new GregorianCalendar();
		StringBuffer sb = new StringBuffer();
		sb.append(cal.get(Calendar.YEAR));
		sb.append(df.format(new Integer(cal.get(Calendar.MONTH)+1)));
		sb.append(df.format(new Integer(cal.get(Calendar.DATE))));
		
		if(entity.getStartYMD().equals("")) {
			entity.setStartYMD(sb.toString());
		}
		
		if(entity.getEndYMD().equals("")){
			entity.setEndYMD(sb.toString());
		}
		
		MonitoringDao dao = new MonitoringDaoImpl();
		entity = dao.getUserOutList(entity);
	
		req.setAttribute("ENTITY", entity);

		return userOutListPage;
	}
}
