package com.mininfo.admin.monitoring.dao;


import com.mininfo.admin.monitoring.model.AccessUserEntity;
import com.mininfo.admin.monitoring.model.MonitoringEntity;
import com.mininfo.admin.monitoring.model.UserOutListEntity;


public interface MonitoringDao {

	public MonitoringEntity getMonthly(MonitoringEntity entity )throws Exception;
	public MonitoringEntity getDaily(MonitoringEntity entity) throws Exception;
	public MonitoringEntity getFailCount(MonitoringEntity entity) throws Exception;
	public AccessUserEntity getDetailFailList(AccessUserEntity entity) throws Exception;
	public UserOutListEntity getUserOutList(UserOutListEntity entity) throws Exception;
	public int getUserOutSearchCount(UserOutListEntity entity) throws Exception;
}
