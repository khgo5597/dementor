package com.mininfo.admin.monitoring.dao;

import java.io.Reader;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.admin.monitoring.model.AccessUserEntity;
import com.mininfo.admin.monitoring.model.MonitoringEntity;
import com.mininfo.admin.monitoring.model.SuccessEntity;
import com.mininfo.admin.monitoring.model.UserOutListEntity;
import com.mininfo.exception.DataAccessException;

public class MonitoringDaoImpl implements MonitoringDao {

	protected final Log logger = LogFactory.getLog(getClass());
	
	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader (resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();
			
			throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}
	
	/* 
	 * MonitoringController [cmd : accessMonthly] Use
	 * Monitoring.xml
	 * getMonthlyTotalCount : SELECT COUNT(*) FROM l_access_log_tbl WHERE DATE_FORMAT(ACCESS_DATE,'%Y') = #searchDate# AND ACCESS_RESULT='S'
	 * Monitoring.xml getMonthly
	*/
	public MonitoringEntity getMonthly(MonitoringEntity entity) throws Exception {
		
		
		try{
			sqlMap.startTransaction();
			int totalCnt = ((Integer)(sqlMap.queryForObject("getMonthlyTotalCount", entity))).intValue();
			
			List list = (List)sqlMap.queryForList("getMonthly", entity);
			
			entity.setList(list);
			
			String[] countArray = new String[12];
			for(int i=0;i<countArray.length;i++){
				countArray[i] = "0";
			}
			
			for(int i=0;i<list.size();i++) {
				
				SuccessEntity successEntity = (SuccessEntity)list.get(i);
				String accessDate = successEntity.getAccessDate();
				String accessCount = successEntity.getAccessCount();
				
				int month = new Integer(accessDate).intValue();
				
				countArray[i+1] = accessCount; //�λ뜃由겼첎占�1 占쎈쵐�쏙옙遺용퓠 占쎈쵐�썲쳞�곷땾 占쎈챸��
			}
			
			logger.info("monthly list= "+list);
			entity.setTotalCount(totalCnt);
			entity.setCountArray(countArray);
			
			logger.info("getMonthly resultEntity= "+entity);
			
			sqlMap.commitTransaction();
			return entity;
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * MonitoringController [cmd : accessDaily] Use
	 * Monitoring.xml
	 * getMonthlyTotalCount : SELECT COUNT(*) FROM l_access_log_tbl WHERE DATE_FORMAT(ACCESS_DATE,'%Y') = #searchDate# AND ACCESS_RESULT='S'
	 * Monitoring.xml getDaily
	*/
	public MonitoringEntity getDaily(MonitoringEntity entity) throws Exception {
		
		try {
	
			GregorianCalendar cal = new GregorianCalendar();
			
			logger.info("entity="+entity);
			
			cal.set(Calendar.YEAR, entity.getReqYear());
			cal.set(Calendar.MONTH, entity.getReqMonth()-1);
			
			int dayOfMonth = cal.getActualMaximum(Calendar.DATE);
			
			logger.info("YEAR="+cal.get(Calendar.YEAR));
			logger.info("MONTH="+(cal.get(Calendar.MONTH)));
			logger.info("LAST DAY="+(cal.getActualMaximum(Calendar.DATE)));
			sqlMap.startTransaction();
			
			int totalCnt = ((Integer)(sqlMap.queryForObject("getDailyTotalCount", entity))).intValue();
			List list = (List)sqlMap.queryForList("getDaily", entity);
			
			logger.info("daily list= "+list);
			
			entity.setList(list);
			
			String[] countArray = new String[dayOfMonth];
			
			for(int i=0;i<countArray.length;i++){
				countArray[i] = "0";
			}
				
				
			for(int i=0;i<list.size();i++){
				SuccessEntity successEntity = (SuccessEntity)list.get(i);
				String accessDate = successEntity.getAccessDate();
				String accessCount = successEntity.getAccessCount();
				
				int date = new Integer(accessDate).intValue();
				int k = Integer.parseInt(accessDate.substring(accessDate.length()-2, accessDate.length()));
				
				if( !accessCount.equals("0")){
					countArray[k-1] = accessCount;
				}else{ 
					logger.info("ERROR CASE : "+ date +"day / access count : "+accessCount);
				}
				
			}	
			
			entity.setTotalCount(totalCnt);
			entity.setCountArray(countArray);
			
			logger.info("getDaily resultEntity= "+entity);
			
			sqlMap.commitTransaction();
			return entity;
			
		} catch(Exception e){
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * MonitoringController [cmd : accessFailCount] Use
	 * Monitoring.xml
	 * getAccessFailTotalCount :  SELECT COUNT(*) FROM l_access_log_tbl WHERE DATE_FORMAT(ACCESS_DATE,'%Y%m') = #searchDate# AND ACCESS_RESULT='F'
	 * Monitoring.xml getAccessFail
	*/
	public MonitoringEntity getFailCount(MonitoringEntity entity)
			throws Exception {
		
		try {
			
			logger.info("getFailCount entity="+entity);
			sqlMap.startTransaction();
			int totalCnt = ((Integer)(sqlMap.queryForObject("getAccessFailTotalCount", entity))).intValue();
			
			List list = (List)sqlMap.queryForList("getAccessFail", entity);
			
			logger.info("fail count list= "+list);
			
			entity.setList(list);
			entity.setTotalCount(totalCnt);
			
			logger.info("getFailCount resultEntity= "+entity);
			System.out.println("getFailCount resultEntity#####"+entity);
			
			sqlMap.commitTransaction();
			return entity;
			
		} catch(Exception e){
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * MonitoringController [cmd : accessFailUserList] Use
	 * Monitoring.xml
	 * getUserAccessFailCount : SELECT COUNT(*) FROM l_access_log_tbl WHERE DATE_FORMAT(ACCESS_DATE,'%Y%m') = #searchDate# AND ACCESS_RESULT='F' AND USER_ID=#user_id#
	 * Monitoring.xml getUserAccessFailList
	*/
	public AccessUserEntity getDetailFailList(AccessUserEntity entity)
			throws Exception {
		try {
			System.out.println(entity.getUser_id());
			logger.info("getDetailFailList entity="+entity);
			sqlMap.startTransaction();
			int totalCnt = ((Integer)(sqlMap.queryForObject("getUserAccessFailCount", entity))).intValue();
			
			System.out.println("entity###"+entity);
			
			List list = (List)sqlMap.queryForList("getUserAccessFailList", entity);
			
			logger.info("fail detail list= "+list);
			System.out.println("fail detail list= "+list);
			
			entity.setList(list);
			entity.setTotalCount(totalCnt);
			
			sqlMap.commitTransaction();
			return entity;
			
		} catch(Exception e){
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
	
	/* 
	 * MonitoringController [cmd : userOutList] Use
	 * Monitoring.xml getIdUserOutList
	 * Monitoring.xml getDateUserOutList
	*/
	public UserOutListEntity getUserOutList(UserOutListEntity entity)
			throws Exception {
		try {
			
			logger.info("getUserOutList input entity="+entity);
	
			List list = null;
			sqlMap.startTransaction();
			if("id".equals(entity.getSearch())){
				list = (List)sqlMap.queryForList("getIdUserOutList", entity);
			} else {
				list = (List)sqlMap.queryForList("getDateUserOutList", entity);
			}
			
			entity.setList(list);
			sqlMap.commitTransaction();
			
			return entity;
			
		} catch(Exception e){
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * PageProcess [method : calculatePage] Use
	 * Monitoring.xml 
	 * getDateUserOutListCount : SELECT count(*) FROM l_user_delete_log_tbl WHERE DATE_FORMAT(DELETE_DATE, '%Y%m%d') BETWEEN #startYMD# AND #endYMD#
	*/
	public int getUserOutSearchCount(UserOutListEntity entity) throws Exception{

		try{
			sqlMap.startTransaction();
			int totalCount = ((Integer)(sqlMap.queryForObject("getDateUserOutListCount", entity))).intValue();
			sqlMap.commitTransaction();
			
			return totalCount;
		}catch(Exception e){
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
}
