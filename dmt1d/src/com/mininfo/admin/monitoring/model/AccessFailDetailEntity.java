package com.mininfo.admin.monitoring.model;

import java.io.Serializable;

public class AccessFailDetailEntity implements Serializable {

	private String access_date;
	private String access_ip;
	
	public String getAccess_date() {
		return access_date;
	}
	public void setAccess_date(String access_date) {
		this.access_date = access_date;
	}
	public String getAccess_ip() {
		return access_ip;
	}
	public void setAccess_ip(String access_ip) {
		this.access_ip = access_ip;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("access_date:").append(access_date).append(" / access_ip:").append(access_ip);
		return sb.toString();
	}
}
