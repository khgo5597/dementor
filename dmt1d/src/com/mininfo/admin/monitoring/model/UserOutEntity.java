package com.mininfo.admin.monitoring.model;

import java.io.Serializable;

public class UserOutEntity implements Serializable {
	
	private String user_id;
	private String in_date;
	private String out_date;
	private String status;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getIn_date() {
		return in_date;
	}
	public void setIn_date(String in_date) {
		this.in_date = in_date;
	}
	public String getOut_date() {
		return out_date;
	}
	public void setOut_date(String out_date) {
		this.out_date = out_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("[ user_id:").append(user_id)
		.append(" / in_date:").append(in_date)
		.append(" / out_date:").append(out_date)
		.append(" / state:").append(status).append("]");
		return sb.toString();
	}
}
