package com.mininfo.admin.monitoring.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MonitoringEntity implements Serializable {
	
	protected final Log logger = LogFactory.getLog(getClass());

	protected int reqYear;
	protected int reqMonth;
	protected String searchDate;
	protected int totalCount;
	protected String[] countArray;
	protected List list;
	
	public int getReqYear() {
		logger.info("===========this.reqYear="+this.reqYear);
		return this.reqYear;
	}
	public void setReqYear(int reqYear) {

		this.reqYear = reqYear;
		
		logger.info("reqYear="+reqYear);
		logger.info("this.reqYear="+this.reqYear);
	}
	public int getReqMonth() {
		return reqMonth;
	}
	public void setReqMonth(int reqMonth) {
		
		this.reqMonth = reqMonth;
	}
	public String getSearchDate() {
		return searchDate;
	}
	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}	
	public void initSearchDate(){
		
		if(reqMonth > 0 ){
	        DecimalFormat df = new DecimalFormat("00");
	        logger.info(df.format(new Integer(reqMonth)));
			setSearchDate(this.reqYear +df.format(reqMonth));
		} else {
			setSearchDate(new Integer(this.reqYear).toString());
		}
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public String[] getCountArray() {
		return countArray;
	}
	public void setCountArray(String[] countArray) {
		this.countArray = countArray;
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("reqYear:").append(reqYear).append("\n")
		.append("reqMonth:").append(reqMonth).append("\n")
		.append("searchDate:").append(searchDate).append("\n")
		.append("totalCount:").append(totalCount).append("\n");
		
		if(countArray != null){
			for(int i=0;i<countArray.length;i++){
				sb.append(countArray[i]).append("\n");
			}
		}
		return sb.toString();
	}
}
