package com.mininfo.admin.monitoring.model;

import java.io.Serializable;

public class AccessUserEntity extends MonitoringEntity implements
		Serializable {
	
	private String userId;

	public String getUser_id() {
		return userId;
	}

	public void setUser_id(String userId) {
		this.userId = userId;
	}
	
	public String toString(){
		
		
		StringBuffer sb = new StringBuffer(super.toString());
		
		//sb.append("user_id:").append(reqYear).append("\n");
		sb.append("userId:").append(userId).append("\n");
		
		if(list != null){
			for(int i=0;i<list.size();i++){
				sb.append(list.get(i).toString()).append("\n");
			}
		}
		return sb.toString();
	}
}
