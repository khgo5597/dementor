package com.mininfo.admin.monitoring.model;

import java.io.Serializable;
import java.util.List;

public class UserOutListEntity implements Serializable {
	
	private String search;
	private String startYMD;
	private String endYMD;
	private String user_id;
	private int startIndex;
	private int endIndex;
	
	protected List list;

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}
	
	public String getStartYMD() {
		return startYMD;
	}

	public void setStartYMD(String startYMD) {
		this.startYMD = startYMD;
	}

	public String getEndYMD() {
		return endYMD;
	}

	public void setEndYMD(String endYMD) {
		this.endYMD = endYMD;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
	
	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("startYMD:").append(startYMD).append("\n")
		.append("endYMD:").append(endYMD).append("\n")
		.append("startIndex:").append(startIndex).append("\n")
		.append("endIndex:").append(endIndex).append("\n");
		
		if(list != null){
			for(int i=0;i<list.size();i++){
				sb.append(list.get(i).toString()).append("\n");
			}
		}
		return sb.toString();
	}
}



