package com.mininfo.admin.monitoring.model;

import java.io.Serializable;

public class SuccessEntity implements Serializable {

	private String accessDate;
	private String accessCount;
	
	public String getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(String accessDate) {
		this.accessDate = accessDate;
	}
	public String getAccessCount() {
		return accessCount;
	}
	public void setAccessCount(String accessCount) {
		this.accessCount = accessCount;
	}
}
