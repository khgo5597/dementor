package com.mininfo.admin.monitoring.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.admin.monitoring.dao.MonitoringDao;
import com.mininfo.admin.monitoring.dao.MonitoringDaoImpl;
import com.mininfo.admin.monitoring.model.UserOutListEntity;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.user.dao.UserDAOImpl;
import com.mininfo.user.model.UserListEntity;

public class PageProcess {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	public final int ROWS = 10;
	
	public PageEntity calculatePage(HttpServletRequest request, HttpServletResponse response, UserOutListEntity entity) throws Exception {

		int current_page=1;  
		int last_page= 1; 
		int total_row = 0;
		
		int first_page = 0;
		int end_page = 0;
		int total_pagegroup = 0;
		int current_pagegroup = 0;

		int start_idx = 0;
		int end_idx = 10;
		
		current_page = CommonUtil.strToInt(request.getParameter("current_page"), 1);
		
		MonitoringDao dao = new MonitoringDaoImpl();
		total_row = dao.getUserOutSearchCount(entity);

		logger.info("UserOut List total count = "+total_row);
		logger.info("UserOut List current_page = "+current_page);
		
		if(current_page!=1) {
			start_idx = (current_page-1) * ROWS + 1;
			end_idx = start_idx+9;
		}

		last_page=total_row /ROWS + 1; 
		if( (total_row % ROWS) == 0 ) {
			last_page=last_page - 1 ;
		}

		first_page = (int)((current_page-1)*10.0F/100.F)*10+1;
		end_page = (first_page+9>last_page)?last_page:first_page+9;
		total_pagegroup = last_page/10 +1;
		current_pagegroup = (current_page-1)/10 +1;

		PageEntity pageEntity = new PageEntity();
		pageEntity.setCurrent_page(current_page);
		pageEntity.setCurrent_pagegroup(current_pagegroup);
		pageEntity.setStart_idx(start_idx);
		pageEntity.setEnd_idx(end_idx);
		pageEntity.setEnd_page(end_page);
		pageEntity.setFirst_page(first_page);
		pageEntity.setLast_page(last_page);
		pageEntity.setTotal_pagegroup(total_pagegroup);
		pageEntity.setTotal_row(total_row);
		
		return pageEntity;
	}
	
	public PageEntity requestPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int current_page = 1;
		String strCurrent_page = request.getParameter("current_page");
		String search_content = request.getParameter("search_content");
		
		if (null != strCurrent_page && ! "".equals(strCurrent_page.trim())) 
			current_page = Integer.parseInt(strCurrent_page);
		
		if (null == search_content) search_content = "";
		
		PageEntity pageEntity = new PageEntity();
		pageEntity.setCurrent_page(current_page);
		
		return pageEntity;
	}
	
	public PageEntity mapPage(Map map) throws Exception {
		
		int current_page = 1;
		
		PageEntity entity = new PageEntity();
		entity.setCurrent_page(CommonUtil.strToInt((String)map.get("current_page")));
		
		return entity;
	}
	
	public PageEntity calculatePage2(HttpServletRequest request, HttpServletResponse response, UserListEntity entity) throws Exception {

		int current_page=1;  
		int last_page= 1; 
		int total_row = 0;
		
		int first_page = 0;
		int end_page = 0;
		int total_pagegroup = 0;
		int current_pagegroup = 0;

		int start_idx = 0;
		int end_idx = 10;
		
		current_page = CommonUtil.strToInt(request.getParameter("current_page"), 1);
		
		UserDAOImpl dao = new UserDAOImpl();
		total_row = dao.getUserListSearchCount(entity);
					
		if(current_page!=1) {
			start_idx = (current_page-1) * ROWS + 1;
			end_idx = start_idx+9;
		}

		last_page=total_row /ROWS + 1; 
		if( (total_row % ROWS) == 0 ) {
			last_page=last_page - 1 ;
		}
		
		first_page = (int)((current_page-1)*10.0F/100.F)*10+1;
		end_page = (first_page+9>last_page)?last_page:first_page+9;
		total_pagegroup = last_page/10 +1;
		current_pagegroup = (current_page-1)/10 +1;

		PageEntity pageEntity = new PageEntity();
		pageEntity.setCurrent_page(current_page);
		pageEntity.setCurrent_pagegroup(current_pagegroup);
		pageEntity.setStart_idx(start_idx);
		pageEntity.setEnd_idx(end_idx);
		pageEntity.setEnd_page(end_page);
		pageEntity.setFirst_page(first_page);
		pageEntity.setLast_page(last_page);
		pageEntity.setTotal_pagegroup(total_pagegroup);
		pageEntity.setTotal_row(total_row);
		
		return pageEntity;
	}
}
