package com.mininfo.admin.monitoring.util;

import java.io.Serializable;

public class PageEntity  implements Serializable{
	
	
	public int total_row;
	public int current_page;
	public int first_page;
	public int end_page;
	public int last_page;
	public int total_pagegroup;
	public int current_pagegroup;
	public int start_idx;
	public int end_idx;
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();

		sb.append("total_row="+total_row).append("\n");
		sb.append("current_page="+current_page).append("\n");
		sb.append("first_page="+first_page).append("\n");
		sb.append("end_page="+end_page).append("\n");
		sb.append("last_page="+last_page).append("\n");
		sb.append("total_pagegroup="+total_pagegroup).append("\n");
		sb.append("current_pagegroup="+current_pagegroup).append("\n");
		sb.append("start_idx="+start_idx).append("\n");
		sb.append("end_idx="+end_idx).append("\n");
	
		return sb.toString();
	}
	
	public int getTotal_row() {
		return total_row;
	}
	public void setTotal_row(int total_row) {
		this.total_row = total_row;
	}
	public int getCurrent_page() {
		return current_page;
	}
	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}
	public int getFirst_page() {
		return first_page;
	}
	public void setFirst_page(int first_page) {
		this.first_page = first_page;
	}
	public int getEnd_page() {
		return end_page;
	}
	public void setEnd_page(int end_page) {
		this.end_page = end_page;
	}
	public int getLast_page() {
		return last_page;
	}
	public void setLast_page(int last_page) {
		this.last_page = last_page;
	}
	public int getTotal_pagegroup() {
		return total_pagegroup;
	}
	public void setTotal_pagegroup(int total_pagegroup) {
		this.total_pagegroup = total_pagegroup;
	}
	public int getCurrent_pagegroup() {
		return current_pagegroup;
	}
	public void setCurrent_pagegroup(int current_pagegroup) {
		this.current_pagegroup = current_pagegroup;
	}
	public int getStart_idx() {
		return start_idx;
	}
	public void setStart_idx(int start_idx) {
		this.start_idx = start_idx;
	}
	public int getEnd_idx() {
		return end_idx;
	}
	public void setEnd_idx(int end_idx) {
		this.end_idx = end_idx;
	}
}
