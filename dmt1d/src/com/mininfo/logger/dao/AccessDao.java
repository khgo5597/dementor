package com.mininfo.logger.dao;

import com.mininfo.user.model.UserEntity;


public interface AccessDao {

	public UserEntity getUser(UserEntity entity) throws Exception;
}
