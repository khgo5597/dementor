package com.mininfo.logger.dao;


import java.io.Reader;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.exception.DataAccessException;
import com.mininfo.logger.model.AccessLogEntity;
import com.mininfo.logger.model.UserOutLogEntity;

public class UserLogDaoImpl implements UserLogDao {

	protected final Log logger = LogFactory.getLog(getClass());

	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader (resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();
			
			throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}
	
	/* 
	 * LogController [cmd : accessLog] Use
	 * MindkeyMain [cmd : verifying(String value, HttpServletRequest req, String date)] Use
	 * MindkeyMain [cmd : verifying(String value, String remoteAddr)] Use
	 * UserLog.xml insertAccessLog 
	 * insertAccessLog : INSERT INTO l_access_log_tbl ( USER_ID, ACCESS_DATE, ACCESS_IP, ACCESS_RESULT)VALUES ( #user_id#, now(), #access_ip#, #access_result#) 
	*/
	public boolean insertAccessLog(AccessLogEntity entity){
		
		try {
			sqlMap.startTransaction();
			sqlMap.insert("insertAccessLog", entity);
			sqlMap.commitTransaction();
			return true;
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * UserController [cmd : userOutAction] Use
	 * UserController [cmd : userStatusEdit] Use
	 * insertUserOutLog :  INSERT INTO l_user_delete_log_tbl ( USER_ID, REG_DATE, DELETE_DATE, DELETE_STATUS ) VALUES (#user_id#, (SELECT USER_REGDATE FROM frame_member WHERE USER_ID=#user_id#), now(), #delete_status#) 
	*/
	public boolean insertUserOutLog(UserOutLogEntity entity) throws Exception {
		try {
			if("Y".equals(entity.getDelete_status())){
				entity.setDelete_status("AS");
			}else if("N".equals(entity.getDelete_status())){
				entity.setDelete_status("AD");
			}
			sqlMap.startTransaction();
			sqlMap.insert("insertUserOutLog", entity);
			sqlMap.commitTransaction();
			return true;
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}

	/* 
	 * did not used 
	*/
	public boolean isAlreadyOutUser(UserOutLogEntity entity) throws Exception {
		
		try {
			sqlMap.startTransaction();
			if(sqlMap.queryForObject("alreadyOutUser", entity) != null) {
				sqlMap.commitTransaction();
				return true;
			} else { 
				sqlMap.commitTransaction();
				return false;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
}
