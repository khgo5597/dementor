package com.mininfo.logger.dao;

import java.io.Reader;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mininfo.exception.DataAccessException;
import com.mininfo.user.model.UserEntity;

public class AccessDaoImpl implements AccessDao {

	
	protected final Log logger = LogFactory.getLog(getClass());

	private static final SqlMapClient sqlMap;
	static {
		try {
			String resource = "SqlMapConfig.xml";
			Reader reader = Resources.getResourceAsReader (resource);
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {

			e.printStackTrace();
			
			throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
	}
	
	/* 
	 * did not used
	*/
	public UserEntity getUser(UserEntity pEntity) throws Exception {

		try {
			sqlMap.startTransaction();
			UserEntity aEntity = (UserEntity)(sqlMap.queryForObject("getUser", pEntity));
			sqlMap.commitTransaction();
			return aEntity;
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e.getMessage(), e);
		} finally{
			try {
				sqlMap.endTransaction();
			}catch(SQLException sqlE) {
				logger.error(sqlE);
			}
		}
	}
}
