package com.mininfo.logger.dao;


import com.mininfo.logger.model.AccessLogEntity;
import com.mininfo.logger.model.UserOutLogEntity;

public interface UserLogDao {

	public boolean insertAccessLog(AccessLogEntity entity) throws Exception;
	
	public boolean insertUserOutLog(UserOutLogEntity entity) throws Exception;
	
	public boolean isAlreadyOutUser(UserOutLogEntity entity) throws Exception;
}
