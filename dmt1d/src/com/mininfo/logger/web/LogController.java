package com.mininfo.logger.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.common.Const;
import com.mininfo.common.util.CommonUtil;
import com.mininfo.exception.DataAccessException;
import com.mininfo.exception.UserNotFoundException;
import com.mininfo.logger.dao.AccessDao;
import com.mininfo.logger.dao.AccessDaoImpl;
import com.mininfo.logger.dao.UserLogDao;
import com.mininfo.logger.dao.UserLogDaoImpl;
import com.mininfo.logger.model.AccessLogEntity;
import com.mininfo.logger.model.UserOutLogEntity;
import com.mininfo.user.model.UserEntity;

public class LogController extends HttpServlet {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		String cmd = req.getParameter("cmd");
		String forwardURL = null;
		try {
			
			if("accessLog".equalsIgnoreCase(cmd)){
				accessLog(req, res);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void accessLog(HttpServletRequest req,
			HttpServletResponse response) throws Exception {

		String userId = CommonUtil.nullToStr(req.getParameter("userId"));
		String accessResult = CommonUtil.nullToStr(req.getParameter("access_result"));
		
		try {
			
			UserEntity accessUser = new UserEntity();
			accessUser.setUser_id(userId);

			UserLogDao dao = new UserLogDaoImpl();
			
			AccessLogEntity entity = new AccessLogEntity();
			entity.setUser_id(userId);
			entity.setAccess_ip(req.getRemoteAddr());
			
			if("fail".equals(accessResult)) {
				entity.setAccess_result(Const.ACCESS_FAIL);
				dao.insertAccessLog(entity);				
			} else {
				entity.setAccess_result(Const.ACCESS_SUCCESS);
				dao.insertAccessLog(entity);
			}			
		} catch (Exception e){
			throw e;
		}
	}
}
