package com.mininfo.logger.model;

import com.mininfo.common.model.BaseObject;

public class AccessLogEntity extends BaseObject {
	
	private String user_id;
	private String access_date;
	private String access_ip;
	private String access_result;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAccess_date() {
		return access_date;
	}
	public void setAccess_date(String access_date) {
		this.access_date = access_date;
	}
	public String getAccess_ip() {
		return access_ip;
	}
	public void setAccess_ip(String access_ip) {
		this.access_ip = access_ip;
	}
	public String getAccess_result() {
		return access_result;
	}
	public void setAccess_result(String access_result) {
		this.access_result = access_result;
	}
}
