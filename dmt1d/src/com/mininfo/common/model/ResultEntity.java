package com.mininfo.common.model;

import java.io.Serializable;

public class ResultEntity implements Serializable {
	
	private int resultCode;
	private String resultMessage;
	private int processCode;
	
	
	public ResultEntity(int resultCode, String resultMessage){
		this.resultCode = resultCode;
		this.resultMessage = resultMessage;
	}
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public int getProcessCode() {
		return processCode;
	}
	public void setProcessCode(int processCode) {
		this.processCode = processCode;
	}
}
