package com.mininfo.common.code;

import java.util.HashMap;
import java.util.Map;


import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.Attributes;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.mininfo.admin.category.dao.CateDAO;
import com.mininfo.admin.category.dao.MysqlCateDAO;
import com.mininfo.common.util.CommonUtil;

public class CodeMessageHandler {

	private static CodeMessageHandler instance;
	
	private static String resource = "CodeMessage.xml";
	
	private static Map codeMap;
	
	public static synchronized CodeMessageHandler getInstance() throws Exception {
		if (instance == null) {
			instance = new CodeMessageHandler();
		}
		return instance;
	}
	
	public String getCodeMessage(String code) {
		
		if(codeMap == null){
			
			
			
			codeMap = new HashMap();

			try {
				
				CateDAO cateDAO = new MysqlCateDAO();
				cateDAO.insertCategoryIconList();
				new CodeMessageParser().readDocument();
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		return CommonUtil.nullToStr(codeMap.get(code));
	}
	
	class CodeMessageParser extends DefaultHandler {
		
		String code;
		String mesg;
		
		CodeMessageParser() {
			super();
			
		}
		
		private void readDocument() throws Exception {
			
			String fileName = getClass().getClassLoader().getResource(resource).getPath();
			
			XMLReader parser = new SAXParser();
	        parser.setContentHandler(this);
	        parser.parse(fileName);
		}
		
		public void startElement(String namespaceURI,
				String lName,
				String qName, 
				Attributes attrs) throws SAXException {

			String eName = lName; 
			
			if ("".equals(eName))
				eName = qName; 
			
			if("message".equals(eName) && attrs != null){
				
				resetCodeMesg();
				for (int i = 0; i < attrs.getLength(); i++) {
					String aName = attrs.getLocalName(i); 
					if ("".equals(aName)) aName = attrs.getQName(i);
					if("code".equals(aName)) code = attrs.getValue(i);
				}
			}
			
			if("br".equals(eName))
				mesg += "<br/>";
		}
		
		public void characters(char buf[], int offset, int len) throws SAXException {
			String a = new String(buf, offset, len);
			if(a != null) mesg += a.trim();
		}	
		
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
			
			if("message".equals(qName)){
				codeMap.put(code, mesg);
			}
		}
		
		public void resetCodeMesg(){
			code = "";
			mesg = "";			
		}
	}
}
