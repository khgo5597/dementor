package com.mininfo.common.verify;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.client.web.ClientController;
import com.mininfo.common.property.DementorProperty;

public class SiteVerify {

	protected static final Log logger = LogFactory.getLog(ClientController.class);
	// 싸이트 검증 함수
	public boolean verify(HttpServletRequest request) throws Exception {
		// Main에서 로그인시 사용하는 전 페이지(refererURL이 존재하는 케이스)
		
		// dementor.properties에 등록 된 주소와
		// 주소창에 입력된 주소와
		// 호출한 전 페이지의 도메인이 동일하다면 true 아니면 false를 리턴한다.
		
		// 등록 된 주소
		String registryUrl = (DementorProperty.getInstance()).getRegistryUrl();
		
		registryUrl = (DementorProperty.getInstance()).getRegistryUrl().replaceAll("http://", "");
		
		// 주소창에 입력 된 주소
		//String typingURL = request.getRequestURL().toString()
		//					.replaceAll(request.getRequestURI(), "")
		//					.replaceAll("http://", "");
		// 현재 페이지를 호출한 전 페이지
		// 만약 강제주소입력으로 바로 올 경우 referer이 null이 됨.
		String refererURL = "";
		if(request.getHeader("REFERER") != null) {
			
			refererURL  = request.getHeader("REFERER").toString().replaceAll("http://", "");
			
			if(refererURL.indexOf('/') != -1) {

				String[] temp = null;
				temp = refererURL.split("/", 2);

//				if(refererURL.equals(registryUrl+(refererURL.substring(refererURL.indexOf('/'),refererURL.length())))) {
//					refererURL = registryUrl;
//				}
				
				System.out.println("refererURL ==> " + temp[0]);
				refererURL = temp[0];

			}
			
/*			
  			if(refererURL.indexOf('/') != -1) {
				//refererURL = refererURL.replaceAll((refererURL.substring(refererURL.indexOf('/'),refererURL.length())), "");
				// userController에서 replaceAll이 안먹힘... 이유를 모르겠음.
				// 아래의 소스는 replaceAll과 동일한 기능임
				if(refererURL.equals(registryUrl+(refererURL.substring(refererURL.indexOf('/'),refererURL.length())))) {
					refererURL = registryUrl;
				}
			}
*/
		}	
		
		// 주소창에 입력 된 주소와 호출한 전주소와 같지 않거나
		// 주소창에 입력 된 주소와 dementor.properties에 등록되지 않은 URL일 경우
		if(!refererURL.equals(registryUrl)) {// || !typingURL.equals(registryUrl)){
			logger.info("refererURL : " + refererURL);
			//logger.info("typingURL : " + typingURL);
			logger.info("registryUrl : " + registryUrl);
			// 클라이언트 주소가  아닌 다른 경로로 접속시 실패 주소로 보냄
			return false;
		}		
		return true;
	}
	
	public boolean verify(HttpServletRequest request, String referer) throws Exception {
		// window.open을 통해 전 페이지(RefererURL)가 존재하지 않는 정보 수정 이용시 
		String registryUrl = (DementorProperty.getInstance()).getRegistryUrl();
		String typingURL = request.getRequestURL().toString()
							.replaceAll(request.getRequestURI(), "")
							.replaceAll("http://", "");		
		if(!typingURL.equals(registryUrl)){
			logger.info("typingURL : " + typingURL);
			logger.info("registryUrl : " + registryUrl);
			return false;
		}		
		return true;
	}
}
