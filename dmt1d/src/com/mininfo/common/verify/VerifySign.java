package com.mininfo.common.verify;

import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.common.crypto.SHA1Dementor;
import com.mininfo.common.property.DementorProperty;
import com.mininfo.common.util.CommonUtil;

public class VerifySign {
	private static HashMap timeOutMap = new HashMap();							// 유효시간 맵 생성 
	
	public boolean isVerifySign(HttpServletRequest request, String otk, String urlId, String userId, String userIp, String now) {
		try {
			//System.out.println(request.get + ";;");
			
			// 접속 중인 아이피와 현재 사용자 아이피와 다르면 검증 실패		
			if(!request.getRemoteAddr().equals("127.0.0.1") && !request.getRemoteAddr().equals("192.168.0.32")) {
				if (!request.getRemoteAddr().equals(userIp)) {
					final Log logger = LogFactory.getLog(getClass());	// 로그
					logger.info("verify error!");
					logger.info("request.getRemoteAddr() = " + request.getRemoteAddr());
					logger.info("userIp = " + userIp);
					return false;
				}
			}
			
			// 디멘터 접속 했던 시간 (dementorConnection.jsp 에서 설정한 시간)
			Long longNow = Long.parseLong(now);
			
			int timeOut = 0;												// 유효시간 초기화
			// 유효시간Map에 등록 되지 않은 URL이라면
			if (timeOutMap.get(urlId) == null) {
				// dementor.properties에서 설정한 유효시간 얻기 (분 단위)
				DementorProperty DP = null;
				timeOut = Integer.parseInt(DP.getInstance().getOtkTimeOut());
				timeOutMap.put(urlId, timeOut);								// 유효시간Map에 등록
			} else {
				// 이미 등록 된 URL이라면 유효시간Map에서 조회한다. (DB에 접속해서 읽어오는 횟수를 URL당 1회로 줄인다.)
				timeOut = ((Integer)timeOutMap.get(urlId)).intValue();		// Object to int
			}
			
			// 유효시간 설정을 한 경우 (만약 0일 경우 유효시간 설정을 안한 상태) 
			if (timeOut != 0) {			
				// 디멘터  접속했던 시간 이후 입력 완료 시간까지 유효시간(XX)분이 지났으면 타임 아웃으로 처리
				// 예) 접속 시간 : 1시 35분, 현재 시간 :1시 39분, 시간 차이 = 4분, 유효시간 3분 = false
				
				// 접속 시간 설정
				int year = Integer.parseInt(longNow.toString().substring(0,4));
				int month = Integer.parseInt(longNow.toString().substring(4,6));
				int date = Integer.parseInt(longNow.toString().substring(6,8));
				int hourOfDay = Integer.parseInt(longNow.toString().substring(8,10));
				int minute = Integer.parseInt(longNow.toString().substring(10,12));
				int second = Integer.parseInt(longNow.toString().substring(12,14));				
				
				Calendar calendar = Calendar.getInstance();
				calendar.set(year, month, date, hourOfDay, minute, second);
				calendar.add(Calendar.MINUTE, timeOut);		// 접속 시간에 유효시간을 더해준다.
				
				// 현재 시간 설정
				Calendar calendar2 = Calendar.getInstance();
				calendar2.setTime(calendar2.getTime());
							
				if (calendar.before(calendar2)) {	// (접속 시간+유효시간)이 현재 시간을 지났는지 체크
					final Log logger = LogFactory.getLog(getClass());	// 로그
					logger.info("verify error!");
					logger.info("connection time = " + calendar.toString());
					logger.info("current time = " + calendar2.toString());
					logger.info("urlId = " + urlId);
					logger.info("timeOut(Minute) = " + timeOut);
					return false;
				}
			}
			
			SHA1Dementor sha1 = new SHA1Dementor(urlId, userId,userIp, now);
			//System.out.println(urlId + " ; " + userId + " ; " + userIp + " ; " + now);
			// otk2 얻기
			String otk2 = CommonUtil.byteArrayToHex(sha1.getOtk());
			if (otk.equals(otk2)) {
				//System.out.println(otk + "\n" + otk2);
				return true;
			} else {
				final Log logger = LogFactory.getLog(getClass());	// 로그
				logger.info("\nVerifySign error!");
				logger.info("\notk1 = " + otk);
				logger.info("\notk2 = " + otk2);
				return false;
			}
		} catch(Exception e) {
			final Log logger = LogFactory.getLog(getClass());	// 로그
			logger.error(e);
			return false;
		}
	}
}
