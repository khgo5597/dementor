package com.mininfo.common.crypto;

import java.math.BigInteger;
import java.security.MessageDigest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mininfo.common.property.DementorProperty;
import com.mininfo.common.util.CommonUtil;

public class SHA1Dementor {
	private static String CRYPTO_KEY = null;// 저장 된 key
	private static byte[] key = null;		// 바이트화 된 key
	private static String keyStr = null;	// 암호화 된 key
	private byte[] sha1 = null;				// sha1 Hash 된 데이터
	private StringBuilder sbData = null;	// key;urlId;userId;userIp;now 를 합함.
	private MessageDigest md = null;		// messageDigest
	private String urlId = null;			// 싸이트 아이디
	private String userId = null;			// 사용자 아이디
	private String userIp = null;			// 사용자 아이피
	private String now = null;				// 사용자 접속 시간
	
	
	public SHA1Dementor(String urlId, String userId, String userIp, String now) {
		this.urlId = urlId;										// URL ID 설정
		this.userId = userId;									// 사용자 아이디 설정
		this.userIp = userIp;									// 사용자 아이피 설정
		this.now = now;											// 접속 시간 설정

		if(keyStr == null) {									// key string 이 null이면  1회만 실행 (static)
			setKey();											// 키 설정
			keyStr = CommonUtil.byteArrayToHex(key);			// byte array to hex (20바이트 hash(Key)를 Hex화)
		}
	}
	
	public SHA1Dementor() {
		if(keyStr == null) {									// key string 이 null이면  1회만 실행 (static)
			setKey();											// 키 설정
			keyStr = CommonUtil.byteArrayToHex(key);			// byte array to hex (20바이트 hash(Key)를 Hex화)
		}
	}
	
	// 암호화 키 얻기 (SHA1 Hash Key)
	private void setKey() {
		try {
			DementorProperty DP = null;
			CRYPTO_KEY = DP.getInstance().getOtkCryptoKey();
			key = getSha1(CRYPTO_KEY);							// sha1
		} catch(Exception e) {
			final Log logger = LogFactory.getLog(getClass());	// 로그
			logger.error(e);
		}
	}
	
	// OTK 얻기 (고객사)
	// 0. key(16진수)
	// 1. URL ID
	// 2. USER ID
	// 3. USER IP
	// 4. NOW
	// SHA1(0+1+2+3+4) = hash A = OTK
	public byte[] getOtk() {
		sbData = new StringBuilder(keyStr);		// key;urlId;userId;userIp;now 를 합함.
		sbData.append(";")
			.append(urlId)
			.append(";")
			.append(userId)
			.append(";")
			.append(userIp)
			.append(";")
			.append(now);
		//System.out.println(sbData);
		return getSha1(sbData.toString());		// sbData를 SHA-1 Hash화	
	}
	
	// HashB 얻기 (고객사, 디멘터)
	// 0. key(16진수)
	// 1. NOW
	// 2. USER IP
	// 3. USER ID
	// 4. URL ID
	// SHA1(0+4+3+2+1) = Hash B
	public byte[] getHashB() {
		sbData = new StringBuilder(keyStr);		// key;now;userIp;userId;urlId 를 합함.
		sbData.append(";")		
			.append(now)
			.append(";")
			.append(userIp)			
			.append(";")
			.append(userId)
			.append(";")
			.append(urlId);
		//System.out.println(sbData);
		return getSha1(sbData.toString());		// sbData를 SHA-1 Hash화	
	}
	
	// OTP1 얻기 (디멘터)
	// userPass xor Hash(B) = OTP1
	public String getOtp1(byte[] hashB, String userPass) {
		try {
			BigInteger bigHashB = new BigInteger(hashB);							// hashB를 BigInteger에 담는다.
			BigInteger bigUserPass = new BigInteger(userPass.trim().getBytes());	// userPass를 BigInteger에 담는다.
			return CommonUtil.byteArrayToHex(bigHashB.xor(bigUserPass).toByteArray());
		} catch(Exception e) {
			final Log logger = LogFactory.getLog(getClass());	// 로그
			logger.error(e);
			return null;
		}	
	}
	
	// OTP2 얻기 (디멘터)
	// Hash(OTP1 + K) = OTP2
	public String getOtp2(String otp1) {
		try {
			String otpKeyStr = otp1 + keyStr;				// OTP1
			return CommonUtil.byteArrayToHex(getSha1(otpKeyStr));
		} catch(Exception e) {
			final Log logger = LogFactory.getLog(getClass());	// 로그
			logger.error(e);
			return null;
		}
	}
	 
	// SHA1 Hash 얻기
	private byte[] getSha1(String data) {
		//System.out.println("\n data : " + data);
		try {
			byte[] hash = data.getBytes("utf-8");
	        md = MessageDigest.getInstance( "SHA1" );
	        md.update( hash );
	        sha1 = md.digest();
	        
	        // should be 20 bytes, 160 bits long
	        //System.out.println( sha1.length );

	        // dump out the hash
	        //for ( byte b : sha1 ) {
	        //    System.out.print( Integer.toHexString( b & 0xff )  );
	        //}
	        //System.out.println();
		} catch(Exception e) {
			final Log logger = LogFactory.getLog(getClass());	// 로그
			logger.error(e);
		}
        return sha1;
	}
}
