package com.mininfo.common.crypto;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

import com.mininfo.common.property.DementorProperty;
import com.mininfo.common.util.CommonUtil;

public class SHA1Client {
	private static String CRYPTO_KEY = null;// 암호 key
	private static byte[] key = null;		// 바이트화 된 key
	private static String keyStr = null;	// 암호화 된 key
	private byte[] sha1 = null;				// sha1 Hash 된 데이터
	private StringBuilder sbData = null;	// key;urlId;userId;userIp;now 를 합함.
	private MessageDigest md = null;		// messageDigest
	private String urlId = null;			// 싸이트 아이디
	private String userId = null;			// 사용자 아이디
	private String userIp = null;			// 사용자 아이피
	private String now = null;				// 사용자 접속 시간
	
	
	public SHA1Client(String urlId, String userId, String userIp, String now) {
		this.urlId = urlId;										// URL ID 설정
		this.userId = userId;									// 사용자 아이디 설정
		this.userIp = userIp;									// 사용자 아이피 설정
		this.now = now;											// 접속 시간 설정

		if(keyStr == null) {									// key string 이 null이면  1회만 실행 (static)
			setKey();											// 키 설정
			keyStr = CommonUtil.byteArrayToHex(key);			// byte array to hex (20바이트 hash(Key)를 Hex화)
			System.out.println("keyStr:"+keyStr);
		}
	}
	
//	 암호화 키 얻기 (SHA1 Hash Key)
	private void setKey() {
		try {
			DementorProperty DP = null;
			CRYPTO_KEY = DP.getInstance().getOtkCryptoKey();
			key = getSha1(CRYPTO_KEY);						// sha1    
		} catch(Exception e) {
			System.out.println(e);
		}    
	}
	
	// OTK 얻기
	// 0. key(16진수)
	// 1. URL ID
	// 2. USER ID
	// 3. USER IP
	// 4. NOW
	// SHA1(0+1+2+3+4) = hash A = OTK
	public byte[] getOtk() {
		sbData = new StringBuilder(keyStr);		// key;urlId;userId;userIp;now 를 합함.
		sbData.append(";")
			.append(urlId)
			.append(";")
			.append(userId)
			.append(";")
			.append(userIp)
			.append(";")
			.append(now);
		System.out.println("sbData:"+sbData);
		return getSha1(sbData.toString());		// sbData를 SHA-1 Hash화	
	}
	
	// HashB 얻기
	// 0. key(16진수)
	// 1. URL ID
	// 2. USER ID
	// 3. USER IP
	// 4. NOW
	// SHA1(0+4+3+2+1) = Hash B
	public byte[] getHashB() {
		sbData = new StringBuilder(keyStr);		// key;now;userIp;userId;urlId 를 합함.
		sbData.append(";")		
			.append(now)
			.append(";")
			.append(userIp)			
			.append(";")
			.append(userId)
			.append(";")
			.append(urlId);
		//System.out.println(sbData);
		return getSha1(sbData.toString());		// sbData를 SHA-1 Hash화	
	}
	
	// OTP2 얻기 (디멘터)
	// Hash(OTP1 + K) = OTP2
	public String getOtp2(String otp1) {
		try {
			String otpKeyStr = otp1 + keyStr;				// OTP1
			return CommonUtil.byteArrayToHex(getSha1(otpKeyStr));
		} catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	// 복호화
	public String getDecrypt(byte[] hashB, String otp1) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(otp1.length()/2);
		for (int i = 0; i < otp1.length();i+=2) {
			byteBuffer.put((byte)Integer.parseInt(otp1.substring(i,i+2),16));
		}
		byte[] otpByte = byteBuffer.array();				// byteBuffer 바이트화
		for (int i = 0; i < otpByte.length; i++) {
			otpByte[i] = (byte)(otpByte[i] ^ hashB[i]);		// 복호화
			//System.out.printf( "%02X ", userPass[i]);
		}
		return new String(otpByte);							// 복호화 된 데이터 String형으로 변환
	}
	 
	// SHA1 Hash 얻기
	private byte[] getSha1(String data) {
		//System.out.println("\n data : " + data);
		try {
			byte[] hash = data.getBytes("utf-8");
	        md = MessageDigest.getInstance( "SHA1" );
	        md.update( hash );
	        sha1 = md.digest();
	        System.out.println("data:"+data);
	        System.out.println("sha1:"+sha1);
	        // should be 20 bytes, 160 bits long
	        //System.out.println( sha1.length );

	        // dump out the hash
	        //for ( byte b : sha1 ) {
	        //    System.out.print( Integer.toHexString( b & 0xff )  );
	        //}
	        //System.out.println();
		} catch(Exception e) {
			System.out.println(e);
		}
        return sha1;
	}
}
