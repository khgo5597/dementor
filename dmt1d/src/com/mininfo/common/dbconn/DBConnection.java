package com.mininfo.common.dbconn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
 
public class DBConnection{

	protected int eigen = 0;
	protected String name = "";
	protected String imgurl = "";
	protected String status = "";

	//dbcon db = new dbcon();
	protected PreparedStatement pstmt = null;
	protected Connection conn = null;
	protected ResultSet rs=null;

	public void connect(){
		try{
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("java:comp/env/jdbc/DementorDB");
			conn = ds.getConnection();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	public void disconnect(){
		try{
			if(rs!=null) rs.close();
		}catch(Exception e) {System.out.println(e);}
		try{
			if(pstmt!=null) pstmt.close();
		}catch(Exception e) {System.out.println(e);}

		try{
			if(conn!=null) conn.close();
		}catch(Exception e) {System.out.println(e);}
	}
	
	public Connection getConnect(){
		Connection connection = null;
		try{
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("java:comp/env/jdbc/DementorDB");
			connection = ds.getConnection();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		return connection;
	}

	public void closeRS(ResultSet rs){
		try{
			if(rs!=null) rs.close();
		}catch(Exception e) {System.out.println(e);}
	}
	
	public void closePS(PreparedStatement ps){
		try{
			if(ps!=null) ps.close();
		}catch(Exception e) {System.out.println(e);}
	}

	public void closeCon(Connection conn){
		try{
			if(conn!=null) conn.close();
		}catch(Exception e) {System.out.println(e);}
	}
	
}