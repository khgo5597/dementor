package com.mininfo.common.property;

import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class DementorProperty {
	
	private static PropertiesConfiguration config = null;
	
	private static DementorProperty instance;
	
	public static synchronized DementorProperty getInstance() throws Exception {
		if (instance == null) {
			instance = new DementorProperty();
		}
		return instance;
	}
	
	private DementorProperty(){
		try {
			config = new PropertiesConfiguration("dementor.properties");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public DementorProperty(String propertyFile) {
		try {
			config = new PropertiesConfiguration(propertyFile);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public int getAccessCount(){
		return config.getInt("ACCESS_COUNT");
	}
	
	public boolean isSessionRelease(){
		
		if(config.getString("RELEASE").equalsIgnoreCase("SESSION")){
			return true;
		} 
		return false;
	}
	
	public boolean isManagerRelease(){
		if(config.getString("RELEASE").equalsIgnoreCase("MANAGER")){
			return true;
		}
		return false;
	}
	
	public String getRegistryUrl(){
		return config.getString("REGISTRY_URL");
	}
	
	public String getLoginSuccessUrl(){
		return config.getString("LOGIN_SUCCESS_URL");
	}
	
	public String getLoginFailUrl(){
		return config.getString("LOGIN_FAIL_URL");
	}
	
	public String getIconKeyNumber(){
		return config.getString("ICON_KEY_NUMBER");
	}
	
	public String getOtkUseYn(){
		return config.getString("OTK_USE_YN");
	}
	
	public String getOtkTimeOut(){
		return config.getString("OTK_TIME_OUT");
	}
	
	public String getOtkCryptoKey(){
		return config.getString("OTK_CRYPTO_KEY");
	}
	
	public String getAuthSendType(){
		return config.getString("AUTH_SEND_TYPE");
	}

	public String getProperty(String propName){
		return config.getString(propName);
	}

	public String[] getProperties(String propName){
		return config.getStringArray(propName);
	}

	public List getPropertyList(String propName){
		return config.getList(propName);
	}

	public void setProperty(String propName, String propValue){
		config.setProperty(propName, propValue);
		try {
			config.save();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
}
