package com.mininfo.common.util;

import java.sql.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;

public class ComUtil {
	
	public static String getFirstDateOfMonth(String YYYYMMDD)
	{
		int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));

		Calendar nowDate = Calendar.getInstance();
		nowDate.set(li_Year, li_Month - 1, 1);

		String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		String month = Integer.toString( nowDate.get( Calendar.MONTH ) + 1 );
		String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		if(month.length() == 1)
			month = "0"+month;
		if(day.length() == 1)
			day = "0"+day;

		String nowDateString = year+month+day;
		return nowDateString;
	}

	public static String getPreFirstDateOfMonth(String YYYYMMDD)
	{
		int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));

		if ( li_Month == 1 ) {  
			li_Year = li_Year -1 ;
			li_Month = 12 ;
		}	else {
			li_Month = li_Month -1 ;
		}

		Calendar nowDate = Calendar.getInstance();
		nowDate.set(li_Year, li_Month - 1, 1);

		String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		String month = Integer.toString( nowDate.get( Calendar.MONTH )+1);
		String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		if(month.length() == 1)
			month = "0"+month;
		if(day.length() == 1)
			day = "0"+day;

		String nowDateString = year+month+day;
		return nowDateString;
	}

	public static String getLastDateOfMonth(String YYYYMMDD)
	{
		int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));

		Calendar nowDate = Calendar.getInstance();
		nowDate.set(li_Year, li_Month, 0);

		String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		String month = Integer.toString( nowDate.get( Calendar.MONTH ) + 1 );
		String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		if(month.length() == 1)
			month = "0"+month;
		if(day.length() == 1)
			day = "0"+day;

		String nowDateString = year+month+day;
		return nowDateString;
	}	

	public static String getPreLastDateOfMonth(String YYYYMMDD)
	{
		int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));

		if ( li_Month == 1 ) {  
			li_Year = li_Year -1 ;
			li_Month = 12 ;
		} else {
			li_Month = li_Month -1 ;
		}

		Calendar nowDate = Calendar.getInstance();
		nowDate.set(li_Year, li_Month, 0);

		String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		String month = Integer.toString( nowDate.get( Calendar.MONTH )  +1);
		String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH )  );
		if(month.length() == 1)
			month = "0"+month;
		if(day.length() == 1)
			day = "0"+day;

		String nowDateString = year+month+day;
		return nowDateString;
	}	

	public static String returnTodayTime()
	{
		String lst_curr_date = "";
		try
		{
			Calendar now = Calendar.getInstance();
			lst_curr_date = Integer.toString( now.get( Calendar.YEAR ) );
			String month = Integer.toString( now.get( Calendar.MONTH ) + 1 );
			String day = Integer.toString( now.get( Calendar.DAY_OF_MONTH ) );
			String hour = Integer.toString( now.get( Calendar.HOUR_OF_DAY ) );
			String minute = Integer.toString( now.get( Calendar.MINUTE) );
			String second = Integer.toString( now.get( Calendar.SECOND) );

			if(month.length() == 1)
				month = "0"+month;
			if(day.length() == 1)
				day = "0"+day;
			if(hour.length() == 1)
				hour = "0"+hour;
			if(minute.length() == 1)
				minute = "0"+minute;
			if(second.length() == 1)
				second = "0"+second;

			lst_curr_date = lst_curr_date + month;
			lst_curr_date = lst_curr_date + day;
			lst_curr_date = lst_curr_date + hour;
			lst_curr_date = lst_curr_date + minute;
			lst_curr_date = lst_curr_date + second;
		}
		catch( Exception e )
		{
		}
		return lst_curr_date;
	}

	public static String returnToday()
	{
		String lst_curr_date = "";
		try
		{
			Calendar now = Calendar.getInstance() ;
			
			now.add(Calendar.DATE, -1);
			lst_curr_date = Integer.toString( now.get( Calendar.YEAR ) );
			String month = Integer.toString( now.get( Calendar.MONTH ) + 1 );
			String day = Integer.toString( now.get( Calendar.DAY_OF_MONTH ) );

			if(month.length() == 1)
				month = "0"+month;
			if(day.length() == 1)
				day = "0"+day;

			lst_curr_date = lst_curr_date + month;
			lst_curr_date = lst_curr_date + day;
		}
		catch( Exception e )
		{
		}
		
		return lst_curr_date;
//		return "20050630";
	}

	public static String returnTodayAdd(int intday)
	{
		String lst_curr_date = "";
		try
		{
			Calendar now = Calendar.getInstance() ;
			
		   now.add(Calendar.DATE, intday);
			lst_curr_date = Integer.toString( now.get( Calendar.YEAR ) );
			String month = Integer.toString( now.get( Calendar.MONTH ) + 1 );
			String day = Integer.toString( now.get( Calendar.DAY_OF_MONTH ) );

			if(month.length() == 1)
				month = "0"+month;
			if(day.length() == 1)
				day = "0"+day;

			lst_curr_date = lst_curr_date + month;
			lst_curr_date = lst_curr_date + day;
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
		return lst_curr_date;
//		return "20050630";
	}

	public static String getSpaceReplaceNull(String lst_Str)
	{
        StringBuffer sb = new StringBuffer();

        for( int i = 0; i < lst_Str.length(); i++ ){
        if( lst_Str.charAt( i ) != ',' )
            sb.append( lst_Str.charAt( i ) );
        }

        return sb.toString();
	}

	public static String getFormattedDate(String lst_DateStr)
	{
		String lst_RetStr = null;

		if (lst_DateStr == null) return "";

		if(lst_DateStr.length() == 14) 
		{
			lst_RetStr = lst_DateStr.substring(0,4) + "."
					 + lst_DateStr.substring(4,6) + "."
					 + lst_DateStr.substring(6,8) + " "
					 + lst_DateStr.substring(8,10) + ":"
					 + lst_DateStr.substring(10,12) + ":"
					 + lst_DateStr.substring(12,14);
		}
		else if(lst_DateStr.length() == 12)
		{
			lst_RetStr = lst_DateStr.substring(0,4) + "."
					 + lst_DateStr.substring(4,6) + "."
					 + lst_DateStr.substring(6,8) + " "
					 + lst_DateStr.substring(8,10) + ":"
					 + lst_DateStr.substring(10,12);
		}
		else if(lst_DateStr.length() == 10)
		{
			lst_RetStr = lst_DateStr.substring(0,4) + "."
					+ lst_DateStr.substring(4,6) + "."
					+ lst_DateStr.substring(6,8) + " "
					+ lst_DateStr.substring(8,10);
		}
		else if(lst_DateStr.length() == 8)
		{
			lst_RetStr = lst_DateStr.substring(0,4) + "." + lst_DateStr.substring(4,6) + "."
					+ lst_DateStr.substring(6,8);
		}
		else if(lst_DateStr.length() == 6)
		{
			lst_RetStr = lst_DateStr.substring(0,4) + "." + lst_DateStr.substring(4,6);
		}
		else
		{
			lst_RetStr = "";
		}
		return lst_RetStr;
	}

	public static boolean isEmpty(String lst_str)
	{
		if (lst_str == null || lst_str.length() == 0)
		{
			return true;
		}
		return false;
	}

	public static String setPriceFormat( String pst_price )
	{
		if ( pst_price != null && !pst_price.equals("")) {

    		boolean minus = false;
    		if ( pst_price.startsWith( "-" ) )
    		{
    			minus = true;
    			pst_price = pst_price.substring( 1, pst_price.length() );
    		}

    		String lst_str = "";
    		int li_comma = pst_price.indexOf( "." , 0 );
    		int li_length = 0;
    		int li_i = 0;
    		if ( li_comma != -1 )
    			li_length = pst_price.substring( 0 , li_comma ).length();
    		else
    			li_length = pst_price.length();

    		if( li_length == 0 )
    			return lst_str;
    		for ( li_i = 0 ; li_i < li_length ; li_i++ )
    		{
    			if ( pst_price.charAt( li_i ) == ',' )
    				return pst_price;
    		}
    		for ( li_i = 1 ; li_i < li_length ; li_i++ )
    		{
    			if ( ( li_i != ( li_length - 1 ) ) && ( ( li_length - li_i ) % 3 == 0 ) )
    			{
    				lst_str += pst_price.charAt( li_i - 1 );
    				lst_str += ",";
    			}
    			else
    			{
    				lst_str += pst_price.charAt( li_i - 1 );
    			}
    		}
    		lst_str += pst_price.charAt( li_i - 1 );
    		if ( li_comma != -1 )
    			lst_str = lst_str + pst_price.substring( li_comma , pst_price.length() );

    		return ( minus ? "-" : "" ) + lst_str;
    	} else	 {
    		return "0";
    	}

	}

	public static String ExchangeEnter( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) == 13 )
				lst_str = lst_str + "\\n";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}

	public static String isSelected (String searchType, String param) {

		if(searchType == null || searchType.trim().equals(""))
			return "";

		if ( searchType.trim().equals(param) ) {
			return "selected";
		}

		return "";
	}

	public static String isSelTag (String searchType, String param1, String param2) {

		if (searchType != null &&  searchType.trim().equals(param1.trim()) ) {
			
			return   "<OPTION  VALUE='"+param1+"' selected>"+param2+"</OPTION>";
        } else {
			return   "<OPTION  VALUE='"+param1+"' >"+param2+"</OPTION>";
		}

	}

	public static String getCommaReplaceNull(String lst_Str)
	{
        StringBuffer sb = new StringBuffer();

        for( int i = 0; i < lst_Str.length(); i++ ){
        if( lst_Str.charAt( i ) != '.' )
            sb.append( lst_Str.charAt( i ) );
        }

        return sb.toString();
	}

	public static String getEncoding(String lst_Str)
	{
		String str="";
		try{
			str = new String(lst_Str.getBytes("8859_1"), "utf-8");
			} catch(UnsupportedEncodingException e) {
			}
			return str;
	}

	public static String getDesc(String lst_Str)
	{

		String str ="";
		
		if ( lst_Str == null || lst_Str.length() == 0 )
        	str= "";
		else		
        	str= " ...............................";

        return str;
	}

	public static String getCookie( HttpServletRequest req , String pst_CookieName )
	{
		String lst_str = "";

		Cookie[] cookies = req.getCookies();
		for ( int i=0 ; i < cookies.length ; i++ )
		{
			if ( cookies[i].getName().equals( pst_CookieName ) == true )
			{
				lst_str = cookies[i].getValue();
				break;
			}
		}
		return lst_str;
	}

	/*******************************************************************************
	*  Method Name	: setCookie(HttpServletResponse response, String cookieName,
											String cookieValue, int expireDay)
	*  Description	: .............
	*******************************************************************************/
	public static void setCookie(HttpServletResponse response, String cookieName,
								String cookieValue, int expireDay)
	{
		if(cookieName != null && cookieValue != null)
		{
			Cookie tmpCookie = new Cookie(cookieName, cookieValue);
			tmpCookie.setMaxAge(expireDay * 3600 * 24);
			response.addCookie(tmpCookie);
		}
	}
	
	/*******************************************************************************
	*  Method Name	: getSession( HttpServletRequest request ,
	*                             String pst_session_name )
	*  Description	: ......................
	*******************************************************************************/
	public static Object getSession( HttpServletRequest request , String pst_session_name )
	{
		Object lso_object;
		HttpSession session = null;
		session = request.getSession( true );
		if ( session.getAttribute( pst_session_name ) != null )
			lso_object = session.getAttribute( pst_session_name );
		else
			lso_object = null;
		return lso_object;
	}
	/*******************************************************************************
	*  Method Name	: getSessionId( HttpServletRequest request )
	*  Description	: ....................
	*******************************************************************************/
	public static String getSessionId( HttpServletRequest request )
	{
		HttpSession session = null;
		session = request.getSession( true );
		return session.getId();
	}
	/*******************************************************************************
	*  Method Name	: setSession( HttpServletRequest request ,
	*                             String pst_session_name ,
	*                             Object pso_session_value )
	*  Description	: ............
	*******************************************************************************/
	public static void setSession( HttpServletRequest request ,  String pst_session_name , Object pso_session_value )
	{
		HttpSession session = null;
		session = request.getSession( true );
		session.setAttribute( pst_session_name , pso_session_value );
	}
	
	/*******************************************************************************
	*  Method Name	: delSession( HttpServletRequest request , String pst_name )
	*  Description	: session...................
	*******************************************************************************/
	public static void delSession( HttpServletRequest request , String pst_name )
	{
		HttpSession session = null;
		session = request.getSession( true );
		session.setAttribute("", pst_name );
	}

/*******************************************************************************
	*  Method Name	: jAlertAndRedirect(HttpServletResponse response, String msg, String url)
	*  Description	: msg.....................
	*******************************************************************************/
	public static void jAlertAndRedirect(HttpServletResponse response, String msg, String url)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println("<script language='javascript'>");
			out.println("alert(\""+msg+"\");");
			out.println("self.location.replace(\""+url+"\");");
			out.println("</script>");
		}
		catch(Exception e)
		{
		}
	}
	/*******************************************************************************
	*  Method Name	: jCloseWin(HttpServletResponse response)
	*  Description	: ..................
	*******************************************************************************/
	public static void jCloseWin(HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println("<script language='javascript'>");
			out.println("self.close();");
			out.println("</script>");
		}
		catch(Exception e)
		{
		}
	}
	/*******************************************************************************
	*  Method Name	: jscriptAlert(HttpServletResponse response, String msg)
	*  Description	: ............................
	*******************************************************************************/
	public static void jscriptAlert(HttpServletResponse response, String msg)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println("<script language='javascript'>");
			out.println("alert(\""+msg+"\");");
			out.println("</script>");
		}
		catch(Exception e)
		{
		}
	}
	/*******************************************************************************
	*  Method Name	: jscriptError(HttpServletResponse response, String msg)
	*  Description	: msg....................
	*******************************************************************************/
	public static void jscriptError(HttpServletResponse response, String msg)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println("<script language='javascript'>");
			out.println("alert(\""+msg+"\");");
			out.println("history.go(-1);");
			out.println("</script>");
		}
		catch(Exception e)
		{
		}
	}
	/*******************************************************************************
	*  Method Name	: jscriptRedirect(HttpServletResponse response, String url)
	*  Description	: ................
	*******************************************************************************/
	public static void jscriptRedirect(HttpServletResponse response, String url)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println("<script language='javascript'>");
			out.println("self.location.replace(\""+url+"\");");
			out.println("</script>");
		}
		catch(Exception e)
		{
		}
	}
	/*******************************************************************************
	*  Method Name	: remSession( HttpServletRequest request )
	*  Description	: session; .
	*******************************************************************************/
	public static void remSession( HttpServletRequest request )
	{
		HttpSession session = null;
		session = request.getSession( true );
		session.invalidate();
	}

	/*******************************************************************************
	*  Method Name	: getFullPost( HttpServletRequest request )
	*  Description	: Post 
	*******************************************************************************/
	public static String getFullPost( HttpServletRequest request )
	{
		
		StringBuffer sb = new StringBuffer();

		for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ){

		    String key = (String)en.nextElement();

		    sb.append ( key + "=" + request.getParameter(key) + "&");

		} 

		String paramStr = sb.toString();

		paramStr = getEncoding(request.getRequestURL()+"?"+paramStr.substring(0, paramStr.length()-1));

 	    return paramStr;
	}

	/*******************************************************************************
	*  Method Name	: ExchangeBr( String pst_str )
	*  Description	: 
	*******************************************************************************/
	public static String ExchangeBr( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) == 13 )
				lst_str = lst_str + "<br>";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}
	/*******************************************************************************
	*  Method Name	:
	*  Description	:
	*******************************************************************************/
	public static String ExchangeDoubleQuot( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) == 34 )
				lst_str = lst_str + "&#34;";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}
	/*******************************************************************************
	*  Method Name	:
	*  Description	:
	*******************************************************************************/
	public static String ExchangeSingleQuot( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) == 39 )
				lst_str = lst_str + "&#39;";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}
	/*******************************************************************************
	*  Method Name	: ExchangeSpace( String pst_str )
	*  Description	: 
	*******************************************************************************/
	public static String ExchangeSpace( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) == 32 )
				lst_str = lst_str + "&nbsp;";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}	
	/*******************************************************************************
	*  Method Name	: 
	*  Description	:
	*******************************************************************************/
	public static String MutiExchange( String pst_str )
	{
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ )
		{
			if ( ( int )pst_str.charAt( i ) 			== 13 )
				lst_str = lst_str + "<br>";
			else if ( ( int )pst_str.charAt( i ) 	== 32 )
				lst_str = lst_str + "&nbsp;";
			else if ( ( int )pst_str.charAt( i )	== 34 )
				lst_str = lst_str + "&#34;";
			else if ( ( int )pst_str.charAt( i ) 	== 39 )
				lst_str = lst_str + "&#39;";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}
	
	/*******************************************************************************
	*  Method Name	: 
	*  Description	:
	*******************************************************************************/
	public static String getAddMonth(String YYYYMMDD, int add){

		  int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		  int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));
		  int li_Day = Integer.parseInt(YYYYMMDD.substring(6, 8));

		  Calendar nowDate = Calendar.getInstance();
		  nowDate.set(li_Year, li_Month - 1 + add, li_Day+1);

		  String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		  String month = Integer.toString( nowDate.get( Calendar.MONTH ) + 1 );
		  String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		  if(month.length() == 1)
		   month = "0"+month;
		  if(day.length() == 1)
		   day = "0"+day;

		  String nowDateString = year+month+day;
		  return nowDateString;
		 }
	
	/*******************************************************************************
	*  Method Name	: 
	*  Description	:
	*******************************************************************************/

		 public static String getAddYear(String YYYYMMDD, int add){

		  int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		  int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));
		  int li_Day = Integer.parseInt(YYYYMMDD.substring(6, 8));

		  Calendar nowDate = Calendar.getInstance();
		  nowDate.set(li_Year+ add, li_Month - 1, li_Day);

		  String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		  String month = Integer.toString( nowDate.get( Calendar.MONTH ) + 1 );
		  String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		  if(month.length() == 1)
		   month = "0"+month;
		  if(day.length() == 1)
		   day = "0"+day;

		  String nowDateString = year+month+day;
		  return nowDateString;
		 }

		 /*******************************************************************************
		  *  Method Name : 
		  *  Description :
		  *******************************************************************************/
		  public static String getAddDay(String YYYYMMDD, int add){

		     int li_Year = Integer.parseInt(YYYYMMDD.substring(0, 4));
		     int li_Month = Integer.parseInt(YYYYMMDD.substring(4, 6));
		     int li_Day = Integer.parseInt(YYYYMMDD.substring(6, 8));

		     Calendar nowDate = Calendar.getInstance();
		     nowDate.set(li_Year, li_Month - 1, li_Day+1+ add);

		     String year = Integer.toString( nowDate.get( Calendar.YEAR ));
		     String month = Integer.toString( nowDate.get( Calendar.MONTH ) + 1 );
		     String day = Integer.toString( nowDate.get( Calendar.DAY_OF_MONTH ) );
		     if(month.length() == 1)
		      month = "0"+month;
		     if(day.length() == 1)
		      day = "0"+day;

		     String nowDateString = year+month+day;
		     return nowDateString;
		    }		 
		 
}
