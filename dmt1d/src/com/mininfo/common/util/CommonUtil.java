package com.mininfo.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

public class CommonUtil {
	public static String ascToKsc(String value) {
		try {
			String strRet = new String(value.getBytes("8859_1"), "KSC5601");
			return strRet;
		} catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public static String kscToAsc(String value) {
		try {
			String strRet = new String(value.getBytes("KSC5601"), "8859_1");
			return strRet;
		} catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public static String convertHtmlBr(String comment) {
		if (comment == null)
			return "";
		int length = comment.length();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			String tmp = comment.substring(i, i + 1);
			if ("\r".compareTo(tmp) == 0) {
				tmp = comment.substring(++i, i + 1);
				if ("\n".compareTo(tmp) == 0)
					buffer.append("<br>\r");
				else
					buffer.append("\r");
			}
			buffer.append(tmp);
		}
		return buffer.toString();
	}
	
	
	public static boolean isEmpty(String value){
		
		if (value == null || value.trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static String nullToStr(Object obj){
		String returnStr = null;
		if (obj == null) return "";
		if (obj instanceof String) {
			returnStr = (String) obj;
		}
		if (returnStr.equalsIgnoreCase("null")) {
			return "";
		}
		return returnStr;
	}
	
	public static int strToInt(String str){
		return strToInt(str,0);
	}
	
	public static int strToInt(String str, int initial){
		if (null==str || "".equals(str.trim())) {
			return initial;
		}
		return Integer.parseInt(str);
	}
	
	public static String stackTraceToString(Throwable ex) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		PrintStream p = new PrintStream(b);
		ex.printStackTrace(p);
		p.close();
		String stackTrace = b.toString();
		try {
			b.close();
		} catch(Exception e) {
			System.out.println(e);
		}

		return CommonUtil.convertHtmlBr(stackTrace);
	}
	
	public static String MutiExchange( String pst_str ) {
		if (null == pst_str) {
			return "";
		}
		
		String lst_str = "";

		for ( int i=0 ; i < pst_str.length() ; i++ ) {
			if ( ( int )pst_str.charAt( i ) 		== 13 )
				lst_str = lst_str + "<br>";
			else if ( ( int )pst_str.charAt( i ) 	== 32 )
				lst_str = lst_str + "&nbsp;";
			else if ( ( int )pst_str.charAt( i )	== 34 )
				lst_str = lst_str + "&#34;";
			else if ( ( int )pst_str.charAt( i ) 	== 39 )
				lst_str = lst_str + "&#39;";
			else
				lst_str = lst_str + pst_str.charAt( i );
		}
		return lst_str;
	}
	
	// E-mail 자르기
	// 호출 : String mail = getMail("admin@mininfo.co.kr",0);
	// 리턴 : admin
	// 호출 : String mail = getMail("admin@mininfo.co.kr",1);
	// 리턴 : mininfo.co.kr
	public static String getMail(String email, int index) {
		if (index == 0) {
			return  email.substring(0,email.indexOf('@'));
		} else {
			return email.substring(email.indexOf('@')+1,email.length());
		}
	}
	
	// 전화번호 자르기
	// 호출 : String phone = getPhone("123-4567-8910",0);
	// 리턴 : 123
	// 호출 : String phone = getPhone("123-4567-8910",1);
	// 리턴 : 4567
	// 호출 : String phone = getPhone("123-4567-8910",2);
	// 리턴 : 8910
	public static String getPhone(String phone, int index) {
		if (index == 0) {
			return  phone.substring(0,phone.indexOf('-'));
		} else if (index == 1) {
			return phone.substring(phone.indexOf('-')+1,phone.lastIndexOf('-'));
		} else {
			return phone.substring(phone.lastIndexOf('-')+1,phone.length());
		}
	}
	
	// 이메일 유효성 검증
	// kang@co.kr 또는 kang@pe.kr 과 같은 포맷인 경우 true가 나오니 필요하시면 처리해주시기 바랍니다.
	public static boolean isEmail(String email) {
		int at = email.indexOf('@');
		int lastAt = email.lastIndexOf('@');
		int dot = email.indexOf('.');
		int lastDot = email.lastIndexOf('.');
		
		if (at == 0) {
			// @ 가 첫번째 나온 경우 
			// 예제 : @mininfo.co.kr
			return false;
		} else if ( lastDot == email.length()) {
			// . 가 마지막에 나온 경우 
			// 예제 : @mininfo.co.kr.
			return false;
		} else if ( at == -1) {
			// @가 없는 경우 
			// 예제 : kangmininfo.co.kr.			
			return false;
		} else if ( dot == -1) {
			// .가 없는 경우 
			// 예제 : kang@mininfocokr			
			return false;
		} else if ( at > dot) {
			// @ 위치가 dot 보다 뒤에 위치 할 경우 
			// 예제 : kang.mininfo@co.kr
			return false;
		} else if ( at != lastAt) {
			// @ 의 갯수가 2개 이상일 경우
			// 예제 : kang@mininfo@.co.kr
			return false;
		} else {
			// 유효한 이메일 주소
			return true;
		}
	}
	//	 올 해 연도 구하는 함수
	//	 ex : getYear();      return = 2008
	public static String getYear() {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		return String.valueOf(year); 
	}
	
	//	 이번 달 구하는 함수
	//	 ex : getMonth();      return = 08
	public static String getMonth() {
		Calendar c = Calendar.getInstance();
		int month = (c.get(Calendar.MONTH)+1);
		StringBuffer bf = new StringBuffer();
		if (month < 10) {
			bf.append("0").append(month);
		} else {
			bf.append(month);
		}
		return bf.toString();
	}	
	
	//	 오늘 날짜 구하는 함수
	//	 ex : getToday("/");      return = 2008/12/09
	//	 ex : getToday("-");      return = 2008-12-09
	//	 ex : getToday("");      return = 20081209

	public String getToday(String token) {
		Calendar c = Calendar.getInstance();
		int YEAR = c.get(Calendar.YEAR);
		int MONTH = (c.get(Calendar.MONTH)+1);
		String cur_month = "";
		String cur_date = "";
		if (MONTH < 10) {
			cur_month = "0" + MONTH;
		} else {
			cur_month = "" + MONTH;
		}
		int DATE = c.get(Calendar.DATE);
		if (DATE < 10) {
			cur_date = "0" + DATE;
		} else {
			cur_date = "" + DATE;
		}
		return YEAR + token + cur_month + token + cur_date;
	}
		
	// 바이트를 hex로
	public static String byteArrayToHex(byte[] ba) { 
	    if (ba == null || ba.length == 0) { 
	        return null; 
	    } 
	    
	    System.out.println("ba:"+ba);
	    
	    StringBuffer sb = new StringBuffer(ba.length * 2); 
	    
	    String hexNumber; 
	    for (int x = 0; x < ba.length; x++) { 
	        hexNumber = "0" + Integer.toHexString(0xff & ba[x]); 
	        sb.append(hexNumber.substring(hexNumber.length() - 2)); 
	    } 
	    return sb.toString(); 
	} 
}
