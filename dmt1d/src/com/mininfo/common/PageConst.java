package com.mininfo.common;

public class PageConst {
	
	
	public static final String failed = "/jsp/common/failed.jsp";
	public static final String contentsPage = "/jsp/admin/contents.jsp";
	public static final String userInFormPage = "/jsp/user/userWrite.jsp";	
	public static final String loginFormPage = "/jsp/user/loginForm.jsp";
	public static final String hintFormPage = "/jsp/display/hint.jsp" ;
	public static final String passSearchFormPage = "/jsp/display/search_pw.jsp" ;
	public static final String authCodeFormPage = "/jsp/display/authCodeForm.jsp" ;
	public static final String main = "/jsp/common/main.jsp";
	public static final String index = "/index.jsp";
	public static final String select = "/jsp/common/select.jsp";
	public static final String showhelp = "/jsp/display/showhelp.jsp";
	public static final String transfer = "/jsp/common/transfer.jsp";
	public static final String dementorConnector = "/dementorConnector.jsp";
	
}
