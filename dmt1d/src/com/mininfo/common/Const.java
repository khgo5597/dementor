package com.mininfo.common;

public class Const {

	public static final String ACCESS_SUCCESS = "S";
	public static final String ACCESS_FAIL = "F";
	public static final String USER_STATUS_IN = "Y";
	public static final String USER_STATUS_OUT = "N";
	
	public static final int CODE_SUCCESS = 1;
	public static final int CODE_FAIL = 2;
		
	public static final int KEY_EDIT = 100;
	
	public static final int LOGIN_FAIL = 500;
	public static final int CLOES = 501;
	public static final int CODE_INDEX = 502;
}
